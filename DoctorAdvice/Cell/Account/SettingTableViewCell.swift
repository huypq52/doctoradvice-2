//
//  SettingTableViewCell.swift
//  BAWatch
//
//  Created by Pham Huy on 9/6/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions
import MGSwipeTableCell

class  SettingTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 44
    }
    
    var buttonSwitch: UISwitch!
    var seperator: UIView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        

        
        buttonSwitch.frame = CGRect(x: bounds.width - Size.padding10.. * 2 - Size.button..,
                                    y: (bounds.height - buttonSwitch.frame.height) / 2,
                                    width: buttonSwitch.frame.width,
                                    height: buttonSwitch.frame.height )
        
        guard let imageView = imageView else { return }
        

        textLabel?.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                  y: Size.padding10.. / 4,
                                  width: buttonSwitch.frame.minX - imageView.frame.maxX - Size.padding10.. * 2,
                                  height: bounds.height / 3)
        
        detailTextLabel?.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                  y: bounds.height / 3 + Size.padding10.. / 4,
                                  width: buttonSwitch.frame.minX - imageView.frame.maxX - Size.padding10.. * 2,
                                  height: bounds.height * 2 / 3 - Size.padding10.. / 2)
        
        seperator.frame = CGRect(x: textLabel!.frame.minX,
                                 y: bounds.height - onePixel(),
                                 width: bounds.width -  textLabel!.frame.minX - Size.padding10..,
                                 height: onePixel())
    }
}

extension SettingTableViewCell {
    func setupAllSubviews() {
        
        setupImageView()
        setupLabel()
        
        buttonSwitch = setupButtonSwitch()
        seperator = setupView()
        
        addSubview(buttonSwitch)
        addSubview(seperator)
        
    }
    
    func setupLabel(){
        textLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        textLabel?.textAlignment = .left
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        detailTextLabel?.numberOfLines = 0
        detailTextLabel?.lineBreakMode = .byWordWrapping

    }
    
    func setupImageView() {
        imageView?.contentMode = .scaleAspectFit
    }
    
    func setupButtonSwitch() -> UISwitch {
        let button = UISwitch()
        return button
    }
    
    func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}
