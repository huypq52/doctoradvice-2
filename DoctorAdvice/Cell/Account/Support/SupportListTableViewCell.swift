//
//  SupportListTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 12/2/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class SupportListTableViewCell: SeperatorTableViewCell {
    
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, label = 20, button = 30
    }
    

    var titleLabel: UILabel!
    var contentLabel: UILabel!
//    var subTitle: UILabel!
    var timeLabel: UILabel!
    var button: UIButton!
    var isNewView: UIView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: Size.padding10..,
                                   y: 0,
                                   width: bounds.width - Size.padding10.. * 2,
                                   height: bounds.height)
        
        titleLabel.frame = CGRect(x: Size.padding10.. / 2,
                                  y: 5,
                                  width: contentView.frame.width - Size.padding10..,
                                  height: 20)
        
        
        let buttonWidth: CGFloat = Utility.widthForView(button.titleLabel?.text ?? "", font: button.titleLabel!.font, height: 30) + 20
        
        button.frame = CGRect(x: Size.padding10.. / 2,
                              y: contentView.frame.height - 24 - Size.padding10.. / 2,
                              width: buttonWidth,
                              height: 24)
        
        button.layer.cornerRadius = button.frame.height / 2
        


        timeLabel.frame = CGRect(x: button.frame.maxX + Size.padding10.. ,
                                 y: contentView.frame.height - 24 - Size.padding10.. / 2 ,
                                 width: contentView.frame.width - button.frame.maxX - Size.padding10.. * 2,
                                 height: 24)
        
//        subTitle.frame = CGRect(x: Size.padding10.. / 2,
//                                y: contentView.frame.height - 24 - 5,
//                                width: timeLabel.frame.minX - Size.padding10..,
//                                height: 24)
        
        contentLabel.frame = CGRect(x: Size.padding10.. / 2,
                                    y: titleLabel.frame.maxY ,
                                    width: contentView.frame.width - Size.padding10..,
                                    height: timeLabel.frame.minY - titleLabel.frame.maxY )
        
        
        isNewView.frame = CGRect(x: contentView.frame.width - 8 - 3, y: 3, width: 8, height: 8)
        isNewView.layer.cornerRadius = isNewView.frame.height / 2
    }
    
    func setup() {
        
        
      
        titleLabel = setupTitle(UIColor.Text.blackMediumColor(),
                                font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!)
        
        contentLabel = setupTitle()
        
        button = setupButton("", selectedTitle: "")
        timeLabel = setupLabel("",
                               textColor: UIColor.Text.grayMediumColor(),
                               font: UIFont(name: FontType.latoRegular.., size: FontSize.small++)!,
                               bgColor: .clear,
                               alignment: .right)
        
//        subTitle = setupLabel("",
//                              textColor: UIColor.Text.grayMediumColor(),
//                              font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!,
//                              bgColor: .clear,
//                              alignment: .left)
        
//        subTitle.lineBreakMode = .byTruncatingTail
        
        isNewView = setupView(.red)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(button)
        contentView.addSubview(contentLabel)
        contentView.addSubview(timeLabel)
//        contentView.addSubview(subTitle)
        contentView.addSubview(isNewView)
        
        contentView.backgroundColor = UIColor.white
        backgroundColor = UIColor.clear
        

        seperator.isHidden = false
        
    }
    
    
    func setupTitle(_ textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!) -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font =  font
        label.numberOfLines = 2
        label.textColor = textColor
        label.backgroundColor = UIColor.clear
        return label
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }

    
    fileprivate func setupButton(_ title: String, selectedTitle: String) -> UIButton {
        let button = UIButton()
        button.isUserInteractionEnabled = false
        button.setTitle(title, for: UIControl.State())
        button.setTitle(selectedTitle, for: .selected)
        button.setTitleColor(UIColor.Text.whiteNormalColor(), for: UIControl.State())
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.contentHorizontalAlignment = .center
        button.clipsToBounds = true
        return button
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Table.tableGroupColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
}
