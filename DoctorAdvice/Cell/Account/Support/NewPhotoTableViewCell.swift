//
//  NewPhotoTableViewCell.swift
//  KO4Teacher
//
//  Created by Pham Huy on 8/29/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import KMPlaceholderTextView

class NewPhotoTableViewCell: UITableViewCell {
    
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, label = 20, button = 40, image = 60
    }
    
    var buttonDelete: UIButton!
    var textView: KMPlaceholderTextView!
    var seperator: UIView!
    
    var padding: CGFloat = 0
    
    var isPadding: Bool = false {
        didSet {
            layoutSubviews()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        contentView.frame = CGRect(x: Size.padding15..,
//                                   y: Size.padding10.. / 2,
//                                   width: bounds.width - Size.padding15.. * 2,
//                                   height: bounds.height - Size.padding10..)
        
        
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        
        textView.frame = CGRect(x: Size.padding10.. / 2,
                                y: contentView.frame.height - (textView.isHidden ? 0 : 55),
                                width: contentView.frame.width - Size.padding10.. ,
                                height: (textView.isHidden ? 0 : 50))
        
        imageView?.frame = CGRect(x: Size.padding10.. ,
                                  y: 0,
                                  width: contentView.frame.width - Size.padding10.. * 2,
                                  height: textView.frame.minY - (textView.isHidden ? 10 : 5))
        
        buttonDelete.frame = CGRect(x: bounds.width - Size.button.. - Size.padding10..  * 5 / 2,
                                    y: Size.padding10.. ,
                                    width: Size.button..,
                                    height: Size.button..)
        
        

        
        buttonDelete.layer.cornerRadius = Size.button.. / 2
        
        seperator.frame = CGRect(x: Size.padding10..,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: onePixel())
    }
    
    func setup() {
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        
        setupImageView()
        buttonDelete = setupButton()
        seperator = setupView()
        textView = setupTextView()
        
        
        contentView.addSubview(textView)
        addSubview(buttonDelete)
        
        contentView.clipsToBounds = true
        //        contentView.layer.masksToBounds = true
        //        contentView.layer.shadowColor   = UIColor.black.colorWithAlphaComponent(0.3).CGColor
        //        contentView.layer.shadowOpacity = 0.3
        //        contentView.layer.shadowRadius  = 2.0
        //        contentView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        
        
        imageView?.clipsToBounds       = true
        imageView?.layer.cornerRadius = textView.isHidden ? 0 : 8
//        imageView?.layer.borderWidth = onePixel()
//        imageView?.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
//        
//        imageView?.layer.shadowColor   = UIColor.black.withAlphaComponent(0.2).cgColor
//        imageView?.layer.shadowOpacity = 0.3
//        imageView?.layer.shadowRadius  = 3.0
//        imageView?.layer.shadowOffset  = CGSize(width: 0.0, height: 1.5)
//        imageView?.layer.masksToBounds = false
 
//        imageView?.clipsToBounds       = false
        
    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.layer.cornerRadius = 0
        imageView?.contentMode = .scaleAspectFill
    }
    
    func setupTitle() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font =  UIFont(name: FontType.latoBold.., size: FontSize.small++)
        label.numberOfLines = 2
        label.textColor = UIColor.Text.blackMediumColor()
        return label
    }
    
    func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.small--)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitleColor(UIColor.white, for: UIControl.State())
        button.clipsToBounds = true
        button.contentHorizontalAlignment = .center
        button.setImage(Icon.Navigation.Delete.tint(.red), for: UIControl.State())
        button.backgroundColor = UIColor.white
        
        button.layer.shadowColor   = UIColor.black.withAlphaComponent(0.5).cgColor
        button.layer.shadowOpacity = 0.4
        button.layer.shadowRadius  = 3.0
        button.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        button.clipsToBounds       = false
        
        return button
    }
    
    func setupTextView() -> KMPlaceholderTextView {
    
        let text = KMPlaceholderTextView()
        text.placeholder = LocalizedString("new_photo_add_description_for_image", comment: "Thêm ghi chú cho ảnh ...")
        text.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        text.placeholderColor = UIColor.Text.grayMediumColor()
        text.textAlignment = .justified
        text.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        text.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        text.layer.cornerRadius = 3
        text.layer.borderWidth = onePixel()
        text.autocapitalizationType = .none
        text.autocorrectionType = .no
        return text
    }
    
}


