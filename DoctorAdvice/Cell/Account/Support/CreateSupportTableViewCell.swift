//
//  CreateSupportTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 12/2/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class CreateSupportTableViewCell: UITableViewCell {
    
    enum Size: CGFloat {
        case buttonWidth = 80, cornerRadius = 8, padding10 = 10, label = 30
    }
    
    var bgTextField: UIView!
    var textField: UITextField!
    var textView: KMPlaceholderTextView!
    var padding: CGFloat = 0
    
    fileprivate var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupAllSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: padding ,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        


        
        bgTextField.frame = CGRect(x:  Size.padding10..  ,
                                 y: 0,
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: 38)

    
        textField.frame = CGRect(x:  Size.padding10.. / 2 ,
                                 y: 0,
                                 width: bgTextField.frame.width - Size.padding10.. ,
                                 height: bgTextField.frame.height)
        
        textView.frame = CGRect(x: Size.padding10..,
                                y: bgTextField.frame.maxY + Size.padding10.. ,
                                width: contentView.frame.width - Size.padding10.. * 2,
                                height: contentView.frame.height - (bgTextField.frame.maxY + Size.padding10.. * 2))
    }
    
}

extension CreateSupportTableViewCell {
    func setupAllSubviews() {
        
        backgroundColor = .clear
        contentView.backgroundColor = .white
        
        
        textField = setupTextField(LocalizedString("support_title_request_support", comment: "Tiêu đề yêu cầu hỗ trợ"))
        textView = setupTextView()
        
        bgTextField = setupView(8, bgColor: .white)

        contentView.addSubview(textView)
        contentView.addSubview(bgTextField)
        bgTextField.addSubview(textField)

        contentView.clipsToBounds = true
        
    }
    
    
    func setupView(_ cornerRadius: CGFloat = 0, bgColor: UIColor = .clear) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    func setupImageView(_ image: UIImage) -> UIImageView {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
         imageView.isUserInteractionEnabled = false
        
        return imageView
    }
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                            textColor: UIColor = .white,
                            alignment: NSTextAlignment = .center,
                            bgColor: UIColor = .clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    
    func setupButton(_ bgColor: UIColor) -> UIButton {
        let button = UIButton()
        button.contentHorizontalAlignment = .center
        button.clipsToBounds = true
        button.setTitleColor(UIColor.Text.blackMediumColor(), for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.small++)!
        button.backgroundColor = bgColor
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.contentMode = .scaleAspectFit
        button.isUserInteractionEnabled = true
        return button
    }
    
    func setupTextView() -> KMPlaceholderTextView {
        let textView = KMPlaceholderTextView()
        textView.backgroundColor = UIColor.white
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .none
        textView.clipsToBounds = true
        textView.layer.cornerRadius = 8

        
        textView.placeholderColor = UIColor.lightGray
        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.placeholder =  LocalizedString("support_label_place_holder_content", comment: "Quý phụ huynh/Học sinh vui lòng điền nội dung và những vấn đề cần hỗ trợ hoặc trao đổi với nhà trường tại đây. Trân trọng cảm ơn!")
        return textView
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String) -> UITextField {
        let textField = UITextField()
        textField.textColor = UIColor.Text.blackMediumColor()
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = false
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textField.isEnabled = true
        textField.backgroundColor = .clear
        textField.returnKeyType = .next
        textField.isUserInteractionEnabled = true
        return textField
    }
}


