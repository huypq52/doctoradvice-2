//
//  CameraCollectionViewCell.swift
//  GPSMobile
//
//  Created by Pham Huy on 6/7/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit

class CameraCollectionViewCell: UICollectionViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15
    }
    
    fileprivate var seperator: UIView!
    
    var picture: UIImageView!
    var webView: UIWebView!
//    var bgView: UIView!
//    var title: UILabel!
    
    var isCellSelected: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = bounds
        
        webView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        picture.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        
//        if isCellSelected {
//
//            UIView.animate(withDuration: 0.3, animations: {
////                self.picture.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: UIScreen.mainScreen().bounds.height)
//                self.picture.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
////                self.picture.frame.origin.y = (self.bounds.height - UIScreen.mainScreen().bounds.height) / 2
//
//
//                self.bgView.frame = CGRect(x: 0, y: self.bounds.height - 70, width: self.bounds.width, height: 70)
//                self.title.frame = CGRect(x: 0, y:  self.bounds.height - 40 - (70 - 40) / 2, width: self.bounds.width, height: 40)
//                self.seperator.frame = CGRect(x: 0, y: self.bounds.height - 1, width: self.bounds.width, height: 1)
//            })
//
//        } else {
//            picture.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
////            picture.frame.origin.y = (bounds.height - UIScreen.mainScreen().bounds.height) / 2
//
//
//            bgView.frame = bounds
//            title.frame = CGRect(x: 0, y: (bounds.height - 40) / 2, width: bounds.width, height: 40)
//            seperator.frame = CGRect(x: 0, y: bounds.height - 1, width: bounds.width, height: 1)
//        }
        
    }
}

extension CameraCollectionViewCell {
    fileprivate func setup() {
        
//        seperator = setupView(UIColor.black)
        picture = setupPicture()
//        bgView = setupView()
//        title = setupLabel()
        webView = setupWebView()
        
    
        contentView.backgroundColor = .white
        clipsToBounds = true
        
        contentView.addSubview(picture)
        contentView.addSubview(webView)
//        addSubview(bgView)
//        addSubview(title)
//        addSubview(seperator)
    }
    
    fileprivate func setupPicture() -> UIImageView {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
//        image.contentMode = .center
        return image
    }
    
    fileprivate func setupLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont(name: FontType.latoBold.., size: 16)!
        label.textAlignment = .center
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.black.withAlphaComponent(0.5)) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupWebView() -> UIWebView {
        let view: UIWebView = UIWebView()
        view.scrollView.isScrollEnabled = false
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = onePixel()
        return view
    }
}

