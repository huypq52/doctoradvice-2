//
//  AccountTableViewCell.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class AccountTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 40
    }
    
    var icon: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        icon.frame = CGRect(x: Size.padding10..,
                            y: (bounds.height - Size.button.. ) / 2,
                            width: Size.button..,
                            height: Size.button..)
        
        icon.layer.cornerRadius = icon.frame.height / 2
        
        textLabel?.frame = CGRect(x: icon.frame.maxX + Size.padding10.. ,
                                  y: 0,
                                  width: bounds.width - icon.frame.maxX - Size.padding10.. *  2,
                                  height: bounds.height)
        
        
        seperatorRightPadding = 15
        seperatorStyle = .padding(textLabel!.frame.minX)
        
    }
}

extension AccountTableViewCell {
    
    fileprivate func setup() {
        
        setupLabel()
        
        icon = setupButton()
        addSubview(icon)
    }
    
    fileprivate func setupImageView() {
        imageView?.backgroundColor = UIColor.Navigation.mainColor()
        imageView?.clipsToBounds = true
        
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.small++)!
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.clipsToBounds = true
        button.backgroundColor = UIColor.Navigation.mainColor()
        return button
    }
}

