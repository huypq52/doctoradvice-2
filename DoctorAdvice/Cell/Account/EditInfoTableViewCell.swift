//
//  EditInfoTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 11/10/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import KMPlaceholderTextView

class EditInfoTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10
    }
    
    var textField: TextFieldIcon!
    var textView: KMPlaceholderTextView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        backgroundColor = UIColor.clear
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        imageView?.contentMode = .center
        
        textField = setupTextField("", icon: Icon.Login.Personal)
        
        textView = setupTextView()
        
        addSubview(textField)
        addSubview(textView)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textField.frame = CGRect(x:  Size.padding10..,
                                 y: Size.padding10.. / 2,
                                 width: bounds.width -  Size.padding10.. * 2,
                                 height: bounds.height - Size.padding10.. / 2)
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: Size.padding10.. ,
                                  width: 30,
                                  height: 30)
        
        textView.frame = CGRect(x:  Size.padding10.. * 3 / 2 + 30,
                                 y: Size.padding10.. / 2,
                                 width: bounds.width -  Size.padding10.. * 5 / 2 - 30,
                                 height: bounds.height - Size.padding10.. / 2)
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String, icon: UIImage, secureTextEntry: Bool = false ) -> TextFieldIcon {
        let textField = TextFieldIcon(textColor: UIColor.Text.blackMediumColor())
        textField.icon.image = icon.tint(UIColor.Text.grayMediumColor())
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = secureTextEntry
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        
        if secureTextEntry {
            textField.returnKeyType = .done
        } else {
            textField.returnKeyType = .next
        }
        
        return textField
    }
    
    func setupTextView() -> KMPlaceholderTextView {
        let textView = KMPlaceholderTextView()
        textView.backgroundColor = .clear
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .none
        textView.textAlignment = .left
        
        textView.placeholderColor = UIColor.Text.grayMediumColor()
        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)

        return textView
    }


}

