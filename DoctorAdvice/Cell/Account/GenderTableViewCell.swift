//
//  GenderTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 8/24/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions
import MGSwipeTableCell

class GenderTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 44
    }
    
    var buttonMale: UIButton!
    var buttonFemale: UIButton!
    var seperator: UIView!
    
    var isMale: Bool = true {
        didSet {
            
            buttonMale.isSelected = isMale
            buttonFemale.isSelected = !buttonMale.isSelected
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        guard let imageView = imageView else { return }
        
        
        
        buttonMale.frame = CGRect(x: imageView.frame.maxX,
                                  y: 0,
                                  width: (contentView.frame.width - imageView.frame.maxX) / 2,
                                  height: contentView.frame.height )
        
        buttonFemale.frame = CGRect(x: buttonMale.frame.maxX,
                                  y: 0,
                                  width: (contentView.frame.width - imageView.frame.maxX) / 2,
                                  height: contentView.frame.height )
        

        
        seperator.frame = CGRect(x: imageView.frame.minX,
                                 y: bounds.height - onePixel(),
                                 width: bounds.width -  imageView.frame.minX - Size.padding10..,
                                 height: onePixel())
    }
    
    @objc func changeGender(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        switch sender.tag {
        case 1:
            buttonFemale.isSelected = !buttonMale.isSelected
            
        default:
            buttonMale.isSelected = !buttonFemale.isSelected
        }
        
        isMale = buttonMale.isSelected
    }
}

extension GenderTableViewCell {
    func setupAllSubviews() {
        
        setupImageView()
        setupLabel()
        
        buttonMale = setupButton(title: LocalizedString("label_general_default_male", comment: "Nam"),
                                 image: Icon.General.RadioOff.tint(UIColor.Text.grayMediumColor()),
                                 imageSelected: Icon.General.RadioOn.tint(UIColor.Navigation.mainColor()),
                                 titleColor: UIColor.Text.blackMediumColor(),
                                 selector: #selector(self.changeGender(_:)))
        buttonMale.tag = 1
        
        buttonFemale = setupButton(title: LocalizedString("label_general_default_female", comment: "Nữ"),
                                   image: Icon.General.RadioOff.tint(UIColor.Text.grayMediumColor()),
                                   imageSelected: Icon.General.RadioOn.tint(UIColor.Navigation.mainColor()),
                                 titleColor: UIColor.Text.blackMediumColor(),
                                 selector: #selector(self.changeGender(_:)))
        
        buttonFemale.tag = 0
        seperator = setupView()
        
        addSubview(buttonMale)
        addSubview(buttonFemale)
        addSubview(seperator)
        
        buttonMale.isSelected = isMale
        buttonFemale.isSelected = !buttonMale.isSelected
        
    }
    
    func setupLabel(){
        textLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        textLabel?.textAlignment = .left
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        detailTextLabel?.numberOfLines = 0
        detailTextLabel?.lineBreakMode = .byWordWrapping
        
    }
    
    func setupImageView() {
        imageView?.contentMode = .scaleAspectFit
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white, selector: Selector) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: -7)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
        return button
    }

    
    func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}

