//
//  ListChildrenTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 11/6/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class ListChildrenTableViewCell: UITableViewCell {
    
    fileprivate enum Size:CGFloat {
        case padding10 = 10, padding5 = 5, padding15 = 15, padding20 = 20, button = 44, label = 24, avatar = 100
    }
    
    var labelName: UILabel!
    var labelChild: UILabel!
    var labelAge: UILabel!
    var labelClass: UILabel!
    var labelAccountType: UILabel!
    var imageViewAvatar: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: Size.padding10..,
                                   y: Size.padding5..,
                                   width: bounds.width - Size.padding10.. * 2,
                                   height: bounds.height - Size.padding5.. * 2)
        
        imageViewAvatar.frame = CGRect(x: Size.padding15.. ,
                                       y: Size.padding15..,
                                       width: (contentView.frame.height - Size.padding15.. * 2) * 4 / 5,
                                       height: contentView.frame.height - Size.padding15.. * 2)
        imageViewAvatar.layer.cornerRadius = 5
        
        
        labelName.frame = CGRect(x: imageViewAvatar.frame.maxX + Size.padding10..,
                                 y: Size.padding10.. ,
                                 width: contentView.frame.width - imageViewAvatar.frame.maxX - Size.padding10.. * 2,
                                 height: Size.label..)
        
        
        
        labelChild.frame = CGRect(x: labelName.frame.minX,
                                  y: labelName.frame.maxY,
                                  width: labelName.frame.width,
                                  height: Size.label..)
        
        labelAge.frame = CGRect(x: labelName.frame.minX,
                                y: labelChild.frame.maxY,
                                width: labelName.frame.width,
                                height: Size.label..)
        
        labelClass.frame = CGRect(x: labelName.frame.minX,
                                y: labelAge.frame.maxY,
                                width: labelName.frame.width,
                                height: Size.label..)
        
        labelAccountType.frame = CGRect(x: labelName.frame.minX,
                                y: labelClass.frame.maxY,
                                width: labelName.frame.width,
                                height: Size.label.. * 2 + 10)
        
        
    }
}


extension ListChildrenTableViewCell {
    fileprivate func setupView() {
        backgroundColor = .clear
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
        contentView.clipsToBounds = true
        clipsToBounds = true
        
        imageViewAvatar = setupImageView()
        labelName = setupLabel(UIFont(name: FontType.latoBold.., size: FontSize.large..)!,
                               textColor: UIColor.Text.blackMediumColor())
        labelName.textAlignment = .center
        
        labelChild = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor.Text.grayNormalColor())
        labelAge = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                              textColor: UIColor.Text.grayNormalColor())
        
        
        labelClass = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor.Text.grayNormalColor())
        
        labelAccountType = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                      textColor: UIColor.Text.grayNormalColor())
        
        labelAccountType.numberOfLines = 0
        labelAccountType.lineBreakMode = .byWordWrapping
        labelAccountType.adjustsFontSizeToFitWidth = false
        
        
        contentView.addSubview(imageViewAvatar)
        contentView.addSubview(labelName)
        contentView.addSubview(labelChild)
        contentView.addSubview(labelAge)
        contentView.addSubview(labelClass)
        contentView.addSubview(labelAccountType)
    }
    
    fileprivate func setupLabel(_ font: UIFont, textColor: UIColor) -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = font
        label.textColor = textColor
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }
    
    fileprivate func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.lightGray
        return imageView
    }
}




