//
//  TeacherDetailTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 12/9/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class TeacherDetailTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 40, avatar = 20
    }
    
    var isPadding = false {
        didSet {
            layoutSubviews()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: isPadding ? Size.padding10.. : 0 ,
                                   y: 0,
                                   width: bounds.width - (isPadding ? Size.padding10.. * 2 : 0),
                                   height: bounds.height)
        
        
        imageView?.frame = CGRect(x: Size.padding15..,
                                  y: 0,
                                  width: Size.avatar..,
                                  height: Size.avatar..)
        
        guard let imageView = imageView else { return }
        
        
        textLabel?.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                  y: 0,
                                  width: contentView.frame.width - (imageView.frame.maxX + Size.padding10..) - Size.padding15..,
                                  height: contentView.frame.height)
        
    }
}

extension TeacherDetailTableViewCell {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        setupLabel()
        setupImageView()

    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.contentMode = .scaleAspectFill
        
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.clipsToBounds = true
        button.backgroundColor = UIColor.clear
        button.setImage(image, for: .normal)
        return button
    }
    
    fileprivate func setupSeperator() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}

