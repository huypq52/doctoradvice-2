//
//  EditNameTableViewCell.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 11/16/17.
//  Copyright © 2017 OMT. All rights reserved.
//


import UIKit
import KMPlaceholderTextView

class EditNameTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10
    }
    
    var firstName: UITextField!
    var lastName: UITextField!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        backgroundColor = UIColor.clear
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        imageView?.contentMode = .center
        
        firstName = setupTextField(LocalizedString("label_general_default_first_name", comment: "Họ"))
        lastName  = setupTextField(LocalizedString("label_general_default_last_name", comment: "Tên"), isFirstName: false)
        

        
        addSubview(firstName)
        addSubview(lastName)
        
        seperatorStyle = .padding(10)
        seperatorRightPadding = 10
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: Size.padding10.. ,
                                  width: 30,
                                  height: 30)
        
        guard let imageView = imageView else { return }
        
        firstName.frame = CGRect(x:  imageView.frame.maxX + Size.padding10.. / 2,
                                 y: Size.padding10.. ,
                                 width: (bounds.width - imageView.frame.maxX) / 2 - Size.padding10.. ,
                                 height: bounds.height - Size.padding10.. * 3 / 2)
        
        lastName.frame = CGRect(x:  firstName.frame.maxX + Size.padding10.. / 2,
                                 y: Size.padding10..,
                                 width: (bounds.width - imageView.frame.maxX) / 2 - Size.padding10..,
                                 height: bounds.height - Size.padding10.. * 3 / 2)
        

    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String, isFirstName: Bool = true) -> UITextField {
        let textField = UITextField()
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textField.clipsToBounds = true
        textField.layer.cornerRadius = 4
        textField.backgroundColor = UIColor.white
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 5, height: 40)
        view.backgroundColor = .clear
        textField.leftView = view
        textField.leftViewMode = .always
        if !isFirstName {
            textField.returnKeyType = .done
        } else {
            textField.returnKeyType = .next
        }
        
        return textField
    }
    
}


