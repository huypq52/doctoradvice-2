//
//  LoginTextViewTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 5/7/19.
//  Copyright © 2018 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions
import KMPlaceholderTextView

class LoginTextViewTableViewCell: UITableViewCell {
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, padding5 = 5, label = 36
    }
    
    var textView: KMPlaceholderTextView!
    
    var seperatorBottom: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        textLabel?.numberOfLines = 2
        
        textView = setupTextView()
        contentView.addSubview(textView)
        contentView.backgroundColor = UIColor.white
        contentView.layer.cornerRadius = 5
        
        
        textView.layer.shadowColor   = UIColor.black.withAlphaComponent(0.2).cgColor
        textView.layer.shadowOpacity = 0.3
        textView.layer.shadowRadius  = 3.0
        textView.layer.shadowOffset  = CGSize(width: 0.0, height: 1.5)
        
        textView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        textView.layer.borderWidth = onePixel()
        textView.layer.cornerRadius = 5
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
       
            contentView.frame = bounds
        
            textLabel?.frame = CGRect(x: Size.padding10..,
                                      y: Size.padding10..  / 2,
                                      width: bounds.width - Size.padding10.. * 2,
                                      height: Size.label..)
        
            textView.frame = CGRect(x:  Size.padding10..,
                                    y: Size.padding10..  / 2 +  Size.label..,
                                    width: contentView.frame.width - Size.padding10.. * 2,
                                    height: bounds.height - Size.padding10..  / 2 - Size.label..)
            
      
    }
    
    fileprivate func setupTextView() -> KMPlaceholderTextView {
        let textView: KMPlaceholderTextView = KMPlaceholderTextView()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.backgroundColor = UIColor.Table.tableGroupColor().alpha(0.3)
        textView.autocorrectionType = .no
        textView.clipsToBounds = true
        return textView
    }
}

