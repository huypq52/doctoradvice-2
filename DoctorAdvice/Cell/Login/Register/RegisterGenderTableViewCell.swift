//
//  RegisterGenderTableViewCell.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/19/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit



class RegisterGenderTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 44
    }
    
    var padding: CGFloat = 0
    var arrayButton: [UIButton] = []
   
    var arrayType: [GenderType] = [.male, .female]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: (contentView.frame.height - 20 - 4) / 2,
                                  width: (imageView?.isHidden ?? true) ? 0 : 20,
                                  height: 20 + 4)
        
        guard let imageView = imageView else { return }
        
        var textWidth: CGFloat = 0
        
        if let textLabel = textLabel, !textLabel.isHidden {
            textWidth = Utility.widthForView(textLabel.text ?? "",
                                             font: textLabel.font,
                                             height: contentView.frame.height)
        }
        
        textLabel?.frame = CGRect(x: textWidth > 0 ? imageView.frame.maxX + Size.padding10.. : 0,
                                  y: 0,
                                  width: textWidth,
                                  height: contentView.frame.height)
        
        
        if textWidth > 0 { textWidth += Size.padding10.. * 2 + imageView.frame.maxX  }
        
        let widthButton: CGFloat = (contentView.frame.width - imageView.frame.maxX  - (textLabel?.frame.maxX ?? 0)) / CGFloat(arrayButton.count)
        
        arrayButton.enumerated().forEach { (i, button) in
 
            button.frame = CGRect(x: textWidth + CGFloat(i) * widthButton,
                                  y: 0,
                                  width: widthButton,
                                  height: contentView.frame.height)
        }
    
    }

}

extension RegisterGenderTableViewCell {
    func setupAllSubviews() {
        
        backgroundColor = .clear
        contentView.backgroundColor = .white
//        contentView.layer.cornerRadius = 5
        contentView.clipsToBounds = true
        imageView?.contentMode = .scaleAspectFit

        setupLabel()
        
  
        arrayButton = []
        arrayType.forEach { type in
            
            let button = setupButton(title: type.title,
                                     image: Icon.General.RadioOff.tint(UIColor.Text.grayMediumColor()),
                                     imageSelected: Icon.General.RadioOn.tint(UIColor.Navigation.mainColor()),
                                     titleColor: UIColor.Text.blackMediumColor())
            
            button.tag = type..
            contentView.addSubview(button)
            
            arrayButton.append(button)
        }
    }
    
    func setupLabel(){
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)
        textLabel?.textAlignment = .left
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        textLabel?.numberOfLines = 2
        textLabel?.lineBreakMode = .byTruncatingTail
    
    }
    

    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: -7)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
        return button
    }

    
    func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}



