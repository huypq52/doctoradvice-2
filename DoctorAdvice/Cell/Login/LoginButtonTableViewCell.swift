//
//  LoginButtonTableViewCell.swift
//  UniOnline
//
//  Created by Pham Huy on 5/17/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
//import PHExtensions


class LoginButtonTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, label = 36, button = 44
    }
    
    var isPadding: Bool = false
    
    var buttonLeft: UIButton!
    var buttonRight: UIButton!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = .clear
        
        
        buttonLeft = setupButton(titleColor: UIColor.Navigation.subColor())
        buttonRight = setupButton(titleColor: UIColor.Navigation.mainColor())

        contentView.addSubview(buttonLeft)
        contentView.addSubview(buttonRight)

        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        if buttonLeft.isHidden {
            
            buttonLeft.frame = .zero
            buttonRight.frame = CGRect(x: Size.padding10.. ,
                                       y: contentView.frame.height - Size.button..,
                                       width: contentView.frame.width - Size.padding10.. * 2,
                                       height: Size.button..)
            
        } else {
            
            buttonLeft.frame = CGRect(x: Size.padding10.. ,
                                      y: contentView.frame.height - Size.button..,
                                      width: contentView.frame.width / 2 - Size.padding10.. - Size.padding10.. / 2,
                                      height: Size.button..)
            
            buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding10.. / 2,
                                       y: contentView.frame.height - Size.button..,
                                       width: contentView.frame.width / 2 - Size.padding10.. - Size.padding10.. / 2,
                                       height: Size.button..)
            
        }
        
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }

}



