//
//  LoginTextFieldTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 5/7/19.
//  Copyright © 2018 KidOnline. All rights reserved.
//



import UIKit
//import PHExtensions


class LoginTextFieldTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, label = 36, image = 24
    }
    
    var padding: CGFloat = 0
    
    var textField: UITextField!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        imageView?.frame = CGRect(x: (imageView?.isHidden ?? true) ? 0 : Size.padding10.. ,
                                  y: (contentView.frame.height - Size.image.. - 2) / 2,
                                  width: (imageView?.isHidden ?? true) ? 0 : Size.image..,
                                  height: Size.image.. + 2)
        
        guard let imageView = imageView else { return }
        
        if let textLabel = textLabel, !textLabel.isHidden {
            
            textLabel.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                      y: Size.padding10..  / 2,
                                      width: bounds.width - Size.padding10.. * 2 - imageView.frame.maxX,
                                      height: Size.label..)
            
            textField.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                    y: Size.padding10..  / 2 +  Size.label..,
                                    width: contentView.frame.width - Size.padding10.. * 2 - imageView.frame.maxX ,
                                    height: contentView.frame.height - Size.padding10.. / 2 - Size.label..)
            
        } else {
            
            textLabel?.frame = .zero
            
            textField.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                    y: 0,
                                    width: contentView.frame.width - Size.padding10.. * 2 - imageView.frame.maxX,
                                    height: contentView.frame.height )
        }
        

    }
}

extension LoginTextFieldTableViewCell {
    
     func setupAllSubviews() {
        
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .white
        contentView.clipsToBounds = true
//        contentView.layer.cornerRadius = 5

        setupLabelDefault()
        textField = setupTextField()
        contentView.addSubview(textField)
        


        
    }
    
    func setupLabelDefault() {
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        textLabel?.textColor = .white
        textLabel?.numberOfLines = 2
    }
    
    func setupImageView() {
        imageView?.contentMode = .scaleAspectFit
    }
    
    func setupTextField(_ textPlaceHoder: String = "") -> UITextField {
        let textField = UITextField()
        
        textField.autocapitalizationType = .none
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textField.textColor = UIColor.Text.blackMediumColor()
        textField.backgroundColor = .white
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
      
//        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: Size.padding10.. / 2, height: Size.label..))
//        textField.leftViewMode = .always
        

        return textField
    }
}


