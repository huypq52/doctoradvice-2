//
//  ContactInfoTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 3/8/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import UIKit
//import PHExtensions


class ContactInfoTableViewCell: SeperatorTableViewCell {
    
    let cellIdentify = "ContactInfoTableViewCell"
    
    fileprivate enum Size:CGFloat {
        case padding10 = 10, padding5 = 5, padding15 = 15, button = 48, label = 22, avatar = 80
    }
    
    var labelName: UILabel!
    var labelSub: UILabel!
    var imageViewAvatar: UIImageView!
    var buttonCall: UIButton!
    var buttonMessage: UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageViewAvatar.frame = CGRect(x: (bounds.width - Size.avatar.. ) / 2 ,
                                       y: Size.padding15..,
                                       width: Size.avatar..,
                                       height: Size.avatar..)
        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.height / 2
        
        
        labelName.frame = CGRect(x:  Size.padding10..,
                                 y: imageViewAvatar.frame.maxY + Size.padding10..,
                                 width: bounds.width -  Size.padding10.. * 2,
                                 height: Size.label..)
        
        labelSub.frame = CGRect(x:  Size.padding10..,
                                 y: labelName.frame.maxY ,
                                 width: bounds.width -  Size.padding10.. * 2,
                                 height: bounds.height - labelName.frame.maxY)
        
        buttonCall.frame = CGRect(x: 0, y: 0, width: Size.button.., height: Size.button..)
        buttonCall.center = CGPoint(x: imageViewAvatar.frame.minX / 2, y: imageViewAvatar.center.y)
        buttonCall.layer.cornerRadius = buttonCall.frame.height / 2
        
        buttonMessage.frame = CGRect(x: 0, y: 0, width: Size.button.., height: Size.button..)
        buttonMessage.center = CGPoint(x: imageViewAvatar.frame.maxX + ( bounds.width - imageViewAvatar.frame.maxX ) / 2, y: imageViewAvatar.center.y)
        buttonMessage.layer.cornerRadius = buttonMessage.frame.height / 2
        

    }
}


extension ContactInfoTableViewCell {
    fileprivate func setupView() {
        backgroundColor = UIColor.white
        
        imageViewAvatar = setupImageView()
        labelName = setupLabel(UIFont(name: FontType.latoLight.., size: FontSize.large..)!, textColor: UIColor.Text.blackMediumColor())
        labelSub = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!, textColor: UIColor.Text.grayNormalColor())
        
        labelSub.numberOfLines = 0
        labelSub.lineBreakMode = .byWordWrapping
        
        buttonCall = setupButton(Icon.Login.Phone)
        buttonMessage = setupButton(Icon.Contact.Message)
        
        
        addSubview(imageViewAvatar)
        addSubview(labelName)
        addSubview(labelSub)
        addSubview(buttonCall)
        addSubview(buttonMessage)

        
    }
    
    fileprivate func setupLabel(_ font: UIFont, textColor: UIColor) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.font = font
        label.textColor = textColor
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        return label
    }
    
    fileprivate func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.lightGray
        return imageView
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.layer.borderColor = UIColor.Navigation.mainColor().cgColor
        button.layer.borderWidth = onePixel()
        button.setImage(image.tint(UIColor.Navigation.mainColor()), for: .normal)
        button.clipsToBounds = true
        return button
    }
}



