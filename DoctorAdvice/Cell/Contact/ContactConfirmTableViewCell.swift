//
//  ContactConfirmTableViewCell.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/24/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//

import UIKit


class ContactConfirmTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 36, avatar = 50, status = 14
    }
    
    var buttonConfirm: UIButton!
    var buttonCancel: UIButton!
    var seperator: UIView!
 
    
    var padding: CGFloat = 10
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: padding ,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        
        let widthButton = Utility.widthForView(buttonConfirm.titleLabel?.text ?? "",
                                               font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!, height: 20) + 30
        
        
        buttonConfirm.frame = CGRect(x: Size.padding10.. + Size.avatar..,
                                     y: contentView.frame.height - Size.button.. - Size.padding10.. / 2,
                                     width: widthButton,
                                     height: Size.button..)
        
        let width = Utility.widthForView(buttonCancel.titleLabel?.text ?? "",
                                               font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!, height: 20) + 30
        
        
        buttonCancel.frame = CGRect(x: buttonConfirm.frame.maxX + Size.padding10.. ,
                                     y: contentView.frame.height - Size.button.. - Size.padding10.. / 2,
                                     width: width,
                                     height: Size.button..)
        
        
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: (buttonConfirm.frame.minY - Size.avatar.. ) / 2,
                                  width: Size.avatar..,
                                  height: Size.avatar..)
        
        

  
        guard let imageView = imageView else { return }
        
        
        if let detailTextLabel = detailTextLabel, !detailTextLabel.isHidden {
            
            
            textLabel?.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                      y: Size.padding10.. / 2,
                                      width: contentView.frame.width - imageView.frame.maxX - Size.padding10.. * 2,
                                      height: buttonConfirm.frame.minY / 2 - Size.padding10.. / 2)
            
            detailTextLabel.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                      y: buttonConfirm.frame.minY / 2,
                                      width: contentView.frame.width - imageView.frame.maxX - Size.padding10.. * 2,
                                      height: buttonConfirm.frame.minY / 2 - Size.padding10.. / 2)
            

            
        } else {
            
            detailTextLabel?.frame = .zero
            textLabel?.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                      y: 0,
                                      width: contentView.frame.width - imageView.frame.maxX - Size.padding10.. * 2,
                                      height: buttonConfirm.frame.minY)
        }
        
        

        
        
        
        seperator.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width - (imageView.frame.maxX + Size.padding10..) - Size.padding10..,
                                 height: onePixel())
        

        
    }
}

extension ContactConfirmTableViewCell {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        setupLabel()
        setupImageView()
        seperator = setupView(UIColor.Misc.seperatorColor())
   
        
        buttonConfirm = setupButton(title: "Xác nhận", titleColor: .white)
        buttonConfirm.backgroundColor = UIColor.Navigation.mainColor()
        buttonCancel = setupButton(title: "Xóa", titleColor: UIColor.Text.blackMediumColor())
        buttonCancel.backgroundColor = .white
        
      
 
        contentView.addSubview(buttonConfirm)
        contentView.addSubview(buttonCancel)
        contentView.addSubview(seperator)
  
    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.contentMode = .scaleAspectFill
        imageView?.layer.cornerRadius = Size.avatar.. / 2
        
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoLight.., size: FontSize.normal++)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
        
    }
    
    func setupStatusView(bgColor: UIColor = .green) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }
    

    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.layer.cornerRadius = 8
        return button
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }

}


