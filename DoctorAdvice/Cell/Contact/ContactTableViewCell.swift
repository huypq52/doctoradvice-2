//
//  ContactTableViewCell.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class ContactTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 44, avatar = 50, status = 14
    }
    
    var buttonMessage: UIButton!
    var seperator: UIView!
    var statusView: UIView!
    var isNewMessageView: UIView!
    
    
    var isOnline: Bool = false {
        didSet {
            if isOnline {
                statusView.backgroundColor = .green
            } else {
                statusView.backgroundColor = .lightGray
            }
        }
    }
    
    var isPadding = true {
        didSet {
            layoutSubviews()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: isPadding ? Size.padding10.. : 0 ,
                                   y: 0,
                                   width: bounds.width - (isPadding ? Size.padding10.. * 2 : 0),
                                   height: bounds.height)
        
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: (contentView.frame.height - Size.avatar.. ) / 2,
                                  width: Size.avatar..,
                                  height: Size.avatar..)
        
        imageView?.layer.cornerRadius = Size.avatar.. / 2
        
        buttonMessage.frame = CGRect(x: contentView.frame.width - Size.button.. - Size.padding10..,
                                     y: (contentView.frame.height - Size.button.. ) / 2,
                                     width: Size.button..,
                                     height: Size.button..)
        
        isNewMessageView.frame  = CGRect(x: buttonMessage.frame.width - Size.status.. ,
                                         y: 5,
                                         width: Size.status.. - 4,
                                         height: Size.status.. - 4)
        isNewMessageView.layer.cornerRadius = isNewMessageView.frame.height / 2
        
        
        guard let imageView = imageView else { return }
        
        
        textLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        textLabel?.frame.size.width = (buttonMessage.isHidden ? contentView.frame.width - Size.padding10.. : buttonMessage.frame.minX ) - (imageView.frame.maxX + Size.padding10..)
        
        detailTextLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        detailTextLabel?.frame.size.width = (buttonMessage.isHidden ? contentView.frame.width - Size.padding10.. : buttonMessage.frame.minX ) - (imageView.frame.maxX + Size.padding10..)
        
        
        
        seperator.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width - (imageView.frame.maxX + Size.padding10..) - Size.padding10..,
                                 height: onePixel())
        
        statusView.frame = CGRect(x: (isPadding ? Size.padding10.. : 0 ) + imageView.frame.maxX - Size.status..,
                                  y: imageView.frame.maxY - Size.status..,
                                  width: Size.status..,
                                  height: Size.status..)
        statusView.layer.cornerRadius = Size.status.. / 2
        
    }
}

extension ContactTableViewCell {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        setupLabel()
        setupImageView()
        seperator = setupView(UIColor.Misc.seperatorColor())
        statusView = setupStatusView()
        isNewMessageView = setupView(.red)
        isNewMessageView.layer.borderWidth = 1
        isNewMessageView.layer.borderColor = UIColor.white.cgColor
        
        
        buttonMessage = setupButton(Icon.Login.Phone.tint(UIColor.Navigation.mainColor()))
        buttonMessage.addSubview(isNewMessageView)
        contentView.addSubview(buttonMessage)
        contentView.addSubview(seperator)
        addSubview(statusView)
    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.contentMode = .scaleAspectFill
        
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoLight.., size: FontSize.normal++)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
        
    }
    
    func setupStatusView(bgColor: UIColor = .green) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.clipsToBounds = true
        button.backgroundColor = UIColor.clear
        button.setImage(image, for: .normal)
        return button
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }

}
