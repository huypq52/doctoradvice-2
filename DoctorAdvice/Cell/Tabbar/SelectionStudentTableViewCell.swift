//
//  SelectionStudentTableViewCell.swift
//  KidOnline
//
//  Created by Pham Huy on 11/7/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class SelectionStudentTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 40, avatar = 42
    }
    
    var seperator: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x:  0 ,
                                   y: 0,
                                   width: bounds.width,
                                   height: bounds.height)
        
        
        imageView?.frame = CGRect(x: Size.padding10.. / 2,
                                  y: (contentView.frame.height - Size.avatar.. ) / 2,
                                  width: Size.avatar..,
                                  height: Size.avatar..)
        
        imageView?.layer.cornerRadius = Size.avatar.. / 2
        
        
        guard let imageView = imageView else { return }
        
        
        textLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        textLabel?.frame.size.width = bounds.width - 10 - (imageView.frame.maxX + Size.padding10..)
        
        detailTextLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        detailTextLabel?.frame.size.width = bounds.width - 10 - (imageView.frame.maxX + Size.padding10..)
        
        
        
        seperator.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width - (imageView.frame.maxX + Size.padding10..) - Size.padding10..,
                                 height: onePixel())
        
    }
}

extension SelectionStudentTableViewCell {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        setupLabel()
        setupImageView()
        seperator = setupSeperator()
        
//        contentView.addSubview(seperator)
    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.contentMode = .scaleAspectFill
        
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.clipsToBounds = true
        button.backgroundColor = UIColor.clear
        button.setImage(image, for: .normal)
        return button
    }
    
    fileprivate func setupSeperator() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}

