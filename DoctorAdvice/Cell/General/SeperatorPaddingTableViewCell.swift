//
//  SeperatorPaddingTableViewCell.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/30/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
//import PHExtensions

class SeperatorPaddingTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10
    }
    
    var isPadding = true
    
    enum SeperatorStyle {
        case hidden
        case padding(CGFloat)
    }
    var seperatorStyle: SeperatorStyle = .padding(15) {
        didSet {
            switch seperatorStyle {
            case .hidden:
                seperator.isHidden = true
            case .padding(_):
                seperator.isHidden = false
            }
        }
    }
    
    var seperatorRightPadding: CGFloat = 15
    
    var seperator: UIView! = {
        var view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    fileprivate func setup() {
        contentView.insertSubview(seperator, aboveSubview: textLabel!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: isPadding ? Size.padding10.. : 0,
                                   y: 0,
                                   width: bounds.width -  (isPadding ? Size.padding10.. * 2 : 0),
                                   height: bounds.height)
        
        switch seperatorStyle {
        case .hidden:
            break
        case .padding(let padding):
            seperator.frame = CGRect(
                x: padding,
                y: contentView.bounds.height - onePixel(),
                width: contentView.bounds.width - padding - seperatorRightPadding,
                height: onePixel())
        }
        
        
    }
    
}

