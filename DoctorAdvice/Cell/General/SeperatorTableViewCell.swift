//
//  SeperatorTableViewCell.swift
//  STaxi
//
//  Created by Hoan Pham on 3/26/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions

class SeperatorTableViewCell: UITableViewCell {

    enum SeperatorStyle {
        case hidden
        case padding(CGFloat)
    }
    var seperatorStyle: SeperatorStyle = .padding(15) {
        didSet {
            switch seperatorStyle {
            case .hidden:
                seperator.isHidden = true
            case .padding(_):
                seperator.isHidden = false
            }
        }
    }
    
    var seperatorRightPadding: CGFloat = 15
    
    var seperator: UIView! = {
        var view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
        }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
//        contentView.insertSubview(seperator, aboveSubview: textLabel!)
        
        addSubview(seperator)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        contentView.insertSubview(seperator, aboveSubview: textLabel!)
        addSubview(seperator)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch seperatorStyle {
        case .hidden:
            break
        case .padding(let padding):
            seperator.frame = CGRect(
                x: padding,
                y: contentView.bounds.height - onePixel(),
                width: bounds.width - padding - seperatorRightPadding,
                height: onePixel())
        }
    
        
    }

}
