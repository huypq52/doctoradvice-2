//
//  TextFieldTableViewCell.swift
//  STaxi
//
//  Created by NangND on 4/2/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions


class TextFieldTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10
    }
    
    var isPadding: Bool = false
    
    var textField: UITextField = {
        var textField = UITextField()
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal++)
        textField.textColor = UIColor.black
        textField.backgroundColor = UIColor.clear
        textField.autocorrectionType = .no
        textField.clearButtonMode = .whileEditing
//        textField.enabled = false
        return textField
        }()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        backgroundColor = UIColor.clear
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        addSubview(textField)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if isPadding {
            textField.frame = CGRect(x: (imageView?.frame)!.maxX + Size.padding10..,
                                     y: 0,
                                     width: bounds.width - ((imageView?.frame)!.maxX + Size.padding10..) - Size.padding10..,
                                     height: bounds.height)
        } else {
            textField.frame = CGRect(x: (self.imageView?.frame)!.maxX + Size.padding15..,
                                     y: 0,
                                     width: self.bounds.width - ((self.imageView?.frame)!.maxX + Size.padding15..) - Size.padding15..,
                                     height: self.bounds.height)
        }
        

        
        seperatorStyle = .padding(15)
        seperatorRightPadding = 15
    }
}

