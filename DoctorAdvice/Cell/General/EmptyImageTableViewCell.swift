//
//  EmptyImageTableViewCell.swift
//  G5TaxiUser
//
//  Created by NangND on 11/20/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions

class EmptyImageTableViewCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        imageView?.contentMode = .scaleAspectFit
        backgroundColor =  UIColor.white
        
        textLabel?.textAlignment = .center
        textLabel?.numberOfLines = 0
        textLabel?.lineBreakMode = .byWordWrapping
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        textLabel?.font = UIFont(name: FontType.latoLight.., size: FontSize.normal++)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView?.frame = CGRect(x: bounds.width / 8,
            y: 0,
            width: bounds.width * 3 / 4,
            height: bounds.height * 3 / 5)
        
        
        textLabel?.frame = CGRect(x: 10,
                                  y: imageView!.frame.maxY,
                                  width: bounds.width - 10 * 2,
                                  height: 60)
    }
}

