//
//  EmptyTableViewCell.swift
//  G5TaxiUser
//
//  Created by NangND on 9/25/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

//import PHExtensions
import UIKit



class EmptyTableViewCell: UITableViewCell {
    
    enum Size: CGFloat {
        case padding10 = 10
    }
    
    var title: UILabel!
    
    var padding: CGFloat = 10
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    


    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        title.frame = CGRect(x: Size.padding10..,
                             y: 0,
                             width: contentView.frame.width - Size.padding10.. * 2,
                             height: contentView.frame.height)
    }
    
    
}

extension EmptyTableViewCell {
    
    
    func setupAllSubviews() {
        
        title = setupLabel(textColor: UIColor.Text.grayMediumColor(),
                           font: UIFont(name: FontType.latoRegular.., size: FontSize.normal++)!,
                           bgColor: .clear, alignment: .center)
        
        contentView.addSubview(title)
        
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .white
    }

    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }

}











