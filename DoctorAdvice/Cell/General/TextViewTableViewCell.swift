//
//  TextViewTableViewCell.swift
//  G5TaxiUser
//
//  Created by NangND on 9/29/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class TextViewTableViewCell: UITableViewCell {
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, padding5 = 5
    }
    
    var textView: KMPlaceholderTextView!
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
        

        contentView.frame = CGRect(x: Size.padding10..,
                                   y: Size.padding10.. / 2,
                                   width: bounds.width - Size.padding10.. * 2,
                                   height: bounds.height - Size.padding10..)
        
        textView.frame = CGRect(x: Size.padding10.. / 2,
                                y: Size.padding10.. / 2,
                                width: contentView.frame.width - Size.padding10.. ,
                                height: contentView.frame.height - Size.padding10.. / 2)
        

        
    }
    

}

extension TextViewTableViewCell {
    
    fileprivate func setup() {
        
        textView = setupTextView()
        contentView.addSubview(textView)
        
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
        backgroundColor = .clear

        layer.shadowColor   = UIColor.black.withAlphaComponent(0.3).cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius  = 3.0
        layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
    }
    
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    
    
    fileprivate func setupTextView() -> KMPlaceholderTextView {
        let textView = KMPlaceholderTextView()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.textAlignment = .left
        textView.backgroundColor = .clear
        textView.clipsToBounds = true
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .sentences
        textView.layer.cornerRadius = 6

        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.placeholderColor = UIColor.Text.grayMediumColor()
        textView.placeholder = ""
        return textView
    }
}
