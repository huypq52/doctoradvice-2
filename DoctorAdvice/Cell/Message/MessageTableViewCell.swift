//
//  MessageTableViewCell.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 40, avatar = 50, time = 100, status = 14
    }
    
    var buttonTime: UIButton!
    var seperator: UIView!
    var statusView: UIView!
    
    var isOnline: Bool = false {
        didSet {
            if isOnline {
                statusView.backgroundColor = .green
            } else {
                statusView.backgroundColor = .lightGray
            }
        }
    }
    
    var unread: Bool = true {
        didSet {
            if unread {
                detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
                detailTextLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)
                buttonTime.setTitleColor(UIColor.Text.blackMediumColor(), for: .normal)
                buttonTime.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.small..)!
                
            } else {
                detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
                detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
                buttonTime.setTitleColor(UIColor.Text.grayMediumColor(), for: .normal)
                buttonTime.titleLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.small..)!
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        imageView?.frame = CGRect(x: Size.padding10..,
                                  y: (contentView.frame.height - Size.avatar.. ) / 2,
                                  width: Size.avatar..,
                                  height: Size.avatar..)
        
        
        
        guard let imageView = imageView else { return }
        

        let timeWidth = Utility.widthForView(buttonTime.titleLabel?.text ?? "",
                                             font: UIFont(name: FontType.latoRegular.., size: FontSize.small..)!,
                                             height: 30) + 10
        
        
        textLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        textLabel?.frame.size.width = contentView.frame.width - timeWidth - Size.padding10.. * 2 - imageView.frame.maxX 
        
        detailTextLabel?.frame.origin.x = imageView.frame.maxX + Size.padding10..
        detailTextLabel?.frame.size.width = contentView.frame.width - Size.padding10.. - (imageView.frame.maxX + Size.padding10.. )
        
        guard let textLabel = textLabel else { return }
        
        buttonTime.frame = CGRect(x: contentView.frame.width - timeWidth - Size.padding10..,
                                  y: textLabel.frame.minY,
                                  width: timeWidth,
                                  height: textLabel.frame.height)
        
        seperator.frame = CGRect(x: imageView.frame.maxX + Size.padding10..,
                                 y: contentView.frame.height - onePixel(),
                                 width: contentView.frame.width - (imageView.frame.maxX + Size.padding10..) - Size.padding10..,
                                 height: onePixel())
        
        
        statusView.frame = CGRect(x: imageView.frame.maxX - Size.status..,
                                  y: imageView.frame.maxY - Size.status..,
                                  width: Size.status..,
                                  height: Size.status..)
        statusView.layer.cornerRadius = Size.status.. / 2
        
    }
}

extension MessageTableViewCell {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        setupLabel()
        setupImageView()
        seperator = setupSeperator()
        buttonTime = setupButton()
        
        
        contentView.addSubview(buttonTime)
        contentView.addSubview(seperator)
        
        statusView = setupStatusView()
        addSubview(statusView)
    }
    
    fileprivate func setupImageView() {
        imageView?.clipsToBounds = true
        imageView?.layer.cornerRadius = Size.avatar.. / 2
        //        imageView?.layer.borderColor = UIColor.Navigation.mainColor().CGColor
        //        imageView?.layer.borderWidth = onePixel()
        imageView?.contentMode = .scaleAspectFill
        
    }
    
    fileprivate func setupLabel() {
        
        textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        detailTextLabel?.textAlignment = .left
        detailTextLabel?.numberOfLines = 2
        detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.clipsToBounds = true
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.Text.grayMediumColor(), for: UIControl.State())
        button.titleLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.small..)
        button.contentHorizontalAlignment = .right
        button.isUserInteractionEnabled = false
        return button
    }
    
    func setupStatusView() -> UIView {
        let view = UIView()
        view.backgroundColor = .green
        view.clipsToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }
    
    fileprivate func setupSeperator() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
}

