//
//  TimetableTableViewCell.swift
//  UniOnline
//
//  Created by Pham Huy on 5/21/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class TimetableTableViewCell: SeperatorTableViewCell {
    
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, label = 22, button = 30, badge = 20
    }

    
    fileprivate var badgeLabel: UILabel!
    var indexLabel: UILabel!
    
    var badgeNumber: Int = 0 {
        didSet {
            
            guard badgeNumber > 0 else { badgeLabel.isHidden = true; return }
            badgeLabel.isHidden = false
            
            guard badgeNumber <= 9 else { badgeLabel.text = "9+"; return }
            badgeLabel.text = badgeNumber.toString(0)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: Size.padding10..,
                                   y: 0,
                                   width: bounds.width - Size.padding10.. * 2,
                                   height: bounds.height)
        
        indexLabel.frame = CGRect(x: Size.padding10.. ,
                                  y: (contentView.frame.height - Size.button..) / 2,
                                  width: Size.button..,
                                  height: Size.button..)
        indexLabel.layer.cornerRadius = indexLabel.frame.height / 2
        
        badgeLabel.frame = CGRect(x: contentView.frame.width - Size.padding15.. - Size.badge..,
                                  y: (contentView.frame.height - Size.badge.. ) / 2,
//                                  y: Size.padding10.. / 2,
                                  width: Size.badge.. ,
                                  height: Size.badge.. )
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height / 2
        
        
        if let text = detailTextLabel?.text, text.count > 0 {
            textLabel?.frame = CGRect(x:  indexLabel.frame.maxX + Size.padding10..,
                                      y: 2,
                                      width: contentView.frame.width - indexLabel.frame.maxX  - Size.padding10.. * 2 - (badgeLabel.isHidden ? 0 : Size.badge..),
                                      height: contentView.frame.height / 2 - 2)
            
            detailTextLabel?.frame = CGRect(x: indexLabel.frame.maxX  + Size.padding10..,
                                            y: contentView.frame.height / 2 ,
                                            width: contentView.frame.width  - indexLabel.frame.maxX - Size.padding10.. * 2 - (badgeLabel.isHidden ? 0 : Size.badge..),
                                            height: contentView.frame.height / 2 - 2)
        } else {
            
            textLabel?.frame = CGRect(x:  indexLabel.frame.maxX + Size.padding10..,
                                      y: 0,
                                      width: contentView.frame.width - indexLabel.frame.maxX  - Size.padding10.. * 2 - (badgeLabel.isHidden ? 0 : Size.badge..),
                                      height: contentView.frame.height )
            
            detailTextLabel?.frame = CGRect.zero
        }
        

        
    
    }
    
    func setup() {
        
        
        contentView.backgroundColor = UIColor.white
        backgroundColor = UIColor.clear

        
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
        
        badgeLabel = setupLabel("", textColor: .white,
                                font: UIFont(name: FontType.latoSemibold.., size: FontSize.small..)!,
                                bgColor: .red,
                                alignment: .center)
        
        
        indexLabel = setupLabel("",
                                textColor: UIColor.Navigation.mainColor(),
                                font: UIFont(name: FontType.latoBold.., size: FontSize.normal..)!,
                                bgColor: .clear,
                                alignment: .center)
        
        indexLabel.layer.borderWidth = onePixel()
        indexLabel.layer.borderColor = UIColor.Navigation.mainColor().cgColor
        
        badgeLabel.isHidden = true
        
        contentView.addSubview(badgeLabel)
        contentView.addSubview(indexLabel)

    }
    
    

    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        label.clipsToBounds = true
        return label
    }

    
}
