//
//  HotlineTableViewCell.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/25/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit


class HotlineTableViewCell: SeperatorTableViewCell {
    

    
    fileprivate enum Size:CGFloat {
        case padding10 = 10, padding5 = 5, padding15 = 15, button = 40, label = 22, avatar = 80
    }
    
    var labelName: UILabel!
    var labelSub: UILabel!
    var buttonCall: UIButton!
    var buttonEmail: UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        buttonEmail.frame = CGRect(x: contentView.frame.width - (buttonEmail.isHidden ? 0 : Size.button..),
                                  y: 0,
                                  width: buttonEmail.isHidden ? 0 : Size.button..,
                                  height: contentView.frame.height)
        
        
        buttonCall.frame = CGRect(x: buttonEmail.frame.minX - (buttonCall.isHidden ? 0 : Size.button..),
                                  y: 0,
                                  width: buttonCall.isHidden ? 0 : Size.button..,
                                  height: contentView.frame.height)
        
        if labelSub.isHidden {
            
            labelSub.frame = .zero
            
            labelName.frame = CGRect(x: Size.padding10..,
                                     y: 0,
                                     width: buttonCall.frame.minX -  Size.padding10.. ,
                                     height: contentView.frame.height)
            
            labelName.numberOfLines = 0
            labelName.lineBreakMode = .byWordWrapping
            
        } else {
            
            labelName.numberOfLines = 2
            labelName.lineBreakMode = .byTruncatingTail
            
            labelName.frame = CGRect(x: Size.padding10..,
                                     y: Size.padding10.. / 2,
                                     width: buttonCall.frame.minX -  Size.padding10.. ,
                                     height: CGFloat.leastNonzeroMagnitude)
            labelName.sizeToFit()
            
            
            labelSub.frame = CGRect(x: Size.padding10..,
                                    y: labelName.frame.maxY ,
                                    width: buttonCall.frame.minX -  Size.padding10.. ,
                                    height: contentView.frame.height - labelName.frame.maxY)
            
            

        }
    }
}


extension HotlineTableViewCell {
    fileprivate func setupView() {
        backgroundColor = UIColor.white
        

        labelName = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                               font: UIFont(name: FontType.latoMedium.., size: FontSize.normal--)!)
        

        
        labelSub = setupLabel(textColor: UIColor.Text.grayMediumColor(),
                              font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!)
        
        buttonCall = setupButton(image: Icon.Login.Phone.tint(UIColor.Navigation.mainColor()))

        buttonEmail = setupButton(image: Icon.Contact.DetailEmail.tint(UIColor.Navigation.mainColor()))

        contentView.addSubview(labelName)
        contentView.addSubview(labelSub)
        contentView.addSubview(buttonCall)
        contentView.addSubview(buttonEmail)
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }

   
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }

}




