//
//  RegisterViewController.swift
//  UniOnline
//
//  Created by Pham Huy on 5/14/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
//import PHExtensions
import UIKit
import SwiftyUserDefaults
import MessageUI
import SnapKit
import Firebase
import FirebaseStorage

import Alamofire

class RegisterViewController: CustomViewController {
    
    enum Row: Int {
        case accountType = 0, fullname,  phone, birthday, gender, address,  email, username, password, rePassword
        
        
        var image: UIImage {
            switch self {
            case .accountType:
                return Icon.Login.Personal
                
            case .fullname:
                return Icon.Login.Personal
                
            case .birthday:
                return Icon.General.Calendar
                
            case .gender:
                return Icon.Account.Gender
                
            case .address:
                return Icon.Contact.DetailAddress
                
            case .email:
                return Icon.Contact.DetailEmail
                
            case .phone:
                return Icon.Contact.DetailPhone
                
            case .username:
                return Icon.Login.Personal
                
            case .password:
                return Icon.Login.Key
                
            case .rePassword:
                return Icon.Login.Key
            }
        }
        
        func toInfo() -> String {
            switch self {
            case .accountType:
                return "Loại tài khoản"
                
            case .fullname:
                return "Họ tên"
                
            case .birthday:
                return  "Ngày sinh"
                
            case .gender:
                return "Giới tính"
                
            case .address:
                return "Địa chỉ"
                
            case .email:
                return "Email"
                
            case .phone:
                return "Số điện thoại"
                
            case .username:
                return "Tên đăng nhập"
                
            case .password:
                return "Mật khẩu"
                
            case .rePassword:
                return "Nhập lại mật khẩu"
            }
        }
    }
    
    var back: UIBarButtonItem!
    
    var datePickerView: DatePickerDialogView?
    var footerView: RegisterFooterView!
    
    var arrayRow: [Row] = [.accountType, .fullname, .phone, .birthday, .gender, .address, .email, .password, .rePassword]
    
    
    var userInfo: User =  User(data: [:])
    
    
    var ref: DatabaseReference!
    
    var username: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(willHiddenBar, animated: true)
    }
    
    //
    //    override var preferredStatusBarStyle : UIStatusBarStyle {
    //        return .lightContent
    //    }
}

extension RegisterViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch arrayRow[indexPath.row] {
        case .accountType:
            let cellIdentify = "RegisterTypeAccountTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! RegisterTypeAccountTableViewCell
            configuaAccountTypeCell(cell, indexPath: indexPath)
            return cell
            
        case .gender:
            let cellIdentify = "RegisterGenderTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! RegisterGenderTableViewCell
            configureGenderCell(cell, atIndexPath: indexPath)
            return cell
            
        default:
            let cellIdentify = "LoginTextFieldTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! LoginTextFieldTableViewCell
            configuaCell(cell, indexPath: indexPath)
            return cell
        }
        
        
    }
    
     func configureGenderCell(_ cell: RegisterGenderTableViewCell, atIndexPath indexPath: IndexPath) {
        
        cell.seperatorStyle = .padding(10)
        cell.seperatorRightPadding = 10
        cell.selectionStyle = .none
        cell.imageView?.image = arrayRow[indexPath.row].image.tint(UIColor.Text.grayMediumColor())
        cell.textLabel?.text = arrayRow[indexPath.row].toInfo()
      cell.textLabel?.textColor = UIColor.Text.grayMediumColor()
      cell.arrayButton.forEach { button in
          
        if let gender = userInfo.gender {
            button.isSelected = button.tag == gender..
        } else {
            button.isSelected = false
        }
          
          button.addTarget(self, action: #selector(self.changeGender(_:)), for: .touchUpInside)
      }
    }
    
    func configuaAccountTypeCell(_ cell: RegisterTypeAccountTableViewCell, indexPath: IndexPath) {
        
        cell.seperatorStyle = .padding(10)
        cell.seperatorRightPadding = 10
        
        cell.selectionStyle = .none
        cell.textLabel?.text = arrayRow[indexPath.row].toInfo()
        cell.textLabel?.textColor = UIColor.Text.grayMediumColor()
        cell.arrayButton.forEach { button in
            
            button.isSelected = button.tag == userInfo.accountType..
            button.addTarget(self, action: #selector(self.changeType(_:)), for: .touchUpInside)
        }
        
    }
    

    func configuaCell(_ cell: LoginTextFieldTableViewCell, indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        
        cell.textLabel?.text = arrayRow[indexPath.row].toInfo()
        cell.textLabel?.textColor = .white
        cell.imageView?.image = arrayRow[indexPath.row].image.tint(UIColor.Text.grayMediumColor())
        cell.textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!
        
        cell.textLabel?.isHidden = true
        cell.seperatorStyle = .padding(10)
        cell.seperatorRightPadding = 10
        
        cell.textField.delegate = self
        cell.textField.placeholder = arrayRow[indexPath.row].toInfo()
        cell.textField.tag = indexPath.row
        
        switch arrayRow[indexPath.row] {
        case .birthday:
            cell.textField.isEnabled = false
            cell.textField.text = Utility.getStringTime(userInfo.dob, formatter: "dd/MM/yyyy")
        default:
            break
        }
  
        if indexPath.row == arrayRow.count - 1 {
            cell.textField.returnKeyType = .done
        } else {
            cell.textField.returnKeyType = .next
        }
        
    }
}


extension RegisterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch arrayRow[indexPath.row] {
        case .accountType:
            return 44
            
        default:
            return 44
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch arrayRow[indexPath.row] {
        case .birthday:
            showDatePickerView(userInfo.dob)
        default:
            break
        }
    }
}

//-------------------------------------------
// MARK:   - SELECTOR
//-------------------------------------------

extension RegisterViewController {
    @objc func back(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonRegister(_ sender: UIButton) {
        
        view.endEditing(true)
        
        //TODO: Dịch
        
        guard userInfo.name.count > 0 else {
            HUD.showMessage("Bạn vui lòng nhập tên!")
            return
        }
        
        guard let _ = userInfo.gender else {
            HUD.showMessage("Bạn vui lòng chọn giới tính!")
            return
        }
        
        guard userInfo.email.count > 0 else {
            HUD.showMessage("Bạn vui lòng nhập email!")
            return
        }
        
        guard password.count >= 6 else {
            HUD.showMessage("Mật khẩu phải có ít nhất 6 ký tự!")
            return
        }
        
        guard password == confirmPassword else {
            HUD.showMessage("Mật khẩu không khớp nhau!")
            return
        }
        
        checkNetworkAndRequestCreateAccount(userInfo.email, password: password)
        
        
    }
    
    @objc func buttonCancel(_ sender: UIButton) {
        
    }
    
    @objc func changeType(_ sender: UIButton) {
        
        userInfo.accountType = AccountType(rawValue: sender.tag) ?? .patient
        
        guard let pos = arrayRow.firstIndex(of: .accountType) else { return }
        guard let cell = table.cellForRow(at: IndexPath(row: pos, section: 0)) as? RegisterTypeAccountTableViewCell else { return }
        
        cell.arrayButton.forEach { $0.isSelected = $0.tag == userInfo.accountType.. }
        
        
    }
    
    @objc func changeGender(_ sender: UIButton) {
        
        userInfo.gender = GenderType(rawValue: sender.tag)
        
        guard let pos = arrayRow.firstIndex(of: .gender) else { return }
        guard let cell = table.cellForRow(at: IndexPath(row: pos, section: 0)) as? RegisterGenderTableViewCell else { return }
        
        cell.arrayButton.forEach { $0.isSelected = $0.tag == (userInfo.gender ?? .male).. }
        
        
    }
    
}

//====================================
// MARK: - DATEPICKER DELEGATE
//====================================

extension RegisterViewController: DatePickerViewDelegate {


    func showDatePickerView(_ time: TimeInterval = Date().timeIntervalSince1970) {

        hiddenKeyboard()

        datePickerView = DatePickerDialogView()
        datePickerView!.delegate = self
        datePickerView?.datePicker.maximumDate = Date()
        datePickerView?.datePicker.minimumDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100.years)
        datePickerView?.labelHeader.text = LocalizedString("general_label_selection_date", comment: "Chọn ngày")
        datePickerView?.datePicker.setDate(Date(timeIntervalSince1970: time), animated: true)
        
        Utility.showView(datePickerView, onView: view)


        AppData.instance.gestureRecognizerShouldReceiveTouch = true
    }


    @objc func selectedDate(_ time: TimeInterval) {
 
        userInfo.dob = time
        datePickerView?.dismissView()

        AppData.instance.gestureRecognizerShouldReceiveTouch = false

        guard let pos = arrayRow.firstIndex(of: .birthday) else { return }
        guard let cell = table.cellForRow(at: IndexPath(row: pos, section: 0)) as? LoginTextFieldTableViewCell else { return }
        cell.textField.text = Utility.getStringTime(time, formatter: "dd/MM/yyyy")
    }
}


//-------------------------------------------
// MARK:   - REQUEST
//-------------------------------------------
extension RegisterViewController {
    
    /**
     Request Login
     */
    func checkNetworkAndRequestCreateAccount(_ email: String, password: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        HUD.showHUD(onView: self.view)
            
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                
                
                
                guard let authResult = authResult else {
                    
                    print(error.debugDescription)
                    self.requestFailed(error)
                    return
                }
                
                self.createAccountRequestSuccess(authResult, password: password)
                
            }
       
        
    }
    
    func createAccountRequestSuccess(_ respond: AuthDataResult, password: String) {
        
        
      
        
        let itemRef = ref.child("users").child(respond.user.uid)
        
        
        let userItem: Parameters = [
            "id": respond.user.uid,
            "full_name": userInfo.name,
            "avatar": userInfo.avatar,
            "account_type": userInfo.accountType..,
            "email": userInfo.email,
            "address": userInfo.address,
            "dob": userInfo.dob,
            "gender": userInfo.gender!..,
            "phone": userInfo.phone]
 
        itemRef.setValue(userItem)
        
        Defaults[.UserName] = userInfo.email
        Defaults[.Password] = password
        
        //TODO: Dịch
        HUD.showMessage("Đăng ký tài khoản thành công") {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    
    /**
     Request đăng nhập thất bại
     */
    func requestFailed(_ error: Error?) {
        
        guard let error = error else { HUD.dismissHUD(); return }
        
        switch error {
            
        default:
            HUD.showMessage(error.localizedDescription, onView: view, position: .center)
        }
        
        
    }
    
    
    
}



//-------------------------------------------
// MARK:   - TEXT FIELD DELEGATE
//-------------------------------------------
extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        scrollHeight = table.rectForRow(at: IndexPath(row: textField.tag, section: 0)).maxY - (view.frame.height - 5)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard textField.tag < arrayRow.count else { return }
        
        let string = textField.text ?? ""
        
        switch arrayRow[textField.tag] {
        case .fullname:
            userInfo.name = string
            
        case .address:
            userInfo.address = string
            
        case .phone:
            userInfo.phone = string
            
        case .email:
            userInfo.email = string
            
        case .password:
            password = string
            
        case .rePassword:
            confirmPassword = string
            
            
        default:
            break
        }
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let indexPathNext: IndexPath = IndexPath(row: textField.tag + 1, section: 0)
        
        
        guard let cell = table.cellForRow(at: indexPathNext) as? LoginTextFieldTableViewCell else {
            textField.resignFirstResponder()
            return false
            
        }
        cell.textField.becomeFirstResponder()
        return false
    }
    
}


extension RegisterViewController {
    /**
     Ẩn bàn phím
     */
    @objc func hiddenKeyboard() {
        view.endEditing(true)
    }
    
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        let scrollPoint = CGPoint(x: 0.0, y: scrollHeight + frame.height)
        
        if scrollPoint.y > 0 {
            table.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
    }
    
}


extension RegisterViewController {
    
    func setupAllSubviews() {
        
        
        title = "Đăng ký tài khoản"
        view.backgroundColor = .clear
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        navigationItem.leftBarButtonItem = back
        
        setupNotificationObserver()
        setupBackground()
        
        
        table = setupTable()
        footerView = setupFooterView()
        view.addSubview(table)
        
        table.tableFooterView = footerView
        
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = UIScreen.main.bounds
//        gradientLayer.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
//        view.layer.insertSublayer(gradientLayer, at: 0)
//
//
//        footerBackground.image =  (UIImage(named: "ImageCould") ?? UIImage()).tint(.white)
    }
    
    func setupAllConstraints() {
        
        footerBackground.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.height.equalTo(UIScreen.main.bounds.width / 4)
            make.bottom.equalTo(view).offset(0)
        }
        
        table.snp.makeConstraints { make in
            make.top.left.right.equalTo(view)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view).offset(-view.safeAreaInsets.bottom)
            } else {
                make.bottom.equalTo(view)
                // Fallback on earlier versions
            }
        }
    }
    
    /**
     setup Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    func setupTable() -> UITableView {
        
        let table: UITableView = UITableView(frame: CGRect.zero, style: .plain)
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.allowsSelection = true
        table.delegate = self
        table.dataSource = self
        table.register(LoginTextFieldTableViewCell.self, forCellReuseIdentifier: "LoginTextFieldTableViewCell")
        table.register(LoginButtonTableViewCell.self, forCellReuseIdentifier: "LoginButtonTableViewCell")
        table.register(RegisterTypeAccountTableViewCell.self, forCellReuseIdentifier: "RegisterTypeAccountTableViewCell")
        table.register(RegisterGenderTableViewCell.self, forCellReuseIdentifier: "RegisterGenderTableViewCell")
        table.keyboardDismissMode = .onDrag
        return table
        
    }
    
    func setupFooterView() -> RegisterFooterView {
        let footerView = RegisterFooterView()
        footerView.padding = 0
        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54)
        footerView.buttonRight.addTarget(self, action: #selector(self.buttonRegister(_:)), for: .touchUpInside)
        footerView.buttonLeft.addTarget(self, action: #selector(self.buttonCancel(_:)), for: .touchUpInside)
        
        return footerView
    }
}









