//
//  ConfirmViewController+Delegate.swift
//  OneTaxi
//
//  Created by Pham Huy on 8/13/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults



//-------------------------------------------
//MARK:    TABLE DATASOURCE
//-------------------------------------------
extension ConfirmViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textFieldArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TextFieldTableViewCell
        configureCell(cell, indexPath: indexPath)
        return cell
        
    }
    
    

    func configureCell(_ cell: TextFieldTableViewCell, indexPath: IndexPath) {
        
        // Setup ImageView
        cell.imageView?.image = textFieldArray[indexPath.row].image.tint(UIColor.lightGray)
        // Setup Text Field
        cell.textField.delegate = self
        cell.textField.placeholder = textFieldArray[indexPath.row].placeHolder
        cell.textField.keyboardType = .default
        cell.textField.isEnabled = true
        cell.textField.keyboardType = .default
        cell.selectionStyle = .none
        
        switch indexPath.row {
            
        case TextField.key..:
            textFieldKey = cell.textField
            
            
        default:
            break
        }
        
    }
}





//-------------------------------------------
// -MARK:    TABLE DELEGATE
//-------------------------------------------
extension ConfirmViewController: UITableViewDelegate {
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        hideKeyboard()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Size.cell..
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return setupFooterViewSection(LocalizedString("notificaiton_confirm_code_send_to_sms_and_email", comment: "Mã xác nhận đã được gửi tới bạn\n qua tin nhắn SMS và Email."))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return onePixel()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return  Utility.heightForView(LocalizedString("notificaiton_confirm_code_send_to_sms_and_email", comment: "Mã xác nhận đã được gửi tới bạn\n qua tin nhắn SMS và Email."), font: UIFont(name: FontType.latoItalic.., size: FontSize.small++)!, width: UIScreen.main.bounds.width - Size.padding15.. * 2) + Size.padding10.. * 2
    }
}

//-------------------------------------------
//MARK: - TextField Delegate
//-------------------------------------------
extension ConfirmViewController: UITextFieldDelegate {
    /**
     Chỉ cho phép nhập 4 kí tự ở textField mã kích hoạt
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return ((textField.text?.count)! + string.count > 6) ? false : true
    }
}
