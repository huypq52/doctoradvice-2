//
//  ConfirmViewController.swift
//  G5TaxiUser
//
//  Created by NangND on 9/21/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
//import PHExtensions
import SnapKit
import UIKit

class ConfirmViewController: CustomViewController {
    
    //-------------------------------------------
    // MARK: - ENUM
    //-------------------------------------------
    
    internal enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 40, cell = 50
    }
    
    /**
    Enum xác định các TextField
    */
    internal enum TextField: Int {
        case key = 0
    }
    
    //-------------------------------------------
    // MARK:  - NAVIBAR BUTTON
    //-------------------------------------------
    
    /**
    Button Back
    */
    internal lazy var back: UIBarButtonItem = {
        var button = UIBarButtonItem(image: Icon.Navigation.Delete, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(_:)))
        button.tintColor = UIColor.white
        return button
        }()
    
    //-------------------------------------------
    // MARK:  - STORYBOARD
    //-------------------------------------------
    
    var confirmView: UIView!
    var labelPhoneNumber: UILabel!
    var cancel: UIButton!
    var resend: UIButton!
    var confirm: UIButton!
    var confirmButtonBottomConstraints: Constraint!

    //-------------------------------------------
    //MARK: - PROPERTIES
    //-------------------------------------------
    
    /**
    Text Field số điện thoại
    */
    var textFieldPhone: UITextField!
    
    /**
    Text Field Tên
    */
    var textFieldKey: UITextField!
    
    /**
    Timer đếm ngược thời gian để gửi lại mã xác nhận
    */
    internal var resendActivationCodeTimer: Timer?
    
    /**
    Title, ảnh của từng cell
    */
    
    internal var textFieldArray: [(image:UIImage,placeHolder: String)] = {
        return [(Icon.Login.Key, LocalizedString("login_confirm_input_confirm_code", comment: "Nhập mã xác nhận"))]
        }()
    
   
    
    /**
     Format thời gian
     */
    internal var dateTimeFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm - dd/MM/yyyy"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()
    
    /**
     12/05/2016: Để Ảnh nền cho đăng ký
     */
    var backgroundView: UIImageView!
    
    
    //-------------------------------------------
    // MARK:  - LIFE CYCLE
    //-------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**
        Setup Default
        */
        title = LocalizedString("login_confirm_title", comment: "Mã xác nhận")
        navigationItem.leftBarButtonItem = back
        view.backgroundColor = UIColor.white
        
        setupAllSubview()

        setupNotificationObserver()
        
        
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard textFieldKey != nil else { return }
        
        textFieldKey.becomeFirstResponder()
    }

}





//-------------------------------------------
//MARK: AccountVerify Delegate
//-------------------------------------------
extension ConfirmViewController {
    
    /**
    Lỗi không nhận được respond từ server
    
    - parameter error:
    */
    func verifyRequestFailed(_ error: Error) {
        HUD.showMessage(LocalizedString("notification_not_connected_server_please_again",
            comment: "Không kết nối được tới máy chủ, bạn hãy thử lại."),
            type: .none,
            onView: view,
            position: .bottomCenter)
    }
    
    /**
    Nhận được respond từ server
    
    - parameter respondCode:
    - parameter email:
    - parameter name:
    */
//    func verifyRequestSucceed(_ response: HTTPVerify.ResponseType) {
//        switch response.status {
//        case .error:
//            HUD.showMessage(LocalizedString("notification_input_code_active_false_please_input_again",
//                comment: "Bạn vừa nhập sai mã xác nhận, xin vui lòng nhập lại."),
//                type: .none,
//                onView: view,
//                position: .bottomCenter,
//                disableUserInteraction: true)
//
//        case .codeTimeout:
//            HUD.showMessage(LocalizedString("notification_code_active_time_over_please_get_code_new",
//                comment: "Mã kích hoạt đã quá thời gian, bạn hãy lấy mã kích hoạt mới."),
//                type: .none,
//                onView: view,
//                position: .bottomCenter,
//                disableUserInteraction: true)
//
//        case .exist:
//
//            if let email = response.email, email.count > 0 {
//                Defaults [.Email] = email
//            }
//
//            if let name = response.name, name.count > 0 {
//                Defaults [.Username] = name
//            }
//            fallthrough
//
//        case .new:
//
//
////            backToMainController()
////            Defaults[.Registered]   = true
////            Defaults[.PhoneNumber]  = response.phone
////            Defaults[.Password]     = response.password
//            
//            /// @author NangND, 17-01-04
//            /// Khi đăng kí thành công thì vào màn hình home luôn k phải chờ
//            ///
//            /*
//            HUD.showMessage(LocalizedString("notification_register_account_suucces",
//                comment: "Đăng ký tài khoản thành công."),
//                type: .none,
//                onView: view,
//                position: .bottomCenter,
//                disableUserInteraction: true) {
//                    self.backToMainController()
//                    Defaults[.Registered]   = true
//                    Defaults[.PhoneNumber]  = response.phone
//                    Defaults[.Password]     = response.password
//            }
//             */
//        }
//    }
}

//-------------------------------------------
//MARK: AccountRegister Delegate
//-------------------------------------------
extension ConfirmViewController {
    /**
    Nhận respond từ server khi người dùng bấm gửi lại mã kích hoạt
    
    - parameter respondCode:
    - parameter message:
    */
//    func registerRequestSucceed(_ response: HTTPResetPassword.RequestType) {
    
//        /**
//        Update lại thời gian cho nút gửi lại mã kích hoạt
//        */
//        Defaults[.RegisterTime] = Date().timeIntervalSince1970
//        updateResendCodeButton()
//
//        /**
//         Thời gian hiển thị thông báo
//         */
//        var durationTime = 3.seconds
//
//        /**
//        Tùy vào respond.code để xác đinh thông báo cần hiển thị và loại thông báo mặc định là Error
//        */
//        var displayMessage: String
//
//        switch response.code {
//        case .normal:
//            displayMessage = LocalizedString("notification_resend_code_active_success", comment: "Mã kích hoạt mới đang được gửi lại cho bạn")
//
//        case .justSend:
//            displayMessage = LocalizedString("notification_sms_send_to_you", comment: "Tin nhắn vừa được gửi cho bạn, hãy sử dụng mã kích hoạt này")
//
//
//        case .temporaryLock:
//            durationTime = 5.seconds
//            switch response.banType {
//            case .forever:
//                displayMessage = LocalizedString("notification_phone_number_temp_lock", comment: "Số điện thoại mà bạn đăng ký đang bị khóa, bạn vui lòng liên hệ trực tiếp với Thành Đông Taxi để đăng ký lại.")
//
//            case .expiredAt(let time):
//                displayMessage = LocalizedString("notification_phone_number_temp_lock_expired_part_1", comment: "Số điện thoại mà bạn đăng ký đang bị khóa, bạn vui lòng đăng ký lại sau ") + dateTimeFormatter.string(from: Date(timeIntervalSince1970: time)) +  "."
//            }
//
//
//        case .permanentLock:
//            durationTime = 5.seconds
//            switch response.banType {
//            case .forever:
//                displayMessage = LocalizedString("notification_phone_number_locked", comment: "Số điện thoại mà bạn đăng ký đang bị khóa, bạn vui lòng liên hệ trực tiếp với Thành Công Taxi để đăng ký lại.")
//
//            case .expiredAt(let time):
//                displayMessage = LocalizedString("notification_phone_number_temp_lock_expired_part_1", comment: "Số điện thoại mà bạn đăng ký đang bị khóa, bạn vui lòng đăng ký lại sau ") + dateTimeFormatter.string(from: Date(timeIntervalSince1970: time)) +  "."
//            }
//
//        case .wrongInfo:
//            displayMessage = LocalizedString("notification_input_infomation_false", comment: "Nhập sai thông tin")
//
//        case .numberTooManyAttempt:
//             durationTime = 5.seconds
//            displayMessage = LocalizedString("notification_phone_number_send_several_times_a_day", comment:  "Số điện thoại của bạn đã gửi đăng ký quá nhiều lần trong 24h qua, vui lòng đăng ký lại sau 24h tới.")
//
//        case .phoneTooManyAttempt:
//            displayMessage = LocalizedString("notification_phone_send_several_times_a_day", comment: "Điện thoại của bạn đã gửi nhiều lần trong ngày.")
//
//        case .abNormal:
//            if let message = response.message {
//                displayMessage = message
//            } else {
//                displayMessage = LocalizedString("notification_has_error_please_try_again", comment: "Có lỗi xảy ra, vui lòng thử lại sau")
//            }
//
//        case .ruleViolence:
//            durationTime = 5.seconds
//            switch response.banType {
//            case .forever:
//                displayMessage = LocalizedString("notification_booking_via_http_banned_no_time", comment: "Tài khoản của bạn đã bị khóa do vi phạm quy định, vui lòng liên hệ trực tiếp với Thành Đông Taxi.")
//
//            case .expiredAt(let time):
//                let dateFormater = DateFormatter()
//                dateFormater.dateFormat = "HH:mm - dd/MM/yyyy"
//                dateFormater.timeZone = TimeZone(secondsFromGMT: 0)
//                displayMessage = LocalizedString("notification_booking_via_http_banned_time", comment: "Tài khoản của bạn đã bị khóa do vi phạm quy định, vui lòng thử lại sau ") + dateFormater.string(from: Date(timeIntervalSince1970: time))
//            }
//
//        case .invalidReference:
//            displayMessage =  LocalizedString("notification_refercence_code_invalidate_please_try_again", comment: "Mã giới thiệu của bạn không đúng. Xin vui lòng kiểm tra lại.")
//
//        }
//
//        if response.reference.count > 0 {
//            Defaults[.ReferenceCode] = response.reference
//        }
//
//        /**
//        Show thông báo
//        */
//        HUD.showMessage(displayMessage,
//            type: .none,
//            onView: view,
//            duration: durationTime,
//            position: .bottomCenter,
//            disableUserInteraction: true)
//    }
    
    
    /**
    Không nhận được respond từ server
    
    - parameter error:
    */
    func registerRequestFailed(_ error: Error) {
        HUD.showMessage(LocalizedString("notification_not_connected_server_please_again",
            comment: "Không kết nối được tới máy chủ, bạn hãy thử lại."),
            type: .none,
            onView: view,
            position: .bottomCenter,
            disableUserInteraction: true)
    }
}



