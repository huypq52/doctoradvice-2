//
//  ConfirmViewController+Request.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 12/1/17.
//  Copyright © 2017 OMT. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
//import PHExtensions
import SnapKit
import UIKit

extension ConfirmViewController {
    
    // MARK: - REQUEST LOGIN
    
    /**
     Request Login
     */
    func checkNetworkAndRequestConfirmCode(_ code: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: view,
                            position: .center)
            return
        }


        HUD.showHUD(onView: view) {
            HTTPManager.instance.confirmCode
                .doRequest(HTTPConfirmCode.RequestType(code: code))
                .completionHandler { result in
                    
                    switch result {
                    case .success(let respond):
                        self.confirmCodeRequestSuccess(respond)
                        
                    case .failure(let error):
                        self.requestFailed(error)
                        
                    }
            }
            
        }
        
    }
    
    
    /**
     Request đăng nhập thành công
     */
    func confirmCodeRequestSuccess(_ respond: HTTPConfirmCode.RespondType) {



        let newVC: NewPasswordViewController = NewPasswordViewController()
        newVC.idUser = respond.id
        newVC.token = respond.token
        navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    /**
     Request Login
     */
    func checkNetworkAndRequestResetPassword(_ email: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        HUD.showHUD(onView: view) {
            HTTPManager.instance.forgotPassword
                .doRequest(HTTPForgotPassword.RequestType(username: email))
                .completionHandler { result in

                    switch result {
                    case .success(let respond):
                        self.resetPasswordRequestSuccess(respond)

                    case .failure(let error):
                        self.requestFailed(error)
                    }
            }
        }
    }
    
    
    /**
     Request đăng nhập thành công
     */
    func resetPasswordRequestSuccess(_ respond: HTTPForgotPassword.RespondType) {


        HUD.showMessage(LocalizedString("notification_login_a_have_confirm_code_send_to_you", comment: "Một mã xác nhận đã được gửi tới số điện thoại và email của bạn, vui lòng sử dụng mã xác nhận này để lấy lại mật khẩu!"), onView: view, position: .center) {
            Defaults[.RegisterTime] = Date().timeIntervalSince1970
            self.updateResendCodeButton()
        }
        
    }
    
    
    /**
     Request đăng nhập thất bại
     */
    func requestFailed(_ error: Swift.Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
    
    
}
