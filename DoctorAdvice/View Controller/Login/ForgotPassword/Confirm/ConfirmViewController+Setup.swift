//
//  ConfirmViewController+Setup.swift
//  OneTaxi
//
//  Created by Pham Huy on 8/13/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import SnapKit


//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension ConfirmViewController {
    func setupAllSubview() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
        view.backgroundColor = UIColor.white
        
        setupImageBackground()
        
        setupConfirmView()
        
        /**
         Setup Label Phone Number
         */
        
        setupLabelPhoneNumber()
        
        /**
         Setup table view
         */
        setupTableView()
        

        /**
         Setup các button
         */
        
        
        confirm = setupButton(LocalizedString("alert_title_confirm", comment: "Xác nhận").uppercased(),
                              titleColor: .white,
                              selector: #selector(self.confirmCode(_:)))
        confirm.backgroundColor = UIColor.Navigation.mainColor()
        
  
        resend = setupButton(LocalizedString("login_confirm_send_confirm_code", comment: "Gửi lại mã xác nhận").uppercased(), titleColor: UIColor.Navigation.subColor(), selector: #selector(self.resendCode(_:)))
     

        view.addSubview(resend)
        view.addSubview(confirm)
        
        
        /**
         Setup thời gian đếm ngược cho button resend
         */
        if !Utility.isVerifyCodeResendable() {
            let time = Utility.stringFromTimeInterval(Utility.verifyCodeResendableTimeLeft(), format:[ .AbsoluteTime, .Minute, .Second])
            resend.setTitle(LocalizedString("login_confirm_active_afer_time", comment: "Kích hoạt lại sau").uppercased() + " " + time, for: UIControl.State())
            updateResendCodeButton()
        }
        
    }
    
    internal func setupAllConstraints() {
        

            confirmView.snp.makeConstraints { make in
                make.top.equalTo(view).offset(10)
                make.leading.equalTo(view)
                make.trailing.equalTo(view)
                make.height.equalTo(170)
            }
            


        confirm.snp.makeConstraints { make in
            make.leading.equalTo(view).offset(10)
            make.height.equalTo(44)
            make.trailing.equalTo(view).offset(-10)
            confirmButtonBottomConstraints = make.bottom.equalTo(resend.snp.top).offset(-7).constraint
        }
        
     
            resend.snp.makeConstraints { make in
                make.leading.equalTo(view).offset(10)
                make.trailing.equalTo(view).offset(-10)
                make.height.equalTo(44)
                make.bottom.equalTo(view).offset(-10)
            }

        labelPhoneNumber.snp.makeConstraints { make in
            make.top.equalTo(confirmView)
            make.leading.trailing.equalTo(confirmView)
            make.height.equalTo(60)
        }
        
        table.snp.makeConstraints { make in
            make.top.equalTo(labelPhoneNumber.snp.bottom)
            make.leading.trailing.equalTo(confirmView)
            make.height.equalTo(100)
        }


    }
    
    
    fileprivate func setupImageBackground() {
        backgroundView = UIImageView()
        backgroundView.image = UIImage(named: "BackgroundIntro")
        backgroundView.contentMode = .scaleAspectFill
        view.addSubview(backgroundView)
    }
    
    
    fileprivate func setupConfirmView() {
        
        confirmView = UIView()
        confirmView.backgroundColor = UIColor.white.alpha(0.9)
//        confirmView.layer.cornerRadius = 5
//        confirmView.layer.borderColor = UIColor.Misc.seperator.cgColor
//        confirmView.layer.borderWidth = onePixel()
        view.addSubview(confirmView)
    }
    
    /**
     Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /**
     Label
     */
    
    fileprivate func setupLabelPhoneNumber() {
        
        labelPhoneNumber = UILabel()
        let phoneNumber = Defaults[.UserName]
        if phoneNumber.count > 0 {
            labelPhoneNumber.text = phoneNumber
        }
        labelPhoneNumber.textAlignment = .center
        labelPhoneNumber.textColor = UIColor.Text.blackNormalColor()
        labelPhoneNumber.font = UIFont(name: FontType.latoLight.., size: 30)
        confirmView.addSubview(labelPhoneNumber)
    }
    
    /**
     Table
     */
    fileprivate func setupTableView() {
        
        table = UITableView(frame: CGRect.zero, style: .grouped)
        table.isScrollEnabled = false
        table.separatorStyle = .none
        table.keyboardDismissMode = .onDrag
        table.backgroundColor = UIColor.clear
        table.delegate = self
        table.dataSource = self
        table.register(TextFieldTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.hideKeyboard)))
        table.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard)))
        table.addGestureRecognizer(UISwipeGestureRecognizer(target: self, action: #selector(self.hideKeyboard)))
        confirmView.addSubview(table)

    }
    
    /**
     Button
     */
    fileprivate func setupButton(_ title: String,
                                 titleColor: UIColor = UIColor.Navigation.mainColor(),
                                 bgColor: UIColor = .white,
                                 disableColor: UIColor? = nil, selector: Selector) -> UIButton {
        
        let button = UIButton()
        button.setTitle(title.uppercased(), for: UIControl.State())
        button.backgroundColor = bgColor
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.layer.borderWidth = onePixel()
        button.layer.borderColor = titleColor.cgColor
        button.setTitleColor(titleColor, for: UIControl.State())
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = 44 / 2
        
        if let _ = disableColor {
            button.setTitleColor(UIColor.lightGray, for: .disabled)
        }
        
        return button
    }
    
    
    internal func setupFooterViewSection(_ title: String) -> UIView {
        
        let sizeHeight =  Utility.heightForView(title, font: UIFont(name: FontType.latoItalic.., size: FontSize.normal--)!, width: confirmView.frame.width - Size.padding15.. * 2) + Size.padding10.. * 2
        
        let label = UILabel(frame: CGRect(x: Size.padding15..,
            y: 0,
            width: confirmView.frame.width - Size.padding15.. * 2,
            height: sizeHeight))
        
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        label.textAlignment = .center
        label.textColor = UIColor.Text.blackMediumColor()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = title
        
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        footerView.addSubview(label)
        return footerView
    }
    
}


