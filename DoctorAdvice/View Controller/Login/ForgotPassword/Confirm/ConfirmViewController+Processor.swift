//
//  ConfirmViewController+Processor.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 12/1/17.
//  Copyright © 2017 OMT. All rights reserved.
//
import Foundation
import SwiftyUserDefaults
//import PHExtensions
import SnapKit
import UIKit
//import CleanroomLogger

//-----------------------------
//-  MARK: SELECTOR
//-----------------------------
extension ConfirmViewController {
    
    /**
     Nút Back
     */
    @objc func back(_ sender: UIBarButtonItem) {
        
        hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    /**
     Xử lý sự kiện nút xác nhận
     
     - parameter sender:
     */
    
    @objc func confirmCode(_ sender: UIButton) {
        
        /**
         *  Kiểm tra định dạng mã kích hoạt
         */
        guard let code = textFieldKey.text, code.count == 6
            else {
                
                print( "Mã xác nhận chưa đúng.")
                HUD.showMessage(LocalizedString("notification_code_active_false",
                                                comment: "Mã xác nhận chưa đúng."),
                                type: .none,
                                onView: view,
                                position: .bottomCenter) {
                                    self.textFieldKey.becomeFirstResponder()
                }
                return
        }
        
        
        /**
         *  Gửi lệnh verify
         */
        
        //        guard let   phone = EncryptionSupport.instance.get(.PhoneNumber),
        //                    let password = EncryptionSupport.instance.get(.GeneratedPassword),
        //                    let uuid = EncryptionSupport.instance.get(.UUID),
        //                    let country = EncryptionSupport.instance.get(.CountryCode)
        //            else {
        //                HUD.showMessage(LocalizedString("notification_has_error_you_please_again", comment: "Có lỗi xảy ra. Bạn vui lòng thử lại."))
        //                return
        //        }
        
        checkNetworkAndRequestConfirmCode(code)
        
        
    }
    
    /**
     Xử lý sự kiện nút gửi lại mã đăng ký
     
     - parameter sender:
     */
    
    @objc func resendCode(_ sender: UIButton) {
        
        checkNetworkAndRequestResetPassword(Defaults[.UsernameForgot])
    }
    
    /**
     Xử lý sự kiện nút hủy xác nhận
     
     - parameter sender:
     */
    
    @objc func cancel(_ sender: UIButton) {
        /**
         Đẩy sang booking
         */
        backToMainController()
    }
    
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Notification) {
        
        /**
         Khi cancelButtonBottomConstraints.constant = 10 (khi bàn phím đang ẩn) thì mới thực hiện animation đẩy các Button lên trên bàn phím
         */
        guard confirmButtonBottomConstraints != nil else { return }
        guard let userInfo = sender.userInfo else { return }
        let height = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size.height
        
        
        let offset = confirm.frame.minY - confirmView.frame.maxY - 10
        
        confirmButtonBottomConstraints.update(offset: -(min(height - 54, offset) + 10))
        UIView.animate(withDuration: 1.second, animations: { self.view.layoutSubviews() })
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Notification) {
        
        
        guard confirmButtonBottomConstraints != nil else { return }
        
        confirmButtonBottomConstraints.update(offset: -10)
        UIView.animate(withDuration: 1.second, animations: { self.view.layoutSubviews() })
    }

}

//-------------------------------------------
//MARK: - PRIVATE METHOD
//-------------------------------------------

extension ConfirmViewController {
    /**
     Ẩn Bàn phím
     */
    @objc func hideKeyboard() {
        for textField in [textFieldPhone, textFieldKey] {
            if let textField = textField {
                textField.resignFirstResponder()
            }
        }
    }
    
    /**
     Đẩy sang booking controller
     */
    fileprivate func backToMainController() {
        
        
        //        sideMenuViewController?.panGestureEnabled = true
        //
        //        guard let navigation = navigationController else { return }
        //
        //        if  navigation.containController(BookingViewController.self) ||
        //            navigation.containController(VRBookingViewController.self) {
        //
        //            changeSelectedMenuItemToHome()
        //            navigation.popToRootViewController(animated: true)
        //
        //            return
        //        }
        //
        //
        //        guard let navigationController = navigationController else { return }
        //        let bookingVC = BookingViewController()
        //
        //        navigationController.setNavigationBarHidden(false, animated: false)
        //
        //        UIView.transition(
        //            with: navigationController.view,
        //            duration: 0.6.second,
        //            options:  .transitionFlipFromLeft,
        //            animations: {
        //                navigationController.pushViewController(bookingVC, animated: false)
        //            },
        //            completion: { _ in
        //
        //                self.changeSelectedMenuItemToHome()
        //                navigationController.removeController(
        //                    RegisterViewController.self,
        //                    ConfirmViewController.self
        //                )
        //            }
        //        )
        
    }
    
    /**
     Cập nhật lại nút gửi lại mã kích hoạt khi chưa hết thời gian 5'
     */
    internal func updateResendCodeButton() {
        resend.isEnabled = false
        
        
        resend.setTitleColor(UIColor.lightGray, for: UIControl.State())
        
        
        /// Cứ mỗi 1s lại check lại xem đã đủ 5p để cho phép gửi lại mã chưa, chưa đủ thì set lại thời gian còn lại
        resendActivationCodeTimer = Timer.every(1.0.second) { [weak self] _, timer in
            guard let `self` = self else { return }
            let timeLeft = Utility.verifyCodeResendableTimeLeft()
            
            if  timeLeft > 0 {
                UIView.performWithoutAnimation({ () -> Void in
                    let time = Utility.stringFromTimeInterval(timeLeft, format: [.AbsoluteTime, .Minute, .Second])
                    self.resend.setTitle(LocalizedString("login_confirm_active_afer_time", comment: "Kích hoạt lại sau").uppercased() + ": " + time,
                                         for: .disabled)
                    self.resend.layoutIfNeeded()
                })
            }
            
            if timeLeft == 0 {
                self.resend.isEnabled = true
                self.resend.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
                
                self.resend.setTitle(LocalizedString("login_confirm_send_confirm_code", comment: "Gửi lại mã xác nhận").uppercased(), for: .normal)
                
                timer?.invalidate()
                
            }
        }
    }
    
}

