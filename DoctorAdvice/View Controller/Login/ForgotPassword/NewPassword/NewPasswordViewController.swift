//
//  NewPasswordViewController.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 12/1/17.
//  Copyright © 2017 OMT. All rights reserved.
//



import UIKit
//import PHExtensions
import SwiftyUserDefaults
// import Async
import SnapKit



enum NewPasswordType: Int {
    case firstLogin = 0, reset
}


protocol NewPasswordViewControllerDelegate: class {
    
    func updatePasswordSuccess(_ isLogin: Bool)
}

class NewPasswordViewController: CustomViewController {
    
    
    
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, headerSection = 20
    }
    
    let arrayTitle: [String] = [LocalizedString("change_password_new_password", comment: "Mật khẩu mới"),
                                LocalizedString("change_password_reinput_new_password", comment: "Nhập lại mật khẩu mới")]
    
    
    var footerView: ChangedPasswordFooterView!
    var idUser: Int!
    var token: String = ""
    
    var textFieldNewPassword = UITextField()
    var textFieldReNewPassword = UITextField()
    weak var delegate: NewPasswordViewControllerDelegate?
    
    //-------------------------------------------
    // MARK: - NAVIBAR BUTTON
    //-------------------------------------------
    
    /**
     Nút BACK
     */
    fileprivate lazy var back: UIBarButtonItem = {
        var button = UIBarButtonItem(image: Icon.Navigation.Delete, style: .plain, target: self, action: #selector(self.back(_:)))
        button.tintColor = UIColor.Navigation.tintColor()
        return button
    }()
    
    var oldPassword: String = ""
    var type: NewPasswordType = .reset
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAllSubviews()
        
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            
            setupAllConstraints()
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
//        switch type {
//        case .firstLogin:
//            navigationController?.setNavigationBarHidden(true, animated: true)
//
//        default:
//            break
//        }
        
        
    }
    
}

extension NewPasswordViewController {
    
    /**
     Lấy thông tin chiều cao của bàn phím khi hiện lên
     Để sau này tính offset cho các màn hình bé
     
     - parameter sender:
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        
        let scrollPoint = CGPoint(x: 0.0, y: scrollHeight + frame.height)
        
        if scrollPoint.y > 0 {
            table.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        table.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
    @objc func back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func changedPassword(_ sender: UIButton) {
        
        
        guard textFieldNewPassword.text == textFieldReNewPassword.text else {
            HUD.showMessage(LocalizedString("notification_change_password_new_message_other", comment: "Mật khẩu mới khác nhau"), onView: self.view)
            return
        }
        
        guard let newPassword = textFieldNewPassword.text, newPassword.count > 5 else {
            HUD.showMessage(LocalizedString("notification_change_password_new_message_short", comment: "Mật khẩu mới quá ngắn, vui lòng nhập lại mật khẩu khác."), onView: self.view)
            return
        }
        
        guard newPassword.count <= 50 else {
            HUD.showMessage(LocalizedString("notification_change_password_new_message_long", comment: "Mật khẩu mới quá dài, vui lòng nhập lại mật khẩu khác."), onView: self.view)
            return
        }
        
        switch type {
        case .firstLogin:
            checkNetworkAndRequestChangedPassword(oldPassword, newPassword: newPassword)
        default:
            checkNetworkAndRequestChangedNewPassword(newPassword)
        }
        
        
    }
    
    //-------------------------------------------
    // MARK: - PRIVATE METHOD
    //-------------------------------------------
    
    /**
     Ẩn bàn phím
     */
    func hideKeyboard() {
   
        textFieldNewPassword.resignFirstResponder()
        textFieldReNewPassword.resignFirstResponder()
    }
}

extension NewPasswordViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentify = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! TextFieldTableViewCell
        configuaCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configuaCell(_ cell: TextFieldTableViewCell, indexPath: IndexPath) {
        cell.seperatorStyle = .padding(15)
        cell.seperatorRightPadding = 15
        cell.textField.isSecureTextEntry = true
        cell.textField.tag = indexPath.section
        cell.textField.delegate = self
        
        switch indexPath.section {
        case 0:
            cell.textField.returnKeyType = .next
            cell.textField.placeholder = LocalizedString("change_password_input_password_new", comment: "Nhập mật khẩu mới")
            textFieldNewPassword = cell.textField
            
            
        case 1:
            cell.textField.returnKeyType = .done
            cell.textField.placeholder = LocalizedString("change_password_reinput_new_password", comment: "Nhập lại mật khẩu mới")
            textFieldReNewPassword = cell.textField
            
            
            
        default:
            break
        }
        
    }
}

extension NewPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 0 {
            textFieldReNewPassword.becomeFirstResponder()
        }
        else {
            textFieldReNewPassword.resignFirstResponder()
        }
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        scrollHeight = table.rectForRow(at: IndexPath(row: 0, section: textField.tag)).maxY - (view.frame.height - 5)
        return true
    }
}


extension NewPasswordViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return setupHeaderViewSection(arrayTitle[section])
    }
}


// MARK: - REQUEST CHANGED PASSWORD
extension NewPasswordViewController {
    /**
     Request Login
     */
    fileprivate func checkNetworkAndRequestChangedNewPassword(_ password: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: view,
                            position: .center)
            return
        }
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        
        HUD.showHUD(onView: view) {
            HTTPManager.instance.changeNewPassword
                .doRequest(HTTPChangeNewPassword.RequestType(id: self.idUser, password: password, token: self.token), headers: headers)

                .completionHandler { result in

                    switch result {
                    case .success(let respond):
                        self.changedNewPasswordSuccess(respond)

                    case .failure(let error):
                        self.changedPasswordFailed(error)
                    }
            }
        }
    }
    
    
    /**
     Request đăng nhập thành công
     */
    func changedNewPasswordSuccess(_ respond: HTTPChangeNewPassword.RespondType) {

        
       
        
        Defaults[.Password] = textFieldNewPassword.text ?? ""
        delegate?.updatePasswordSuccess(false)
        HUD.showMessage(LocalizedString("notification_login_new_password_update_success", comment: "Cập nhật mật khẩu thành công"), onView: view, position: .center) {
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    /**
     Request Login
     */
    func checkNetworkAndRequestChangedPassword(_ oldPassword: String, newPassword: String) {
        
        
        guard let info = AppData.instance.myInfo else { return }
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        
        HUD.showHUD(onView: self.view) {
            HTTPManager.instance.changePassword
                .doRequest(HTTPChangedPassword.RequestType(userID: info.id,
                                                           oldPassword: oldPassword,
                                                           newPassword: newPassword), headers: headers)
                
                .completionHandler { result in
                    
                    switch result {
                    case .success(let respond):
                        self.changedPasswordSuccess(respond)
                        
                    case .failure(let error):
                        self.changedPasswordFailed(error)
                    }
            }
        }
    }
    
    
    /**
     Request đăng nhập thành công
     */
    func changedPasswordSuccess(_ respond: HTTPChangedPassword.RespondType) {
        
       
        Defaults[.Password] = textFieldNewPassword.text ?? ""
        delegate?.updatePasswordSuccess(true)
        HUD.showMessage(LocalizedString("notification_login_new_password_update_success", comment: "Cập nhật mật khẩu thành công"), onView: view, position: .center) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    

    
    func changedPasswordFailed(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
}


extension NewPasswordViewController {
    func setupAllSubviews() {
        
        
        
        navigationItem.leftBarButtonItem = back
        view.backgroundColor = UIColor.white
        
        title = LocalizedString("login_new_password_title", comment: "Cập nhật mật khẩu")
        
        setupTable()
        setupFooter()
        setupNotificationObserver()
        
        view.addSubview(table)
        view.addSubview(footerView)
        
    }
    
    func setupAllConstraints() {
        footerView.snp.makeConstraints { make in
            
            make.trailing.left.bottom.equalTo(view)
            make.height.equalTo(54)
        }
        
        table.snp.makeConstraints { make in
            make.trailing.leading.top.equalTo(view)
            make.bottom.equalTo(footerView.snp.top)
        }
    }
    
    fileprivate func setupNotificationObserver() {
                NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupTable() {
        
        table = UITableView(frame: CGRect.zero, style: .grouped)
        table.backgroundColor = UIColor.white
        table.separatorStyle = .none
        table.allowsSelection = false
        table.backgroundColor = UIColor.white
        table.register(TextFieldTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.keyboardDismissMode = .onDrag
        table.delegate = self
        table.dataSource = self
    }
    
    func setupFooter() {
        
        footerView = ChangedPasswordFooterView()
        footerView.buttonLeft.addTarget(self, action: #selector(self.back(_:)), for: .touchUpInside)
        footerView.buttonLeft.isHidden = true
        footerView.buttonRight.addTarget(self, action: #selector(self.changedPassword(_:)), for: .touchUpInside)
        footerView.layoutSubviews()
    }
    
    fileprivate func setupHeaderViewSection(_ title: String) -> UIView {
        let label = UILabel(frame: CGRect(x: Size.padding15..,
                                          y: 25,
                                          width: UIScreen.main.bounds.width - Size.padding15.. * 2,
                                          height: Size.headerSection..))
        label.font = UIFont(name: FontType.latoBold.., size: FontSize.small++)
        label.textAlignment = .left
        label.textColor = UIColor.Text.blackMediumColor()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = title
        
        let seperator = UIView()
        seperator.backgroundColor = UIColor.Misc.seperatorColor()
        seperator.frame = CGRect(x: 0, y: label.frame.maxY , width: UIScreen.main.bounds.width, height: onePixel())
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(label)
        //        headerView.addSubview(seperator)
        return headerView
    }
}

