//
//  LoginViewController+Request.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/3/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Async
import Alamofire
//import RealmSwift
import SnapKit
import Crashlytics
import FlatUIKit
//import CleanroomLogger

import Firebase


// MARK: - REQUEST LOGIN

extension LoginViewController {
    
    
    /**
     Request Login
     */
    func checkNetworkAndRequestLogin(_ userName: String, password: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        Crashlytics.sharedInstance().setUserIdentifier(userName)
        
        
        HUD.showHUD(onView: self.view)  {
            
            
            Auth.auth().signIn(withEmail: userName, password: password) { authResult, error in
                          
                          
                          
                          guard let authResult = authResult else {
                              
                              print(error.debugDescription)
                              self.requestFailed(error)
                              return
                          }
                          
                          self.loginRequestSuccess(authResult)
                          
                      }
        

        }
        
    }
    
    
    func loginRequestSuccess(_ respond: AuthDataResult) {
        
        
        HUD.dismissHUD()
        
        
        Defaults[.IsLogin]      = true
        Defaults[.UserName]     = loginView.userName.text ?? ""
        Defaults[.Password]     = loginView.password.text ?? ""
        Defaults[.RememberMe]   = loginView.remember.isSelected
        
        
        
        ref.child("users").child(respond.user.uid).observeSingleEvent(of: .value, with: { snapshot in
            
            guard let data = snapshot.value as? [String : Any] else { return }
            
            
            print("data: \(data)")
            
            AppData.instance.myInfo = User(data: data)
            
            
            guard let window = UIApplication.shared.keyWindow else { return }
            UIView.transition(with: window,
                              duration: 0.5,
                              options: [.transitionCrossDissolve],
                              animations: {
                                let oldState = UIView.areAnimationsEnabled
                                UIView.setAnimationsEnabled(false)
                                window.rootViewController = CustomTabbarController()
                                UIView.setAnimationsEnabled(oldState)
                                
            }, completion: nil)
            
        })
        
        
        
    }
       
       //----------------------------
       // MARK: - REQUEST FAILED
       //----------------------------
       
       /**
        Request đăng nhập thất bại
        */
       func requestFailed(_ error: Error?) {
           
           guard let error = error else { HUD.dismissHUD(); return }
           
           switch error {
               
           default:
               HUD.showMessage(error.localizedDescription, onView: view, position: .center)
           }
           
           
       }
    
    /**
     Request đăng nhập thành công
     */
//    func loginRequestSuccess(_ respond: HTTPLogin.RespondType) {
//
//
//        /*
//         Đăng nhập
//         */
//        if let version = respond.version, version > getBuildVersion() {
//
//            HUD.dismissHUD()
//            Defaults[.UpdateRequired] = true
//            showAlertController(.updateRequired)
//            return
//        }
//
//
//        Defaults[.IsLogin]      = true
//        Defaults[.UserName]     = loginView.userName.text ?? ""
//        Defaults[.Password]     = loginView.password.text ?? ""
//        Defaults[.RememberMe]   = loginView.remember.isSelected
//
//        #if VINSCHOOL
//
//        #else
//        Defaults[.SchoolCode]   = (loginView.schoolCode.text ?? "").lowercased()
//        #endif
//
//
//
//        guard respond.token.count > 0 else {
//            HUD.showMessage(LocalizedString("notification_login_not_get_respond", comment: "Không lấy được thông tin, vui lòng thử lại sau."), onView: self.view, position: .center)
//            return
//        }
//
//        Defaults[.Token]                = respond.token
//        Defaults[.FirebaseDBToken]      = respond.firebaseDBToken
//
//
//        AppData.instance.isCanChat      = respond.isCanChat
//        AppData.instance.myInfo         = respond.user
//        AppData.instance.studentList    = respond.students
//        AppData.instance.schools        = respond.schools
//        AppData.instance.classList      = respond.classList
//        AppData.instance.abouts         = respond.abouts
//        AppData.instance.postTypes      = respond.postTypes
//        AppData.instance.canUpdateInfo  = respond.canUpdateInfo
//
//        Utility.realmConfiguration(Defaults[.UserName])
//
//        Crashlytics.sharedInstance().setUserIdentifier(Defaults[.UserName])
//
//        if AppData.instance.isCanChat {
//
//            // [START signinwithcustomtoken]
//            Auth.auth().signIn(withCustomToken: Defaults[.FirebaseDBToken]) { (user, error) in
//
//                if let _ = error {
//                    print("Login firebase Failed")
//                    AppData.instance.isCanChat = false
//
//                } else {
//                    print("Login firebase Success")
//                }
//
//                // [END_EXCLUDE]
//            }
//            // [END signinwithcustomtoken]
//        }
//
//
//        let position = respond.students.firstIndex(where: { $0.id == Defaults[.StudentID] }) ?? 0
//
//        AppData.instance.modules    = respond.students[position].modules.sorted { $0.indexOrder < $1.indexOrder }
//        Defaults[.StudentID]        = respond.students[position].id
//        Defaults[.SchoolID]         = respond.students[position].schoolID
//        Defaults[.ClassID]          = respond.students[position].classID
//
//
//        let account: RealmAccount = RealmAccount(username: loginView.userName.text ?? "",
//                                                 password: "",
//                                                 userID: String(respond.user.id),
//                                                 name: respond.user.name,
//                                                 rememberMe: loginView.remember.isSelected,
//                                                 lastUpdate: 0,
//                                                 listIDRead: [],
//                                                 listIDNotificationDelete: [],
//                                                 listIDNotificationRead: [])
//
//        if let accountTemp = DatabaseSupport.instance.getAccount(loginView.userName.text ?? "") {
//            account.lastUpdate = accountTemp.lastUpdate
//        }
//
//        DatabaseSupport.instance.insertAccount(account)
//        Defaults[.LastUpdateTime] = account.lastUpdate
//
//
//        guard !respond.firstLogin else {
//
//
//            HUD.showMessage(LocalizedString("notification_login_you_please_update_password_default", comment: "Bạn vui lòng cập nhật mật khẩu mới thay mật khẩu mặc định.")) {
//
//
//                let newVC: NewPasswordViewController = NewPasswordViewController()
//                newVC.idUser = respond.user.id
//                newVC.type = .firstLogin
//                newVC.oldPassword = Defaults[.Password]
//                newVC.delegate = self
//                let naviVC = UINavigationController(rootViewController: newVC)
//
//                Utility.configureAppearance(navigation: naviVC)
//                self.present(naviVC, animated: true, completion: nil)
//            }
//
//
//
//            return
//        }
//
//
//        guard let window = UIApplication.shared.keyWindow else { return }
//        UIView.transition(with: window,
//                          duration: 0.5,
//                          options: [.transitionFlipFromLeft],
//                          animations: {
//                            let oldState = UIView.areAnimationsEnabled
//                            UIView.setAnimationsEnabled(false)
//                            AppData.instance.isReloadContact = true
//                            AppData.instance.isAutoLogin = false
//                            AppData.instance.willReloadHome = true
//                            window.rootViewController = self.setupTabBarController()
//                            UIView.setAnimationsEnabled(oldState)
//
//        }, completion: nil)
//
//
////        checkNetworkAndRequestHome()
//
//    }

    
//    /**
//     Request Login
//     */
//    func checkNetworkAndRequestHome() {
//        
//        /* thông báo cho người dùng không có mạng */
//        guard networkReachable() else {
//            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
//                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
//                            onView: self.view,
//                            position: .center)
//            return
//        }
//        
//
//        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
//        HUD.showHUD(LocalizedString("notificaiton_login_loading_data", comment: "Đang tải dữ liệu ...")) {
//            HTTPManager.instance.getHomeInfo
//                .doRequest(HTTPGetHome.RequestType(studentID: Defaults[.StudentID], date: Date().timeIntervalSince1970, getPost: true, getTimetable: true), headers: headers)
//                .completionHandler { result in
//                    switch result {
//                    case .success(let respond):
//                        self.getHomeInfoRequestSuccess(respond)
//                        
//                    case .failure(let error):
//                        self.requestFailed(error)
//                    }
//            }
//            
//        }
//        
//    }
//    
//    
//    /**
//     Request đăng nhập thành công
//     */
//    func getHomeInfoRequestSuccess(_ respond: HTTPGetHome.RespondType) {
//        
//        AppData.instance.posts              = respond.posts
//        AppData.instance.listTimeTables         = respond.timetables
//        
//        HUD.dismissHUD()
//        guard let window = UIApplication.shared.keyWindow else { return }
//        UIView.transition(with: window,
//                          duration: 0.5,
//                          options: [.transitionFlipFromLeft],
//                          animations: {
//                            let oldState = UIView.areAnimationsEnabled
//                            UIView.setAnimationsEnabled(false)
//                            AppData.instance.isReloadContact = true
//                            AppData.instance.isAutoLogin = false
//                            window.rootViewController = self.setupTabBarController()
//                            UIView.setAnimationsEnabled(oldState)
//                            
//        }, completion: nil)
//        
//        
//    }
    
    
    
    /**
     Request đăng nhập thất bại
     */
    func requestFailed(_ error: Swift.Error) {
        
//        switch error {
//        case HTTPError.paramatersInvalid(let string) where string.count > 0:
//
//            HUD.dismissHUD()
//
//            let alert = UIAlertController(title: LocalizedString("alert_title_message", comment: "Thông báo"),
//                                          message: string,
//                                          preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"),
//                                          style: .default,
//                                          handler: nil))
//
//            present(alert, animated: true)
//
//
//        default:
//            HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
//        }
        
        
    }
    

    
  
 
    
    // MARK: - REQUEST RESET PASSWORD
    
    /**
     Request Login
     */
    func checkNetworkAndRequestResetPassword(_ email: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
//        HUD.showHUD(onView: self.view)  {
//            HTTPManager.instance.forgotPassword
//                .doRequest(HTTPForgotPassword.RequestType(username: email))
//                .completionHandler { result in
//
//                    switch result {
//                    case .success(let respond):
//                        self.forgotPasswordRequestSuccess(respond, email: email)
//
//                    case .failure(let error):
//                        self.requestFailed(error)
//                    }
//            }
//        }
    }
    
    
    /**
     Request đăng nhập thành công
     */
//    func forgotPasswordRequestSuccess(_ respond: HTTPForgotPassword.RespondType, email: String) {
//
//
//
//        hiddenKeyboard()
//
//
//        Defaults[.UsernameForgot] = email
//
//        forgotPasswordView?.dismissView()
//        let confirmVC: ConfirmViewController = ConfirmViewController()
//
//        let naviVC: UINavigationController = UINavigationController(rootViewController: confirmVC)
//        Utility.configureAppearance(navigation: naviVC)
//
//        switch respond.status {
//        case 15:
//
//            HUD.showMessage(LocalizedString("notification_login_a_have_confirm_code_send_to_you", comment: "Một mã xác nhận đã được gửi tới số điện thoại và email của bạn, vui lòng sử dụng mã xác nhận này để lấy lại mật khẩu!"), onView: view, position: .center) {
//
//                self.present(naviVC, animated: true)
//                return
//            }
//
//        default:
//            HUD.dismissHUD()
//
//             present(naviVC, animated: true)
//        }
//
//
//
//    }
}



