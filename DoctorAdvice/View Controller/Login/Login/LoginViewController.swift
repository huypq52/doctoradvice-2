//
//  LoginViewController.swift
//  KidOnline
//
//  Created by Pham Huy on 6/2/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Async
import Alamofire
//import RealmSwift
import SnapKit
import Crashlytics
import FlatUIKit
import Firebase


class LoginViewController: CustomViewController {
    

    var logoBA: UIImageView!
    var labelFooter: UILabel!
    var bgImageView: UIImageView!
    var loginView: LoginCenterView!
    var buttonHotline: UIButton!
    var buttonRegister: UIButton!

    var loginViewCenterYConstraint: Constraint!
    var logoBottomConstraint:       Constraint!
    var backgroundBottomConstraint: Constraint!
    var languageRightConstraint:    Constraint!
    
    var buttonLanguage: FUISwitch!
    
    var forgotPasswordView: ForgotPasswordView?
    
    var selectedTextFeild: UITextField?
    
    enum AlertType {
        case updateRequired
        case callOperator
        
        func alertTitle() -> String {
            switch self {
                
            case .callOperator:
                return LocalizedString("alert_title_confirm", comment: "Xác nhận")
                
            default:
                return LocalizedString("alert_title_message", comment: "Thông báo")
            }
        }
        
        func alertMessage() -> String {
            switch self {
                
            case .updateRequired:
                return LocalizedString("notification_login_kidonline_have_new_version_you_please_update", comment: "Hiện tại SchoolOnline đã có phiên bản mới, bạn vui lòng cập nhật lại ứng dụng.")
                
            case .callOperator:
                return LocalizedString("notification_login_you_want_call_kidonline", comment: "Bạn có muốn gọi đến số hotline của SchoolOnline không?")
            }
        }
    }

    
    /// Trường hợp bị kick từ server thì ko auto login
    var shouldAutoLogin: Bool = true
    
    var isFirstLoad: Bool = true
    
    var ref: DatabaseReference!

    var userInfoRefHandle: DatabaseHandle?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("Bắt đầu vào màn hình LOGIN")
        
        ref = Database.database().reference()
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
       
        
        loginView.remember.isSelected = Defaults[.RememberMe]
        if Defaults[.RememberMe] {
            loginView.userName.text = Defaults[.UserName]
            loginView.password.text = Defaults[.Password]
     
        }
        
        if Defaults[.IsLogin] {
            
            loginView.userName.text = Defaults[.UserName]
            loginView.password.text = Defaults[.Password]
            
            checkNetworkAndRequestLogin(Defaults[.UserName], password: Defaults[.Password])
 
        }

    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animationFirstLoad()
        
        if Defaults[.RememberMe] {
            
            loginView.userName.text = Defaults[.UserName]
            loginView.password.text = Defaults[.Password]
            
        } else if Defaults[.UserName] != loginView.userName.text {
            
            loginView.userName.text = ""
            loginView.password.text = ""
            
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        navigationController?.setNavigationBarHidden(true, animated: false)
       
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    deinit {
           print("deinit called")
           
           if let refHandle = userInfoRefHandle {
               ref.removeObserver(withHandle: refHandle)
        }
        
    }
    
}






