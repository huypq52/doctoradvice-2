//
//  LoginViewController+Setup.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/3/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import FlatUIKit
//import CleanroomLogger
import SwiftyUserDefaults
//import Realm
//import RealmSwift


//==================================
// MARK: - SETUP
//==================================

extension LoginViewController {
    func setupAllSubviews() {
        
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .red
        
    
        setupNotificationObserver()
        
        setupBackgroundLogin()
        setupBackground()
        
        setupLoginView()
        setupLabel()
        setupLogo()
        

        buttonHotline = setupButton(title: LocalizedString("function_label_support", comment: "Hỗ trợ"),
                                    image: Icon.Login.Phone.tint(UIColor.Navigation.mainColor()),
                                    titleColor: UIColor.Text.blackMediumColor())
        buttonHotline.contentHorizontalAlignment = .left
        buttonHotline.titleLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)!
        buttonHotline.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)
        buttonHotline.addTarget(self, action: #selector(self.tapOnHotline(_:)), for: .touchUpInside)
        
        
        buttonRegister = setupButton(title: "Đăng ký", titleColor: UIColor.Navigation.mainColor())
        buttonRegister.addTarget(self, action: #selector(self.register(_:)), for: .touchUpInside)
        buttonRegister.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)!
        
        
        buttonLanguage = setupSwitchButton()
        view.addSubview(buttonLanguage)
        view.addSubview(buttonHotline)
        view.addSubview(buttonRegister)
        
    
        loginView.alpha     = 0.0
        buttonHotline.alpha = 0.0
        labelFooter.alpha   = 0.0
        buttonRegister.alpha   = 0.0
        footerBackground.alpha = 1.0
        
        
        print( "status height \(UIApplication.shared.statusBarFrame.height)")
        
    }
    
    func setupAllConstraints() {
        
        bgImageView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
        footerBackground.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)

            
            
            #if VINSCHOOL
            make.height.equalTo(UIScreen.main.bounds.width * 2 / 5)
            backgroundBottomConstraint = make.bottom.equalTo(view).offset(UIScreen.main.bounds.width * 3 / 5).constraint
            #else
            make.height.equalTo(UIScreen.main.bounds.width / 4)
            backgroundBottomConstraint = make.bottom.equalTo(view).offset(UIScreen.main.bounds.width / 4).constraint
            #endif
            
            footerBackground.contentMode = .top
        }
        
        loginView.snp.makeConstraints { make in
            
            make.width.equalTo(282)
            make.height.equalTo(213)
            make.centerX.equalTo(view)
            loginViewCenterYConstraint = make.centerY.equalTo(view).offset(-5).constraint
        }
        
        buttonRegister.snp.makeConstraints { make in
            
            make.centerX.equalTo(view)
            make.width.equalTo(200)
            make.height.equalTo(44)
            make.top.equalTo(loginView.snp.bottom).offset(5)
          
        }
        
        logoBA.snp.makeConstraints { make in
            
            make.centerX.equalTo(view)
            make.width.equalTo(290)
            make.height.equalTo(90)
            logoBottomConstraint = make.bottom.equalTo(loginView.snp.top).offset( (90 + 213) / 2 ).constraint
          
        }
        
        
        labelFooter.snp.makeConstraints { make in
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view).offset(-view.safeAreaInsets.bottom / 3)
            } else {
                make.bottom.equalTo(view)
                // Fallback on earlier versions
            }
            make.leading.equalTo(view).offset(5)
            make.trailing.equalTo(view).offset(-5)
            make.height.equalTo(38)
        }
        

        
        buttonLanguage.snp.makeConstraints { make in
            
            languageRightConstraint = make.right.equalTo(view).offset(60).constraint
            
//            make.right.equalTo(view).offset(-10)
            make.width.equalTo(60)
            make.height.equalTo(28)
            make.top.equalTo(view).offset(UIApplication.shared.statusBarFrame.height + 10)
        }
        
        
        buttonHotline.snp.makeConstraints { make in
            
            
            make.left.equalTo(view).offset(10)
            make.width.equalTo(140)
            make.height.equalTo(38)
            if #available(iOS 11.0, *) {
                make.top.equalTo(view).offset(self.view.safeAreaInsets.top + 5)
            } else {
                // Fallback on earlier versions
                make.top.equalTo(view).offset(25)
            }
        }
        
    }
    
    
    /**
     setup Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupBackgroundLogin() {
        
        bgImageView = UIImageView()
        //        bgImageView.backgroundColor = UIColor(rgba: "#90d5f2")
        
        bgImageView.backgroundColor = .white
        view.addSubview(bgImageView)
        
        bgImageView.contentMode = .center
        bgImageView.isUserInteractionEnabled = true
        
        bgImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard)))
        bgImageView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard)))
    }
    
    func setupLoginView() {
        
        loginView = LoginCenterView()
        loginView.login.addTarget(self, action: #selector(self.login(_:)), for: .touchUpInside)
        loginView.remember.addTarget(self, action: #selector(self.remember(_:)), for: .touchUpInside)
        loginView.forgotPassword.addTarget(self, action: #selector(self.forgotPassword(_:)), for: .touchUpInside)
        loginView.userName.delegate = self
        loginView.password.delegate = self
        loginView.backgroundColor = .clear
        loginView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard)))
        view.addSubview(loginView)
    }
    
    func setupLabel() {
        
        labelFooter = UILabel()
        labelFooter.textAlignment = .center
        labelFooter.font = UIFont(name: FontType.latoRegular.., size: FontSize.small++)
        labelFooter.text = String(format: LocalizedString("login_copy_right_label", comment: "%@ © Copyright Pham Quang Huy"), Utility.getStringTime(Date().timeIntervalSince1970, formatter: "yyyy"))
        labelFooter.textColor = UIColor.Text.blackMediumColor()
        labelFooter.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openWebHome(_:))))
        labelFooter.isUserInteractionEnabled = true
        view.addSubview(labelFooter)
        
    }
    
    func setupLogo() {
        
        logoBA = UIImageView(image: Icon.General.LogoKidsOnline)
        logoBA.contentMode = .scaleAspectFit
        view.addSubview(logoBA)
    }
    
    func setupSwitchButton() -> FUISwitch {
        let buttonSwitch: FUISwitch = FUISwitch()
        
        buttonSwitch.isOn = Defaults[.SelectedLanguage] == LanguageValue.english..
        buttonSwitch.offLabel.text = "VI"
        buttonSwitch.offLabel.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        buttonSwitch.offLabel.textColor = .white
        buttonSwitch.offBackgroundColor = UIColor.Navigation.mainColor()
        buttonSwitch.offColor = .white
        
        
        buttonSwitch.onLabel.text = "EN"
        buttonSwitch.onLabel.textColor = .white
        buttonSwitch.onBackgroundColor = UIColor.Navigation.mainColor()
        buttonSwitch.onLabel.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        buttonSwitch.onColor = .white
        buttonSwitch.layer.cornerRadius = 28 / 2
        buttonSwitch.addTarget(self, action: #selector(self.changeLanguage(_:)), for: .valueChanged)
        return buttonSwitch
    }
    
    func setupVisualEffect() -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        blurEffectView.alpha = 1.0
        return blurEffectView
    }
    
    

    func setupForgotPassword() -> ForgotPasswordView {
        let view = ForgotPasswordView()
        view.textFieldEmail.delegate = self
        view.buttonSend.addTarget(self, action: #selector(self.sendEmail(_:)), for: .touchUpInside)
        view.haveActivationCode.addTarget(self, action: #selector(self.haveActiveCode(_:)), for: .touchUpInside)
        view.haveActivationCode.isHidden = true
        return view
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
}
