//
//  LoginViewController+Processor.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/3/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import FlatUIKit
import Firebase
import SwiftyUserDefaults
//import Realm
//import RealmSwift


//==================================
// MARK: - SELECTOR
//==================================

extension LoginViewController {
    @objc func login(_ sender: UIButton) {
        
        hiddenKeyboard()
        
        let trimmer = { (text: String?) -> String? in
            return text?.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        


        
        
        guard let username = trimmer(loginView.userName.text), username.count > 0,
            let password = trimmer(loginView.password.text), password.count > 0
            
            else    {
                HUD.showMessage("Bạn vui lòng điền đầy đủ email và mật khẩu", onView: view, position: .center)
                return
        }
        
        
        checkNetworkAndRequestLogin(username, password: password)
        
    }
    
    @objc func remember(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @objc func forgotPassword(_ sender: UIButton) {
        showForgotPasswordView()
        

    }
    
    @objc func sendEmail(_ sender: UIButton) {
        
        guard let forgotPasswordView = forgotPasswordView else { return }

        guard let email = forgotPasswordView.textFieldEmail.text, email.count > 0 else {
            
            HUD.showMessage("Bạn vui lòng nhập email đã đăng ký!")
            return
        }
        

        Auth.auth().sendPasswordReset(withEmail: email) { error in
            // Your code here

            if let error = error {
                
                print("error: \(error)")
                
            } else {
                
                self.forgotPasswordView?.dismissView()
                HUD.showMessage("Một đường dẫn đã được gửi tới email của bạn, vui lòng click vào đường dẫn này để thiết lập lại mật khẩu mới!", onView: self.view, position: .center)
            }
        }
    }
    
    // Show trang chủ
    @objc func openWebHome(_ sender: UIGestureRecognizer) {
        
    }
    
    
    /// Gọi hotline
    @objc func labelContatAction(_ sender: UIGestureRecognizer) {
        showAlertController(.callOperator)
    }
    
    /// Gọi hotline
    @objc func tapOnHotline(_ sender: UIButton) {
        showHotlineView()
    }
    

    @objc func haveActiveCode(_ sender: UIButton) {
        
        hiddenKeyboard()
        
        
        
        /**
         Check số điện thoại
         */
        guard let username = forgotPasswordView?.textFieldEmail.text, username.count > 0 else {
//            HUD.showMessage(LocalizedString("notification_login_you_not_input_username", comment: "Bạn chưa nhập tên đăng nhập"),
//                            onView: view)
            forgotPasswordView?.textFieldEmail.becomeFirstResponder()
            return
        }
        
//        /**
//         Check số điện thoại có bị thay đổi không
//         */
//        guard username == Defaults[.UsernameForgot] else {
//            HUD.showMessage(LocalizedString("notification_login_username_is_changed_please_input_again", comment: "Tên đăng nhập đã thay đổi, bạn vui lòng lấy mã lại"), onView: view)
//            return
//        }
//
//
//        guard schoolCode == Defaults[.SchoolForgot] else {
//            HUD.showMessage(LocalizedString("notification_login_school_change_please_again_code_confirm", comment: "Mã trường đã thay đổi vui lòng lấy lại mã"), onView: view)
//            return
//        }
        
        
//        Defaults[.UsernameForgot] = username
//        Defaults[.SchoolForgot] = schoolCode
        
        
        
//        guard Defaults[.RegisterTime] > Date().timeIntervalSince1970 - 1.day  else {
//            
//            HUD.showMessage("Mã xác nhận đã hết hạn")
//            
//            return
//        }
        
//        checkNetworkAndRequestGetAPI(schoolCode, type: .haveCodeActive)
        
    }
    
    
    @objc func register(_ sender: UIButton) {
        
        let registerVC = RegisterViewController()
        navigationController?.pushViewController(registerVC, animated: true)
        
    }
}



//==================================
// MARK: - PRIVATE METHOD
//==================================

extension LoginViewController {
    
    @objc func changeLanguage(_ sender: FUISwitch) {
        
        
        Defaults[.SelectedLanguage] = sender.isOn ? LanguageValue.english.. : LanguageValue.vietnamese..
        Language.instance.setLanguage(sender.isOn ? .english : .vietnamese)
        
        
        HUD.showMessage("Đang đổi ngôn ngữ ...", onView: self.view,
            disableUserInteraction: true) {

                let naviVC: UINavigationController = UINavigationController(rootViewController: LoginViewController())
                Utility.configureAppearance(navigation: naviVC)

                let window = UIApplication.shared.keyWindow
                UIView.transition(with: window!,
                                  duration: 0.5,
                                  options: [.curveEaseInOut],
                                  animations: {
                                    window!.rootViewController = naviVC
                }, completion: { _ in


                })

                return
        }
    }
    
    
    /**
     Ẩn bàn phím
     */
    @objc func hiddenKeyboard() {
        loginView.userName.resignFirstResponder()
        loginView.password.resignFirstResponder()

        
        guard let selectedTextFeild = selectedTextFeild else { return }
        selectedTextFeild.resignFirstResponder()
    }
    
    
    /**
     Update ứng dụng
     */
    @objc func updateApplication() {
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/id" + AppleID) else { return }
        UIApplication.shared.openURL(url)
    }
    
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        if let forgotPasswordView = forgotPasswordView {
            if let selectedTextFeild = selectedTextFeild,
                (selectedTextFeild == forgotPasswordView.textFieldEmail) { return }
        }
        
        let scrollHeight = (loginView.frame.maxY - view.frame.height) + frame.height
        print( "show bàn phím \(scrollHeight) --- \(loginView.frame.maxY) --- \(view.frame.height)")
        guard scrollHeight > 5 else { return }
        
        
        loginViewCenterYConstraint.update(offset: -scrollHeight)
        UIView.animate(withDuration: 1.second) { self.view.layoutSubviews() }
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
         print( "Ẩn bàn phim")
        
        loginViewCenterYConstraint.update(offset: -5)
        UIView.animate(withDuration: 0.1.second) { self.view.layoutSubviews() }
    }
    
    
    /**
     Show Alert
     */
    
    func showAlertController(_ type: AlertType) {
        self.dismiss(animated: false, completion: nil)
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        
        
        switch type {
            
        case .callOperator:
            /**
             Bỏ qua
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
            
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                Utility.callPhoneNumber(KidsPhoneNumber)
            }))
            
            
        case .updateRequired:
            
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_update", comment: "Cập nhật"), style: .default, handler: { [unowned self] _ in
                self.updateApplication()
            }))
            
        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    func showForgotPasswordView() {
        forgotPasswordView = setupForgotPassword()
        

        forgotPasswordView?.textFieldEmail.text = Defaults[.UserName]
        
        Utility.showView(forgotPasswordView, onView: self.view)

        self.forgotPasswordView?.textFieldEmail?.becomeFirstResponder()
   
    }
    
    /// Hiển thị view Hotline
    @objc func showHotlineView() {
        
//        hotlineView = HomeHotlineView()
//        hotlineView?.delegate = self
//        Utility.showView(hotlineView, onView: view)
 
    }
}


/*----------------------------------------*
 //  - MARK:        ANIMATION
 *----------------------------------------*/

extension LoginViewController {
    
    func animationFirstLoad() {
        
        guard isFirstLoad else { return }
        isFirstLoad = false
        
        #if VINSCHOOL
        
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            backgroundBottomConstraint.update(offset: -30)
            
        case .screen4_7Inch:
            backgroundBottomConstraint.update(offset: -10)
            
        
            
        default:
            if #available(iOS 11.0, *) {
                backgroundBottomConstraint.update(offset:  -self.view.safeAreaInsets.bottom * 2 / 3)
            } else {
                backgroundBottomConstraint.update(offset: 0)
            }
        }
        
        
        
        #else
        
        backgroundBottomConstraint.update(offset: 0)
        #endif
        
       
        
        
        logoBottomConstraint.update(offset: -28)
        languageRightConstraint.update(offset: -10)
        
        loginView.alpha = 0.0
        labelFooter.alpha = 0.0
        buttonHotline.alpha = 0.0
        buttonRegister.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
            
            UIView.animate(withDuration: 0.5) {
                self.loginView.alpha = 1.0
                self.labelFooter.alpha = 1.0
                self.buttonHotline.alpha = 1.0
                self.buttonRegister.alpha = 1.0
            }
            
        })
        
        //    backgroundBottomConstraint.up  = make.bottom.equalTo(view).offset(0).constraint
        
        //    logoBottomConstraint = make.bottom.equalTo(loginView.snp.top).offset(-28).constraint
    }
}












