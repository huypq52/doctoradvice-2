//
//  LoginViewController+Delegate.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/3/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import FlatUIKit
//import CleanroomLogger
import SwiftyUserDefaults
//import Realm
//import RealmSwift
import MessageUI

//-------------------------------------------
// MARK:   - TEXT FIELD DELEGATE
//-------------------------------------------
extension LoginViewController: UITextFieldDelegate {
    /**
     Hàm validate các textfield
     
     :param: textField
     :param: range
     :param: string
     
     :returns:
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        /**
         *  Name TextField: không cho nhập quá 30 ký tự
         */
        if textField == loginView.userName {
            var newLength =  string.count - range.length
            
            if let name = textField.text {
                newLength += name.count
            }
            
            return (newLength > 50) ? false : true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginView.userName {
            loginView.password.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        selectedTextFeild = textField
        return true
    }
}


//extension LoginViewController: NewPasswordViewControllerDelegate {
//
//    func updatePasswordSuccess(_ isLogin: Bool) {
//
//        loginView.password.text = Defaults[.Password]
//
//        guard let window = UIApplication.shared.keyWindow else { return }
//        UIView.transition(with: window,
//                          duration: 0.5,
//                          options: [.transitionFlipFromLeft],
//                          animations: {
//                            let oldState = UIView.areAnimationsEnabled
//                            UIView.setAnimationsEnabled(false)
//                            AppData.instance.isReloadContact = true
//                            AppData.instance.isAutoLogin = false
//                            AppData.instance.willReloadHome = true
//                            window.rootViewController = self.setupTabBarController()
//                            UIView.setAnimationsEnabled(oldState)
//
//        }, completion: nil)
//
////        checkNetworkAndRequestHome()
//
//    }
//}


