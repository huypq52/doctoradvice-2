//
//  EditProfileViewController.swift
//  KidOnline
//
//  Created by Pham Huy on 11/10/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import FirebaseStorage
import SwiftyUserDefaults
import SKPhotoBrowser
import SnapKit
import Async
import MXParallaxHeader
import Alamofire
import Firebase
import FirebaseDatabase


protocol EditProfileViewControllerDelegate: class {
    func updateInfo()
}



// 1:firstname, 2:lastname,3:gender, 4:email, 5:phone,6:dob,7:address,8:avatar

enum CanUpdateType: Int {
    case firstname = 1, lastname, gender, email, phone, dob, address, avatar
}

class EditProfileViewController: CustomViewController {

    enum AlertType {
        case changedAvater
        case changeCover
        
        
        func alertTitle() -> String {
            switch self {
            case .changedAvater :
                return LocalizedString("personal_seletion_avatar", comment: "Chọn ảnh đại diện")
                
            case .changeCover:
                return LocalizedString("personal_seletion_cover", comment: "Chọn ảnh bìa")
            }
        }
        
        func alertMessage() -> String? {
            switch self {
            case .changedAvater, .changeCover:
                return nil
            }
        }
    }
    
    
    // 1:firstname, 2:lastname,3:gender, 4:email, 5:phone,6:dob,7:address,8:avatar
    
    enum Row: Int {
        case name = 0, phone, email, birthDay, gender, address
        
        var title: String {
            switch self {
            case .name:
                return LocalizedString("child_edit_name", comment: "Họ tên")
                
            case .phone:
                return LocalizedString("child_edit_name", comment: "Họ tên")
                
            case .email:
                return LocalizedString("child_edit_name", comment: "Họ tên")
                
            case .birthDay:
                return LocalizedString("child_edit_name", comment: "Họ tên")
                
            case .gender:
                return LocalizedString("child_edit_name", comment: "Họ tên")
                
            case .address:
                return LocalizedString("child_edit_name", comment: "Họ tên")
            }
        }
        
        var image: UIImage {
            switch self {
            case .name:
                return Icon.Login.Personal
                
            case .phone:
                return Icon.Login.Phone
                
            case .email:
                return Icon.Contact.DetailEmail
                
            case .birthDay:
                return Icon.General.Calendar
                
            case .gender:
                return Icon.Account.Gender
                
            case .address:
                return Icon.Contact.DetailAddress
            }
        }
    }
    
    var back: UIBarButtonItem!
    var done: UIBarButtonItem!
    
    var isSelectedCover: Bool = false
    
    
    var datePickerView: DatePickerDialogView?
    var topView: AccountTopView!

    var dataArray: [Row] = []
    
    var ref: DatabaseReference!
    lazy var storageRef: StorageReference = Storage.storage().reference().child("users")

    fileprivate var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    
    var userInfo: User!
    
    var coverImageNew: UIImage?
    var avatarImage: UIImage?
    var showAlert: Bool = false
    
    var selectedTextField: UITextField?
    weak var delegate: EditProfileViewControllerDelegate?
    
    //====================================
    // MARK: - CYCLE LIFE
    //====================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard let info = AppData.instance.myInfo else { return }
        userInfo = info
        
         ref = Database.database().reference()
        
        dataArray = [.name, .phone, .email, .birthDay, .gender, .address]

        setupAllSubviews()
        view.setNeedsUpdateConstraints()
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

//-------------------------------------------
//  MARK: - SKPhotoBrowser DELEGATE
//-------------------------------------------
extension EditProfileViewController: SKPhotoBrowserDelegate {
    
}

//-------------------------------------------
//  MARK: - ACTION SHEET DELEGATE
//-------------------------------------------

extension EditProfileViewController: UIActionSheetDelegate, UINavigationControllerDelegate {
    
   @objc  func viewAvatar(_ sender: UIGestureRecognizer) {
        print("Show avatar")
        
        guard let myInfo = AppData.instance.myInfo else { return }


        var photos: [SKPhoto] = []

        if let image = myInfo.imageAvatar {
            photos.append(SKPhoto.photoWithImage(image))
        } else {
            photos.append(SKPhoto.photoWithImageURL(myInfo.avatar))
        }

        let browser = SKPhotoBrowser(photos: photos)
        browser.delegate = self
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    

    
    /**
     Show Alert tùy theo type
     */
    fileprivate func showAlertController(_ type: AlertType) {
        
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .actionSheet)
        
        guard let alertController = alertController else { return }
        
        alertController.addAction(UIAlertAction(title: LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
        
        switch type {
        case .changedAvater, .changeCover:
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_library", comment: "Thư viện"), style: .default, handler: {[unowned self] _ in
                self.showImagePicker(.photoLibrary)
                }))
            
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_camera", comment: "Camera"), style: .default, handler: {[unowned self] _ in
                self.showImagePicker(.camera)
                }))
            
        }
        
        
        if UI_USER_INTERFACE_IDIOM() != .phone {
            if let popoverController = alertController.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: 0, y: 0, width: view.frame.width , height: self.topView.buttonChangedAvatar.frame.maxY)
                popoverController.sourceView = self.view
            }
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    /**
     Show controller chụp ảnh hoặc ảnh từ thư viện
     */
    fileprivate func showImagePicker(_ source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        present(picker, animated: true, completion: nil)
    }
    
}

//-------------------------------------------
//  MARK: - IMAGE PICKER DELEGATE
//-------------------------------------------

extension EditProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        dismiss(animated: false, completion: nil)
        let croppedImage = cropImage(image, toSize:CGSize(width: 720, height: 720))
        
        avatarImage = croppedImage
        
        topView.avatar.image = croppedImage
        topView.avatar.alpha = 0.0
        topView.avatar.contentMode = .scaleAspectFill
        UIView.animate(withDuration: 0.5.second) {
            self.topView.avatar.alpha = 1.0
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
 
        dismiss(animated: false, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        let croppedImage = cropImage(image, toSize:CGSize(width: 720, height: 720))
        
        
        if isSelectedCover {
            isSelectedCover = false
            
            coverImageNew = croppedImage
            
            topView.cover.image = croppedImage
            topView.cover.alpha = 0.0
            topView.cover.contentMode = .scaleAspectFill
            UIView.animate(withDuration: 0.5.second) {
                self.topView.cover.alpha = 1.0
            }
            
        } else {
            
            
            avatarImage = croppedImage
            
            topView.avatar.image = croppedImage
            topView.avatar.alpha = 0.0
            topView.avatar.contentMode = .scaleAspectFill
            UIView.animate(withDuration: 0.5.second) {
                self.topView.avatar.alpha = 1.0
            }
            
        }
        

    }
    
    fileprivate func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 720 x 720
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: 720, height: 720 * image.size.height / image.size.width) }
        else { newSize = CGSize(width: 720 * image.size.width / image.size.height, height: 720) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension EditProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch dataArray[indexPath.row]  {
        case .gender:
            let cellIdentifier = "GenderTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GenderTableViewCell
            
            configureGenderCell(cell, atIndexPath: indexPath)
            return cell
            

        default:
            let cellIdentifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! EditInfoTableViewCell
            
            configureCell(cell, atIndexPath: indexPath)
            return cell
        }
        

    }
    

    
    fileprivate func configureGenderCell(_ cell: GenderTableViewCell, atIndexPath indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        cell.imageView?.image = dataArray[indexPath.row].image.tint(UIColor.Text.grayMediumColor())
        cell.isMale = userInfo.gender == .male
        

    }
    
    fileprivate func configureCell(_ cell: EditInfoTableViewCell, atIndexPath indexPath: IndexPath) {
        
        
        cell.clipsToBounds = true
        cell.selectionStyle = .none
        cell.textField.icon.image = dataArray[indexPath.row].image.tint(UIColor.Text.grayMediumColor())
        cell.textField.placeholder = dataArray[indexPath.row].title
        cell.textField.seperator.isHidden = false
        cell.textField.isEnabled = true
        cell.textField.delegate = self
        cell.textField.tag = indexPath.row
        cell.textView.isHidden = true
        
       
        switch dataArray[indexPath.row] {
        case .name:
            cell.textField.text = userInfo.name
        
        case .phone:
            cell.textField.text = userInfo.phone
            
        case .email:
            cell.textField.text = userInfo.email
   
        case .birthDay:
            cell.textField.isEnabled = false
            cell.textField.text = dateFormatter.string(from: Date(timeIntervalSince1970: userInfo.dob))

        case .address:
            cell.textField.text = userInfo.address


        default:
            break
        }
        
        
    }
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension EditProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let myInfo = AppData.instance.myInfo else { return }
        switch dataArray[indexPath.row] {
        case .birthDay:
            
            showDatePickerView(myInfo.dob)

        default:
            break
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch dataArray[indexPath.row] {
        case .address, .birthDay:
            return 50
        default:
            return 50
        }
        
        
    }
}

// MARK: - SELECTOR
extension EditProfileViewController {
     @objc func back(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func done(_ sender: UIBarButtonItem) {
        hiddenKeyboard()
        checkNetworkAndRequestUpdateInfo()
    }
    
    /**
     Chọn ảnh đại diện khác
     */
    @objc func changeAvatar(_ sender: UIGestureRecognizer) {
        
        showAlertController(.changedAvater)
    }
    
    /// Chọn ảnh bìa khác
    @objc func changeCover(_ sender: UIGestureRecognizer) {
        
        isSelectedCover = true
        showAlertController(.changeCover)
    }
}


//====================================
// MARK: - DATEPICKER DELEGATE
//====================================

extension EditProfileViewController: DatePickerViewDelegate {


    func showDatePickerView(_ time: TimeInterval = Date().timeIntervalSince1970) {

        hiddenKeyboard()

        datePickerView = DatePickerDialogView()
        datePickerView!.delegate = self
        datePickerView?.datePicker.maximumDate = Date()
        datePickerView?.datePicker.minimumDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - 100.years)
        datePickerView?.labelHeader.text = LocalizedString("general_label_selection_date", comment: "Chọn ngày")
        datePickerView?.datePicker.setDate(Date(timeIntervalSince1970: time), animated: true)
        
        Utility.showView(datePickerView, onView: view)

//        table.isScrollEnabled = false
        AppData.instance.gestureRecognizerShouldReceiveTouch = true
    }


    @objc func selectedDate(_ time: TimeInterval) {
        dateSelected = time
        userInfo.dob = time
        datePickerView?.dismissView()

//        table.isScrollEnabled = true
        AppData.instance.gestureRecognizerShouldReceiveTouch = false


        
        guard let pos = dataArray.firstIndex(of: .birthDay),
            let cell = table.cellForRow(at: IndexPath(row: pos, section: 0)) as? EditInfoTableViewCell else { return }

        cell.textField.text =  Utility.getStringTime(time, formatter: "dd/MM/yyyy")
    }
}


// MARK: - REQUEST CHANGED PASSWORD
extension EditProfileViewController {
    /**
     Request Login
     */
    fileprivate func checkNetworkAndRequestUpdateInfo() {
        

        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        

        if let cell = table.cellForRow(at: IndexPath(row: Row.gender.., section: 0)) as? GenderTableViewCell {
            userInfo.gender = cell.isMale ? .male : .female
        }

        HUD.showHUD(onView: self.view)
        
        let itemRef = ref.child("users").child(userInfo.uid)
        
        
        let userItem: [String : Any] = [
            "id": userInfo.uid,
            "full_name": userInfo.name,
            "avatar": userInfo.avatar,
            "account_type": userInfo.accountType..,
            "email": userInfo.email,
            "address": userInfo.address,
            "dob": userInfo.dob,
            "gender": userInfo.gender!..,
            "phone": userInfo.phone]
        
        itemRef.setValue(userItem)
        
        
        guard let avatar = avatarImage else {
            
            HUD.showMessage(LocalizedString("notification_child_edit_update_info_success", comment: "Cập nhật thông tin thành công!"))
            
            AppData.instance.myInfo = userInfo
            
            delegate?.updateInfo()
            
            return
        }

        uploadImage(avatar)
        

    }
    
    
    func uploadImage(_ image: UIImage) {
            

            let imageData = image.jpegData(compressionQuality: 0.9)
            let imagePath = userInfo.uid + "/\((Date.timeIntervalSinceReferenceDate * 1000).toString(0)).jpg"
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            
            let riversRef = storageRef.child(imagePath)
            
            HUD.showHUD()
            // 6
            riversRef.putData(imageData!, metadata: metadata) { [unowned self] (metadata, error) in
                
             

                
                if let error = error {
                    print("Error uploading photo: \(error)")
                    
                    HUD.showMessage("Gửi ảnh không thành công", onView: self.view)
                    return
                }
                
     
                riversRef.downloadURL { (url, error) in

                    guard let downloadURL = url else {  return }
                    
                    print("downloadURL.absoluteURL : \(downloadURL.absoluteString)")
                    
                    self.uploadPhotoSuccess(downloadURL.absoluteString)
                    
                }
            }
            
        }
        
        func uploadPhotoSuccess(_ link: String) {
            

            ///Delete avatar cũ
            
            if userInfo.avatar.count > 0 {
                //Removes image from storage
                Storage.storage().reference(forURL: userInfo.avatar).delete { error in
                    if let error = error {
                        print(error)
                    } else {
                        // File deleted successfully
                        print("Xóa ảnh thành công")
                    }
                }
            }
    
            ref.child("users").child(userInfo.uid).child("avatar").setValue(link)
            
            HUD.showMessage(LocalizedString("notification_child_edit_update_info_success", comment: "Cập nhật thông tin thành công!"))

            userInfo.avatar = link
            AppData.instance.myInfo = userInfo
            
            delegate?.updateInfo()
        }
    
    func updateInfoFailed(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
}

//-------------------------------------------
// MARK:   - TEXT FIELD DELEGATE
//-------------------------------------------
extension EditProfileViewController: UITextFieldDelegate {
    /**
     Hàm validate các textfield
     
     :param: textField
     :param: range
     :param: string
     
     :returns:
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var newLength =  string.count - range.length
        
        if let name = textField.text {
            newLength += name.count
        }
        
        return (newLength > 500) ? false : true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        guard textField.tag + 1 < dataArray.count else {
//            textField.resignFirstResponder()
            return false
        }
        
     
        
        
        var nextIndex = textField.tag + 1
        
       
        
        switch dataArray[nextIndex] {
        case .birthDay:
            showDatePickerView(userInfo.dob)
            
//             textField.resignFirstResponder()
            return false
        
        default:
            break
        }
        
        let indexPathNext: IndexPath = IndexPath(row: nextIndex, section: 0)
        
        guard let cell = table.cellForRow(at: indexPathNext) as? EditInfoTableViewCell else { return false }
        cell.textField.becomeFirstResponder()
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        scrollHeight = table.rectForRow(at: IndexPath(row: textField.tag, section: 0)).maxY - (view.frame.height - 5)
        
        
        print("table.rectForRow(at: IndexPath(row: textField.tag, section: 0)).maxY: \(table.rectForRow(at: IndexPath(row: textField.tag, section: 0)).maxY)")
        print("scrollHeight: \(scrollHeight)")
        
        selectedTextField = textField
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let text = textField.text, text.count > 0 else { return }
        
        switch dataArray[textField.tag] {
        case .name:
            userInfo.name = text
            
        case .phone:
            userInfo.phone = text
            
        case .email:
            userInfo.email = text
            
        case .address:
            userInfo.address = text
            
        default:
            break
        }
    }
}

extension EditProfileViewController {
    
    /**
     Sự kiện bàn phím hiện lên
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        print("aaaaa")
        
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        
        let scrollPoint = CGPoint(x: 0.0, y: scrollHeight + frame.height)

        if scrollPoint.y > 0 {
            table.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    /**
     Sự kiện bàn phím ẩn đi
     */
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
        print("bbbb")
        
    }
    
    
    @objc func hiddenKeyboard() {
        
        view.endEditing(true)
        
        guard let selectedTextField = selectedTextField else { return }
        selectedTextField.resignFirstResponder()
    }
    
}

//-------------------------------
// - MARK: MX DELEGATE
//-------------------------------

extension EditProfileViewController: MXParallaxHeaderDelegate {
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        
    }
}


//====================================
// MARK: - SETUP ALL SUBVIEW
//====================================

extension EditProfileViewController {
    fileprivate func setupAllSubviews() {
        
        
        
        title =  LocalizedString("personal_title", comment: "Thông tin cá nhân")
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        navigationItem.leftBarButtonItem = back
        
        done = setupBackBarButton(Icon.Navigation.Done, selector: #selector(self.done(_:)), target: self)
        navigationItem.rightBarButtonItem = done
        
        topView = setupHeaderView()
        topView.contentMode = .scaleAspectFill
        topView.image = cropImage(Icon.General.ArrowRight, toSize: CGSize(width: UIScreen.main.bounds.width,
                                                                                height: UIScreen.main.bounds.width))

        
        setupTable()
        
        setupNotificationObserver()
        


        var link: String = "https://images.pexels.com/photos/440731/pexels-photo-440731.jpeg?auto=compress&cs=tinysrgb&h=350"
        
        if let user = AppData.instance.myInfo, user.cover.count > 0 {
            link = user.cover.getFullLink()
        }
         
        table.tableHeaderView = topView
        
//        table.parallaxHeader.view = topView // You can set the parallax header view from the floating view
//        table.parallaxHeader.height = UIScreen.main.bounds.width / 16 * 9 + AccountTopView.Size.avatar.. / 2 + AccountTopView.Size.button.. / 2
//        table.parallaxHeader.mode = .fill
//        table.parallaxHeader.minimumHeight = 0
//        table.parallaxHeader.delegate = self
        topView.cover.downloadedFrom(link: link, placeHolder: nil)
        
    }
    
    
    func setupAllConstraints() {
        table.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
    }
    
    /**
     setup Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    fileprivate func setupTable() {
        table = UITableView(frame: CGRect.zero, style: .plain)
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.keyboardDismissMode = .onDrag
        table.register(EditInfoTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.register(GenderTableViewCell.self, forCellReuseIdentifier: "GenderTableViewCell")
        table.register(EditNameTableViewCell.self, forCellReuseIdentifier: "EditNameTableViewCell")
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        view.addSubview(table)
    }
    
    fileprivate func setupHeaderView() -> AccountTopView {
        let headerView = AccountTopView(frame: .zero)
        headerView.frame = CGRect(x: 0,
                                  y: 0,
                                  width: UIScreen.main.bounds.width,
                                  height: UIScreen.main.bounds.width / 16 * 9 + AccountTopView.Size.avatar.. / 2)
        
        headerView.isUserInteractionEnabled = true
        headerView.clipsToBounds = true
        headerView.buttonChangedAvatar.isHidden = true
        
        headerView.cover.image = Icon.General.CoverDefault
        headerView.cover.isUserInteractionEnabled = true
        headerView.cover.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.changeCover(_:))))

//        headerView.seperator.isHidden = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.changeAvatar(_:)))
        headerView.avatar.isUserInteractionEnabled = true
        headerView.avatar.addGestureRecognizer(tap)
    

        headerView.avatar.image = Icon.Account.AvatarDefault
        
        if let image = userInfo.imageAvatar {
            headerView.avatar.image = image
            return headerView
        }

        headerView.avatar.downloadedFrom(link: userInfo.avatar, placeHolder: Icon.Account.AvatarDefault, completionHandler: { image in
            self.userInfo.imageAvatar = image
        })
        
        return headerView
    }
    
    internal func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.image = Icon.General.CoverDefault
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.changeCover(_:))))
        return imageView
    }
}

