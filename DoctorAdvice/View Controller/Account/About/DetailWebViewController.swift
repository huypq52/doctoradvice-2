//
//  DetailWebViewController.swift
//  KidOnline
//
//  Created by Pham Huy on 11/18/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import Async
import WebKit
import SwiftyUserDefaults

class DetailWebViewController: UIViewController {
    
    enum FileType: Int {
        case html = 0, pdf, doc
    }
    
    var webView: WKWebView!
    
    var processView: UIProgressView!
    
    var theBool: Bool = false
    var myTimer: Timer!
    
    fileprivate lazy var back: UIBarButtonItem = {
        var button = UIBarButtonItem(image: Icon.Navigation.Back, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(_:)))
        button.tintColor = UIColor.white
        return button
    }()
    
    
    var type: FileType = .html
    
    var urlString: String?
    
    
    var didSetupAllConstraints: Bool = false
    
    var isTermAssessment: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = back
        
        setupAllSubviews()
        
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if !didSetupAllConstraints {
            setupAllConstaints()
            didSetupAllConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        processView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 8)
    }
    
    //  -   MARK: Selector
     @objc func back(_ sender: UIBarButtonItem) {
        
        
//        guard isTermAssessment else {
//
//            _ = navigationController?.popViewController(animated: true)
//            return
//        }
//
        
        if webView.canGoBack {
            webView.goBack()
        } else {
            _ = navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    ///=========================
    
    func funcToCallWhenStartLoadingYourWebview() {
        processView.progress = 0.0
        processView.isHidden = false
        theBool = false
        myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(self.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    @objc func timerCallback() {
        if theBool {
            if processView.progress >= 1 {
                processView.isHidden = true
                myTimer.invalidate()
            } else {
                processView.progress += 0.1
            }
        } else {
            processView.progress += 0.05
            if processView.progress >= 0.95 {
                processView.progress = 0.95
            }
        }
    }
}

//extension DetailWebViewController: UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        funcToCallWhenStartLoadingYourWebview()
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        funcToCallCalledWhenUIWebViewFinishesLoading()
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
////        HUD.showMessage(LocalizedString("nofication_gerenal_not_connect_to_server", comment: "Không kết nối được với máy chủ"))
//    }
//}



extension DetailWebViewController : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        funcToCallCalledWhenUIWebViewFinishesLoading()
        guard !webView.isLoading else { return }
        
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        
        
        funcToCallWhenStartLoadingYourWebview()
    }
}

extension DetailWebViewController : WKUIDelegate {
    
    @available(iOS 10.0, *)
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        
        return true
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if let targetFrame = navigationAction.targetFrame {
            
            if !targetFrame.isMainFrame {
                webView.load(navigationAction.request)
            }
        } else {
            webView.load(navigationAction.request)
        }
        return nil
        
    }
}



extension DetailWebViewController {
    func setupAllSubviews() {
        
        view.backgroundColor = UIColor.white
        
        webView = setupWebView()
        view.addSubview(webView)
        
        processView = setupProcessView()
        view.addSubview(processView)
        
        
        guard let urlString = urlString else { return }
        guard let url = URL(string: urlString) else { return }
        
        webView.load(URLRequest(url: url))
    }
    
    
    func setupAllConstaints() {
        webView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
    }
    
    
    func setupWebView() -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        
        let web = WKWebView(frame: .zero, configuration: webConfiguration)
        
        web.backgroundColor = UIColor.white
        web.uiDelegate = self
        web.navigationDelegate = self
        //        web.delegate = self
        //        web.scalesPageToFit = true
        web.scrollView.maximumZoomScale = 15 // set as you want.
        web.scrollView.minimumZoomScale = 1 // set as you want.
        web.scrollView.showsHorizontalScrollIndicator = false
        // web.enableLongPressingToSaveImage()
        web.allowsBackForwardNavigationGestures = true
        if #available(iOS 9.0, *) {
            web.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        return web
    }
    
    func setupProcessView() -> UIProgressView {
        let view = UIProgressView()
        view.progressTintColor = UIColor.Navigation.subColor()
        return view
    }
}

