//
//  AboutViewController.swift
//  KidOnline
//
//  Created by Pham Huy on 11/6/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
//import PHExtensions
import UIKit
import SwiftyUserDefaults
import MessageUI

class AboutViewController: CustomViewController {
    
    var yearFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter
    }()
    
    var back: UIBarButtonItem!
    
    var headerView: AboutHeaderView!
    var footerView: AboutFooterView!
    var supportView: AboutSupportView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAllSubviews()
        
        view.setNeedsUpdateConstraints()
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: willHiddenBar && isFirst)
        isFirst = false
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isBack && willHiddenBar { navigationController?.setNavigationBarHidden(true, animated: true) }
        
    }
}

extension AboutViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.instance.abouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentify = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! SeperatorTableViewCell
        configuaCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configuaCell(_ cell: SeperatorTableViewCell, indexPath: IndexPath) {
        
        cell.textLabel?.text = AppData.instance.abouts[indexPath.row].title
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        cell.textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)!
        
    }
}


extension AboutViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let detailVC: DetailWebViewController = DetailWebViewController()
        
        detailVC.title = AppData.instance.abouts[indexPath.row].title
        detailVC.urlString = AppData.instance.abouts[indexPath.row].link
        navigationController?.pushViewController(detailVC, animated: true)
    }
}


extension AboutViewController {
     @objc func back(_ sender: UIBarButtonItem) {
        
        isBack = true
        _ = navigationController?.popViewController(animated: true)
    }
    
    /**
     Nút RATE
     
     - parameter sender:
     */
    @objc func rate(_ sender: UIButton) {
        let url = URL(string: String(format: "itms-apps://itunes.apple.com/app/id%@", AppleID))
        UIApplication.shared.openURL(url!)
    }
    
//    func call(_ sender: UIButton) {
//        guard let info = AppData.instance.supportInfo, info.phone.count > 0 else { return }
//        Utility.callPhoneNumber(info.phone)
//    }
    
    
    /**
     Gọi hotline
     */
    @objc func call(_ sender: UIButton) {
        
//        guard let info = AppData.instance.supportInfo, info.phone.count > 0 else { return }
//
//        let phone = info.phone
//            .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//            .replacingOccurrences(of: " ", with: "")
//            .replacingOccurrences(of: ".", with: "")
//
//        guard phone.count > 0 else { return }
//
//        #if (arch(i386) || arch(x86_64)) && os(iOS)
//            log("on simulator - not calling")
//        #else
//            let tel = "telprompt://" + phone//TODO: sửa hot line của Staxi
//            UIApplication.shared.openURL(URL(string: tel)!)
//        #endif
    }
    
    @objc func email(_ sender: UIButton) {
//        guard let info = AppData.instance.supportInfo, info.email.count > 0 else { return }
//
//
//        guard MFMailComposeViewController.canSendMail() else {
//
//            HUD.showMessage("Email: \(info.email)", onView: self.view)
//            return
//        }
//
//        let mail = MFMailComposeViewController()
//        mail.mailComposeDelegate = self
//        mail.setToRecipients([info.email])
//        present(mail, animated: true, completion: nil)
    }
    
    @objc func skype(_ sender: UIButton) {
        
//        guard let info = AppData.instance.supportInfo, info.skype.count > 0 else { return }
//
//
//        guard  UIApplication.shared.canOpenURL(URL(string: "skype:")!) else {
//            HUD.showMessage("Skype: \(info.skype)", onView: self.view)
//            return
//        }
//
//
//        UIApplication.shared.openURL(URL(string: "skype:\(info.skype)")!)
        

    }
}

// MARK: - REQUEST
extension AboutViewController: MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        switch result {
        case .cancelled:
            print("Cancel sent email")
        case .saved:
            HUD.showMessage(LocalizedString("about_label_save_mail_success", comment: "Đã lưu vào nháp thành công."), onView: self.view)
        case .sent:
            HUD.showMessage(LocalizedString("about_label_send_mail_success", comment: "Gửi email thành công."), onView: self.view)
        case .failed:
            HUD.showMessage(LocalizedString("notification_has_error_please_try_again", comment: "Có lỗi xảy ra, vui lòng thử lại sau"), onView: self.view)
            print("error: \(String(describing: error))")

        @unknown default:
            break
        }
    }
}

// MARK: - REQUEST
extension AboutViewController {
    
    //----------------------------------------
    // - MARK: GET GUIDE
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetGuide() {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
//        HUD.showHUD(onView: self.view) {
//            HTTPManager.instance.getGuide
//                .doRequest(HTTPGetGuideInfomation.RequestType(), headers: headers)
//                .completionHandler { result in
//                    switch result {
//                    case .success(let respond):
//                        self.getGuideRequestSuccess(respond)
//
//                    case .failure(let error):
//                        self.requestFaild(error)
//                        break
//                    }
//            }
//        }
    }
    
    /**
     Request lấy danh sách bài viết thành công
     */
//    fileprivate func getGuideRequestSuccess(_ respond: HTTPGetGuideInfomation.RespondType) {
//
//        HUD.dismissHUD()
//
//        let detailVC = DetailWebViewController()
//        detailVC.title = LocalizedString("about_cell_title_help", comment: "Hướng dẫn sử dụng")
//        detailVC.urlString = respond.link
//        navigationController?.pushViewController(detailVC, animated: true)
//
//
//    }
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    fileprivate func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
    
}

extension AboutViewController {
    
    func setupAllSubviews() {
        
        
        title = LocalizedString("about_tilte", comment: "Thông tin về KidsOnline")
        view.backgroundColor = UIColor.white
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        navigationItem.leftBarButtonItem = back
        
        
        setupHeader()
        setupFooter()
        setupTable()
    }
    
    func setupAllConstraints() {
        
        headerView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(view)
            make.height.equalTo(140)
        }
        
        footerView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalTo(view)
            make.height.equalTo(80)
        }
        
        table.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view)
            make.bottom.equalTo(footerView.snp.top)
            make.top.equalTo(headerView.snp.bottom)
        }
    }
    
    func setupTable() {
        
        table = UITableView(frame: CGRect.zero, style: .plain)
        table.backgroundColor = UIColor.white
        table.separatorStyle = .none
        table.allowsSelection = true
        table.backgroundColor = UIColor.white
        table.register(SeperatorTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.keyboardDismissMode = .onDrag
        table.delegate = self
        table.dataSource = self
        view.addSubview(table)
        
        
        supportView = setupSupportView()
        supportView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 110)
//        table.tableFooterView = supportView
    }
    
    func setupSupportView() -> AboutSupportView {
        let view = AboutSupportView()
        view.phone.addTarget(self, action: #selector(self.call(_:)), for: .touchUpInside)
        view.email.addTarget(self, action: #selector(self.email(_:)), for: .touchUpInside)
        view.skpye.addTarget(self, action: #selector(self.skype(_:)), for: .touchUpInside)
        return view
    }
    
    /**
     setup Header
     */
    func setupHeader() {
        
        headerView = AboutHeaderView()
        view.addSubview(headerView)
        
        headerView.logo.image = UIImage(named: "LogoAbout")
        
        guard let info = Bundle.main.infoDictionary else { return }
        guard let text = info["CFBundleShortVersionString"] as? String else { return }
        headerView.version.text = LocalizedString("about_label_version", comment: "Phiên bản: ") + text
        
    }
    
    /**
     setup Footer
     */
    func setupFooter() {
        
        footerView = AboutFooterView()
        footerView.copyRight.text = String(format: LocalizedString("login_copy_right_label", comment: "%@ © Copyright Pham Quang Huy"), yearFormatter.string(from: Date()))
        
//        footerView.share.addTarget(self, action: #selector(self.share(_:)), for: .touchUpInside)
        footerView.rate.addTarget(self, action: #selector(self.rate(_:)), for: .touchUpInside) 
//
//        footerView.terms.setTitle(LocalizedString("about_terms", comment: "Điều khoản sử dụng"), for: .normal)
//        footerView.terms.addTarget(self, action: #selector(self.term(_:)), for: .touchUpInside)
//        

        view.addSubview(footerView)
    }
}






