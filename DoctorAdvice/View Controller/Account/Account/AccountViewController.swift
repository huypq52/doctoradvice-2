//
//  AccountViewController.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import SKPhotoBrowser
import SnapKit
import MXParallaxHeader
import FlatUIKit
import Firebase

enum ChangedPasswordStatus: Int {
    case failed = 0
    case success = 1
    
}

class AccountViewController: CustomViewController {
    
    
    enum Row: Int {

        case changePassword = 0, setting, about, logout
        
        var title: String {
            switch self {

            case .changePassword:
                return "Đổi mật khẩu"
                
            case .setting:
                return LocalizedString("notification_default_setting", comment: "Cài đặt")
                
            case .about:
                return LocalizedString("account_label_item_about", comment: "Giới thiệu")
                
            case .logout:
                return LocalizedString("about_label_logout", comment: "Đăng xuất")
            }
        }
    }

    
    enum AlertType {
        case updateAvatar
        
        func alertTitle() -> String {
            switch self {
            case .updateAvatar:
                return LocalizedString("alert_title_message", comment: "Thông báo")
            }
        }
        
        func alertMessage() -> String? {
            switch self {
  
            case .updateAvatar:
                return LocalizedString("notification_account_you_want_update_avatar", comment: "Bạn chưa có ảnh đại diện, bạn có muốn thêm ảnh đại diện không?")
            }
        }
    }
    
    
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    

    var topView: AccountTopView!
    var headerView: PersonalHeaderView!
    var buttonLanguage: FUISwitch!


    //====================================
    // MARK: - CYCLE LIFE
    //====================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)

    }
}

//-------------------------------------------
//  MARK: - SKPhotoBrowser DELEGATE
//-------------------------------------------
extension AccountViewController: SKPhotoBrowserDelegate {
    
}

//-------------------------------------------
//  MARK: - ACTION SHEET DELEGATE
//-------------------------------------------

extension AccountViewController: UIActionSheetDelegate, UINavigationControllerDelegate {
    
    @objc func changeLanguage(_ sender: FUISwitch) {
        
        
        Defaults[.SelectedLanguage] = sender.isOn ? LanguageValue.english.. : LanguageValue.vietnamese..
        Language.instance.setLanguage(sender.isOn ? .english : .vietnamese)
        
        
        HUD.showMessage(
            LocalizedString("notification_sidebar_changing_language", comment: "Đang đổi ngôn ngữ ..."), onView: self.view,
            disableUserInteraction: true) { 
                
                
                let window = UIApplication.shared.keyWindow
                UIView.transition(with: window!,
                                  duration: 0.5,
                                  options: [.transitionFlipFromLeft],
                                  animations: {
                                    let oldState = UIView.areAnimationsEnabled
                                    UIView.setAnimationsEnabled(false)
                                    AppData.instance.isAutoLogin = false
                                    AppData.instance.isReloadContact = false
                                    
                                    let tabbarVC = CustomTabbarController()
                                    window?.rootViewController = tabbarVC
                                    UIView.setAnimationsEnabled(oldState)
                                    
                }, completion: nil)
                
                
                
        }
        
        return
    }

    /// Xem ảnh bìa
    @objc func viewCover(_ sender: UIGestureRecognizer) {
        
        guard let userInfo = AppData.instance.myInfo else { return }
        
        var photos: [SKPhoto] = []
        
        if let image = topView.cover.image {
            photos.append(SKPhoto.photoWithImage(image))
        } else {
            photos.append(SKPhoto.photoWithImageURL(userInfo.cover))
        }
        
        let browser = SKPhotoBrowser(photos: photos)
        browser.delegate = self
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    

    /// Xem ảnh đại diện
    @objc func viewAvatar(_ sender: UIGestureRecognizer) {
        print("Show avatar")
        
        guard let userInfo = AppData.instance.myInfo else { return }
        
//        guard let myInfo = AppData.instance.myInfo else { return }
//
//        guard let _ = URL(string: myInfo.avatar.hasPrefix("http") ? myInfo.avatar : DomainKid + myInfo.avatar) else {
//            showAlertController(.updateAvatar)
//            return
//        }
//
//
//
        var photos: [SKPhoto] = []

        if let image = topView.avatar.image {
            photos.append(SKPhoto.photoWithImage(image))
        } else {
            photos.append(SKPhoto.photoWithImageURL(userInfo.avatar))
        }

        let browser = SKPhotoBrowser(photos: photos)
        browser.delegate = self
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    /**
     Chọn ảnh đại diện khác
     */
    @objc func changeAvatar(_ sender: UIButton) {
        
        guard let userInfo = AppData.instance.myInfo else { return }
        
        let editVC: EditProfileViewController = EditProfileViewController()
        editVC.delegate = self
        editVC.userInfo = userInfo
        editVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(editVC, animated: true)
    }
    
    /**
     Show Alert tùy theo type
     */
    fileprivate func showAlertController(_ type: AlertType) {
        
    
        
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        
        guard let alertController = alertController else { return }
        
        alertController.addAction(UIAlertAction(title: LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
        
        switch type {
            
        case .updateAvatar:
            /**
             Đồng ý
             */
            alertController.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                
                guard let userInfo = AppData.instance.myInfo else { return }
                let editVC = EditProfileViewController()
                editVC.delegate = self
                editVC.showAlert = true
                editVC.userInfo = userInfo
                self.navigationController?.pushViewController(editVC, animated: true)
                
            }))
            
        }
        

        present(alertController, animated: true, completion: nil)
        
    }
    
    
    /**
     Show controller chụp ảnh hoặc ảnh từ thư viện
     */
    fileprivate func showImagePicker(_ source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        present(picker, animated: true, completion: nil)
    }
    
    @objc func back(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }

}

//-------------------------------------------
//  MARK: - IMAGE PICKER DELEGATE
//-------------------------------------------

extension AccountViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        dismiss(animated: false, completion: nil)
        let croppedImage = cropImage(image, toSize:CGSize(width: 1024, height: 1024))
        
//        Utility.saveImageToFile(croppedImage)
        
        topView.avatar.image = croppedImage
        topView.avatar.alpha = 0.0
        topView.avatar.contentMode = .scaleAspectFill
        UIView.animate(withDuration: 0.5.second) {
            self.topView.avatar.alpha = 1.0
        }
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        dismiss(animated: false, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        let croppedImage = cropImage(image, toSize:CGSize(width: 720, height: 720))
        
        topView.avatar.image = croppedImage
        topView.avatar.alpha = 0.0
        topView.avatar.contentMode = .scaleAspectFill
        UIView.animate(withDuration: 0.5.second) {
            self.topView.avatar.alpha = 1.0
        }
    }
    
    fileprivate func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: 1024, height: 1024 * image.size.height / image.size.width) }
        else { newSize = CGSize(width: 1024 * image.size.width / image.size.height, height: 1024) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension AccountViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Row.logout++
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "UITableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        

        guard let row = Row(rawValue: indexPath.row) else { return }
        
        cell.clipsToBounds = true
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        cell.textLabel?.text = row.title
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        cell.textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal++)
    
        
    }
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension AccountViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        guard let row = Row(rawValue: indexPath.row) else { return }

        switch row{
        case .changePassword:
            let changePasswordVC:ChangedPasswordViewController = ChangedPasswordViewController()
            changePasswordVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(changePasswordVC, animated: true)
//
//        case .setting:
//            let settingVC: SettingViewController = SettingViewController()
//            settingVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(settingVC, animated: true)
//
//        case .about:
//            let aboutVC: AboutViewController = AboutViewController()
//            aboutVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(aboutVC, animated: true)

        case .logout:
            guard let userInfo = AppData.instance.myInfo, networkReachable() else {
                self.logout()
                return
            }
            checkNetworkAndRequestLogout(userInfo.uid)


        default:
            break


        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
}



//====================================
// MARK: - EDIT PROFILE DELEGATE
//====================================

extension AccountViewController: EditProfileViewControllerDelegate {

    func updateInfo() {

        guard let myInfo = AppData.instance.myInfo else { return }

        headerView.name.text = myInfo.name
        if myInfo.dob > 0 {
            headerView.date.text = dateFormatter.string(from: Date(timeIntervalSince1970: myInfo.dob))
        } else {
            headerView.date.text = myInfo.email
        }

        topView.avatar.image = Icon.Account.AvatarDefault

        if let image = myInfo.imageAvatar {
            topView.avatar.image = image
            return
        }

        topView.avatar.downloadedFrom(link: myInfo.avatar, placeHolder: Icon.Account.AvatarDefault,  completionHandler: { image in
            AppData.instance.myInfo?.imageAvatar = image
        })
    }
}

//-------------------------------------
// MARK: - REQUEST
//-------------------------------------
extension AccountViewController {
    /**
     Request Logout
     */
    fileprivate func checkNetworkAndRequestLogout(_ userId: String) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        

        HUD.showHUD(LocalizedString("notification_account_logoutting", comment: "Đăng xuất ..."), onView: self.view) {
            self.logout()
        }
    }
    

    func requestFailed(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center) {
            self.logout()
        }
    }
    
    
    func logout() {
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: LoginViewController())
        Utility.configureAppearance(navigation: navigationController)
        
        Defaults[.IsLogin] = false
        Defaults[.TrueName] = ""
        Defaults[.BirthDay] = 0
        
        do {
            try Auth.auth().signOut()
        } catch let error {
            print("Logout Firebase Failed \(error)")
        }
        

        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let navibar = self.tabBarController {
            if let items = navibar.tabBar.items {
                items.forEach { $0.badgeValue = nil }
            }
        }
        
        
        guard let window = UIApplication.shared.keyWindow else { return }
        UIView.transition(with: window,
                          duration: 0.5,
                          options: [.transitionFlipFromLeft],
                          animations: {
                            window.rootViewController = navigationController
        }, completion: { _ in
            AppData.instance.resetData()
            URLCache.shared.removeAllCachedResponses()
            
        })

    }
    
}


//-------------------------------
// - MARK: SETUP VIEW
//-------------------------------

extension AccountViewController: MXParallaxHeaderDelegate {
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        
    }
}

//====================================
// MARK: - SETUP ALL SUBVIEW
//====================================

extension AccountViewController {
    fileprivate func setupAllSubviews() {

        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .white
        
        setupBackground()
        headerView = setupHeaderView()
        setupTable()
        
        buttonLanguage = setupSwitchButton()
        view.addSubview(buttonLanguage)
        
        topView = setupTopView()
        
        
        
        
        topView.contentMode = .scaleAspectFill
        topView.image = cropImage(Icon.General.ArrowRight, toSize: CGSize(width: UIScreen.main.bounds.width,
                                                                                height: UIScreen.main.bounds.width))

        table.tableHeaderView = headerView
        
        

        
        table.parallaxHeader.view = topView // You can set the parallax header view from the floating view
        table.parallaxHeader.height = UIScreen.main.bounds.width / 16 * 9 + AccountTopView.Size.avatar.. / 2  + AccountTopView.Size.button.. / 2

        table.parallaxHeader.mode = .fill
        table.parallaxHeader.minimumHeight = 0
        table.parallaxHeader.delegate = self

        
        
        var link: String = "https://images.pexels.com/photos/440731/pexels-photo-440731.jpeg?auto=compress&cs=tinysrgb&h=350"

        if let user = AppData.instance.myInfo, user.cover.count > 0 {  link = user.cover.getFullLink() }
        topView.cover.downloadedFrom(link: link, placeHolder: nil)
    }
    
    
    func setupAllConstraints() {
        table.snp.makeConstraints { make in
            make.bottom.left.right.equalTo(view)
            make.top.equalTo(view).offset(-24)
        }
        
        buttonLanguage.snp.makeConstraints { make in
            make.right.equalTo(view).offset(-10)
            make.width.equalTo(60)
            make.height.equalTo(28)
             make.top.equalTo(view).offset(UIApplication.shared.statusBarFrame.height + 10)
        }
        
        footerBackground.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            #if VINSCHOOL
            make.height.equalTo(UIScreen.main.bounds.width * 3 / 5)
            make.bottom.equalTo(view).offset(50)
            
            #else
            make.height.equalTo(UIScreen.main.bounds.width / 4)
            make.bottom.equalTo(view).offset(0)
            #endif
        }
    }
    
    fileprivate func setupTable() {
        table = UITableView(frame: CGRect.zero, style: .plain)
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        
        table.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        table.clipsToBounds = false
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        view.addSubview(table)
    }
    
    fileprivate func setupHeaderView() -> PersonalHeaderView {
        let headerView = PersonalHeaderView()
        headerView.clipsToBounds = false
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerView.viewHeight())

        guard let myInfo = AppData.instance.myInfo else { return headerView }

        headerView.name.text = myInfo.name
        headerView.date.text = LocalizedString("account_label_user_name", comment: "Tài khoản") + ": " + Defaults[.UserName]
        headerView.date.font = UIFont(name: FontType.latoLight.., size: FontSize.normal..)!

     
        
        return headerView
    }
    

    func setupSwitchButton() -> FUISwitch {
        let buttonSwitch: FUISwitch = FUISwitch()
        
        buttonSwitch.isOn = Defaults[.SelectedLanguage] == LanguageValue.english..
        buttonSwitch.offLabel.text = "VI"
        buttonSwitch.offLabel.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        buttonSwitch.offLabel.textColor = .white
        buttonSwitch.offBackgroundColor = UIColor.Navigation.mainColor()
        buttonSwitch.offColor = .white
        
        
        buttonSwitch.onLabel.text = "EN"
        buttonSwitch.onLabel.textColor = .white
        buttonSwitch.onBackgroundColor = UIColor.Navigation.mainColor()
        buttonSwitch.onLabel.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        buttonSwitch.onColor = .white
        
        buttonSwitch.layer.cornerRadius = 28 / 2
        buttonSwitch.addTarget(self, action: #selector(self.changeLanguage(_:)), for: .valueChanged)
        return buttonSwitch
    }

    
    func setupTopView() -> AccountTopView {
        let view = AccountTopView(frame: CGRect(x: 0, y: 0, width: 600, height: 200))
        view.isUserInteractionEnabled = true
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewCover(_:)))
        view.cover.isUserInteractionEnabled = true
        view.cover.addGestureRecognizer(tap)
        
        view.buttonChangedAvatar.addTarget(self, action: #selector(self.changeAvatar(_:)), for: .touchUpInside)
        
        let tapAvatar = UITapGestureRecognizer(target: self, action: #selector(self.viewAvatar(_:)))
        view.avatar.isUserInteractionEnabled = true
        view.avatar.addGestureRecognizer(tapAvatar)
        view.avatar.image = Icon.Account.AvatarDefault
        
        guard let myInfo = AppData.instance.myInfo else { return view }
   
        if let image = myInfo.imageAvatar {
            view.avatar.image = image
            return view
        }
        
        view.avatar.downloadedFrom(link: myInfo.avatar, placeHolder: Icon.Account.AvatarDefault,  completionHandler: { image in
            AppData.instance.myInfo?.imageAvatar = image
        })
        
        
        return view
    }
}
