//
//  CustomTabbarController+Alert.swift
//  KidOnline
//
//  Created by Pham Huy on 11/17/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults

extension CustomTabbarController {
    
    enum AlertTypeDefault {

        case schoolLocked(String)
        case newUpdate(RealmNotification)
        case showNotification(RealmNotification)
        
        func alertTitle() -> String {
            switch self {
                
            case .schoolLocked:
                return LocalizedString("alert_title_message", comment: "Thông báo")
                
            case .showNotification(let notification):
                return notification.title
                
            case .newUpdate(let _):
            
//                switch data.type {
//
//                case 2, 3, 12, 13: /// Vào đơn xin nghỉ
//                    guard data.classID != 0 else { return LocalizedString("alert_title_message", comment: "Thông báo")  }
//                    let classInfo = AppData.instance.studentList
//                        .map { $0.classInfo }
//                        .filter { $0!.id == data.classID }
//                        .first
//
//                    guard let classTemp = classInfo else { return LocalizedString("alert_title_message", comment: "Thông báo") }
//                    return LocalizedString("general_label_class", comment: "Lớp") + " " + classTemp!.name
//
//                default:
//                    guard data.schoolID != 0  else { return LocalizedString("alert_title_message", comment: "Thông báo") }
//                    let school = AppData.instance.studentList
//                        .map { $0.schoolInfo }
//                        .filter { $0.id == data.schoolID }
//                        .first
//
//                    guard let schoolInfo = school else { return LocalizedString("alert_title_message", comment: "Thông báo") }
//                    return LocalizedString("general_label_school", comment: "Trường") +  " " + schoolInfo.name
//                }

               return LocalizedString("alert_title_message", comment: "Thông báo")
                
//            default:
//                return LocalizedString("alert_title_message", comment: "Thông báo")
            }
        }
        
        func alertMessage() -> String {
            switch self {
            case .newUpdate(let data):
                return data.title
                
            case .schoolLocked(let message):                
                return message
                
            case .showNotification(let notification):
                return notification.subTitle
            }
        }
    }
    
    
    
    /**
     Show Alert
     */
    
    internal func showAlertControllerDefault(_ type: AlertTypeDefault) {
        self.dismiss(animated: false, completion: nil)
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)

        
        switch type {
            
        case .schoolLocked,  .showNotification(_):
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .cancel, handler: nil))
            
        case .newUpdate(let data):
            
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: { _ in
                AppData.instance.showAlertWhenNotification = false
                Defaults[.WillUpdate] = nil
            }))
            
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_show_now", comment: "Xem ngay") , style: .default, handler: { _ in
                self.checkUpdateNotificaton(data)
            }))
            
        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
}







