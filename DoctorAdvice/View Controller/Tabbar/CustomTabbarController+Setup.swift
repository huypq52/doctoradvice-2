//
//  CustomTabbarController+Setup.swift
//  SchoolOnline
//
//  Created by Pham Huy on 5/25/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Popover



extension CustomTabbarController {
    
    
     func setupAllSubviews() {
        
        
//        UITabBar.appearance().shadowImage = UIImage()
//        UITabBar.appearance().backgroundImage = UIImage()

        setupViewControllers()
        setupTabbar()
    }
    
     func setupViewControllers() {
        
        viewControllers = AppData.instance.tabbarItems
            .map { item in
                let naviVC = UINavigationController(rootViewController: item.getViewController())
                Utility.configureAppearance(navigation: naviVC)
                return naviVC
                
        }
    }

    func setupTabbar() {
        
        tabBar.barStyle = .default
        tabBar.backgroundColor = UIColor.Navigation.mainColor()
        tabBar.barTintColor = UIColor.white
        tabBar.isTranslucent = false
        tabBar.tintColor = .gray
        tabBar.tintColor = UIColor.Navigation.mainColor()
        
        
        for (i,item)  in tabBar.items!.enumerated() {
            
            let tabbarItem = AppData.instance.tabbarItems[i]
            
            item.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: FontType.latoSemibold.., size: FontSize.small..)!],
                                        for: .normal)
            
            
            item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
            item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .selected)

            item.tag = i
            item.selectedImage = tabbarItem.image.tint(UIColor.Navigation.mainColor())
            item.image = tabbarItem.image.tint(UIColor(rgba: "#bdc1c9"))
            item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            
            
//                        item.tag = i
//                        item.selectedImage = tabbarItem.image.tint(UIColor.Navigation.mainColor())
//                        item.image = tabbarItem.image.tint(UIColor(rgba: "#bdc1c9"))
//                        item.title = tabbarItem.title
//                        item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.Text.grayMediumColor()], for: .normal)
//                        item.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.Navigation.mainColor()], for: .selected)

        }
    }
    
    

    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        
        return label
    }

}
