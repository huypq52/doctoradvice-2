//
//  CustomTabbarController+Delegate.swift
//  SchoolOnline
//
//  Created by Pham Huy on 5/25/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Popover


extension CustomTabbarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let naviVC = viewController as? UINavigationController, naviVC.viewControllers.count > 0 else { return true }
        guard let _ = naviVC.viewControllers[0] as? AboutViewController else { return true }
        return false
    }
}


//=========================================
// MARK: - TABLE DATASOURCE
//========================================

extension CustomTabbarController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.instance.studentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifi = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifi, for: indexPath) as! SelectionStudentTableViewCell
        configCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configCell(_ cell: SelectionStudentTableViewCell, indexPath: IndexPath) {
        let student = AppData.instance.studentList[indexPath.row]
        
        
        if student.id == Defaults[.StudentID] {
            cell.contentView.backgroundColor = UIColor.Navigation.mainColor().alpha(0.1)
        } else {
            cell.contentView.backgroundColor = UIColor.white
        }
        
        cell.seperator.isHidden = (indexPath.row == AppData.instance.studentList.count - 1)
        cell.textLabel?.text = student.fullName
        cell.imageView?.downloadedFrom(link: student.avatar, placeHolder: Icon.Account.AvatarDefault)
        
    }
}

//=========================================
// MARK: - TABLE DELEGATE
//========================================

extension CustomTabbarController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        popover.dismiss()
        

        guard AppData.instance.studentList[indexPath.row].id != Defaults[.StudentID] else { return }
        
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        let studentInfo: Student =  AppData.instance.studentList[indexPath.row]
        
        
        
//        HUD.showMessage(String(format: "Bạn đã chọn xem thông tin của học sinh %@ thành công", studentInfo.lastName))
        
        Defaults[.StudentID] = studentInfo.id
        Defaults[.ClassID] = studentInfo.classID
        Defaults[.SchoolID] = studentInfo.schoolID
        AppData.instance.modules = studentInfo.modules.sorted { $0.indexOrder < $1.indexOrder }

        AppData.instance.posts = []
        AppData.instance.currentOffsetAlbum = 1
        AppData.instance.postUpdateLastTime = 0
        AppData.instance.willShowLoadAddAlbum = true

        labelName.text = studentInfo.lastName.count > 0 ? studentInfo.lastName : studentInfo.fullName
        avatar.downloadedFrom(link: studentInfo.avatar, placeHolder: Icon.Account.AvatarDefault)
      
        NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
        
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        naviVC.popToRootViewController(animated: true)
        
        
//        if let student = Utility.getStudentFromID(Defaults[.StudentID]), student.schoolInfo.isLocked {
//
//            guard UIApplication.shared.applicationState == .active else { return }
//
//            showAlertControllerDefault(.schoolLocked(String(format: LocalizedString("home_label_school_is_locked", comment: "Trường của bé %@ hiện đang tạm ngưng sử dụng dịch vụ."), student.name)))
//
//        }
 
        
    }
}
