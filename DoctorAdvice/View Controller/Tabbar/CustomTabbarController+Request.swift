//
//  CustomTabbarController+Request.swift
//  KidOnline
//
//  Created by Pham Huy on 11/17/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import SKPhotoBrowser


//-------------------------------
// MARK: REQUEST HTTP DEFAULT
//-------------------------------
extension CustomTabbarController {
    
    //----------------------------------------
    // - MARK: GET POST DETAIL
    //----------------------------------------
    
    internal func defaultCheckNetworkAndRequestGetPostDetail(_ postID: Int, studentID: Int = Defaults[.StudentID], focusComment: Bool) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view) {
            HTTPManager.instance.getAnnouncementDetail
                .doRequest(HTTPGetAnnouncementDetail.RequestType(announcementID: postID), headers: headers)
                .completionHandler { result in

                    if self.willPushChangedStudent {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
                        AppData.instance.willReloadHome = true
                        self.willPushChangedStudent = false
                    }

                    Defaults[.WillUpdate] = nil



                    switch result {
                    case .success(let respond):
                        self.getPostDetailRequestSuccess(respond, focusComment: focusComment)

                    case .failure(let error):
                        self.requestFaild(error)
                        break
                    }
            }
        }
    }
    
    /**
     Request lấy danh sách bài viết thành công
     */
    fileprivate func getPostDetailRequestSuccess(_ respond: HTTPGetAnnouncementDetail.RespondType, focusComment: Bool) {

         Defaults[.WillUpdate] = nil

        HUD.dismissHUD()

        guard respond.announcement.link.count > 0 else {


            let postDetailVC = PostDetailViewController()
            postDetailVC.functionInfo = MainFunction.listArticle.toInfo()
            postDetailVC.post = respond.announcement
            postDetailVC.focusComment = focusComment
            postDetailVC.hidesBottomBarWhenPushed = true

            guard let naviVC = self.selectedViewController as? UINavigationController else { return }


            if naviVC.containController(PostDetailViewController.self) {
                naviVC.removeController(PostDetailViewController.self)
            }

            naviVC.pushViewController(postDetailVC, animated: true)

            return
        }


        let postDetailVC =  PostNewDetailViewController()
        postDetailVC.postID = respond.announcement.id
        postDetailVC.urlString = respond.announcement.link
        postDetailVC.focusComment = focusComment
        
        postDetailVC.showComment = respond.announcement.showComment
        postDetailVC.canLike = respond.announcement.canLike
        postDetailVC.hidesBottomBarWhenPushed = true

        guard let naviVC = self.selectedViewController as? UINavigationController else { return }


        if naviVC.containController(PostDetailViewController.self) {
            naviVC.removeController(PostDetailViewController.self)
        }

        naviVC.pushViewController(postDetailVC, animated: true)
    }


    
 
    //----------------------------------------
    // - MARK: GET SURVEY DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetSurveyDetail(_ surveyID: Int, studentID: Int, date: Double = Date().timeIntervalSince1970) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getSurveyDetail
            .doRequest(HTTPGetSurveyDetail.RequestType(surveyID: surveyID, studentID: studentID, time: date), headers: headers)
            .completionHandler { result in

                Defaults[.WillUpdate] = nil
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getSurveyDetailRequestSuccess(respond)

                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    fileprivate func getSurveyDetailRequestSuccess(_ respond: HTTPGetSurveyDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(SurveyInfoViewController.self) {
            naviVC.removeController(SurveyInfoViewController.self)
        }
        
        let surveyVC: SurveyInfoViewController = SurveyInfoViewController()
        surveyVC.surveyInfo = respond
        surveyVC.hidesBottomBarWhenPushed = true
        naviVC.pushViewController(surveyVC, animated: true)
        
    }
    
    
    
    //----------------------------------------
    // - MARK: GET EXTRACURRICULAR DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetExtracurricularDetail(_ extraID: Int, studentID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getDetailExtracurricular
            .doRequest(HTTPGetDetailExtracurricular.RequestType(studentID: studentID, extracurricularID: extraID), headers: headers)
            .completionHandler { result in
                
                if self.willPushChangedStudent {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
                    AppData.instance.willReloadHome = true
                    self.willPushChangedStudent = false
                }
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getExtracurricularDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getExtracurricularDetailRequestSuccess(_ respond: HTTPGetDetailExtracurricular.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(ExtraDetailViewController.self) {
            naviVC.removeController(ExtraDetailViewController.self)
        }
        

        let detailVC: ExtraDetailViewController = ExtraDetailViewController()
        detailVC.extracurricular = respond.extracurricular
        naviVC.pushViewController(detailVC, animated: true)
        
    }
    
    
    
    //----------------------------------------
    // - MARK: GET QUIZZE DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetQuizzeDetail(_ quizzeID: Int, studentID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getQuizzeDetail
            .doRequest(HTTPGetQuizzeDetail.RequestType(studentID: studentID, quizID: quizzeID), headers: headers)
            .completionHandler { result in
                
//                if self.willPushChangedStudent {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
//                    AppData.instance.willReloadHome = true
//                    self.willPushChangedStudent = false
//                }
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getQuizzeDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getQuizzeDetailRequestSuccess(_ respond: HTTPGetQuizzeDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(QuizzeDetailViewController.self) {
            naviVC.removeController(QuizzeDetailViewController.self)
        }
        

        let detailVC: QuizzeDetailViewController = QuizzeDetailViewController()
        detailVC.assignmentInfo = respond.quizze
        naviVC.pushViewController(detailVC, animated: true)
        
    }
    
    
    
    //----------------------------------------
    // - MARK: GET EL ASSIGNMENT DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetElAssignmentDetail(_ assignmentID: Int, studentID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getElAssignment
            .doRequest(HTTPGetElAssignmentDetail.RequestType(studentID: studentID, elAssignmentID: assignmentID), headers: headers)
            .completionHandler { result in
                
                //                if self.willPushChangedStudent {
                //                    NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
                //                    AppData.instance.willReloadHome = true
                //                    self.willPushChangedStudent = false
                //                }
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getElAssignmentDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getElAssignmentDetailRequestSuccess(_ respond: HTTPGetElAssignmentDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(ElAssignmentDetailViewController.self) {
            naviVC.removeController(ElAssignmentDetailViewController.self)
        }
        
        
        let detailVC: ElAssignmentDetailViewController = ElAssignmentDetailViewController()
        detailVC.assignmentInfo = respond.elAssignment
        naviVC.pushViewController(detailVC, animated: true)
        
    }
    
    
    
    
    //----------------------------------------
    // - MARK: GET SUPPORT DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetSupportDetail(_ supportID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getSupportDetail
            .doRequest(HTTPGetSupportDetail.RequestType(supportID: supportID), headers: headers)
            .completionHandler { result in
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getSupportDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getSupportDetailRequestSuccess(_ respond: HTTPGetSupportDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(SupportNewDetailViewController.self) {
            naviVC.removeController(SupportNewDetailViewController.self)
        }
        
        
        let detailVC: SupportNewDetailViewController = SupportNewDetailViewController()
        detailVC.supportInfo = respond.supportInfo
        naviVC.pushViewController(detailVC, animated: true)
        
    }
    
    
    //----------------------------------------
    // - MARK: GET ABSENT DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetAbsentDetail(_ absentID: Int, studentID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getAbsentDetail
            .doRequest(HTTPGetAbsentDetail.RequestType(studentID: studentID, absentID: absentID), headers: headers)
            .completionHandler { result in
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getAbsentDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getAbsentDetailRequestSuccess(_ respond: HTTPGetAbsentDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(AbsentRequestDetailViewController.self) {
            naviVC.removeController(AbsentRequestDetailViewController.self)
        }
        
        
        let detailVC: AbsentRequestDetailViewController = AbsentRequestDetailViewController()
        detailVC.absentRequest = respond.absentRequest
        naviVC.pushViewController(detailVC, animated: true)
        
    }
    
    
    
    //----------------------------------------
    // - MARK: GET FINANCE DETAIL
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetFinanceDetail(_ contractID: Int, studentID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        

        HUD.showHUD(onView: self.view)
        HTTPManager.instance.getFinanceDetail
            //TODO: Check new version
            .doRequest(HTTPGetFinanceDetail.RequestType(studentID: studentID, termID: contractID, newVersion: true))
            .completionHandler { result in
                
                Defaults[.WillUpdate] = nil
                
                HUD.dismissHUD()
                switch result {
                case .success(let respond):
                    self.getFinanceDetailRequestSuccess(respond)
                    
                case .failure(let error):
                    self.requestFaild(error)
                    break
                }
        }
        
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
    func getFinanceDetailRequestSuccess(_ respond: HTTPGetFinanceDetail.RespondType) {
        
        HUD.dismissHUD()
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        
        
        
        if naviVC.containController(FinanceDetailViewController.self) {
            naviVC.removeController(FinanceDetailViewController.self)
        }
        
        
        if respond.attachFiles.count == 0 {
            
            let detailVC = FinanceDetailViewController()
            detailVC.detailInfo = respond
            naviVC.pushViewController(detailVC, animated: true)
            
        } else {
            
            let detailVC = DetailWebViewController()
            detailVC.title = respond.attachFiles.first?.title ?? ""
            detailVC.urlString = respond.attachFiles.first?.link ?? ""
            naviVC.pushViewController(detailVC, animated: true)
            
        }
        

        
    }
    
    
    
    
    
    //---------------------------
    // MARK: - REQUEST FAILED
    //---------------------------
    
    /**
     Request thất bại
     */
    func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
}
