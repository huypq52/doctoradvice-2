//
//  CustomTabbarController.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/2/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Popover
import Firebase

enum TabbarItem: Int {
    case home = 0, medical, contact, message, account
    
    var title: String {
        switch self {
        case .home:
            return "Trang chủ"
            
        case .medical:
            return "Bệnh án"
            
        case .contact:
            return "Danh bạ"
            
        case .message:
            return "Tin nhắn"
            
        case .account:
            return "Tài khoản"
            
        }
    }
    
    var image: UIImage {
        switch self {
        case .home:
            return Icon.TabBar.Home
            
        case .medical:
            return Icon.TabBar.Medical
            
        case .contact:
            return Icon.TabBar.Contact
            
        case .message:
            return Icon.TabBar.Message
            
        case .account:
            return Icon.TabBar.Account
            
        }
    }
    
    func getViewController() -> UIViewController {
        switch self {
        case .home:
            return HomeViewController()
            
        case .medical:
            return MedicalHistoryViewController()
            
        case .contact:
            return ContactViewController()
            
        case .message:
            return MessageViewController()
            
        case .account:
            return AccountViewController()
            
        }
    }
}

class CustomTabbarController: UITabBarController {
    
    enum Size: CGFloat {
        case avatar = 50, padding5 = 5, padding10 = 10, label = 14
    }
    
    /// VAR
    var popover: Popover!
    var isShowPopup: Bool = false
    var alertController: UIAlertController?
    
    var willPushChangedStudent: Bool = false
    
    
    
    var ref: DatabaseReference!
    var guestRef: DatabaseReference!
    var listHandle: [(refHandle: DatabaseReference, handle: DatabaseHandle)] = []
    
    private let config = Config.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()

        guestRef = ref.child("users")
        
        
        if let userInfo = AppData.instance.myInfo {

            switch userInfo.accountType {
            case .doctor:
                 AppData.instance.tabbarItems = [.home, .contact, .message, .account]
            case .patient:
                AppData.instance.tabbarItems = [.home, .medical, .contact, .message, .account]
            }
            
        }

        setupAllSubviews()
        
//        delegate = self
        
        Utility.updateNotificaitonBadge(tabbar: self.tabBar)
        
        
        observeUsersOnline()
        

        
//        guard let data = Defaults[.WillUpdate] else { return }
//        guard let notification = Utility.getRealmNotification(data) else { return }
//
//        DatabaseSupport.instance.insertNotification(notification)
//        checkUpdateNotificaton(notification)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        

    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        

//        guard item.tag != 2 else { showPopup(); return }
//
//
//        if item.tag == 3 && Defaults[.WillUpdateNotification] && ( Date().timeIntervalSince1970 - Defaults[.LastUpdateTime] > 10.seconds )  {
//            Defaults[.WillUpdateNotification] = false
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.WillUpdateNotification..), object: nil)
//
//        }
//
//        if item.tag == 0 && AppData.instance.willReloadHome {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
//        }


    }
    
    
    
    @objc func observeUsersOnline() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB

        removeObserveOnline()
        
        
        guard let info = AppData.instance.myInfo else { return }
        
   
            
        let handle = ref.child("users").child(info.uid).child("call_info").observe(.value, with: { snapshot in
            
            print("snapshot.value Call: \(snapshot.value)")
                
            guard let data = snapshot.value as? [String : Any] else { return }
            guard let state = StateUser(rawValue: data["state"] as? Int ?? 0) else { return }
            
            guard state != info.state else { return }
            
            AppData.instance.myInfo?.state = state
            switch state {
            case .ring:
                
                guard let naviVC = self.selectedViewController as? UINavigationController, !naviVC.containController(VideoCallViewController.self) else { return }
                guard let guestID = data["user_id_call"] as? String, guestID.count > 0 else { return }
                guard let timeCall = data["time_call"] as? Double, Date().timeIntervalSince1970 - timeCall < 45.seconds else { return }
                
    
                self.guestRef.child("\(guestID)").observeSingleEvent(of: .value, with: { snapshot in
                    
                    guard let data = snapshot.value as? [String : Any] else { return }
                    
                    let signalClient = SignalingClient()
                    let webRTCClient = WebRTCClient(iceServers: self.config.webRTCIceServers)
                    //              let mainViewController = MainViewController(
                    //                signalClient: signalClient,
                    //                webRTCClient: webRTCClient)
                            
                            
                let videoVC = VideoCallViewController()
                            videoVC.signalClient = signalClient
                            videoVC.webRTCClient = webRTCClient
                
                    videoVC.hidesBottomBarWhenPushed = true
                    videoVC.guestInfo = GuestInfo(data: data)
                    videoVC.typeView = .received
                    videoVC.modalPresentationStyle = .fullScreen
//                    naviVC.pushViewController(videoVC, animated: true)
                    
                     let naVC = UINavigationController(rootViewController: videoVC)
                    naVC.modalPresentationStyle = .fullScreen
                
                    self.present(naVC, animated: true, completion: nil)

                    
                })
                
            default:
                break
            }
                
        })
            
            listHandle.append((ref.child("users").child(info.uid), handle: handle))

    }
    
    func removeObserveOnline() {
        listHandle.forEach { $0.refHandle.removeObserver(withHandle:  $0.handle) }
    }
}



