//
//  CustomTabbarController+Selector.swift
//  KidOnline
//
//  Created by Pham Huy on 11/18/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import Popover

//-------------------------------
// MARK: SELECTOR
//-------------------------------

extension CustomTabbarController {
    
    @objc func checkUpdateNotificaton(_ data: RealmNotification) {
        
        
        
        Utility.sendLogEvent(AnalyticsType.openNotification)
        if !data.isRead { Utility.sendLogEvent(AnalyticsType.uniqueOpenNotification) }
        
        
//        if AppData.instance.isCanChat && data.type == 82 {
//            
//            self.selectedViewController = self.viewControllers?[1]
//            self.tabBar.tintColor = UIColor.Navigation.mainColor()
//        }
        

        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        self.tabBar.isHidden = false
        
        data.markedAsRead()
        
        

        let notificationNotRead = DatabaseSupport.instance.getAllNotificationNotRead()
        UIApplication.shared.applicationIconBadgeNumber = notificationNotRead.count
        if let items = tabBar.items {
            for item in items {
                if item.tag == 3 {
                    if notificationNotRead.count > 99 {
                        item.badgeValue = "99+"
                    } else if notificationNotRead.count > 0 {
                        item.badgeValue = String(notificationNotRead.count)
                    } else {
                        item.badgeValue = nil
                    }
                }
            }
        }
        
        print("data.type: \(data.type)")

        switch data.type {
            
//            2    Notification bai viet => Chuyen den man hinh chi tiet bai viet
//            3    Notification xac nhan gui/cap nhat/xoa comment bai viet  => Chuyen den man hinh chi tiet bai viet focus vao comment box
//            4    Notification quan ly/giao vien chu nhiem reply lai comment bai viet => Chuyen den man hinh chi tiet bai viet focus vao comment box
            
        case 2, 3, 4:
            
            if data.type == 2 {
                self.changedStudent(data.studentID)
                if willPushChangedStudent {
                    AppData.instance.willReloadHome = true
                    willPushChangedStudent = false
                }
            }
            defaultCheckNetworkAndRequestGetPostDetail(data.termID, studentID: data.studentID, focusComment: data.type != 2)
            
            
//            5    Notification gui ket qua diem danh hoc sinh(Truong hop gui: Di muon, Vang CP, Vang KP hoac truong hop thay doi cac trang thai khi cap nhat) => Vao man hinh chi tiet hoat dong hang ngay(focus diem danh lop chu nhiem)
//            6    Notification nhan xet/dan do hang ngay lop chu nhiem(Vao man hinh chi tiet hoat dong hang ngay) => Vao man hinh chi tiet hoat dong hang ngay(focus nhan xet lop chu nhiem)
//            7    Notification gui ket qua diem danh hoc sinh(Truong hop gui: Di muon, Vang CP, Vang KP hoac truong hop thay doi cac trang thai khi cap nhat) - lop hoc - tiet => Vao man hinh chi tiet hoat dong hang ngay(focus diem danh lop hoc)
//            8    Notification nhan xet/dan do hang ngay lop hoc => Vao man hinh chi tiet hoat dong hang ngay(focus nhan xet lop hoc)
//            24    Notification diem danh xe buyt  => Vao man hinh chi tiet hoat dong hang ngay(focus theo doi xe buyt)
//            25    Notification lien quan gui hoat dong hang ngay => Vao man hinh chi tiet hoat dong hang ngay
            
        case 5, 6, 7, 8, 24, 25:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            let dailyVC: DailyViewController = DailyViewController()
        
            dailyVC.dateSelected = data.date
            naviVC.pushViewController(dailyVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
//            9    Notification thay doi thoi khoa bieu lop hoc => Den man hinh thoi khoa bieu tuan (Mo het cac ngay)
//            10    Notification  cap nhat thoi khoa bieu ngay  => Den man hinh thoi khoa bieu tuan (Mo het cac ngay)
//            11    Notification them/cap nhat noi dung hoc tuan => Den man hinh noi dung hoc cua lop (focus vao tuan chua bai hoc)
//        case 9,10, 11:
//            /// Bỏ qua do chưa có màn hình
//
            
            
//            12    Notification them/cap nhat/nhac nho deadline/thong bao diem bai tap => Den man hinh chi tiet bai tap
//        case 12:
//            /// Màn hình chi tiết bài tập
//
            
//            13    Notification cham/cap nhat diem diem bai tap  => Den man hinh chi tiet bai tap
//        case 13:
//            /// Màn hình chi tiết điểm
            
            
            
//            14    Notification open hoat dong ngoai khoa => Den man hinh chi tiet ngoai khoa
//            15    Notification thong bao tinh trang don dang ky => Den man hinh chi tiet ngoai khoa(focus vao don dang ky)
            
        case 14, 15:
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            checkNetworkAndRequestGetExtracurricularDetail(data.termID, studentID: data.studentID)
            
            
//            16    Notification them diem van minh hoc sinh => Den man hinh lich su cham diem van minh cua hoc sinh(Focus vao semester_index)
//            17    Notification them diem van minh lop hoc => Den man hinh lich su cham diem van minh cua lop hoc(Focus vao ngay cham)
        case 16, 17:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }

            let behaviorVC: BehaviorViewController = BehaviorViewController()
            behaviorVC.semesterID = data.termID
            naviVC.pushViewController(behaviorVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
            
//            18    Notification ve bao cao danh gia tien bo MOET => Den man hinh chi tiet bao cao tien bo MOET
//            19    Notification ve bao cao danh gia tien bo CAM=> Den man hinh chi tiet bao cao tien bo CAM

//        case 18, 19:
//            break
            
            
//            23    Notification nhac nho xe buyt => Vao man hinh xe buyt
        case 23:
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            let detailVC: BusViewController = BusViewController()
            naviVC.pushViewController(detailVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
            
//            26    Notification lien quan den menu => Vao man hinh thuc don
        case 26:
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            let detailVC: MenuViewController = MenuViewController()
            detailVC.dateSelected = data.date
            naviVC.pushViewController(detailVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
            
//            NOTIFICATION_BEHAVIOR_V2    27    Notification tai khoan van minh=> Den man hinh lich su cham diem van minh cua hoc sinh(Focus vao semester_index)
        case 27:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            
            guard let schoolInfo = (AppData.instance.schools.filter { $0.id == data.schoolID }).first else { return }
            guard let semester = (schoolInfo.semesterList.filter { $0.id == data.termID }).first else { return }
            
            
            let behaviorVC: BehaviorScoreViewController = BehaviorScoreViewController()
            behaviorVC.semesterSelected = semester
            naviVC.pushViewController(behaviorVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
//NOTIFICATION_TEACHER_REPLY_DAILYCOMMENT    28    Notification giao vien phan hoi nhan xet hang ngay khi phu huynh phan hoi => Focus vao man hinh phan hoi nhan xet, theo object_id la id cua nhan xet hang ngay
            
//            32    Notification thong bao den giao vien khi phu huynh comment tren thong bao
//            33    Notification thong bao den giao vien khi phu huynh comment nhan xet hang ngay
//            34    Notification thong bao den giao vien khi phu huynh comment support
            
        case 28, 32, 33, 34:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            
            let commentVC: CommentViewController = CommentViewController()
            commentVC.showLike = false
            
            switch data.type {
            case 28:
                commentVC.commentType = .daily
            
            case 32:
                commentVC.commentType = .post
                
            case 33:
                commentVC.commentType = .daily
                
            case 34:
                commentVC.commentType = .support
                
            default:
                break
            
            }
            
            
            commentVC.objectID = data.termID
            commentVC.comment = data.subTitle
            commentVC.hidesBottomBarWhenPushed = true
            naviVC.pushViewController(commentVC, animated: true)

            Defaults[.WillUpdate] = nil
            
        case 30:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            let detailVC: DetailWebViewController = DetailWebViewController()
            detailVC.title  = LocalizedString("general_label_detail", comment: "Chi tiết")
            detailVC.urlString = data.url
            naviVC.pushViewController(detailVC, animated: true)
            
            Defaults[.WillUpdate] = nil

            
        case 31:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            checkNetworkAndRequestGetSurveyDetail(data.termID, studentID: data.studentID, date: data.date)
            
            
//            35    Notification thong bao thong tin y te
//            142    Notification thong bao ve y te
        case 35, 142:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            let detailVC: MedicalBookViewController = MedicalBookViewController()
            naviVC.pushViewController(detailVC, animated: true)
            
            
            Defaults[.WillUpdate] = nil
            
//            36    Notification phe duyet ho so hoc sinh
//            37    Notification phe duyet ho so can bo nhan vien
//            38    Notification thong bao ve su kien
//            39    Notification thong bao danh gia thang cua hoc sinh
//            140    Notification cham diem van minh
//            141    Notification thong bao dang ky mua item

//            143    Notification thong bao lich thi
//            144    Notification gui thong bao quiz cho hoc sinh
            

        case 144, 148:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            checkNetworkAndRequestGetQuizzeDetail(data.termID, studentID: data.studentID)
        
            
//            145    Notification gui thong bao elearning_assignment cho hoc sinh
//            146    Notification nhac hoc sinh lam bai elearning_assignment Online
        case 145, 146, 147:

            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }

            checkNetworkAndRequestGetElAssignmentDetail(data.termID, studentID: data.studentID)
        
            
            /// 149: Đơn xin nghỉ
            
            
        case 149:
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            
            checkNetworkAndRequestGetAbsentDetail(data.termID, studentID: data.studentID)
            
            
        case 150:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            guard let commentType = CommentType(rawValue: data.objectType) else { return }
            
            let commentVC: CommentViewController = CommentViewController()
            commentVC.showLike = false
            commentVC.commentType = commentType
            commentVC.objectID = data.termID
            commentVC.comment = data.dataContent
            commentVC.dataTitle = data.dataTitle
            commentVC.hidesBottomBarWhenPushed = true
            naviVC.pushViewController(commentVC, animated: true)
            
            Defaults[.WillUpdate] = nil
            
            /// Chi tiết tài chính
        case 151, 153:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            checkNetworkAndRequestGetFinanceDetail(data.termID, studentID: data.studentID)
            
            
//        81    Notification support
            
        case 81:
            
            self.changedStudent(data.studentID)
            if willPushChangedStudent {
                AppData.instance.willReloadHome = true
                willPushChangedStudent = false
            }
            
            checkNetworkAndRequestGetSupportDetail(data.termID)

        case 82:
            
            Defaults[.WillUpdate] = nil

            guard AppData.instance.isCanChat else { return }

            if naviVC.containController(MessageDetailViewController.self) {
                naviVC.removeController(MessageDetailViewController.self)
            }

            let messageDetailVC = MessageDetailViewController()
            
            
            if let info = Utility.getUserInfoFromID(data.termID) as? GuestInfo  {
                messageDetailVC.guestInfo = info
            } else {
                messageDetailVC.guestInfo = GuestInfo(id: data.termID,
                                                      name: data.title,
                                                      avatar: data.avatar,
                                                      type: [data.objectType],
                                                      imageAvatar: nil, relationshipVN: "", relationshipEN: "", phone: "", email: "", address: "", schoolName: "", schoolID: "", lastUpdate: Date().timeIntervalSince1970, isNewMessage: true)
            }
            
            messageDetailVC.hidesBottomBarWhenPushed = true
            naviVC.pushViewController(messageDetailVC, animated: false)
            
            break
            
            
        default:
            
            showAlertControllerDefault(.showNotification(data))
            Defaults[.WillUpdate] = nil
        }

   
    }
    
    
    /*
     *  Cập nhật lại số lượng thông báo
     */
    @objc func updateNumberNotification(_ notification: Notification) {
        
        guard let data = notification.userInfo else { return  }
        guard let number = data["data"] as? Int else {  return }

        if let items = tabBar.items {
            for item in items {
                if item.tag == 3 {
                    if number > 99 {
                        item.badgeValue = "99+"
                    } else if number > 0 {
                        item.badgeValue = String(format: "%d", number)
                    } else {
                        item.badgeValue = nil
                    }
                }
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = number
    }
    
   
    @objc func changedStudent(_ studentID: Int) {
        
        guard studentID != 0  else { return }
        guard studentID != Defaults[.StudentID] else { return }
        let student = AppData.instance.studentList.filter { $0.id == studentID  }.first
        guard let studentInfo = student else { return }
        
        
        Defaults[.StudentID]    = studentInfo.id
        Defaults[.ClassID]      = studentInfo.classID
        Defaults[.SchoolID]     = studentInfo.schoolID
        AppData.instance.modules = studentInfo.modules.sorted { $0.indexOrder < $1.indexOrder }

        AppData.instance.posts = []
        AppData.instance.postUpdateLastTime = 0
        AppData.instance.willShowLoadAddAlbum = true
//        setupTabbar()
        
        willPushChangedStudent = true
        

 
        labelName.text = getStringName(studentInfo.fullName, stringTwo: studentInfo.lastName)
        
        if let image = studentInfo.imageAvatar {
            avatar.image = image
        } else {
            avatar.downloadedFrom(link: studentInfo.avatar, placeHolder: Icon.Account.AvatarDefault) { image in
                studentInfo.imageAvatar = image
            }
        }
        

        
        
        guard let naviVC = self.selectedViewController as? UINavigationController else { return }
        naviVC.popToRootViewController(animated: true)
        
//        if let student = Utility.getStudentFromID(Defaults[.StudentID]), student.schoolInfo.isLocked {
//
//            guard UIApplication.shared.applicationState == .active else { return }
//
//            showAlertControllerDefault(.schoolLocked(String(format: LocalizedString("home_label_school_is_locked", comment: "Trường của bé %@ hiện đang tạm ngưng sử dụng dịch vụ."), student.name)))
//
//        }
        
    }
    
    
    func setupNotificationDefault() {
        
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(self.update(_:)),
                                                         name: Notification.Name(rawValue: AppNotification.WillUpdate..),
                                                         object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadItemTabbar(_:)),
                                               name: Notification.Name(rawValue: AppNotification.ReloadItemTabbar..),
                                               object: nil)
    }
    
    
    @objc func reloadItemTabbar(_ sender: Foundation.Notification) {
        setupTabbar()
    }
    
    
    @objc func update(_ notification: Foundation.Notification) {
        
        guard let userInfo = notification.userInfo else { return }
        guard let data = userInfo["data"] as? RealmNotification else { return }
        
        let notificationNotRead: [RealmNotification] = DatabaseSupport.instance.getAllNotificationNotRead()
        UIApplication.shared.applicationIconBadgeNumber = notificationNotRead.count
        if let items = tabBar.items {
            for item in items {
                if item.tag == 3 {
                    if notificationNotRead.count > 99 {
                        item.badgeValue = "99+"
                    } else if notificationNotRead.count > 0 {
                        item.badgeValue = String(format: "%d", notificationNotRead.count)
                    } else {
                        item.badgeValue = nil
                    }
                }
            }
        }
        
        guard AppData.instance.showAlertWhenNotification else {
            checkUpdateNotificaton(data)
            return
        }
        showAlertControllerDefault(.newUpdate(data))
    }
}


//=========================================
// MARK: - PRIVATE METHOD
//========================================

extension CustomTabbarController {
    
    /**
     Show popup chọn học sinh
     */
    
    @objc func showPopup() {
        
        let tablePopover = setupTable()
        
        let popoverOption: [PopoverOption] = [
            .type(.up),
            .blackOverlayColor(UIColor.black.alpha(0.3)),
            .arrowSize(CGSize(width: 12, height: 0)),
            .cornerRadius(5),
            //            .animationIn(0.35),
            //            .animationOut(0.3),
            .sideEdge(0)
        ]
        
        popover = Popover(options: popoverOption, showHandler: {
            self.isShowPopup = true
        }, dismissHandler: {
            self.isShowPopup = false
        })
        popover.show(tablePopover, point: CGPoint(x: view.frame.width / 2 , y: tabBar.frame.minY + bgAvatar.frame.minY))
    }
    
    func getStringName(_ stringOne: String, stringTwo: String?) -> String {
        guard let string = stringTwo, string.count > 0 else { return stringOne }
        return string
    }
    
    func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: 40, height: 40 * image.size.height / image.size.width) }
        else { newSize = CGSize(width: 40 * image.size.width / image.size.height, height: 40) }
        
        //        UIGraphicsBeginImageContext(newSize)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
}
