//
//  EmptyTableDataSource.swift
//  G5TaxiUser
//
//  Created by NangND on 10/5/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import PHExtensions


protocol EmptyTableDataSourceDelegate: class {
    func getEmptyTableCellHeight() -> CGFloat
}


class EmptyTableDataSource: NSObject {
    enum EmptyTableType: Int {
        case post = 0
        case reference
        case absentRequest
        case message
        case notification
        case behaviorScore
        case receipt
        

        func toInfo() -> (title: String, image: UIImage) {
            switch self {
            case .post:
                return (LocalizedString("empty_table_now_not_post", comment: "Hiện chưa có bài viết nào"), Icon.EmptyTable.Notification )
                
            //TODO: Dịch
            case .reference:
                return (LocalizedString("empty_table_now_not_reference", comment: "Hiện nhà trường chưa có quy định nào"), Icon.EmptyTable.Notification )
                
            case .absentRequest:
                return (LocalizedString("empty_table_today_not_absent_request", comment: "Hôm nay không có đơn xin nghỉ học."), Icon.EmptyTable.Notification)
                
            case .message:
                return (LocalizedString("empty_table_function_updating", comment: "Chức năng đang cập nhật"), Icon.EmptyTable.Notification)
                
            case .notification:
                return (LocalizedString("empty_table_function_notificaiton", comment: "Hiện tại, chưa có thông báo nào"), Icon.EmptyTable.Notification)
                
            case .behaviorScore:
                return (LocalizedString("empty_table_function_behavior_score", comment: "Tháng này chưa có điểm văn minh."), Icon.EmptyTable.Notification)
                
            case .receipt:
                return (LocalizedString("empty_table_function_receipt", comment: "Bạn chưa có hóa đơn nào") , Icon.EmptyTable.Notification)
            }

        }
    }
    
    var tableType: EmptyTableType = .post
    weak var delegate: EmptyTableDataSourceDelegate?
}


//MARK: UITableViewDataSource
extension EmptyTableDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.allowsSelection = false
//        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
      
//        tableView.backgroundColor = UIColor.white
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "EmptyCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! EmptyImageTableViewCell
        configureEmptyImageCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureEmptyImageCell(_ cell: EmptyImageTableViewCell, atIndexPath indexPath: IndexPath) {
        cell.imageView?.image = tableType.toInfo().image
        cell.textLabel?.text = tableType.toInfo().title
        cell.backgroundColor = .clear
    }
    
}

//MARK: UITableViewDelegate

extension EmptyTableDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let height = delegate?.getEmptyTableCellHeight()
        if height == nil { return UIScreen.main.bounds.height - 100 }
        return height!

    }
}
