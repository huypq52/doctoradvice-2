//
//  CustomViewController+Alert.swift
//  KidOnline
//
//  Created by Pham Huy on 11/17/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//
//
//import UIKit
////import PHExtensions
//import SwiftyUserDefaults
//
//extension CustomViewController {
//    
//    enum AlertTypeDefault {
//        case NewUpdatePost(UpdatePost)
//        
//        case NewUpdateAlbum(UpdatePost)
//        
//        func alertTitle() -> String {
//            switch self {
//            case .NewUpdatePost:
//                return "Có bài viết mới"
//                
//            default:
//                return LocalizedString("alert_title_message", comment: "Thông báo")
//            }
//        }
//        
//        func alertMessage() -> String {
//            switch self {
//            case .NewUpdatePost(let updatePost):
//                return updatePost.title
//                
//            default:
//                return ""
//                
//            }
//        }
//    }
//    
//    
//    
//    /**
//     Show Alert
//     */
//    
//    internal func showAlertControllerDefault(type: AlertTypeDefault) {
//        self.dismissViewControllerAnimated(false, completion: nil)
//        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .Alert)
//        alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .Cancel, handler: nil))
//        
//        switch type {
//            
//        case .NewUpdatePost(let updatePost):
//            /**
//             Đồng ý
//             */
//            alertController?.addAction(UIAlertAction(title: "Xem ngay", style: .Default, handler: { _ in
//                self.defaultCheckNetworkAndRequestGetPostDetail(updatePost.postID, studentID: updatePost.studentID)
//            }))
//            
//        default:
//            break
//        }
//        self.presentViewController(alertController!, animated: true, completion: nil)
//    }
//    
//}
//
////-------------------------------
//// MARK: PRIVATE METHOD DEFAULT
////-------------------------------
//
//extension CustomViewController {
//    
//    
//    func setupNotificationDefault() {
//        NotificationCenter.default.addObserver(self,
//                                                         selector: #selector(self.updatePost),
//                                                         name: Notification.WillUpdatePost..,
//                                                         object: nil)
//    }
//    
//    func setupRemoveNotificationDefault() {
////        NotificationCenter.default.removeObserver(self, name: Notification.WillUpdatePost.., object: nil)
//        
//    }
//    
//    func updatePost(notification: NSNotification) {
//        
//        guard let userInfo = notification.userInfo else { return }
//        guard let updatePost = userInfo["data"] as? UpdatePost else { return }
//        showAlertControllerDefault(.NewUpdatePost(updatePost))
//    }
//}
//
//
//
////-------------------------------
//// MARK: REQUEST HTTP DEFAULT
////-------------------------------
//extension CustomViewController {
//    
//    //----------------------------------------
//    // - MARK: GET POST LIST
//    //----------------------------------------
//    
//    internal func defaultCheckNetworkAndRequestGetPostDetail(postID: Int, studentID: Int = Defaults[.StudentID]) {
//        
//        /* thông báo cho người dùng không có mạng */
//        guard networkReachable() else {
//            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
//                comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
//                            position: .center,
//                            onView: self.view)
//            return
//        }
//        
//        
//        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
//        HUD.showHUD(onView: self.view) {
//            HTTPManager.instance.getPostDetail
//                .doRequest(HTTPGetPostDetail.RequestType(studentID: studentID, postID: postID), headers: headers)
//                .completionHandler { result in
//                    switch result {
//                    case .success(let respond):
//                        self.getPostDetailRequestSuccess(respond)
//                        
//                    case .failure(let error):
//                        self.getPostDetailRequestFaild(error)
//                        break
//                    }
//            }
//        }
//    }
//    
//    /**
//     Request lấy danh sách bài viết thành công
//     */
//    private func getPostDetailRequestSuccess(respond: HTTPGetPostDetail.RespondType) {
//        
//        Defaults[.WillUpdatePostID] = 0
//        
//        HUD.dismissHUD()
//
//        let postDetailVC = PostDetailViewController()
//        postDetailVC.functionInfo = MainFunction.listArticle.toInfo()
//        postDetailVC.post = respond.post
//        postDetailVC.comments = respond.comments
//        navigationController?.pushViewController(postDetailVC, animated: true)
//        
//        
//        
////        navcon = (UINavigationController*)myTabBarController.selectedViewController;
////        [navcon pushViewController:someViewController animated:YES];
//        
////        let naviVC = tabBarController?.selectedViewController as? UINavigationController
////        naviVC?.pushViewController(postDetailVC, animated: true)
//    }
//    
//    /**
//     Request lấy danh sách bài viết thất bại
//     */
//    private func getPostDetailRequestFaild(error: ErrorType) {
//        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
//    }
//    
//
//    
//}
