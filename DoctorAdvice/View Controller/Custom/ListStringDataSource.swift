//
//  ListStringDataSource.swift
//  KO4Teacher
//
//  Created by Pham Huy on 8/28/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

protocol ListStringDataSourceDelegate: class {
    func didSelectedString(_ type: ListStringType, text: String, indexPath: IndexPath)
}

enum ListStringType: Int {
    case amount = 0, note
}

class ListStringDataSource: NSObject {

    init(listString: [String], type: ListStringType) {
        super.init()
        
        self.listString = listString
        self.type = type
    }
    
    var type: ListStringType = .note
    var listString: [String]?
    weak var delegate: ListStringDataSourceDelegate?
}

extension ListStringDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        guard let listString = listString, listString.count > 0 else { return 44}
        
        let height = Utility.heightForView(listString[indexPath.row],
                                           font:  UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!,
                                           width: tableView.frame.width - 15 * 2) + 10
        
        return max(44, height)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let listString = listString, listString.count > 0 else { return }
        delegate?.didSelectedString(type, text: listString[indexPath.row], indexPath: indexPath)
    }
}

extension ListStringDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = listString {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentify = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! SeperatorTableViewCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: SeperatorTableViewCell, indexPath: IndexPath) {
        guard let listString = listString, listString.count > 0 else { return }
        
        cell.textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!
        cell.textLabel?.text = listString[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
    }
    
}
