//
//  CustomViewController.swift
//  KO4Teacher
//
//  Created by Pham Huy on 9/7/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import CleanroomLogger
//import PHExtensions


class CustomViewController: UIViewController {
    
    /// View
    var table: UITableView = UITableView()
    var alertController: UIAlertController?
    var textViewSelected: UITextView?
    
    /// Var
    var willHiddenBar: Bool = false
    var isFirst: Bool = true
    var isBack: Bool = false
    
    var didSetupConstraints: Bool = false
    var dateSelected: TimeInterval = Date().timeIntervalSince1970
    var scrollHeight: CGFloat = 0
        
    
     var footerBackground: UIImageView!
    
    var dateFormatterDefault: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        willHiddenBar = navigationController?.navigationBar.isHidden ?? false
    }
    

    
    deinit {
        print( "-------------------------------------------------")
        print( "\t \t \(type(of: self)) DE-INIT")
        print( "-------------------------------------------------")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        print( "-------------------------------------------------")
        print( "\t \t \(type(of: self)) REQUIRED INIT")
        print( "-------------------------------------------------")
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        print( "-------------------------------------------------")
        print( "\t \t \(type(of: self)) INIT")
        print( "-------------------------------------------------")

    }
    
    
    /**
     Setup Bar Button
     */
    func setupNotificationObserver(_ selectorShow: Selector, selectorHide: Selector) {
        NotificationCenter.default.addObserver(self,
                                                         selector: selectorShow,
                                                         name: UIResponder.keyboardWillShowNotification,
                                                         object: nil)
        
        NotificationCenter.default.addObserver(self,
                                                         selector: selectorHide,
                                                         name: UIResponder.keyboardWillHideNotification,
                                                         object: nil)
    }
    
    /**
     Setup Bar Button
     */
    func setupBackBarButton(_ image: UIImage, selector: Selector, target: AnyObject? = nil) -> UIBarButtonItem {
        let button = UIBarButtonItem(image: image, style: .plain, target: target, action: selector)
        button.tintColor = .white
        return button
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.register(cellClass, forCellReuseIdentifier: "Cell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = UIColor.Table.tablePlainColor()
        return table
    }
    
    func setupBackground(_ color: UIColor = .white) {
        
        view.backgroundColor = color
        
        
        #if VINSCHOOL
        footerBackground = UIImageView(image: UIImage(named: "ImageCould") ?? UIImage())
        footerBackground.alpha = 0.2
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .bottom
        } else {
            footerBackground.contentMode = .bottom
        }

        #else
        footerBackground = UIImageView(image: UIImage(named: "ImageCould")?.tint(UIColor.Navigation.mainColor()) ?? UIImage())
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .scaleAspectFill
        } else {
            footerBackground.contentMode = .scaleAspectFill
        }
        #endif
        

        

        
        view.addSubview(footerBackground)
    }
    
    func setupBackgroundFill() {
        
        view.backgroundColor = UIColor.Navigation.mainColor()
        
        
        #if VINSCHOOL
        footerBackground = UIImageView(image: UIImage(named: "ImageCould") ?? UIImage())
        footerBackground.alpha = 0.2
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .bottom
        } else {
            footerBackground.contentMode = .bottom
        }

        #else
        footerBackground = UIImageView(image: UIImage(named: "ImageCould")?.tint(.white) ?? UIImage())
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .scaleAspectFill
        } else {
            footerBackground.contentMode = .scaleAspectFill
        }
        
        #endif
        


        

        
        
        view.addSubview(footerBackground)
    }
}
