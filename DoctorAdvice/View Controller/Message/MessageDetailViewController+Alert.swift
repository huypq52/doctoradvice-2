//
//  MessageDetailViewController+Alert.swift
//  KO4Teacher
//
//  Created by Pham Huy on 2/28/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import Foundation
//import PHExtensions
import UIKit

extension MessageDetailViewController {
    
    enum AlertType {
        case selectionImage
        case callGuest(String, String)
        
        func alertTitle() -> String {
            switch self {
            case .selectionImage :
                return LocalizedString("notification_general_button_selection_image", comment: "Chọn ảnh")
                
            default:
                return LocalizedString("alert_title_confirm", comment: "Xác nhận")
            }
        }
        
        func alertMessage() -> String? {
            switch self {
            case .selectionImage:
                return nil
                
            case .callGuest(let string, _):
                return String(format: LocalizedString("notification_gerenal_you_want_call", comment: "Bạn có muốn gọi cho %@"), string)
                
            }
        }
    }
    
    /**
     Show Alert
     */
    
    /**
     Show Alert tùy theo type
     */
    func showAlertController(_ type: AlertType) {
        
        self.dismiss(animated: false, completion: nil)
        
        switch type {
        case .selectionImage:
            alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .actionSheet)
        default:
            alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        }
        
        
        
        
        
        guard let alertController = alertController else { return }
        
        alertController.addAction(UIAlertAction(title: LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
        
        switch type {
        case .selectionImage:
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_library", comment: "Thư viện"), style: .default, handler: {[unowned self] _ in
                self.showImagePicker(.photoLibrary)
            }))
            
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_camera", comment: "Camera"), style: .default, handler: {[unowned self] _ in
                self.showImagePicker(.camera)
            }))
            
            
            
        case .callGuest(_, let phone):
            
            /**
             Đồng ý
             */
            alertController.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                
                Utility.callPhoneNumber(phone)
            }))
            
        }
    
        
        if UI_USER_INTERFACE_IDIOM() != .phone {
            if let popoverController = alertController.popoverPresentationController {
                // show action sheet
                popoverController.sourceRect = CGRect(x: 0, y: 0, width: view.frame.width , height: 0)
                popoverController.sourceView = self.view
            }
        }
        present(alertController, animated: true, completion: nil)
    }
    
}
