//
//  MessageSearchDataSource.swift
//  KO4Teacher
//
//  Created by Pham Huy on 3/2/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit


//-------------------------------------------
//MARK: - BOOKING SEARCH DATASOURCE PROTOCOL
//-------------------------------------------
protocol MessageSearchDataSourceDelegate: class {
    func didSelectedChannel(_ channel: Channel)
}

//-------------------------------------------
//MARK: - BOOKING SEARCH DATASOURCE CLASS
//-------------------------------------------
class MessageSearchDataSource: NSObject {
    
    var dateTimeFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd/MM/yyyy"
        return formatter
    }()
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, headerSection = 35
    }
    
    fileprivate enum Section: Int {
        case teacher = 0,  guardian
        
        var title: String {
            get {
                switch self {
                case .teacher:
                    return LocalizedString("general_label_teacher", comment: "Giáo viên")
                    
                case .guardian:
                    return LocalizedString("general_label_parent", comment: "Phụ huynh")
                    
                }
            }
        }
    }
    
    
    var data: [AnyObject]?
    var searchResult: [AnyObject]?
    
    var channelsResult: [Channel] = []
    
    weak var delegate: MessageSearchDataSourceDelegate?
}


//-------------------------------------------
//MARK: - UITABLE VIEW DATASOURCE
//-------------------------------------------
extension MessageSearchDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return channelsResult.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MessageCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MessageTableViewCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: MessageTableViewCell, atIndexPath indexPath: IndexPath) {
        
        guard let info = AppData.instance.myInfo else { return }
        
        let channel = channelsResult[indexPath.row]
        
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = channel.roomName
        cell.detailTextLabel?.text = nil
        
        if Date().timeIntervalSince1970 - channel.time > 1.days {
            cell.buttonTime.setTitle(dateTimeFormatter.string(from: Date(timeIntervalSince1970: channel.time)), for: .normal)
            
        } else {
            cell.buttonTime.setTitle(Utility.stringFromPastTimeToText(channel.time), for: .normal)
        }
        
        if let content = channel.content, content.count > 0 {
            cell.detailTextLabel?.text = channel.content
        }
        else if let photo = channel.photo, photo.count > 0 {
            if info.uid == channel.senderID {
                cell.detailTextLabel?.text = LocalizedString("message_label_you_send_one_image", comment: "Bạn đã gửi 1 ảnh")
            } else {
                cell.detailTextLabel?.text = LocalizedString("message_label_you_revice_one_image", comment: "Đã gửi bạn 1 ảnh")
            }
            
            
        }
        
        cell.unread = false
        if channel.senderID != info.uid {
            cell.unread = channel.unread
        }
        
        
        
        guard let guestInfo = Utility.getUserInfoFromID(Utility.getIdFromRoomName(channel.roomName)) as? GuestInfo else { return  }
        
        cell.textLabel?.text = guestInfo.name
        cell.imageView?.downloadedFrom(link: guestInfo.avatar, placeHolder: Icon.Account.AvatarDefault)
        cell.isOnline = guestInfo.isOnline
        

        
    }
}


//-------------------------------------------
//MARK: - UITABLE VIEW DELEGATE
//-------------------------------------------

extension MessageSearchDataSource: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        delegate?.didSelectedChannel(channelsResult[indexPath.row])
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
   }






