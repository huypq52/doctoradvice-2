//
//  MessageViewController.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import Firebase
import SwiftyUserDefaults

struct Channel {
    let id: String
    let senderID: String
    let roomName: String
    var content: String?
    var photo: String?
    var time: TimeInterval
    var unread: Bool
    
    func name() -> String {
        
        if let guestInfo = Utility.getUserInfoFromID(Utility.getIdFromRoomName(self.roomName)) as? GuestInfo {
            return guestInfo.name
        }
        
        return roomName
    }
}

class MessageViewController: CustomViewController {
    
    var dateTimeFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd/MM/yyyy"
        return formatter
    }()
    
    enum Section: Int {
        case newChannel = 0, currentChannel
    }
    
    var searchBar: UIView!
    var searchController: UISearchController!
    fileprivate var searchDataSource: MessageSearchDataSource!
    
    var addButton: UIBarButtonItem!
    var back: UIBarButtonItem!
    
    var emptyDataSouce: EmptyTableDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
    }
    

    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension MessageViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.instance.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MessageCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MessageTableViewCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: MessageTableViewCell, atIndexPath indexPath: IndexPath) {
        
        guard let info = AppData.instance.myInfo else { return }
        
        let channel =  AppData.instance.channels[indexPath.row]
        
        cell.unread = false
        cell.isOnline = false
        cell.textLabel?.text = channel.name()
        cell.detailTextLabel?.text = nil
        
        
        if Date().timeIntervalSince1970 - channel.time > 1.days {
            cell.buttonTime.setTitle(dateTimeFormatter.string(from: Date(timeIntervalSince1970: channel.time)), for: .normal)
            
        } else {
            cell.buttonTime.setTitle(Utility.stringFromPastTimeToText(channel.time), for: .normal)
        }
        
        if let content = channel.content, content.count > 0 {
            cell.detailTextLabel?.text = channel.content
        }
        else if let photo = channel.photo, photo.count > 0 {
            if info.uid == channel.senderID {
                cell.detailTextLabel?.text = LocalizedString("message_label_you_send_one_image", comment: "Bạn đã gửi 1 ảnh")
            } else {
                cell.detailTextLabel?.text = LocalizedString("message_label_you_revice_one_image", comment: "Đã gửi bạn 1 ảnh")
            }
        }
        
        if channel.senderID != info.uid { cell.unread = channel.unread }
        
        guard let guestInfo = Utility.getUserInfoFromID(Utility.getIdFromRoomName(channel.roomName)) as? GuestInfo else { return  }

        cell.isOnline = guestInfo.isOnline
        cell.imageView?.downloadedFrom(link: guestInfo.avatar, placeHolder: Icon.Account.AvatarDefault)
    }
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension MessageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        AppData.instance.channels[indexPath.row].unread = false
        table.reloadRows(at: [indexPath], with: .none)
        updateNumberNotification()
        
        guard let guestInfo = Utility.getUserInfoFromID(Utility.getIdFromRoomName(AppData.instance.channels[indexPath.row].roomName)) as? GuestInfo else { return }
        
        
        let messageDetailVC = MessageDetailViewController()
        messageDetailVC.guestInfo = guestInfo
        navigationController?.pushViewController(messageDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//====================================
// MARK: - TABLE DELEGATE
//====================================

extension MessageViewController: EmptyTableDataSourceDelegate {
    func getEmptyTableCellHeight() -> CGFloat {
        return table.frame.height
    }
}

//-------------------------------------------
//MARK: - SEARCH CONTROLLER DELEGATE
//-------------------------------------------

extension MessageViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        
        searchController.searchBar.setShowsCancelButton(true, animated: false)
        let view = searchController.searchBar.subviews[0]
        for item in view.subviews {
            if let textField = item as? UITextField {
                textField.autocorrectionType = .no
                textField.autocapitalizationType = .none
                textField.returnKeyType = .search
            }
            
            if let button = item as? UIButton {
                button.setTitle(LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), for: UIControl.State())
                button.setTitleColor(UIColor.white, for: UIControl.State())
                button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
            }
        }
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        searchController.searchBar.resignFirstResponder()
        
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        
    }
}


//-------------------------------------------
//MARK: - SEARCH BAR DELEGATE
//-------------------------------------------

extension MessageViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        
        var channelResult: [Channel] = []
        for channel in AppData.instance.channels {
            if Utility.normalizeString(channel.name()).contains(Utility.normalizeString(searchText)) {
                channelResult.append(channel)
            }
        }
        
        searchDataSource.channelsResult = channelResult
        
        
        
        guard let searchResultsController = searchController.searchResultsController as? UITableViewController else { return }
      
        searchResultsController.tableView.reloadData()

    }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
}

//-------------------------------------------
//MARK: - CONTACT SEARCH DELEGATE
//-------------------------------------------
extension MessageViewController: MessageSearchDataSourceDelegate {
    
    func didSelectedChannel(_ channel: Channel) {
        

        guard let index = AppData.instance.channels.firstIndex(where: { $0.id == channel.id }) else { return  }
        
        AppData.instance.channels[index].unread = false
        table.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        
        
        let messageDetailVC = MessageDetailViewController()
        if let guestInfo = Utility.getUserInfoFromID(Utility.getIdFromRoomName(channel.roomName)) as? GuestInfo {
            messageDetailVC.guestInfo = guestInfo
            navigationController?.pushViewController(messageDetailVC, animated: true)
            
        }

        
    }
}

//--------------------------------
// MARK: - SELECTOR
//--------------------------------

extension MessageViewController {
    @objc func addChannel(_ sender: UIBarButtonItem) {
        
//        guard let classInfo = AppData.instance.classList.first else { return }
//
//
        let contactVC = ContactViewController()

        contactVC.contactType = .message
        contactVC.delegate = self
        let naviVC = UINavigationController(rootViewController: contactVC)
        Utility.configureAppearance(navigation: naviVC, tintColor: UIColor.Navigation.mainColor())
        present(naviVC, animated: true, completion: nil)
        
    }
    
    @objc func reloadMessage(_ sender: Notification) {
        table.reloadData()
    }
    
    @objc func back(_ sender: UIBarButtonItem) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
}

//--------------------------------
// MARK: - PRIVATE METHOD
//--------------------------------
extension MessageViewController {
    
    func updateNumberNotification() {
        guard let info = AppData.instance.myInfo else { return }
        let notificationNotRead = (AppData.instance.channels.filter { $0.unread == true }.filter{ $0.senderID != info.uid }).count
        if let items = self.tabBarController?.tabBar.items {
            for item in items {
                if item.tag == 2 {
                    if notificationNotRead > 99 {
                        item.badgeValue = "99+"
                    } else if notificationNotRead > 0 {
                        item.badgeValue = String(notificationNotRead)
                    } else {
                        item.badgeValue = nil
                    }
                }
            }
        }
        
    }
    
}

extension MessageViewController: ContactViewControllerDelegate {
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    func didSelectedGuestInfo(_ info: GuestInfo) {
        
        dismiss(animated: false, completion: nil)
        let messageDetailVC = MessageDetailViewController()
        messageDetailVC.guestInfo = info
        navigationController?.pushViewController(messageDetailVC, animated: true)
    }
}

//--------------------------------
// MARK: - SETUP
//--------------------------------

extension MessageViewController {
    fileprivate func setupAllSubviews() {
        
        title = "Tin nhắn"
        view.backgroundColor = .white
        
        addButton = setupBarButton(image: Icon.General.Add, selector: #selector(self.addChannel(_:)))
        navigationItem.rightBarButtonItem = addButton
        
        
        setupTable()
        
        
        setupDataSource()
        setupSearchController()
        setupSearchBar()
        setupNotification()
        
        if #available(iOS 11.0, *) {
            searchBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 56))
            
        } else {
            searchBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        }
        
        searchBar.addSubview(searchController.searchBar)
        searchBar.clipsToBounds = true
        searchController.searchBar.sizeToFit()
        
        
        table.tableHeaderView = searchBar
        
        
        emptyDataSouce = EmptyTableDataSource()
        emptyDataSouce.tableType = .message
        emptyDataSouce.delegate = self

    }
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadMessage(_:)),
                                               name: NSNotification.Name(rawValue: AppNotification.ReloadMessage..),
                                               object: nil)
    }
    
    func setupAllConstraints() {

        table.snp.makeConstraints { make in
            make.bottom.top.leading.trailing.equalTo(view)
        }
    }
    
    
    func setupBarButton(image: UIImage, selector: Selector) -> UIBarButtonItem {
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: selector)
        button.tintColor = .white
        return button
    }
    
    fileprivate func setupTable() {
        
        table = UITableView(frame: .zero, style: .grouped)
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.register(EmptyImageTableViewCell.self, forCellReuseIdentifier: "EmptyCell")
        table.register(MessageTableViewCell.self, forCellReuseIdentifier: "MessageCell")
        view.addSubview(table)
    }
    
    /**
     Setup DataSource
     */
    fileprivate func setupDataSource() {
        searchDataSource = MessageSearchDataSource()
        searchDataSource.delegate = self
    }
    
    /**
     Setup Search Controller
     */
    fileprivate func setupSearchController() {
        
        let searchResultsController = UITableViewController(style: .grouped)
        searchResultsController.tableView = setupTable(dataSource: searchDataSource, cellClass: MessageTableViewCell.self)
        
        searchController = CustomSearchController(searchResultsController: searchResultsController)
        searchController.delegate = self
        //        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
    }
    
    /**
     Setup Search Bar
     */
    fileprivate func setupSearchBar() {
        
//        searchController.searchBar.placeholder = LocalizedString("search_bar_placeholder", comment: "Tìm kiếm")
//        searchController.searchBar.searchBarStyle = .prominent
//        searchController.searchBar.tintColor = UIColor.Navigation.backgroundColor()
//        searchController.searchBar.backgroundColor = UIColor.lightGray
//        searchController.searchBar.delegate = self
//        searchController.searchBar.sizeToFit()
//        searchController.searchBar.isTranslucent = false
//
//        searchController.searchBar.layer.borderWidth = onePixel()
//        searchController.searchBar.layer.borderColor = UIColor.Navigation.mainColor().cgColor
//        searchController.searchBar.barTintColor = UIColor.Navigation.mainColor()
        
        searchController.searchBar.placeholder = LocalizedString("search_bar_placeholder", comment: "Tìm kiếm")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.tintColor = UIColor(rgba: "#d8d8d8")
        searchController.searchBar.backgroundColor = UIColor.lightGray
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.isTranslucent = false
        
        searchController.searchBar.layer.borderWidth = onePixel()
        searchController.searchBar.layer.borderColor = UIColor(rgba: "#d8d8d8").cgColor
        searchController.searchBar.barTintColor = UIColor(rgba: "#d8d8d8")
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.register(cellClass, forCellReuseIdentifier: "MessageCell")
        table.register(ContactTableViewCell.self, forCellReuseIdentifier: "ContactCell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = UIColor.Table.tableGroupColor()
        
        return table
    }
    
}
