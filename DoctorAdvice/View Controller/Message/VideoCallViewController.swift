//
//  VideoCallViewController.swift
//  DoctorAdvice
//
//  Created by Nguyen Nang on 1/15/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit
import SwiftyUserDefaults
import MessageUI
import WebRTC
import Firebase
import AVFoundation
import SnapKit


class VideoCallViewController: CustomViewController {
    
    enum SoundType: Int {
        case calling = 0, ring, busy, disconnect
        
        var soundName: String {
            switch self {
            case .calling:
                return "phone-calling"
                
            case .ring:
                return "phone-ring"
                
            case .busy:
                return "phone-busy"
                
            case .disconnect:
                return "phone-disconnect"
            }
        }
    }
    
    enum TypeView: Int {
        case call = 0, received
    }
    
    enum StateView {
        case connecting
        case ring
        case calling
        case time(Double)
        case lostConnect
        
        
        var title: String {
            switch self {
            case .connecting:
                return "Đang kết nối ..."
                
            case .ring:
                return "Đang đổ chuông ..."
                
            case .calling:
                return "Đang gọi"
                
            case .time(_):
                return ""
                
            case .lostConnect:
                return "Mất kết nối!"
            }
        }
    }
    
    enum Size: CGFloat {
        case avatar = 56, button = 54
    }
    

    var bgView: UIImageView!
    var headerView: VideoCallHeaderView!
    var footerView: VideoCallFooterView!
    var debugView: VideoCallDebugView!
    
    
    var bottomFooterViewConstraint: Constraint!
    var topHeaderViewConstraint: Constraint!
    
    var localVideoView: UIView!
    var remoteVideoView: UIView!
    
    #if arch(arm64)
    
    var localRenderer: RTCMTLVideoView? = nil
    
        // Using metal (arm64 only)
//        localRenderer = RTCMTLVideoView(frame: self.localVideoView.frame )
//        let remoteRenderer = RTCMTLVideoView(frame: self.remoteVideoView.frame)
//        localRenderer?.videoContentMode = .scaleAspectFill
//        remoteRenderer.videoContentMode = .scaleAspectFill
    #else
    
    var localRenderer: RTCEAGLVideoView? = nil
        // Using OpenGLES for the rest
//        localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
//        let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
    #endif
    
    

    var guestInfo: GuestInfo!
    
    var signalClient: SignalingClient!
    var webRTCClient: WebRTCClient!
    
    var typeView: TypeView = .call
    
    var ref: DatabaseReference!
       
    var guestRef: DatabaseReference!
    
    
    private var currentPerson = ""
    private var oppositePerson = ""
    
    private var signalingConnected: Bool = false {
       didSet {
        
        print(signalingConnected ? "Connected" : "Not connected")
        
         DispatchQueue.main.async {
           if self.signalingConnected {
            
            self.audioPlayer?.stop()
            
//            self.debugView.signalingStatusLabel.text = "Connected"
//             self.debugView.signalingStatusLabel.textColor = UIColor.green

//            self.footerView.buttonVideo.sendActions(for: .touchUpInside)
           }
           else {
//             self.debugView.signalingStatusLabel.text = "Not connected"
//             self.debugView.signalingStatusLabel.textColor = UIColor.red

//            self.footerView.buttonEndCall.sendActions(for: .touchUpInside)
           }
         }
       }
     }
     
     private var hasLocalSdp: Bool = false {
       didSet {
         DispatchQueue.main.async {
//           self.localSdpStatusLabel?.text = self.hasLocalSdp ? "✅" : "❌"
            
            
            self.debugView.localSdpStatusLabel.text = self.hasLocalSdp ? "✅" : "❌"
            
            print("hasLocalSdp: \(self.hasLocalSdp)")
         }
       }
     }
     
     private var localCandidateCount: Int = 0 {
       didSet {
         DispatchQueue.main.async {
//           self.localCandidatesLabel?.text = "\(self.localCandidateCount)"
            
            self.debugView.localCandidatesLabel.text = "\(self.localCandidateCount)"
         }
       }
     }
     
     private var hasRemoteSdp: Bool = false {
       didSet {
         DispatchQueue.main.async {
//           self.remoteSdpStatusLabel?.text = self.hasRemoteSdp ? "✅" : "❌"
            
            self.debugView.remoteSdpStatusLabel.text = self.hasRemoteSdp ? "✅" : "❌"
            
            self.footerView.buttonAnswer.isUserInteractionEnabled = self.hasRemoteSdp
            print("hasRemoteSdp: \(self.hasRemoteSdp)")
         }
       }
     }
     
     private var remoteCandidateCount: Int = 0 {
       didSet {
         DispatchQueue.main.async {
//           self.remoteCandidatesLabel?.text = "\(self.remoteCandidateCount)"
            
            self.debugView.remoteCandidatesLabel.text = "\(self.remoteCandidateCount)"
         }
       }
     }
     
     private var speakerOn: Bool = true {
       didSet {
//         let title = "Speaker: \(self.speakerOn ? "On" : "Off" )"
//         self.speakerButton?.setTitle(title, for: .normal)
        
        self.footerView.buttonSpeak.isSelected = speakerOn
       }
     }
     
     private var mute: Bool = false {
       didSet {
//         let title = "Mute: \(self.mute ? "on" : "off")"
//         self.muteButton?.setTitle(title, for: .normal)
        
        self.footerView.buttonMute.isSelected = mute
       }
     }
    
        private var video: Bool = true {
           didSet {
    //         let title = "Mute: \(self.mute ? "on" : "off")"
    //         self.muteButton?.setTitle(title, for: .normal)
            
            self.footerView.buttonVideo.isSelected = !video
           }
         }
    
    var position: AVCaptureDevice.Position = .front {
        didSet {
            
            
        }
    }
    
    var timer: Timer? = nil
    var startTime: Double? = nil
    var audioPlayer: AVAudioPlayer?
    var state: StateView = .connecting {
        didSet {
            
            switch state {
            case .time(let startTime):
                self.startTime = startTime
            
                countTime()
                timer?.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1.second, target: self, selector: #selector(self.countTime), userInfo: nil, repeats: true)
                
            default:
                headerView.labelSub.text = state.title
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()
        guestRef = ref.child("users")
        
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
        
        signalingConnected = false
        hasLocalSdp = false
        hasRemoteSdp = false
        localCandidateCount = 0
        remoteCandidateCount = 0
        speakerOn = false
//        self.webRTCStatusLabel?.text = "New"
        self.debugView.webRTCStatusLabel.text = "New"
        
        webRTCClient.delegate = self
        signalClient.delegate = self
        
    
        currentPerson = AppData.instance.myInfo?.uid ?? ""
       oppositePerson = guestInfo.id
        
        signalClient.listenSdp(from: currentPerson)
        signalClient.listenCandidate(from: currentPerson)
        
        
        switch typeView {
        case .call:

//            footerView.buttonEndCall.isSelected = true
//            webRTCClient.offer { (sdp) in
//                self.hasLocalSdp = true
//                self.signalClient.send(sdp: sdp, to: self.oppositePerson)
//            }
            
            
            guestRef.child(guestInfo.id).observeSingleEvent(of: .value, with: { snapshot in
                
                guard let data = snapshot.value as? [String : Any] else { return }
                let info = User(data: data)
                
                if case .normal = info.state {
                    
                    let time = Date().timeIntervalSince1970
                    print("Call time: \(time)")
                    
                    self.guestRef.child(self.oppositePerson).child("call_info").child("state").setValue(StateUser.ring..)
                    self.guestRef.child(self.oppositePerson).child("call_info").child("user_id_call").setValue(self.currentPerson)
                    self.guestRef.child(self.oppositePerson).child("call_info").child("time_call").setValue(time)
                }
                
                
                self.playSound(.calling)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.seconds) {
                    
                    self.state = .ring
                    self.footerView.buttonEndCall.isSelected = true
                    
                    self.webRTCClient.offer { (sdp) in
                        self.hasLocalSdp = true
                        self.signalClient.send(sdp: sdp, to: self.oppositePerson)
                    }
                    
                }
            })
            
            

        default:
            
            self.playSound(.ring)
            self.state = .calling
  
        }
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)

    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer?.invalidate()

    }
    
    deinit {
        audioPlayer?.stop()
        AppData.instance.myInfo?.state = .normal
    }
}


extension VideoCallViewController: SignalClientDelegate {
  func signalClientDidConnect(_ signalClient: SignalingClient) {
    self.signalingConnected = true
  }
  
  func signalClientDidDisconnect(_ signalClient: SignalingClient) {
    self.signalingConnected = false
  }
  
  func signalClient(_ signalClient: SignalingClient, didReceiveRemoteSdp sdp: RTCSessionDescription) {
    print("Received remote sdp")
    self.webRTCClient.set(remoteSdp: sdp) { (error) in
      self.hasRemoteSdp = true
    }
  }
  
  func signalClient(_ signalClient: SignalingClient, didReceiveCandidate candidate: RTCIceCandidate) {
    print("Received remote candidate")
      self.remoteCandidateCount += 1
      self.webRTCClient.set(remoteCandidate: candidate)
  }
}

extension VideoCallViewController: WebRTCClientDelegate {
  
  func webRTCClient(_ client: WebRTCClient, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
    print("discovered local candidate")
    self.localCandidateCount += 1
    self.signalClient.send(candidate: candidate, to: self.oppositePerson)
  }
  
  func webRTCClient(_ client: WebRTCClient, didChangeConnectionState state: RTCIceConnectionState) {
    let textColor: UIColor
    switch state {
    case .connected:
        textColor = .green
//        footerView.buttonVideo.sendActions(for: .touchUpInside)
                

    case .completed:
      textColor = .green

    case .disconnected:
      textColor = .orange
//      footerView.buttonEndCall.sendActions(for: .touchUpInside)
        
    case .failed, .closed:
      textColor = .red
    case .new, .checking, .count:
      textColor = .black
    @unknown default:
      textColor = .black
    }
    DispatchQueue.main.async {
//      self.webRTCStatusLabel?.text = state.description.capitalized
//      self.webRTCStatusLabel?.textColor = textColor
        
        switch state {
        case .connected:
            
            self.audioPlayer?.stop()
            self.state = .time(Date().timeIntervalSince1970)
            self.remoteVideoView.isUserInteractionEnabled = true
            print("Mo Video")
            self.footerView.buttonVideo.sendActions(for: .touchUpInside)
            break
        case .disconnected:
            print("Dong video")
            self.state = .lostConnect
            self.footerView.buttonEndCall.isSelected = true
            self.footerView.buttonEndCall.sendActions(for: .touchUpInside)
            
        case .failed, .closed:
            print("failed, .closed")
//            self.footerView.buttonEndCall.sendActions(for: .touchUpInside)
            
        @unknown default:
            break
        }
        
        
        self.debugView.webRTCStatusLabel.text = state.description.capitalized
        self.debugView.webRTCStatusLabel.textColor = textColor
    }
  }
  
  func webRTCClient(_ client: WebRTCClient, didReceiveData data: Data) {
    DispatchQueue.main.async {
      let message = String(data: data, encoding: .utf8) ?? "(Binary: \(data.count) bytes)"
      let alert = UIAlertController(title: "Message from WebRTC", message: message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }
}




extension VideoCallViewController {
  
    @objc func endCall(_ sender: UIButton) {
        
        audioPlayer?.stop()
        guard sender.isSelected else {
            
            sender.isSelected = true
            
            webRTCClient.offer { (sdp) in
                  self.hasLocalSdp = true
                  self.signalClient.send(sdp: sdp, to: self.oppositePerson)
                }
            return
        }
        
        self.signalClient.deleteSdpAndCandidate(for: self.currentPerson)
        self.webRTCClient.closePeerConnection()
        
        self.webRTCClient.createPeerConnection()
        self.hasLocalSdp = false
        self.localCandidateCount = 0
        self.hasRemoteSdp = false
        self.remoteCandidateCount = 0
        
        self.guestRef.child(self.currentPerson).child("call_info").child("state").setValue(StateUser.normal..)
        self.guestRef.child(self.currentPerson).child("call_info").child("user_id_call").setValue("")
        self.guestRef.child(self.oppositePerson).child("call_info").child("time_call").setValue(0)
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func buttonAnswer(_ sender: UIButton) {
        
        audioPlayer?.stop()
        self.webRTCClient.answer { (localSdp) in
            self.hasLocalSdp = true
            self.signalClient.send(sdp: localSdp, to: self.oppositePerson)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.second) {
                self.footerView.buttonEndCall.isSelected = true
                self.footerView.buttonAnswer.isHidden = true
                self.footerView.layoutSubviews()
                
                self.webRTCClient.offer { (sdp) in
                      self.hasLocalSdp = true
                      self.signalClient.send(sdp: sdp, to: self.oppositePerson)
                    }
            
            }
        }
        
    }
    
    @objc func changeCamera(_ sender: UIButton) {
        
        print("sender: \(sender.isSelected)")
        
        sender.isSelected = !sender.isSelected

            position = sender.isSelected ? .back : .front
  
        
        if signalingConnected {
            
            
            #if arch(arm64)
                // Using metal (arm64 only)
                localRenderer = RTCMTLVideoView(frame: self.localVideoView.frame )
                let remoteRenderer = RTCMTLVideoView(frame: self.remoteVideoView.frame)
                localRenderer?.videoContentMode = .scaleAspectFill
                remoteRenderer.videoContentMode = .scaleAspectFill
            #else
                // Using OpenGLES for the rest
                localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
                let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
            #endif
            guard let localRenderer = self.localRenderer else { return }

            self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
            self.webRTCClient.renderRemoteVideo(to: remoteRenderer)
            

            self.embedView(localRenderer, into: localVideoView)
            self.embedView(remoteRenderer, into: self.remoteVideoView)
            self.view.sendSubviewToBack(remoteRenderer)
            
            
            
        } else {
            
            switch typeView {
            case .call:
                
                #if arch(arm64)
                // Using metal (arm64 only)
                localRenderer = RTCMTLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
                
                localRenderer?.videoContentMode = .scaleAspectFill
                
                #else
                // Using OpenGLES for the rest
                localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
                #endif
                
                
                guard let localRenderer = self.localRenderer else { return }
                
                self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
         
                
                self.embedView(localRenderer, into: self.remoteVideoView)
                
                
                
            default:
                break
            }
        }
    
        
    }
    
    @objc func buttonMute(_ sender: UIButton) {
        
        self.mute = !self.mute
        if self.mute {
          self.webRTCClient.muteAudio()
        }
        else {
          self.webRTCClient.unmuteAudio()
        }
        
    }
    
    @objc func buttonSpeak(_ sender: UIButton) {
         
        
         self.speakerOn = !self.speakerOn
         if self.speakerOn {
           self.webRTCClient.speakerOn()
         }
         else {
           self.webRTCClient.speakerOff()
         }
         
     }
    
    @objc func buttonVideo(_ sender: UIButton) {
        
//        self.video = !self.video
//        if self.mute {
//            self.webRTCClient.muteAudio()
//        }
//        else {
//            self.webRTCClient.unmuteAudio()
//        }
        
        
        self.video = !self.video
        
        if video {
            
            #if arch(arm64)
                 // Using metal (arm64 only)
                 localRenderer = RTCMTLVideoView(frame: self.localVideoView.frame )
                 let remoteRenderer = RTCMTLVideoView(frame: self.remoteVideoView.frame)
                 localRenderer?.videoContentMode = .scaleAspectFill
                 remoteRenderer.videoContentMode = .scaleAspectFill
             #else
                 // Using OpenGLES for the rest
                 localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
                 let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
             #endif
            guard let localRenderer = self.localRenderer else { return }

             self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
             self.webRTCClient.renderRemoteVideo(to: remoteRenderer)
             

             self.embedView(localRenderer, into: localVideoView)
             self.embedView(remoteRenderer, into: self.remoteVideoView)
             self.view.sendSubviewToBack(remoteRenderer)
            
        } else {
            
            #if arch(arm64)
                 // Using metal (arm64 only)
                 localRenderer = RTCMTLVideoView(frame: self.localVideoView.frame )
                 let remoteRenderer = RTCMTLVideoView(frame: self.remoteVideoView.frame)
                 localRenderer?.videoContentMode = .scaleAspectFill
                 remoteRenderer.videoContentMode = .scaleAspectFill
             #else
                 // Using OpenGLES for the rest
                 localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
                 let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
             #endif

             guard let localRenderer = self.localRenderer else { return }
             self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
             self.webRTCClient.renderRemoteVideo(to: remoteRenderer)
             

             self.embedView(localRenderer, into: localVideoView)
             self.embedView(remoteRenderer, into: self.remoteVideoView)
             self.view.sendSubviewToBack(remoteRenderer)
        }
    }
 
    
    @objc func tapOnLocalView(_ sender: UIGestureRecognizer) {
        
        //TODO: Change camera local
        
        if position == .front  {
            position = .back
        } else {
            position = .front
        }

        flip()
    }
    
    @objc func tapOnRemoteView(_ sender: UIGestureRecognizer) {
         
        guard let _ = sender.view else { return }
        animationVisibleFooterView(headerView.isHidden)
   
     }
    
    
    @objc func flip() {
//        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight]
        
        
        if let localRenderer = self.localRenderer {
            
            
//            #if arch(arm64)
//                // Using metal (arm64 only)
//                let localRenderer = RTCMTLVideoView(frame: self.localVideoView.frame )
//
//                localRenderer.videoContentMode = .scaleAspectFill
//
//            #else
//                // Using OpenGLES for the rest
//                let localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
//
//            #endif
            

            self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
            

            self.embedView(localRenderer, into: localVideoView)
       
            
            
        }
        

        UIView.transition(with: self.localVideoView, duration: 1.0, options: transitionOptions, animations: {
//            self.firstView.isHidden = true
            
            
            
            
            
        })

//        UIView.transition(with: secondView, duration: 1.0, options: transitionOptions, animations: {
//            self.secondView.isHidden = false
//        })
    }
}

//-----------------------------
// MARK: - PRIVATE METHOD
//------------------------------

extension VideoCallViewController {
    
    func playSound(_ type: SoundType) {
        if let audioPlayer = audioPlayer, audioPlayer.isPlaying { audioPlayer.stop() }

        guard let soundURL = Bundle.main.url(forResource: type.soundName, withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            audioPlayer?.prepareToPlay()
            audioPlayer?.volume = 1.0
            audioPlayer?.numberOfLoops = -1

            audioPlayer?.play()
        } catch let error {
//            Print.detailed(error.localizedDescription)
            print("error: \(error)")
        }
    }

    
    @objc func countTime() {
        
        let time: Double = Date().timeIntervalSince1970 - (startTime ?? 0)
         
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        headerView.labelSub.text = String(format:"%02i:%02i:%02i", hours, minutes, seconds)

    }
    
    func animationVisibleFooterView(_ visible: Bool) {
        
        if #available(iOS 11.0, *) {
            topHeaderViewConstraint.update(offset: visible ? view.safeAreaInsets.top : -headerView.frame.height)
            bottomFooterViewConstraint.update(offset: visible ? -view.safeAreaInsets.bottom : footerView.frame.height)
        } else {
            topHeaderViewConstraint.update(offset: visible ? 0 : -headerView.frame.height)
            bottomFooterViewConstraint.update(offset: visible ? 0 : footerView.frame.height)
        }
 
        
       
        headerView.alpha = visible ? 0.0 : 1.0
        footerView.alpha = visible ? 0.0 : 1.0

        headerView.isHidden = false
        footerView.isHidden = false

        
        UIView.animate(withDuration: 0.4, animations: {
            
            self.headerView.alpha = visible ? 1.0 : 0.0
            self.footerView.alpha = visible ? 1.0 : 0.0
            self.view.layoutSubviews()
            
        }, completion: { _ in
            
            self.headerView.isHidden = !visible
            self.footerView.isHidden = !visible

        })
    }
    
}

extension VideoCallViewController {
    
    func setupAllSubviews() {
        
        
    
        view.backgroundColor = .white
    
        
        
        
        headerView = setupHeaderView()
        footerView = setupFooterView()
        debugView = setupDebugView()
        
        remoteVideoView = setupView(.clear)
        localVideoView = setupView(.clear)
        localVideoView.layer.cornerRadius = 8
        localVideoView.clipsToBounds = true
        
        
        localVideoView.isUserInteractionEnabled = true
        localVideoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnLocalView(_:))))
        
        remoteVideoView.isUserInteractionEnabled = false
        remoteVideoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRemoteView(_:))))
        
        bgView = setupBgView()
        
        view.addSubview(bgView)
        view.addSubview(remoteVideoView)
        
        
        view.addSubview(footerView)
        view.addSubview(debugView)
        debugView.isHidden = true
        
        view.addSubview(localVideoView)
        view.addSubview(headerView)
        
        switch typeView {
        case .call:
            
            #if arch(arm64)
            // Using metal (arm64 only)
            let localRenderer = RTCMTLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
            
            localRenderer.videoContentMode = .scaleAspectFill
            
            #else
            // Using OpenGLES for the rest
            let localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
            let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
            #endif
            
            self.webRTCClient.startCaptureLocalVideo(renderer: localRenderer, position: position)
            
            self.embedView(localRenderer, into: self.remoteVideoView)
            
            
            
        default:
            break
        }
        


    }
    
    func setupAllConstraints() {
        
        bgView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
        remoteVideoView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
        localVideoView.snp.makeConstraints { make in
            make.top.equalTo(view).offset(25)
            make.right.equalTo(view).offset(-15)
            
             make.height.equalTo(180)
            make.width.equalTo(180 * 2 / 3)
        }
        
    
        headerView.snp.makeConstraints { make in
            make.left.right.equalTo(view)
            make.height.equalTo(75)
            if #available(iOS 11.0, *) {
                topHeaderViewConstraint = make.top.equalTo(view).offset(view.safeAreaInsets.top).constraint
            } else {
                topHeaderViewConstraint = make.top.equalTo(view).offset(0).constraint
            }
        }
        
        
        footerView.snp.makeConstraints { make in
            if #available(iOS 11.0, *) {
                bottomFooterViewConstraint = make.bottom.equalTo(view).offset(-view.safeAreaInsets.bottom).constraint
            } else {
                bottomFooterViewConstraint = make.bottom.equalTo(view).constraint
            }
            make.height.equalTo(75)
            make.left.right.equalTo(view)
        }
        
        
        debugView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.bottom.equalTo(footerView.snp.top)
            make.left.right.equalTo(view)
        }

    }
    
    
    private func embedView(_ view: UIView, into containerView: UIView) {
        containerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        containerView.layoutIfNeeded()
    }
    
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupHeaderView() -> VideoCallHeaderView {
        let view = VideoCallHeaderView()
        view.avatar.downloadedFrom(link: guestInfo.avatar, placeHolder: Icon.Account.AvatarDefault)
        view.labelName.text = guestInfo.name
        view.labelSub.text = state.title
        view.buttonChange.addTarget(self, action: #selector(self.changeCamera(_:)), for: .touchUpInside)
        view.isUserInteractionEnabled = true
        view.buttonChange.isUserInteractionEnabled = true
        view.buttonChange.isEnabled = true
        view.buttonChange.isHidden = true
        
        view.clipsToBounds = false
        view.layer.masksToBounds = true
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 2.0
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        return view
    }
    
    func setupFooterView() -> VideoCallFooterView {
        let view = VideoCallFooterView()
        view.buttonEndCall.addTarget(self, action: #selector(self.endCall(_:)), for: .touchUpInside)
        view.buttonVideo.addTarget(self, action: #selector(self.buttonVideo(_:)), for: .touchUpInside)
        view.buttonSpeak.addTarget(self, action: #selector(self.buttonSpeak(_:)), for: .touchUpInside)
        view.buttonMute.addTarget(self, action: #selector(self.buttonMute(_:)), for: .touchUpInside)
        view.buttonAnswer.addTarget(self, action: #selector(self.buttonAnswer(_:)), for: .touchUpInside)
        view.buttonAnswer.isHidden = typeView == .call
        
        
        view.buttonVideo.isSelected = !video
        view.buttonSpeak.isSelected = speakerOn
        view.buttonMute.isSelected = mute
        
        return view
    }
    
    func setupDebugView() -> VideoCallDebugView {
         let view = VideoCallDebugView()
         
         return view
     }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: Icon.Account.AvatarDefault)
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.white.alpha(0.8).cgColor
        imageView.layer.cornerRadius = Size.avatar.. / 2
        imageView.downloadedFrom(link: guestInfo.avatar, placeHolder: Icon.Account.AvatarDefault)
        return imageView
    }
    

    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white,
                     selector: Selector,
                     bgColor: UIColor = .clear) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.clipsToBounds = true
        button.backgroundColor = bgColor
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupBgView() -> UIImageView {
        let imageView = UIImageView(image: UIImage(named: "bg_video_call"))
        imageView.contentMode = .scaleToFill
        return imageView
    }
}







