//
//  MessageDetailViewController.swift
//  KO4Teacher
//
//  Created by Pham Huy on 2/25/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import UIKit
//import PHExtensions
import Firebase
import JSQMessagesViewController
import Photos
import FirebaseStorage
import SKPhotoBrowser
import SwiftyUserDefaults
import AVKit
import Alamofire


class MessageDetailViewController: JSQMessagesViewController {
    
//    define('CHAT_FILE_TYPE',0); //File đính kèm
//    define('CHAT_IMAGE_TYPE',1); //File Ảnh
//    define('CHAT_VIDEO_TYPE',2);//File Video
    
//    - file_name: tên của file/ảnh upload lên
//    - file_url: đường dẫn từ trên fireabase
//    - file_type: type của file upload lên
    
    
    enum ChatFileType: Int {
        case attach = 0, image, video
    }
    
    
    /// BAR BUTTON
    var back: UIBarButtonItem!
    var buttonCall: UIBarButtonItem!
    var buttonVideo: UIBarButtonItem!
    
    var alertController: UIAlertController?
    
    /// VAR
    var messages = [JSQMessage]()
    var allowSelectionImage: Bool = true
    
    /// REF
    var roomRef: DatabaseReference!
    var guestRef: DatabaseReference!
    var messageRef: DatabaseReference!
    
    var userIsTypingRef: DatabaseReference!
    var customIsTypingRef: DatabaseReference!
    
    var channelRefHandle: DatabaseHandle?
    var newMessageRefHandle: DatabaseHandle?
    var updatedMessageRefHandle: DatabaseHandle?
    
    var usersTypingQuery: DatabaseQuery!
    
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    

    lazy var storageRef: StorageReference = Storage.storage().reference().child("chats")
    
    var storagePhotoRef: StorageReference? = nil
    
    let imageURLNotSetKey = "NOTSET"
    var photoMessageMap = [String: JSQPhotoMediaItem]()
     var timeSendNotification: TimeInterval? = nil
    
    var numberMessage: Int = 0
    var isSendNew: Bool = false
    
    var type: Int? = nil
    
    private var localTyping = false // 2
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            // 3
            localTyping = newValue
            userIsTypingRef.setValue(newValue ? 1 : 0)
        }
    }
    
    var guestInfo: GuestInfo!
    var info: User!
    var roomName: String!
    
    private let config = Config.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
   
        type = guestInfo.type..
        
        
        guard let infoTemp = AppData.instance.myInfo else { return }
        info = infoTemp
        

        roomName = Utility.getRoomNameWithID(guestInfo.id)
        
        roomRef = Database.database().reference()
            .child("chats").child(roomName)
        
        
     
        
        messageRef  = roomRef.child("messages")
        userIsTypingRef = roomRef.child("users").child("\(info.uid)").child("typing")
        customIsTypingRef = roomRef.child("users").child("\(guestInfo.id)").child("typing")
        usersTypingQuery = customIsTypingRef.queryOrdered(byChild: "typing")
        
        messages = Utility.getMessagesFromRoomName(roomName)
        finishReceivingMessage()
        
        setupAllSubviews()
        
        
        
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 34.0, height: 34.0)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        observeMessages()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.subviews.forEach { if $0.tag >= 1000 { $0.removeFromSuperview()} }
//        navigationController!.navigationBar.barTintColor = UIColor.Controller.contact()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        collectionView.collectionViewLayout.springinessEnabled = true
        observeTyping()
        
        
        self.collectionView.reloadData()
        
        
    }
    
    
    deinit {
        print("deinit called")
        
        if let refHandle = channelRefHandle {
            roomRef.removeObserver(withHandle: refHandle)
        }
        
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        
        if let refHandle = newMessageRefHandle {
            messageRef.queryLimited(toLast:200).removeObserver(withHandle: refHandle)
        }
        
        if let photoRef = storagePhotoRef {
            photoRef.delete(completion: nil)
        }
        
        
    }
    
    
    /// Bấm vào nút gửi tin nhắn
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        
        let itemRef = messageRef.childByAutoId() // 1
        let messageItem: Parameters = [ // 2
            "content": text ?? "",
//            "file_url": "",
//            "file_type": "",
//            "file_name": "",
            "senderId": senderId,
            "time": date.timeIntervalSince1970,
            "unread" : 1] as [String : Any]


            itemRef.setValue(messageItem) // 3


        
        roomRef.child("lastSenderId").setValue(self.senderId)
        roomRef.child("hasUnreadMessage").setValue(1)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
        
        isTyping = false
        
        isSendNew = true
        
        finishSendingMessage() // 5
    }
    
    
    /// Bấm vào nút chọn ảnh
    @objc override func didPressAccessoryButton(_ sender: UIButton) {
        guard allowSelectionImage else { HUD.showMessage(LocalizedString("notification_message_sending_image", comment: "Đang gửi ảnh ...")); return }
        showAlertController(.selectionImage)
    }
    
}

//-------------------------------------------
//  MARK: - IMAGE PICKER DELEGATE
//-------------------------------------------

extension MessageDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        

        dismiss(animated: false, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }

        let imageData = cropImage(image, toSize:CGSize(width: 1024, height: 1024)).jpegData(compressionQuality: 0.9)
        let imagePath = roomName + "/\((Date.timeIntervalSinceReferenceDate * 1000).toString(0)).jpg"
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        
        
        guard let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: true) else { return }
        guard let message = JSQMessage(senderId: senderId, senderDisplayName: senderId + "\(self.info.uid)", date: Date(), media: mediaItem) else { return }
        messages.append(message)
        collectionView.reloadData()
        
        allowSelectionImage = false
        
        
        let riversRef = storageRef.child(imagePath)
        
        // 6
        riversRef.putData(imageData!, metadata: metadata) { [unowned self] (metadata, error) in
            
            self.allowSelectionImage = true
            
            if let pos = self.messages.firstIndex(where: { $0.senderDisplayName == self.senderId + "\(self.info.uid)" }) {
                 self.messages.remove(at: pos)
            }
            
            if let error = error {
                print("Error uploading photo: \(error)")
                HUD.showMessage(LocalizedString("notificaiton_message_send_image_failed", comment: "Gửi ảnh không thành công"), onView: self.view)
                return
            }
            
            self.collectionView.reloadData()
            
            
            riversRef.downloadURL { (url, error) in

                guard let downloadURL = url else {
                    self.sendPhotoMessage(self.storageRef.child(imagePath).description)
                    return
                }
                
                print("downloadURL.absoluteURL : \(downloadURL.absoluteString)")
                
                self.sendPhotoMessage(downloadURL.absoluteString ?? self.storageRef.child(imagePath).description)
                
            }

        }
    }
    
    fileprivate func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        
        
        let maxSize: CGFloat = 720
        
        if image.size.width >= image.size.height { newSize = CGSize(width: maxSize, height: maxSize * image.size.height / image.size.width) }
        else { newSize = CGSize(width: maxSize * image.size.width / image.size.height, height: maxSize) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
    
}

//-----------------------------------
// MARK: -   PRIVATE METHOD
//-----------------------------------

extension MessageDetailViewController  {
    func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    func addMessage(withId id: String, name: String, text: String, time: TimeInterval) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: Date(timeIntervalSince1970: time), text: text) {
            messages.append(message)
        }
    }
    
    
    func addPhotoMessage(withId id: String, senderDisplayName: String, key: String, date: Date, mediaItem: JSQPhotoMediaItem) {
        
        if let message = JSQMessage(senderId: id, senderDisplayName: senderDisplayName, date: date, media: mediaItem) {
            messages.append(message)
            if (mediaItem.image == nil) { photoMessageMap[key] = mediaItem }
            collectionView.reloadData()
        }
    }
    
    
    func sendPhotoMessage(_ link: String) {
        let itemRef = messageRef.childByAutoId()
        
        let messageItem: Parameters = [
            "content": "",
            "file_url": link,
            "senderId": senderId,
            "time": Date().timeIntervalSince1970,
            "file_type": ChatFileType.image..,
            "unread" : 1
            ] as [String : Any]
        

        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
        
        isSendNew = true
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["file_url": url])
    }
    
    func fetchImageDataAtURLFull(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        // 1
        
        
        UIImageView().downloadedFrom(link: photoURL, placeHolder: nil) { image in
            mediaItem.image = image
            Utility.updateMessageDataWith(self.roomName, messages: self.messages)
            self.collectionView.reloadData()
          
            guard let key = key else { return }
            self.photoMessageMap.removeValue(forKey: key)
            
        }

    }
    
    func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        // 1
        let storageRef = Storage.storage().reference(forURL: photoURL)
        
//        storagePhotoRef = Storage.storage().reference(forURL: photoURL)
        
        // 2
        storageRef.getData(maxSize: INT64_MAX, completion: { [unowned self] (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            // 3
            storageRef.getMetadata(completion: { [unowned self] (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                // 4
                if (metadata?.contentType == "image/gif") {
                    //                    mediaItem.image = UIImage.gifWithData(data!)
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.collectionView.reloadData()
                
                // 5
                guard key != nil else { return }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        })
    }
    
    
    /**
     Show controller chụp ảnh hoặc ảnh từ thư viện
     */
    func showImagePicker(_ source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        present(picker, animated: true, completion: nil)
    }
}

//---------------------------------
// MARK: - OBSERVE
//---------------------------------

extension MessageDetailViewController {
    
    /// Sự kiện người dùng gõ tin nhắn
    func observeTyping() {
        userIsTypingRef.onDisconnectRemoveValue()
        usersTypingQuery.observe(.value) { [weak self] (data: DataSnapshot) in
            
            guard let `self` = self else { return }
            self.showTypingIndicator = (data.value as? Int) == 1
            self.scrollToBottom(animated: true)
        }
    }
    
    
    /// Sự kiện có tin nhắn mới
    func observeMessages() {
        
        let messageQuery = messageRef.queryLimited(toLast:200)
        newMessageRefHandle = messageQuery.observe(.childAdded, with: {(snapshot) -> Void in
            
            print("snapshot: \(snapshot)")
  

                        guard let messageData = snapshot.value as? [String : Any] else { return }
            
            
            
            guard let id = messageData["senderId"] as? String else { return }
            
            let text: String = messageData["content"] as? String ?? ""
            let unread: Bool = (messageData["unread"] as? Int ?? 0) == 1
            let time: Double = messageData["time"] as? TimeInterval ?? Date().timeIntervalSince1970
            let fileUrl: String = messageData["file_url"] as? String ?? ""
            let fileName: String = messageData["file_name"] as? String ?? ""
            let fileType: ChatFileType = ChatFileType(rawValue: messageData["file_type"] as? Int ?? 1) ?? .image
            
            
//            guard fileUrl.count > 0 else {
//
//                guard text.count > 0 else { return }
//
//                guard let message = JSQMessage(senderId:  String(id),
//                                               senderDisplayName: snapshot.key,
//                                               date: Date(timeIntervalSince1970: time),
//                                               text: text) else { return }
//
//                guard !self.messages.contains(message) else { return }
//                self.messages.append(message)
//
//
//                self.finishReceivingMessage()
//                Utility.updateMessageDataWith(self.roomName, messages: self.messages)
//
//
//                guard unread else { return }
//
//                if id != self.info.id {
//                    self.messageRef.child(snapshot.key).child("unread").setValue(0)
//                } else {
//                    self.checkNetworkAndRequestGetSendChatNotificaiton()
//                }
//
//                return
//            }
            
            switch fileType {
            case .image where fileUrl.count > 0:
                
                guard let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.info.uid) else { return }
                
                guard let message = JSQMessage(senderId: String(id),
                                               senderDisplayName: snapshot.key,
                                               date: Date(timeIntervalSince1970: time),
                                               media: mediaItem) else { return }
                
                guard !self.messages.contains(message) else { return }
                
                self.messages.append(message)
                if (mediaItem.image == nil) { self.photoMessageMap[snapshot.key] = mediaItem }
                self.collectionView.reloadData()
                

                if fileUrl.hasPrefix("gs://") {
                    self.fetchImageDataAtURL(fileUrl, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                } else {
                    self.fetchImageDataAtURLFull(fileUrl, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                }
                
           
            case .video where fileUrl.count > 0:
                
                guard let url = URL(string: fileUrl) else { return }
                guard let mediaItem = JSQVideoMediaItem(fileURL: url, isReadyToPlay: true)  else { return }
                mediaItem.appliesMediaViewMaskAsOutgoing = id == self.info.uid
                
                
                guard let message = JSQMessage(senderId: String(id),
                                               senderDisplayName: snapshot.key,
                                               date: Date(timeIntervalSince1970: time),
                                               media: mediaItem) else { return }
                
                
                
                guard !self.messages.contains(message) else { return }
                self.messages.append(message)
                
//                if (mediaItem.image == nil) { self.photoMessageMap[snapshot.key] = mediaItem }
                self.collectionView.reloadData()
                

                
//                UIImageView().downloadedFrom(link: photoURL) { image in
//                    mediaItem.image = image
//                    Utility.updateMessageDataWith(self.roomName, messages: self.messages)
//                    self.collectionView.reloadData()
//
//                    guard let key = key else { return }
//                    self.photoMessageMap.removeValue(forKey: key)
//
//                }
//
//
//                if fileUrl.hasPrefix("gs://") {
//                    self.fetchImageDataAtURL(fileUrl, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
//                } else {
//                    self.fetchImageDataAtURLFull(fileUrl, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
//                }

            case .attach where fileUrl.count > 0:
                
                
                guard let message = JSQMessage(senderId:  String(id),
                                               senderDisplayName: snapshot.key,
                                               date: Date(timeIntervalSince1970: time),
                                               text: text + "\n" + fileUrl ) else { return }
                
                guard !self.messages.contains(message) else { return }
                self.messages.append(message)
                
            
        
                
                
            default:
                
                guard let message = JSQMessage(senderId:  String(id),
                                               senderDisplayName: snapshot.key,
                                               date: Date(timeIntervalSince1970: time),
                                               text: text) else { return }
                
                guard !self.messages.contains(message) else { return }
                self.messages.append(message)
                

            }
            
            
            self.finishReceivingMessage()
            Utility.updateMessageDataWith(self.roomName, messages: self.messages)
            guard unread else { return }
            
            if id != self.info.uid {

                self.messageRef.child(snapshot.key).child("unread").setValue(0)
                
            } else {
                self.checkNetworkAndRequestGetSendChatNotificaiton()
            }
        
        })
        

    }
    
}


//---------------------------------------
// MARK: - COLLECTION DELEGATE
//---------------------------------------

extension MessageDetailViewController  {
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let message = messages[indexPath.item]
        return getAvatar(message.senderId)
    }
    
    func getAvatar(_ id: String) -> JSQMessagesAvatarImage {
        
        var avatarImage: UIImage = Icon.Account.AvatarDefault
        
        guard id == guestInfo.id else { return JSQMessagesAvatarImageFactory.avatarImage(with: avatarImage, diameter: 40) }
        
        if let image = guestInfo.imageAvatar { return JSQMessagesAvatarImageFactory.avatarImage(with: image, diameter: 40) }
        
        
       
        UIImageView().downloadedFrom(link: guestInfo.avatar, placeHolder: Icon.Account.AvatarDefault) { image in
            
            self.guestInfo.imageAvatar = image
            avatarImage = image
            
        }
    
        
        return JSQMessagesAvatarImageFactory.avatarImage(with: avatarImage, diameter: 40)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        
        cell.isUserInteractionEnabled = true
        cell.tag = indexPath.item
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnMessage(_:))))
        
        return cell
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        
        if indexPath.item > 0 {
            if messages[indexPath.item - 1].date.timeIntervalSince1970 >= messages[indexPath.item].date.timeIntervalSince1970 - 1.minutes { return 0 }
        }
        
        guard let _ = messages[indexPath.item].date else { return 0 }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        guard let date = messages[indexPath.item].date else { return NSAttributedString(string: "") }
        
        
        
        let datesAreInTheSameDay = Calendar.current.isDate(date, equalTo: Date(), toGranularity:.day)
        
        if Calendar.current.isDateInToday(date) {
            return NSAttributedString(string: Utility.getStringTime(date.timeIntervalSince1970, formatter: "HH:mm"))
        } else {
            return NSAttributedString(string: Utility.getStringTime(date.timeIntervalSince1970, formatter: "HH:mm dd/MM/yyyy"))
        }
    }
    
}

//-----------------------------------
// MARK: -   TEXTVIEW DELEGATE
//-----------------------------------
extension MessageDetailViewController {
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        isTyping = textView.text != ""
    }
}



//-----------------------------------
// MARK: -   SELECTOR
//-----------------------------------

extension MessageDetailViewController {
    @objc func back(_ sender: UIBarButtonItem) {
        
        if let refHandle = newMessageRefHandle {
            messageRef.queryLimited(toLast:200).removeObserver(withHandle: refHandle)
        }
        
        if numberMessage > 0 {
            numberMessage = 1000
            checkNetworkAndRequestGetSendChatNotificaiton()
        }
//        else {
            _ = navigationController?.popViewController(animated: true)
//        }
        
        
    }
    
    
    @objc func callPhone(_ sender: UIButton) {
        showAlertController(.callGuest(guestInfo.name, guestInfo.phone))
    }
    
    @objc func callVideo(_ sender: UIButton) {
        
        let signalClient = SignalingClient()
              let webRTCClient = WebRTCClient(iceServers: self.config.webRTCIceServers)
//              let mainViewController = MainViewController(
//                signalClient: signalClient,
//                webRTCClient: webRTCClient)
        
        
        let videoVC = VideoCallViewController()
        videoVC.modalPresentationStyle = .fullScreen
        videoVC.guestInfo = guestInfo
        videoVC.signalClient = signalClient
        videoVC.webRTCClient = webRTCClient
        let naviVC = UINavigationController(rootViewController: videoVC)
        naviVC.modalPresentationStyle = .fullScreen
        present(naviVC, animated: true, completion: nil)
        
        

     }
    
    @objc func tapOnMessage(_ sender: UIGestureRecognizer) {
        
        guard let view = sender.view, view.tag < messages.count else { return }
        

        if let _ = messages[view.tag].media as? JSQPhotoMediaItem {
            
            var photos: [SKPhoto] = []
            
            var position: Int = -1
            for (i, message) in messages.enumerated() {
                if let image = message.media as? JSQPhotoMediaItem, image.image != nil {
                    
                    photos.insert(SKPhoto.photoWithImage(image.image), at: 0)
                    
                    if position >= 0 { position += 1 }
                    if i == view.tag { position = 0 }
                }
            }
            
            guard photos.count > 0 else { return }
            
            let browser = SKPhotoBrowser(photos: photos)
            browser.initializePageIndex(position >= 0 ? position : 0)
            present(browser, animated: true, completion: {})
            
        } else if let videoItem = messages[view.tag].media as? JSQVideoMediaItem {
            
            let player = AVPlayer(url: videoItem.fileURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        

        
        
        
    }
}

//====================================
// MARK: - REQUEST ATTENDANCE
//====================================

extension MessageDetailViewController {
    
    //----------------------------------------
    // - MARK: GET ATTENDANCE LIST
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetSendChatNotificaiton(_ type: Int = 1) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() && isSendNew else { return }

        
//        if let info = Utility.getUserInfoFromID(guestInfo.id) as? GuestInfo {
//            guard info.lastUpdate < Date().timeIntervalSince1970 - 30.seconds else { return }
//        }
//
//
//        if let time = timeSendNotification, time == guestInfo.lastUpdate {
//            return
//        }
        
        if numberMessage != 1000 { numberMessage += 1 }
        guard numberMessage >= 5 else { return }
        
//        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
//
//        HTTPManager.instance.sendChatNotification
//            .doRequest(HTTPSendChatNotification.RequestType(name: self.info.name,
//                                                            avatar: self.info.avatar,
//                                                            senderID: self.info.id,
//                                                            receiverID: self.guestInfo.id,
//                                                            type: self.type ?? 3,
//                                                            studentID: self.guestInfo.studentID), headers: headers)
//            .completionHandler { result in
//
//                if self.numberMessage == 1000 {
//                    self.numberMessage = 0
//                }
//
//                self.numberMessage = 0
//
//                switch result {
//                case .success(let respond):
//                    self.sendChatRequestSuccess(respond)
//
//                case .failure(let error):
//                    self.getShowAttendanceListRequestFaild(error)
//                    break
//                }
//        }
    }
    
    /**
     Request lấy danh sách lớp thành công
     */
//    fileprivate func sendChatRequestSuccess(_ respond: HTTPSendChatNotification.RespondType) {
//
//         timeSendNotification = guestInfo.lastUpdate
//    }
    
    /**
     Request lấy danh sách lớp thất bại
     */
    fileprivate func getShowAttendanceListRequestFaild(_ error: Error) {
        
        print("error: \(error)")
        timeSendNotification = guestInfo.lastUpdate
    }
    
    
}

//-----------------------------------
// MARK: -   SETUP
//-----------------------------------
extension MessageDetailViewController {
    fileprivate func setupAllSubviews() {
        
        title = guestInfo.name
        back = setupBarButton(image: Icon.Navigation.Back, selector: #selector(self.back(_:)))
        buttonCall = setupBarButton(image: Icon.Login.Phone, selector: #selector(self.callPhone(_:)))
        buttonVideo = setupBarButton(image: Icon.General.CallVideo, selector: #selector(self.callVideo(_:)))
        
        navigationItem.leftBarButtonItem = back
        
        var arrayButton: [UIBarButtonItem] = [buttonVideo]
        if guestInfo.phone.count > 0 { arrayButton.append(buttonCall) }
        
        navigationItem.rightBarButtonItems = arrayButton

        senderId = info.uid
        senderDisplayName = info.name
    }
    
    func setupBarButton(image: UIImage, selector: Selector) -> UIBarButtonItem {
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: selector)
        button.tintColor = .white
        return button
    }
    
    
    private func buildMainViewController() -> UIViewController {
       let signalClient = SignalingClient()
       let webRTCClient = WebRTCClient(iceServers: self.config.webRTCIceServers)
       let mainViewController = MainViewController(
         signalClient: signalClient,
         webRTCClient: webRTCClient)
       let navViewController = UINavigationController(rootViewController: mainViewController)
       return navViewController
     }
}



