//
//  ContactSearchDataSource.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit
//import PHExtensions


//-------------------------------------------
//MARK: - BOOKING SEARCH DATASOURCE PROTOCOL
//-------------------------------------------

protocol ContactSearchDataSourceDelegate: class {
    func callPhone(_ name: String, phone: String)
    func didSelectedGuestInfo(_ info: GuestInfo)
    func openRoomChatWithInfo(_ info: GuestInfo)
}

//-------------------------------------------
//MARK: - BOOKING SEARCH DATASOURCE CLASS
//-------------------------------------------
class ContactSearchDataSource: NSObject {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, headerSection = 35
    }
    
    enum Section: Int {
           case search, contact
           
           var title: String {
               get {
                   switch self {
                   case .search:
                       return "Kết quả theo"
                       
                   case .contact:
                       return "Danh bạ"
                   }
               }
           }
       }
    
    

    var searchResult: [GuestInfo] = []
    var contactResult: [GuestInfo] = []
    
    var arraySection: [Section] = [.search, .contact]

    
    weak var delegate: ContactSearchDataSourceDelegate?
    
    @objc func call(_ sender: UIButton) {
        
        
        switch arraySection[sender.tag / 1000] {
            
        case .search:
            let teacher = searchResult[sender.tag % 1000]
            delegate?.callPhone(teacher.name, phone: teacher.phone)
            
        case .contact:
            let teacher = contactResult[sender.tag % 1000]
            delegate?.callPhone(teacher.name, phone: teacher.phone)
        }
        
    }
    
    
     @objc func openRoomChat(_ sender: UIButton) {
        
        
        var guestInfo: GuestInfo? = nil
        
        switch arraySection[sender.tag / 1000] {
            
        case .search:
            guestInfo = searchResult[sender.tag % 1000]

        case .contact:
            guestInfo = contactResult[sender.tag % 1000]
        }
        
        guard let info = guestInfo else { return }
        
        delegate?.openRoomChatWithInfo(info)
    }
}


//-------------------------------------------
//MARK: - UITABLE VIEW DATASOURCE
//-------------------------------------------
extension ContactSearchDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arraySection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch arraySection[section] {
        case .search:
            return searchResult.count
            
        case .contact:
            return contactResult.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch arraySection[indexPath.section] {
        case .search:
            
            let cellIdentifier = "ContactCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ContactTableViewCell
                configureCell(cell, atIndexPath: indexPath)
                return cell
            
            
        default:
            let cellIdentifier = "ContactCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ContactTableViewCell
            configureCell(cell, atIndexPath: indexPath)
            return cell
        }
        
        
    }
    

    
    fileprivate func configureCell(_ cell: ContactTableViewCell, atIndexPath indexPath: IndexPath) {
        
        cell.backgroundColor = .clear
        
        var guestInfo: GuestInfo?
        
        switch arraySection[indexPath.section] {
        case .search:
            guestInfo = searchResult[indexPath.row]
            cell.seperator.isHidden = (indexPath.row == searchResult.count - 1)
            
            
        case .contact:
            guestInfo = contactResult[indexPath.row]
            cell.seperator.isHidden = (indexPath.row == contactResult.count - 1)
            
        }
        
        guard let guardian = guestInfo else { return }
        cell.isOnline = guardian.isOnline
        cell.isPadding = true
        cell.statusView.isHidden = !AppData.instance.isCanChat
        cell.buttonMessage.tag = indexPath.section * 1000 + indexPath.row
        cell.buttonMessage.isHidden = false
        
        
        cell.detailTextLabel?.numberOfLines = 2
        cell.backgroundColor = .clear
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = guardian.name
        cell.detailTextLabel?.text = guardian.email
        
        
//        if AppData.instance.isCanChat {
//
//            cell.isNewMessageView.isHidden = !guardian.isNewMessage
//            cell.buttonMessage.addTarget(self, action: #selector(self.openRoomChat(_:)), for: .touchUpInside)
//            cell.buttonMessage.setImage(Icon.TabBar.Message.tint(UIColor.Navigation.mainColor()), for: .normal)
//
//        } else {
            
            cell.isNewMessageView.isHidden = true
            cell.buttonMessage.isHidden = !(guardian.phone.count > 0)
            cell.buttonMessage.addTarget(self, action: #selector(self.call(_:)), for: .touchUpInside)
            cell.buttonMessage.setImage(Icon.Login.Phone.tint(UIColor.Navigation.mainColor()), for: .normal)
//        }
        
        if let image = guardian.imageAvatar { cell.imageView?.image = image; return }

        cell.imageView?.downloadedFrom(link: guardian.avatar, placeHolder: Icon.Account.AvatarDefault) { image in
            guardian.imageAvatar = image
        }
    
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderViewSection()
        headerView.label.font = UIFont(name: FontType.latoBold.., size: FontSize.small..)
        headerView.bgView.backgroundColor = UIColor.Navigation.mainColor()


        let sectionInfo = arraySection[section]
        headerView.label.text = sectionInfo.title.uppercased()


        let footerView = FooterViewSection()
        footerView.backgroundColor = UIColor.clear

        let nilView = UIView()
        nilView.backgroundColor = UIColor.clear


        switch sectionInfo {
        case .search:
            return searchResult.count > 0 ? headerView : nilView
            
        case .contact:
            return contactResult.count > 0 ? headerView : nilView
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        let footerView = FooterViewSection()
        footerView.backgroundColor = UIColor.clear
        
        
        let nilView = UIView()
        nilView.backgroundColor = UIColor.clear
        
        
        switch arraySection[section] {
        case .search:
            return searchResult.count > 0 ? footerView : nilView
            
        case .contact:
            return contactResult.count > 0 ? footerView : nilView
            
        }
        
    }
    
}


//-------------------------------------------
//MARK: - UITABLE VIEW DELEGATE
//-------------------------------------------

extension ContactSearchDataSource: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        

        switch arraySection[indexPath.section] {
        case .search:
            delegate?.didSelectedGuestInfo(searchResult[indexPath.row])
            
        case .contact:
            delegate?.didSelectedGuestInfo(contactResult[indexPath.row])
            
       
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        switch arraySection[section] {
        case .search:
            return searchResult.count > 0 ? 38 : onePixel()

        case .contact:
            return contactResult.count > 0 ? 38 : onePixel()
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch arraySection[section] {
        case .search:
            return searchResult.count > 0 ? 10 : onePixel()
            
        case .contact:
            return contactResult.count > 0 ? 10 : onePixel()

        }
    }
}






