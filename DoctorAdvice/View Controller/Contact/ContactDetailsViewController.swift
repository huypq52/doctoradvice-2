//
//  ContactDetailsViewController.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/25/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import MessageUI
import SKPhotoBrowser

/**
 Thông tin hiển thị của 1 cell gồm Title và Icon
 */

struct TitleCell {
    var title: String!
    var icon: UIImage?
    
    init(title: String, icon: UIImage?){
        self.title = title
        self.icon = icon
    }
}


class ContactDetailsViewController: CustomViewController {
    
    
    /**
     Enum xác định các Size
     */
    fileprivate enum Size: CGFloat {
        case padding = 15, headerSection = 35, footerSection = 5, button = 44
    }


    enum Row: Int {
        case parentsInfo = 0, phone, address, email, work, medical
        
        var title: String {
            switch self {
            case .parentsInfo:
                return ""
                
            case .phone:
                return "Số điện thoại"
                
            case .address:
                return "Địa chỉ"
                
            case .email:
                return "Email"
                
            case .work:
                return "Công việc"
                
            case .medical:
                return "Bệnh án"
            }
        }
        
        var icon: UIImage {
            switch self {
            case .parentsInfo:
                return Icon.Login.Personal
                
            case .phone:
                return Icon.Contact.DetailPhone
                
            case .address:
                return Icon.Contact.DetailAddress
                
            case .email:
                return Icon.Contact.DetailEmail
                
            case .work:
                return Icon.Contact.DetailJob
                
            case .medical:
                return Icon.Contact.DetailEmail
            }
        }
        
    }


    enum AlertType {
        case callParent(String, String)
        
        func alertTitle() -> String {
            switch self {
            case .callParent:
                return LocalizedString("alert_title_confirm", comment: "Xác nhận")
            }
        }
        
        func alertMessage() -> String {
            switch self {
                
            case .callParent(let string, _):
                return String(format: LocalizedString("notification_gerenal_you_want_call", comment: "Bạn có muốn gọi cho %@"), string)
            }
        }
    }

    

    fileprivate var back: UIBarButtonItem!
    
    
    var info: GuestInfo!
    
    var arrayRow: [Row] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = LocalizedString("contact_detail_title", comment: "Chi tiết liên hệ")
        
        switch info.type {
        case .doctor:
            
            arrayRow = [.parentsInfo, .email, .phone, .address, .work]
            
        case .patient:
            
            arrayRow = [.parentsInfo, .email, .phone, .address, .work, .medical]
            
        }
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}

extension ContactDetailsViewController {
     @objc func back(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func showPhoto(_ sender: UIGestureRecognizer) {

        guard let imageView = sender.view as? UIImageView else { return }
        
        let photo = SKPhoto.photoWithImage(imageView.image!)
        
        let browser = SKPhotoBrowser(photos: [photo])
//        browser.delegate = self
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    @objc func openCall(_ sender: UIButton) {
        showAlertController(.callParent(info.name, info.phone))
    }
    
    @objc func openMessage(_ sender: UIButton) {
        

        let messageDetailVC = MessageDetailViewController()
        messageDetailVC.guestInfo = info
        navigationController?.pushViewController(messageDetailVC, animated: true)
        
    }
}


extension ContactDetailsViewController {
    /**
     Show Alert
     */
    
    fileprivate func showAlertController(_ type: AlertType) {
        self.dismiss(animated: false, completion: nil)
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        
        
        switch type {
            
        case .callParent(_, let phone):
            /**
             Bỏ qua
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
            
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                
                Utility.callPhoneNumber(phone)
                }))
            
        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
}

extension ContactDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return Row.Work++
        return arrayRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch arrayRow[indexPath.row] {
        case .parentsInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactInfoTableViewCell", for: indexPath) as! ContactInfoTableViewCell
            configuaParentsCell(cell, indexPath: indexPath)
            return cell
            
        default:
            let identifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SeperatorPaddingTableViewCell
            configuaCell(cell, indexPath: indexPath)
            return cell
            
        }
        
    }
    
    
    func configuaCell(_ cell: SeperatorPaddingTableViewCell, indexPath: IndexPath) {
        cell.selectionStyle = .none
        cell.clipsToBounds = true
        cell.isPadding = false
        
        cell.seperatorRightPadding = 15
        cell.seperatorStyle = .padding(15)
        
        guard let info = info else { return }


        cell.accessoryType = .none
        cell.textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        cell.detailTextLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)

        cell.imageView?.image = arrayRow[indexPath.row].icon.tint(UIColor.Text.grayMediumColor())
        cell.textLabel?.text = arrayRow[indexPath.row].title
        

        switch arrayRow[indexPath.row] {
            
        case .phone:
            cell.detailTextLabel?.text = info.phone 
            
        case .address:
            cell.detailTextLabel?.text = info.address
            
        case .email:
            cell.detailTextLabel?.text = info.email 
            
        case .work:
            cell.detailTextLabel?.text = info.email
            
        case .medical:
            cell.accessoryType = .disclosureIndicator
            cell.detailTextLabel?.text = nil
            
        default:
            cell.detailTextLabel?.text = nil
        }
    }
    
    func configuaParentsCell(_ cell: ContactInfoTableViewCell, indexPath: IndexPath) {
        
        cell.clipsToBounds = true
        
        cell.selectionStyle = .none
        cell.seperatorStyle = .hidden
        
        guard let info = info else { return }
        
        cell.labelName.text = info.name
        cell.labelSub.text = info.type.title
        

        cell.imageViewAvatar.image = Icon.Account.AvatarDefault
        cell.imageViewAvatar.isUserInteractionEnabled = true
        cell.imageViewAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showPhoto(_:))))
        cell.imageViewAvatar.tag = indexPath.row
        
        cell.buttonCall.addTarget(self, action: #selector(self.openCall(_:)), for: .touchUpInside)
        cell.buttonMessage.addTarget(self, action: #selector(self.openMessage(_:)), for: .touchUpInside)
        
        if let userInfo = AppData.instance.myInfo, userInfo.guestFriend.contains(info.id) {
            
            cell.buttonMessage.isHidden = false
            cell.buttonCall.isHidden = info.phone.count == 0
        } else {
            cell.buttonMessage.isHidden = true
            cell.buttonCall.isHidden = true
        }
        
 

        cell.imageViewAvatar.downloadedFrom(link: info.avatar, placeHolder: Icon.Account.AvatarDefault)

    }

}

extension ContactDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch arrayRow[indexPath.row] {
        case .phone:
            
            showAlertController(.callParent(info.name, info.phone))
            
        case .email:
            
            guard info.email.count > 0 else { return }
            
            
            guard MFMailComposeViewController.canSendMail() else { return }
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([info.email])
            present(mail, animated: true, completion: nil)
            
        case .medical:
            
            let medicalVC = MedicalHistoryViewController()
            medicalVC.userID = info.id
            navigationController?.pushViewController(medicalVC, animated: true)
            
        default:
            break
        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case Row.parentsInfo..:
            
            var height: CGFloat = 150
            
            if info.email.count > 0 {
                
                height += 50
            }
            
            return height
            
        case Row.phone..:
            return info.phone.count > 0 ? 50 : 0
            
        case Row.address..:
            return info.address.count > 0 ? 50 : 0
            
        case Row.email..:
            return info.email.count > 0 ? 50 : 0
            
        default:
            return 50
        }
    }
}

// MARK: - REQUEST
extension ContactDetailsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        switch result {
        case .cancelled:
            print("Cancel sent email")
        case .saved:
            HUD.showMessage(LocalizedString("about_label_save_mail_success", comment: "Đã lưu vào nháp thành công."), onView: self.view)
        case .sent:
            HUD.showMessage(LocalizedString("about_label_send_mail_success", comment: "Gửi email thành công."), onView: self.view)
        case .failed:
            HUD.showMessage(LocalizedString("notification_has_error_please_try_again", comment: "Có lỗi xảy ra, vui lòng thử lại sau"), onView: self.view)
            print("error: \(String(describing: error))")
            
        @unknown default:
            break
        }
    }
}

extension ContactDetailsViewController {
    fileprivate func setupAllSubviews() {
        
        
        
        title = "Thông tin chi tiết"
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        navigationItem.leftBarButtonItem = back
        

        
        setupTable()
    }
    
    fileprivate func setupTable() {
        table = UITableView(frame: CGRect.zero, style: .plain)
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.backgroundColor = UIColor.white
        
        table.register(ParentsDetailTableViewCell.self, forCellReuseIdentifier: "ParentsCell")
        table.register(SeperatorPaddingTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.register(ContactInfoTableViewCell.self, forCellReuseIdentifier: "ContactInfoTableViewCell")
        
        view.addSubview(table)
    }

    
    func setupAllConstraints() {
        table.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
    }

}
