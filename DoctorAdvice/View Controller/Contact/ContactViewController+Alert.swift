//
//  ContactViewController+Alert.swift
//  KidOnline
//
//  Created by Pham Huy on 12/18/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults

extension ContactViewController {
    
    
    enum AlertType {
        
        case callParent(String, String)
        
        func alertTitle() -> String {
            switch self {
            case .callParent:
                return LocalizedString("alert_title_confirm", comment: "Xác nhận")
            }
        }
        
        func alertMessage() -> String {
            switch self {
                
            case .callParent(let string, _):
                return String(format: LocalizedString("notification_gerenal_you_want_call", comment: "Bạn có muốn gọi cho %@"), string)
            }
        }
    }
    
    
    /**
     Show Alert
     */
    
    internal func showAlertController(_ type: AlertType) {
        self.dismiss(animated: false, completion: nil)
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
        
        switch type {
            
        case .callParent(_, let phone):
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                
                Utility.callPhoneNumber(phone)
            }))
            
            
        }
        self.present(alertController!, animated: true, completion: nil)
    }
}

