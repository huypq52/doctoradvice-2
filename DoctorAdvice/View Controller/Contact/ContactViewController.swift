//
//  ContactViewController.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/23/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit
//import PHExtensions
import SwiftyUserDefaults
import Firebase
import Alamofire



protocol ContactViewControllerDelegate: class {
    func dismissViewController()
    func didSelectedGuestInfo(_ info: GuestInfo)
}

public enum ContactType {
    case contact
    case message

}

class ContactViewController: CustomViewController {
    
    enum Section: Int {
        case suggest = 0, confirm, contact
        
        var title: String {
            get {
                switch self {
                case .suggest:
                    return "Gợi ý kết bạn"
                    
                case .confirm:
                    return "Lời mời kết bạn"
                    
                case .contact:
                    return "Danh bạ"
                }
            }
        }
    }
    
    var search: UIBarButtonItem!
    var buttonBack: UIBarButtonItem!
    
    var arraySection: [Section] = [.suggest, .confirm, .contact]
    
    var suggestList: [GuestInfo] = []
    var confirmList: [GuestInfo] = []

    //-------------------------------------------
    //MARK: - SEARCH CONTROLLER
    //-------------------------------------------
    
    
    var searchController: UISearchController!
    var searchDataSource: ContactSearchDataSource!
    
    var searchBar: UIView!
    
    
    var refreshControl: UIRefreshControl!
    
    
    var ref: DatabaseReference!
    
    var guestRef: DatabaseReference!
    var messageRef: DatabaseReference!
    
    var confirmRefHandle: DatabaseHandle?
    var sendRefHandle: DatabaseHandle?
    var friendHandle: DatabaseHandle?
    
    var listRoomName: [String] = []
    var listHandle: [(refHandle: DatabaseReference, handle: DatabaseHandle)] = []
    
    var contactType: ContactType = .contact
    
    weak var delegate: ContactViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()
        
        
        
        guestRef = ref.child("users")
        messageRef = ref.child("chats")
        
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
        
        observeHandle()
        switch contactType {
        case .contact:
             observeGuests()
        default:
            //TODO: Dịch
            title = "Chọn liên hệ"
            navigationItem.leftBarButtonItem = buttonBack
        }
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    deinit {
        
        print("Contact View Controller Deinit")
        removeObserveOnline()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        footerBackground.frame = CGRect(x: 0,
                                        y: view.frame.height  - (UIScreen.main.bounds.width / 4),
                                        width: UIScreen.main.bounds.width,
                                        height: UIScreen.main.bounds.width / 4)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        guard searchController != nil else { return }
        
        searchController.searchBar.frame.size.width = size.width
    }
    
    @objc func reloadData() {
        
        self.table.reloadData()
    }
    
    
    func observeHandle() {
        
        guard let userInfo = AppData.instance.myInfo else { return }
        
            friendHandle = guestRef.child(userInfo.uid).child("guest_friend").observe(.value, with: {(snapshot) -> Void in
                
                print("snapshot guest_confirm_friend: \(snapshot)")
                
                
                let data = snapshot.value as? [String] ?? []
        
                
                AppData.instance.guestInfoList = data.map { GuestInfo(id: $0) }
                
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.ReloadOnline..), object: nil, userInfo: nil)
                
                self.suggestList = self.suggestList
                     .filter { $0.id != userInfo.uid }
                     .filter { !userInfo.guestConfirmRequest.contains($0.id) }
                     .filter { !userInfo.guestSendRequest.contains($0.id) }
                 
                
                self.table.reloadData()
                
                AppData.instance.guestInfoList.forEach { item in

                    self.guestRef.child(item.id).observeSingleEvent(of: .value, with: { snapshot in
                        
                        guard let data = snapshot.value as? [String : Any] else { return }
                        
                        let info = GuestInfo(data: data)
                        
                        if let pos = AppData.instance.guestInfoList.firstIndex(where: { $0.id == info.id }) {
                            AppData.instance.guestInfoList[pos] = info
                        } else {
                            AppData.instance.guestInfoList.append(info)
                        }
                        
                        if let pos = AppData.instance.guestInfoList.firstIndex(where: { $0.id == info.id }) {
                            AppData.instance.guestInfoList[pos] = info
                            
                            self.table.reloadRows(at: [IndexPath(row: pos, section: self.arraySection.firstIndex(of: .contact) ?? 2)], with: .none)
                        }
                        
                    })
                }
                
                
                if let pos = self.arraySection.firstIndex(of: .contact) {
                    self.table.reloadSections(IndexSet(integer: pos), with: .none)
                }
            })
        
        
        
        guard contactType == .contact else { return }
        

        
        confirmRefHandle = guestRef.child(userInfo.uid).child("guest_confirm_friend").observe(.value, with: {(snapshot) -> Void in
            
            print("snapshot guest_confirm_friend: \(snapshot)")
            
            
            let data = snapshot.value as? [String] ?? []
            
            self.confirmList = data.map { GuestInfo(id: $0) }
            
            
            data.forEach { item in
                
                
                self.guestRef.child(item).observeSingleEvent(of: .value, with: { snapshot in
                    
                    guard let data = snapshot.value as? [String : Any] else { return }
                    
                    let info = GuestInfo(data: data)
                    
                    if let pos = self.confirmList.firstIndex(where: { $0.id == info.id }) {
                        self.confirmList[pos] = info
                        
                        self.table.reloadRows(at: [IndexPath(row: pos, section: self.arraySection.firstIndex(of: .confirm) ?? 1)], with: .none)
                        
                        
                    }
                    
                })
            }
            
            
            self.suggestList = self.suggestList
                .filter { $0.id != userInfo.uid }
                .filter { !userInfo.guestConfirmRequest.contains($0.id) }
                .filter { !userInfo.guestSendRequest.contains($0.id) }
            
            
            if let pos = self.arraySection.firstIndex(of: .confirm) {
                self.table.reloadSections(IndexSet(integer: pos), with: .none)
            }
        })
        
        

        
        
        
        
        
        sendRefHandle = guestRef.child(userInfo.uid).child("guest_send_friend").observe(.value, with: {(snapshot) -> Void in
            
            print("snapshot guest_send_friend: \(snapshot)")
            
            
            let data = snapshot.value as? [String] ?? []
            
            print("guest_send_friend data: \(data)")
            
            AppData.instance.myInfo?.guestSendRequest = data
            
//            if let pos = self.arraySection.firstIndex(of: .confirm) {
//                self.table.reloadSections(IndexSet(integer: pos), with: .none)
//            }
            
            self.table.reloadData()
        })
        
        if let handle = confirmRefHandle {
            listHandle.append((guestRef.child(userInfo.uid).child("guest_confirm_friend"), handle: handle))
        }
        
        if let handle = sendRefHandle {
            listHandle.append((guestRef.child(userInfo.uid).child("guest_send_friend"), handle: handle))
        }
        
        if let handle = friendHandle {
            listHandle.append((guestRef.child(userInfo.uid).child("guest_friend"), handle: handle))
        }
    }
    
    func observeGuests() {
        
        
        suggestList = []
        guestRef.observeSingleEvent(of: .value, with: { snapshot in
            
            for child in snapshot.children {
                
                print("child: \(child)")
                
                guard let snap = child as? DataSnapshot,
                    let data = snap.value as? [String : Any] else { continue }
                
                                
                
                let guestInfo = GuestInfo(data: data)
                
                if let userInfo = AppData.instance.myInfo,
                    userInfo.uid != guestInfo.id,
                    !userInfo.guestConfirmRequest.contains(guestInfo.id),
                    !userInfo.guestFriend.contains(guestInfo.id) {
                     self.suggestList.append(guestInfo)
                }
            }
            
            
            
            self.reloadTable()
            
            
        })
        
    }
    
    
    
    func removeObserveOnline() {
        listHandle.forEach { $0.refHandle.removeObserver(withHandle: $0.handle) }
    }
    
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension ContactViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arraySection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch arraySection[section] {
        case .suggest:
            return suggestList.count
            
        case .confirm:
            return confirmList.count
            
        case .contact:
            return AppData.instance.guestInfoList.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch arraySection[indexPath.section] {
        case .suggest:
            
            let cellIdentifier = "ContactAddFriendTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ContactAddFriendTableViewCell
            configureAddCell(cell, atIndexPath: indexPath)
            return cell
            
        case .confirm:
            
            let cellIdentifier = "ContactConfirmTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ContactConfirmTableViewCell
            configureConfirmCell(cell, atIndexPath: indexPath)
            return cell
            
            
        case .contact:
            let cellIdentifier = "ContactTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ContactTableViewCell
            configureCell(cell, atIndexPath: indexPath)
            return cell
        }
        
        
    }
    
    
    func configureConfirmCell(_ cell: ContactConfirmTableViewCell, atIndexPath indexPath: IndexPath) {
        
        
        cell.seperator.isHidden = (indexPath.row == confirmList.count - 1)
        let guardian = confirmList[indexPath.row]
        
        
        
        cell.buttonConfirm.tag = indexPath.section * 1000 + indexPath.row
        cell.buttonConfirm.addTarget(self, action: #selector(self.confirmFriend(_:)), for: .touchUpInside)
        
        cell.buttonCancel.tag = indexPath.section * 1000 + indexPath.row
        cell.buttonCancel.addTarget(self, action: #selector(self.deleteFriend(_:)), for: .touchUpInside)
        
        
        cell.detailTextLabel?.numberOfLines = 1
        cell.backgroundColor = .clear
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = guardian.name
        cell.detailTextLabel?.text = guardian.type.title
        
        
        cell.imageView?.downloadedFrom(link: guardian.avatar, placeHolder: Icon.Account.AvatarDefault)
    }
    
    func configureAddCell(_ cell: ContactAddFriendTableViewCell, atIndexPath indexPath: IndexPath) {
        
        
        cell.seperator.isHidden = (indexPath.row == suggestList.count - 1)
        let guardian = suggestList[indexPath.row]
        
        
        
        cell.buttonAdd.tag = indexPath.section * 1000 + indexPath.row
        cell.buttonAdd.isHidden = false
        cell.buttonAdd.addTarget(self, action: #selector(self.addFriend(_:)), for: .touchUpInside)
        
        if let userInfo = AppData.instance.myInfo, userInfo.guestSendRequest.contains(guardian.id) {
            cell.buttonAdd.isSelected = true
            cell.buttonAdd.backgroundColor = UIColor.Text.grayMediumColor()
            
        } else {
            cell.buttonAdd.isSelected = false
            cell.buttonAdd.backgroundColor = UIColor.Navigation.mainColor()
        }
        
        
        
        cell.detailTextLabel?.numberOfLines = 1
        cell.backgroundColor = .clear
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = guardian.name
        cell.detailTextLabel?.text = guardian.type.title
        
        
        cell.imageView?.downloadedFrom(link: guardian.avatar, placeHolder: Icon.Account.AvatarDefault)
    }
    
    
    func configureCell(_ cell: ContactTableViewCell, atIndexPath indexPath: IndexPath) {
        
        
        let guardian: GuestInfo = AppData.instance.guestInfoList[indexPath.row]
        cell.seperator.isHidden = (indexPath.row == AppData.instance.guestInfoList.count - 1)
        cell.isOnline = guardian.isOnline
        cell.isPadding = true
        cell.statusView.isHidden = !AppData.instance.isCanChat
        cell.buttonMessage.tag = indexPath.section * 1000 + indexPath.row
        cell.buttonMessage.isHidden = false
        
        
        cell.detailTextLabel?.numberOfLines = 2
        cell.backgroundColor = .clear
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = guardian.name
        cell.detailTextLabel?.text = guardian.email
        
        //        if AppData.instance.isCanChat {
        //
        //            cell.isNewMessageView.isHidden = !guardian.isNewMessage
        //            cell.buttonMessage.addTarget(self, action: #selector(self.openRoomChat(_:)), for: .touchUpInside)
        //            cell.buttonMessage.setImage(Icon.TabBar.Message.tint(UIColor.Navigation.mainColor()), for: .normal)
        //
        //        } else {
        
        cell.isNewMessageView.isHidden = true
        cell.buttonMessage.isHidden = !(guardian.phone.count > 0)
        cell.buttonMessage.addTarget(self, action: #selector(self.callPhone(_:)), for: .touchUpInside)
        cell.buttonMessage.setImage(Icon.Login.Phone.tint(UIColor.Navigation.mainColor()), for: .normal)
        //        }
        
        
        if let guestInfo = Utility.getUserInfoFromID(guardian.id) as? GuestInfo {
            cell.isOnline = guestInfo.isOnline
        }

               
        
        if let image = guardian.imageAvatar { cell.imageView?.image = image; return }
        
        cell.imageView?.downloadedFrom(link: guardian.avatar, placeHolder: Icon.Account.AvatarDefault) { image in
            guardian.imageAvatar = image
        }
        
    }
    
    
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension ContactViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        switch contactType {
        case .contact:
            
            let contactDetailVC: ContactDetailsViewController = ContactDetailsViewController()
            contactDetailVC.title = "Chi tiết"
            
            switch arraySection[indexPath.section] {
            case .suggest:
                contactDetailVC.info = suggestList[indexPath.row]
                
            case .confirm:
                contactDetailVC.info = confirmList[indexPath.row]
                
            case .contact:
                contactDetailVC.info = AppData.instance.guestInfoList[indexPath.row]
                
            }
            
            navigationController?.pushViewController(contactDetailVC, animated: true)
            
            
        case .message:
            delegate?.didSelectedGuestInfo(AppData.instance.guestInfoList[indexPath.row])
        }
        
        

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch arraySection[indexPath.section] {
        case .confirm:
            return 65 + 36
        default:
             return 65
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch arraySection[section] {
        case .suggest where suggestList.count > 0:
            return 38
            
        case .confirm where confirmList.count > 0:
            return 38
            
        case .contact where AppData.instance.guestInfoList.count > 0:
            return 38
            
        default:
            return CGFloat.leastNonzeroMagnitude
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch arraySection[section] {
        case .suggest where suggestList.count > 0:
            return 10
            
        case .confirm where confirmList.count > 0:
            return 10
            
        case .contact where AppData.instance.guestInfoList.count > 0:
            return 10
            
        default:
            return CGFloat.leastNonzeroMagnitude
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        
        
        let headerView: HeaderViewSection = HeaderViewSection()
        headerView.label.font = UIFont(name: FontType.latoBold.., size: FontSize.small..)
        headerView.bgView.backgroundColor = UIColor.Navigation.mainColor()
        
        headerView.label.text = arraySection[section].title
        
        headerView.backgroundColor = .clear
        
        switch arraySection[section] {
        case .suggest:
            return suggestList.count > 0 ? headerView : nil
            
        case .confirm:
            return confirmList.count > 0 ? headerView : nil
            
        default:
            return AppData.instance.guestInfoList.count > 0 ? headerView : nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        let footerView = FooterViewSection()
        footerView.backgroundColor = UIColor.clear
        footerView.button.isHidden = true
        
        switch arraySection[section] {
        case .suggest:
            return suggestList.count > 0 ? footerView : nil
            
        case .confirm:
            return confirmList.count > 0 ? footerView : nil
            
        default:
            return AppData.instance.guestInfoList.count > 0 ? footerView : nil
        }
        
        
    }
    
    
}

//-------------------------------------------
//MARK: - SEARCH CONTROLLER DELEGATE
//-------------------------------------------

extension ContactViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        
        searchController.searchBar.setShowsCancelButton(true, animated: false)
        let view = searchController.searchBar.subviews[0]
        for item in view.subviews {
            if let textField = item as? UITextField {
                textField.autocorrectionType = .no
                textField.autocapitalizationType = .none
                textField.returnKeyType = .search
            }
            
            if let button = item as? UIButton {
                button.setTitle(LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), for: .normal)
                button.setTitleColor(UIColor.white, for: .normal)
                button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)
            }
        }
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        searchController.searchBar.resignFirstResponder()
        
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        
    }
    
    
}


//-------------------------------------------
//MARK: - SEARCH BAR DELEGATE
//-------------------------------------------

extension ContactViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let normalText: String = Utility.normalizeString(searchText)
        
        searchDataSource.contactResult  = AppData.instance.guestInfoList.filter { $0.searchName.contains(normalText)  }
        
        //        searchDataSource.teachersResult  = patients.filter { Utility.normalizeString($0.name).contains(normalText) || Utility.normalizeString($0.searchName).contains(normalText)  }
        
        searchContactWithKeyWord(normalText)
        
        guard let searchResultsController = searchController.searchResultsController as? UITableViewController else { return }
        searchResultsController.tableView.reloadData()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
}

//-------------------------------------------
//MARK: - CONTACT SEARCH DELEGATE
//-------------------------------------------
extension ContactViewController: ContactSearchDataSourceDelegate {
    
    internal func didSelectedGuestInfo(_ info: GuestInfo) {
        
        
        switch contactType {
        case .contact:
            let contactDetailVC: ContactDetailsViewController = ContactDetailsViewController()
            contactDetailVC.title = LocalizedString("general_label_detail", comment: "Chi tiết")
            contactDetailVC.info = info
            navigationController?.pushViewController(contactDetailVC, animated: true)
            
        case .message:
            delegate?.didSelectedGuestInfo(info)
        }
        
        

    }
    
    
    func openRoomChatWithInfo(_ guestInfo: GuestInfo) {
        
        dismiss(animated: true, completion: nil)
        
        let messageDetailVC: MessageDetailViewController = MessageDetailViewController()
        messageDetailVC.guestInfo = guestInfo
        navigationController?.pushViewController(messageDetailVC, animated: true)
        
        
    }
    
    func callPhone(_ name: String, phone: String) {
        
        searchController.isActive = false
        
        showAlertController(.callParent(name, phone))
    }
}

//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------
extension ContactViewController {
    @objc func search(_ sender: UIBarButtonItem){
        searchController.isActive = true
    }
    
    @objc func back(_ sender: UIButton) {
        
        switch contactType {
        case .contact:
            
            _ = navigationController?.popViewController(animated: true)
            
        case .message:
           dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func callPhone(_ sender: UIButton) {
        
        switch arraySection[sender.tag / 1000] {
        case .suggest:
            return
            
        case .confirm:
            return
            
        case .contact:
            showAlertController(.callParent(AppData.instance.guestInfoList[sender.tag % 1000].name, AppData.instance.guestInfoList[sender.tag % 1000].phone))
        }
        
        
    }
    
    
    @objc func confirmFriend(_ sender: UIButton) {
        
        guard var userInfo = AppData.instance.myInfo else { return }
        
        print("userInfo.guestFriend xxxx: \(userInfo.guestFriend)")
        
        let guestInfo = confirmList[sender.tag % 1000]
        
        if let pos = userInfo.guestConfirmRequest.firstIndex(where: { $0 == guestInfo.id }) {
            userInfo.guestConfirmRequest.remove(at: pos)
        }
        
        if !userInfo.guestFriend.contains(guestInfo.id) {
            userInfo.guestFriend.append(guestInfo.id)
        }
        
        
        AppData.instance.myInfo?.guestConfirmRequest = userInfo.guestConfirmRequest
        AppData.instance.myInfo?.guestFriend = userInfo.guestFriend
        
        print("userInfo.guestFriend: \(userInfo.guestFriend)")
        
        
        HUD.showHUD()
        
        guestRef.child(guestInfo.id).observeSingleEvent(of: .value, with: { snapshot in
            
            guard let data = snapshot.value as? [String : Any] else { return }
            var info = User(data: data)
            
            HUD.dismissHUD()
            
            
            if let pos = info.guestSendRequest.firstIndex(where: { $0 == userInfo.uid }) {
                info.guestSendRequest.remove(at: pos)
            }
            
            
            if !info.guestFriend.contains(userInfo.uid) {
                info.guestFriend.append(userInfo.uid)
            }
            
            
            print("userInfo.guestFriendaaaa: \(userInfo.guestFriend)")
            print("info.guestFriendaaaa: \(info.guestFriend)")
            
            
            self.guestRef.child(userInfo.uid).child("guest_confirm_friend").setValue(userInfo.guestConfirmRequest)
            self.guestRef.child(userInfo.uid).child("guest_friend").setValue(userInfo.guestFriend)
            
            self.guestRef.child(guestInfo.id).child("guest_send_friend").setValue(info.guestSendRequest)
            self.guestRef.child(guestInfo.id).child("guest_friend").setValue(info.guestFriend)
            
            self.table.reloadData()
            

            
        })
        
        
        
    }
    
    @objc func deleteFriend(_ sender: UIButton) {
        
        guard var userInfo = AppData.instance.myInfo else { return }
        
        let guestInfo = confirmList[sender.tag % 1000]
        
        if let pos = userInfo.guestConfirmRequest.firstIndex(where: { $0 == guestInfo.id }) {
            userInfo.guestConfirmRequest.remove(at: pos)
        }
        
 
        AppData.instance.myInfo?.guestConfirmRequest = userInfo.guestConfirmRequest
        
        HUD.showHUD()
        
        guestRef.child(guestInfo.id).observeSingleEvent(of: .value, with: { snapshot in
            
            guard let data = snapshot.value as? [String : Any] else { return }
            var info = User(data: data)
            
            HUD.dismissHUD()
            
            
            if let pos = info.guestSendRequest.firstIndex(where: { $0 == userInfo.uid }) {
                info.guestSendRequest.remove(at: pos)
            }
            

            
            self.guestRef.child(userInfo.uid).child("guest_confirm_friend").setValue(userInfo.guestConfirmRequest)

            self.guestRef.child(guestInfo.id).child("guest_send_friend").setValue(info.guestSendRequest)
        
            self.table.reloadData()
            
        })
        
    }
    
    
    @objc func addFriend(_ sender: UIButton) {
        
        guard var userInfo = AppData.instance.myInfo else { return }
        
        let guestInfo = suggestList[sender.tag % 1000]
        
        if sender.isSelected {
            
            if let pos = userInfo.guestSendRequest.firstIndex(where: { $0 == guestInfo.id }) {
                userInfo.guestSendRequest.remove(at: pos)
            }
        } else {
            userInfo.guestSendRequest.append(guestInfo.id)
            
        }
        
        AppData.instance.myInfo?.guestSendRequest = userInfo.guestSendRequest
        
        

        
        
        HUD.showHUD()
        
        guestRef.child(guestInfo.id).observeSingleEvent(of: .value, with: { snapshot in
            
            guard let data = snapshot.value as? [String : Any] else { return }
            var info = User(data: data)
            
            HUD.dismissHUD()
            
            if sender.isSelected {
                
                if let pos = info.guestConfirmRequest.firstIndex(where: { $0 == userInfo.uid }) {
                    info.guestConfirmRequest.remove(at: pos)
                }
            } else {
                info.guestConfirmRequest.append(userInfo.uid)
                
            }
            
            self.guestRef.child(guestInfo.id).child("guest_confirm_friend").setValue(info.guestConfirmRequest)
            self.guestRef.child(userInfo.uid).child("guest_send_friend").setValue(userInfo.guestSendRequest)
            
            self.table.reloadData()
            
        })
        
        
        
    }
    
    
    
    @objc func reloadTable() {
        
        table.reloadData()
    }
    
    @objc func reloadTableSection(_ notification: Notification) {
        
        guard let userInfo = notification.userInfo else { return }
        guard let guestID = userInfo["id"] as? String else { table.reloadData(); return }
        
        
        guard let section = arraySection.firstIndex(of: .contact) else { return  }
        
        
        if let pos = AppData.instance.guestInfoList.firstIndex(where: { $0.id == guestID }),
            section < table.numberOfSections,
            pos < table.numberOfRows(inSection: section) {
            
            
            if let cell = table.cellForRow(at: IndexPath(row: pos, section: section)) as? ContactTableViewCell {
                
                configureCell(cell, atIndexPath: IndexPath(row: pos, section: section))
                
            } else {
                table.reloadRows(at: [IndexPath(row: pos, section: section)], with: .none)
            }
            
            
        }
        
    }
    
    @objc func openRoomChat(_ sender: UIButton) {
        
        
        switch arraySection[sender.tag / 1000] {
        case .suggest:
            break
            
        case .confirm:
            return
            
        case .contact:
            let messageDetailVC: MessageDetailViewController = MessageDetailViewController()
            messageDetailVC.guestInfo = AppData.instance.guestInfoList[sender.tag % 1000]
            navigationController?.pushViewController(messageDetailVC, animated: true)
        }
    }
    
    
}

// MARK: - PRIVATE METHOOD
extension ContactViewController {

    func searchContactWithKeyWord(_ key: String) {
        
        
        
        var searchData: [GuestInfo] = []
        
        guestRef
            .queryOrdered(byChild: "email")
            .queryStarting(atValue: key)
            .queryEnding(atValue: key+"\u{f8ff}")
            .observeSingleEvent(of: .value, with: { snapshot in
                
                print("XXXXXXXXXX: \(snapshot)")
                
                for child in snapshot.children {
                    
                    print("child: \(child)")
                    
                    guard let snap = child as? DataSnapshot,
                        let data = snap.value as? [String : Any] else { continue }
                    
                    
                    
                    
                    let guestInfo = GuestInfo(data: data)
                    
                    if let userInfo = AppData.instance.myInfo,
                        userInfo.uid != guestInfo.id,
                        !userInfo.guestConfirmRequest.contains(guestInfo.id),
                        !userInfo.guestFriend.contains(guestInfo.id) {
                        searchData.append(guestInfo)
                    }
                }
                
                
                self.searchDataSource.searchResult = searchData
                
                
                guard let searchResultsController = self.searchController.searchResultsController as? UITableViewController else { return }
                searchResultsController.tableView.reloadData()
                
                
            })
        
        
//        guestRef.observeSingleEvent(of: .value, with: { snapshot in
//
//            for child in snapshot.children {
//
//                print("child: \(child)")
//
//                guard let snap = child as? DataSnapshot,
//                    let data = snap.value as? [String : Any] else { continue }
//

//                let guestInfo = GuestInfo(data: data)
//
//                if let userInfo = AppData.instance.myInfo,
//                    userInfo.uid != guestInfo.id,
//                    !userInfo.guestConfirmRequest.contains(guestInfo.id),
//                    !userInfo.guestFriend.contains(guestInfo.id) {
//                    searchData.append(guestInfo)
//                }
//            }
//
//
//            self.searchDataSource.searchResult = searchData
//
//
//            guard let searchResultsController = self.searchController.searchResultsController as? UITableViewController else { return }
//             searchResultsController.tableView.reloadData()
//
//
//        })
        
        
    }
}


extension ContactViewController {
    
    
    // MARK: - REQUEST CONTACT
    func checkNetworkAndRequestGetContact() {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            
            
            self.refreshControl.endRefreshing()
            
            return
        }
        
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        HUD.showHUD(onView: self.view)  {
            
            
            //            HTTPManager.instance.getContact
            //                .doRequest(HTTPGetContact.RequestType(studentID: Defaults[.StudentID], schoolID: Defaults[.SchoolID]), headers: headers)
            //                .completionHandler { result in
            //                     self.refreshControl.endRefreshing()
            //                    switch result {
            //                    case .success(let respond):
            //                        self.getContactRequestSuccess(respond)
            //
            //                    case .failure(let error):
            //                        self.requestFaild(error)
            //                        if AppData.instance.isCanChat {  self.observeUsersOnline() }
            //                    }
            //            }
            
        }
        
    }
    
    /**
     Request đăng nhập thành công
     */
    //    func getContactRequestSuccess(_ respond: HTTPGetContact.RespondType) {
    //
    //        HUD.dismissHUD()
    ////        AppData.instance.isAutoLogin = false
    ////        AppData.instance.isReloadContact = false
    //        AppData.instance.guestInfoList = respond.guestInfoList
    //
    //        reloadTable()
    //    }
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
    
}


//====================================
// MARK: - SETUP
//====================================

extension ContactViewController {
    func setupAllSubviews() {
        
        title = LocalizedString("tab_bar_item_contact", comment: "Danh bạ")
        view.backgroundColor = .white
        
        buttonBack = setupBackBarButton(Icon.Navigation.Delete, selector: #selector(self.back(_:)), target: self)
        
        
        setupTableDefault()
        setupDataSource()
        setupSearchController()
        setupSearchBar()
        setupBackground()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadTable),
                                               name: Notification.Name(rawValue: AppNotification.ReloadContact..),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadTableSection(_:)),
                                               name: Notification.Name(rawValue: AppNotification.ReloadTableContact..),
                                               object: nil)
        
        
        
        
        if #available(iOS 11.0, *) {
            searchBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 56))
            
        } else {
            searchBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        }
        
        searchBar.addSubview(searchController.searchBar)
        searchBar.clipsToBounds = true
        searchController.searchBar.sizeToFit()
        
        
        table.tableHeaderView = searchBar
    }
    
    
    func setupAllConstraints() {
        
        table.snp.makeConstraints { make in
            make.bottom.top.leading.trailing.equalTo(view)
        }
    }
    
    
    fileprivate func setupSearchBarButtonItem() {
        search = UIBarButtonItem(image: Icon.Navigation.Help, style: .plain, target: self, action: #selector(self.search(_:)))
        search.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = search
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTableDefault() {
        
        table = UITableView(frame: .zero, style: .grouped)
        table.delegate = self
        table.dataSource = self
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.clipsToBounds = false
        table.layer.masksToBounds = true
        table.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        table.layer.shadowOpacity = 0.3
        table.layer.shadowRadius = 3.0
        table.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        table.register(ContactTableViewCell.self, forCellReuseIdentifier: "ContactTableViewCell")
        table.register(ContactAddFriendTableViewCell.self, forCellReuseIdentifier: "ContactAddFriendTableViewCell")
        table.register(ContactConfirmTableViewCell.self, forCellReuseIdentifier: "ContactConfirmTableViewCell")
        view.addSubview(table)
    }
    
    /**
     Setup DataSource
     */
    fileprivate func setupDataSource() {
        searchDataSource = ContactSearchDataSource()
        searchDataSource.delegate = self
        
    }
    
    /**
     Setup Search Controller
     */
    fileprivate func setupSearchController() {
        
        let searchResultsController = UITableViewController(style: .plain)
        searchResultsController.tableView = setupTable(dataSource: searchDataSource, cellClass: ContactTableViewCell.self)
        searchResultsController.tableView.clipsToBounds = true
        searchController = CustomSearchController(searchResultsController: searchResultsController)
        searchController.delegate = self
        //        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
    }
    
    /**
     Setup Search Bar
     */
    fileprivate func setupSearchBar() {
        
        
        searchController.searchBar.placeholder = LocalizedString("search_bar_placeholder", comment: "Tìm kiếm")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.tintColor = UIColor(rgba: "#d8d8d8")
        searchController.searchBar.backgroundColor = UIColor.lightGray
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.isTranslucent = false
        
        searchController.searchBar.layer.borderWidth = onePixel()
        searchController.searchBar.layer.borderColor = UIColor(rgba: "#d8d8d8").cgColor
        searchController.searchBar.barTintColor = UIColor(rgba: "#d8d8d8")
        
        
        
        
        
    }
    
    /**
     Setup Table
     */
    func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .grouped)
        table.register(cellClass, forCellReuseIdentifier: "ContactCell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = UIColor.Table.tableGroupColor()
        return table
    }
    
    func setupBackground() {
        
        #if VINSCHOOL
        footerBackground = UIImageView(image: UIImage(named: "ImageCould") ?? UIImage())
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .bottom
        } else {
            footerBackground.contentMode = .bottom
        }
        
        #else
        footerBackground = UIImageView(image: UIImage(named: "ImageCould")?.tint(UIColor.Navigation.mainColor()) ?? UIImage())
        if UI_USER_INTERFACE_IDIOM() == .phone {
            footerBackground.contentMode = .scaleAspectFill
        } else {
            footerBackground.contentMode = .scaleAspectFill
        }
        #endif
        
        view.insertSubview(footerBackground, at: 0)
    }
    
    
}

