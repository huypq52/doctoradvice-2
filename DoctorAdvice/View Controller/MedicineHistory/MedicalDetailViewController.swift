//
//  MedicalDetailViewController.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/31/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit
import SwiftyUserDefaults
import Popover
import Photos
import DKImagePickerController
import Firebase
import SKPhotoBrowser
import Alamofire



protocol MedicalDetailViewControllerDelegate: class {
    func createMedicalSuccess(_ medical: MedicalInfo)
    func deleteMedicalSuccess(_ medical: MedicalInfo)
}

class MedicalDetailViewController: CustomViewController {
    
    /// ENUM
    enum AlertType {
        case deleteMedical
        
        func alertTitle() -> String {
            switch self {
            case .deleteMedical:
                return "Xác nhận"
            }
        }
        
        func alertMessage() -> String? {
            switch self {
            case .deleteMedical:  return "Bạn có muốn xóa bệnh án này không?"
            }
        }
    }
    
    
    enum Section: Int {
        case systom = 0, dateDiscovery, date, result, image
        
        var title: String {
            switch self {
            case .systom:
                return "Triệu chứng"
                
            case .dateDiscovery:
                return "Ngày phát hiện"
                
            case .date:
                return "Ngày khám"
                
            case .result:
                return "Kết quả khám"
                
            case .image:
                return "Chi tiết hồ sơ bệnh án"
                
            }
        }
    }
    
    
    var back: UIBarButtonItem!
    var edit: UIBarButtonItem!
    var buttonDelete: UIBarButtonItem!
    
    weak var delegate: MedicalDetailViewControllerDelegate?
    
    var ref: DatabaseReference!
    lazy var storageRef: StorageReference = Storage.storage().reference().child("users")
    
    
    
    
    /// VAR
    
    var arraySection: [Section] = [.systom, .dateDiscovery, .date, .result, .image]
    
    var medicalInfo: MedicalInfo = MedicalInfo(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension MedicalDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arraySection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch arraySection[section] {
        case .image:
            return medicalInfo.listPhoto.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch arraySection[indexPath.section] {
        case .image:
            let cellIdentifier = "NewPhotoTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! NewPhotoTableViewCell
            configuaTextPhoto(cell, indexPath: indexPath)
            return cell
            
        case .result:
            let cellIdentify = "TextViewTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! TextViewTableViewCell
            configuaTextViewCell(cell, indexPath: indexPath)
            return cell
            
            
        default:
            
            let cellIdentify = "SeperatorValueTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! SeperatorValueTableViewCell
            configuaCell(cell, indexPath: indexPath)
            return cell
            
        }
        
        
        
    }
    
    func configuaTextViewCell(_ cell: TextViewTableViewCell, indexPath: IndexPath) {
        
        cell.textView.isEditable = false
        cell.textView.text = medicalInfo.resultContent
        cell.textView.delegate = self
        cell.textView.tag = indexPath.section
        cell.textView.placeholder = arraySection[indexPath.section].title
    }
    
    func configuaCell(_ cell: SeperatorValueTableViewCell, indexPath: IndexPath) {
        
        cell.isPadding = false
        cell.textLabel?.text = arraySection[indexPath.section].title
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        cell.textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        
        cell.detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
        cell.detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        
        cell.seperatorStyle = .padding(10)
        cell.seperatorRightPadding = 10
        cell.selectionStyle = .none
        
        switch arraySection[indexPath.section] {
        case .dateDiscovery:

            cell.detailTextLabel?.text = Utility.getStringTime(medicalInfo.dateDiscovery, formatter: "dd/MM/yyyy")
            
        case .date:

            cell.detailTextLabel?.text = Utility.getStringTime(medicalInfo.date, formatter: "dd/MM/yyyy")
            
        case .systom:
     
            cell.detailTextLabel?.text = medicalInfo.symptom
            
            
        default:
            break
        }
    }
    
    
    func configuaTextPhoto(_ cell: NewPhotoTableViewCell, indexPath: IndexPath) {
        
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.buttonDelete.isHidden = true
        cell.buttonDelete.tag = indexPath.row
        cell.textView.tag = indexPath.section * 1000 + indexPath.row
        cell.textView.delegate = self
        cell.textView.isHidden = true
        cell.imageView?.downloadedFrom(link: medicalInfo.listPhoto[indexPath.row], placeHolder: nil)
        
        cell.layer.shadowColor   = UIColor.black.withAlphaComponent(0.3).cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius  = 3.0
        cell.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        
    }
    
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension MedicalDetailViewController: UITableViewDelegate {
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        switch arraySection[indexPath.section] {
        case .image:
            return UIScreen.main.bounds.width / 16 * 9
            
        case .result:
            return 150
            
        case .systom:
            return 64

        default:
            return 44
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch arraySection[section] {
        case .systom, .date, .dateDiscovery:
            return nil
            
            
        default:
            guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MedicalCreateHeaderSectionView") as? MedicalCreateHeaderSectionView else { return nil }
            
            headerView.labelTitle.text = arraySection[section].title
            headerView.labelTitle.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!
            headerView.buttonAdd.isHidden = true
            
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        switch arraySection[section] {
        case .systom, .date, .dateDiscovery:
            return CGFloat.leastNonzeroMagnitude
            
        default:
            return 44
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}




//-------------------------------------------
//MARK: - SELECTION CENTER DELEGATE
//-------------------------------------------
extension MedicalDetailViewController: UITextFieldDelegate {
    
    /**
     Hàm validate các textfield
     
     :param: textField
     :param: range
     :param: string
     
     :returns:
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var newLength =  string.count - range.length
        
        if let name = textField.text {
            newLength += name.count
        }
        
        return (newLength > 200) ? false : true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textViewSelected?.becomeFirstResponder()
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        scrollHeight = table.rectForRow(at: IndexPath(row: 0, section: textField.tag)).maxY - (view.frame.height - 5)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        medicalInfo.symptom = textField.text ?? ""
    }
}


//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------
extension MedicalDetailViewController {
    
    @objc func back(_ sender: UIBarButtonItem) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func edit(_ sender: UIButton) {
        
        let createVC = MedicalCreateViewController()
        createVC.typeView = .edit(medicalInfo)
        navigationController?.pushViewController(createVC, animated: true)
        
    }
    
    @objc func buttonDelete(_ sender: UIButton) {
        
        showAlertController(.deleteMedical)
        
    }
    
    /**
     Show Alert tùy theo type
     */
    func showAlertController(_ type: AlertType) {
        
        
        
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        
        
        guard let alertController = alertController else { return }
        
        
        
        switch type {
        case .deleteMedical:
            
            alertController.addAction(UIAlertAction(title: LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
            
            alertController.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: {[unowned self] _ in
                
                
                
                self.deleteMedical()
                
                
            }))
            
        }
        
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            present(alertController, animated: true, completion: nil)
        } else {
            if let popoverController = alertController.popoverPresentationController {
                // show action sheet
                popoverController.sourceRect = CGRect(x: 0, y: 0, width: view.frame.width , height: 44)
                popoverController.sourceView = self.view
            }
            present(alertController, animated: true, completion: nil)
        }
        
    }
    
}


//====================================
// MARK: - PRIVATE METHOOD
//====================================
extension MedicalDetailViewController {
    
    
    func deleteMedical() {
        
        guard let userInfo = AppData.instance.myInfo else { return }
        
        ref.child("users").child(userInfo.uid).child("medicals").child(medicalInfo.id).removeValue { error, _ in
            
            
            
            if let error = error {
                print(error)
            }
            else {
                
                if self.medicalInfo.listPhoto.count > 0 {
                    
                    let storage = Storage.storage()
                    
                    self.medicalInfo.listPhoto.enumerated().forEach { (i, photo) in
                        
                        let storageRef = storage.reference(forURL: photo)
                        
                        //Removes image from storage
                        storageRef.delete { error in
                            if let error = error {
                                print(error)
                            } else {
                                // File deleted successfully
                                
                                print("Xóa ảnh thành công")
                                
                                
                            }
                            
                            if i == self.medicalInfo.listPhoto.count - 1 {
                                self.delegate?.deleteMedicalSuccess(self.medicalInfo)
                                HUD.showMessage("Xóa bệnh án thành công!") {
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        
                    }
                    
                } else {
                    self.delegate?.deleteMedicalSuccess(self.medicalInfo)
                    HUD.showMessage("Xóa bệnh án thành công!") {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
                
                
                
            }
            
        }
    }
    
}

//====================================
// MARK: - REQUEST
//====================================
extension MedicalDetailViewController {
    
    
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    fileprivate func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
    
}


extension MedicalDetailViewController {
    /**
     Lấy thông tin chiều cao của bàn phím khi hiện lên
     Để sau này tính offset cho các màn hình bé
     - parameter sender:
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        
        let scrollPoint = CGPoint(x: 0.0, y: scrollHeight + frame.height)
        
        if scrollPoint.y > 0 {
            table.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
    }
    
    func hiddenKeyboard() {
        guard let textView = textViewSelected else { return }
        textView.resignFirstResponder()
    }
}

//-------------------------------
// -MARK: SETUP VIEW
//-------------------------------
extension MedicalDetailViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        textViewSelected = textView
        
        scrollHeight = table.rectForRow(at: IndexPath(row: 0, section: textView.tag)).maxY - view.frame.height
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        switch arraySection[textView.tag] {
        case .result:
            medicalInfo.resultContent = textView.text
        default:
            break
        }
        
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count + text.count - range.length <= 5000
    }
}



//====================================
// MARK: - SETUP
//====================================

extension MedicalDetailViewController {
    fileprivate func setupAllSubviews() {
        
        title = "Chi tiết"
        
        view.backgroundColor = UIColor.Table.tableGrayColor()
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        
        
        if let userInfo = AppData.instance.myInfo, userInfo.accountType == .patient, userInfo.uid == medicalInfo.createBy {
            edit = setupBackBarButton(Icon.Navigation.Edit, selector: #selector(self.edit(_:)), target: self)
            buttonDelete = setupBackBarButton(Icon.Navigation.Trash, selector: #selector(self.buttonDelete(_:)), target: self)
            
            navigationItem.rightBarButtonItems = [buttonDelete, edit]
        }
        
        
        
        
        navigationItem.leftBarButtonItem = back
        
        
        
        setupTable()
        setupNotificationObserver()
        
    }
    
    
    func setupAllConstraints() {
        
        table.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
    }
    
    /**
     setup Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /**
     Setup Bar Button
     */
    func setupBarTitle(_ title: String, selector: Selector, target: AnyObject? = nil) -> UIBarButtonItem {
        let button = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)
        button.tintColor = .white
        return button
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable() {
        table = UITableView(frame: CGRect.zero, style: .grouped)
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.register(SeperatorValueTableViewCell.self,
                       forCellReuseIdentifier: "SeperatorValueTableViewCell")
        
        table.register(TextFieldTableViewCell.self,
                       forCellReuseIdentifier: "EmptyCell")
        
        table.register(NewPhotoTableViewCell.self,
                       forCellReuseIdentifier: "NewPhotoTableViewCell")
        
        table.register(TextViewTableViewCell.self,
                       forCellReuseIdentifier: "TextViewTableViewCell")
        
        table.register(MedicalCreateHeaderSectionView.self,
                       forHeaderFooterViewReuseIdentifier: "MedicalCreateHeaderSectionView")
        
        table.delegate = self
        table.dataSource = self
        table.keyboardDismissMode = .onDrag
        view.addSubview(table)
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.register(cellClass, forCellReuseIdentifier: "Cell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = UIColor.Table.tablePlainColor()
        return table
    }
    
}








