//
//  MedicineHistoryViewController.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/26/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit
import SnapKit
import SwiftyUserDefaults
import Firebase


struct MedicalInfo {
    
    var id: String
    var symptom: String
    var dateDiscovery: Double
    var date: Double
    var resultContent: String
    var listPhoto: [String]
    var createdAt: Double
    var createBy: String
    
    init(data: [String : Any], id: String? = nil) {
        
        self.id = data["id"] as? String ?? id ?? ""
        

        self.createdAt = data["created_at"] as? Double ?? 0
        self.createBy = data["created_by"] as? String ?? ""
        
        self.symptom = data["symptom"] as? String ?? ""
        self.dateDiscovery = data["date_discovery"] as? Double ?? 0
        self.date = data["date"] as? Double ?? 0
        self.resultContent = data["result_content"] as? String ?? ""
        self.listPhoto = data["list_photo"] as? [String] ?? []
    }
}



class MedicalHistoryViewController: CustomViewController {
    

     var buttonAdd: UIBarButtonItem!
    var buttonBack: UIBarButtonItem!
    
    var dataArray: [MedicalInfo] = []
    
    
    var ref: DatabaseReference!
    var userID: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
         
        if let userInfo = AppData.instance.myInfo,  userInfo.accountType == .patient, userID == nil {
            userID = userInfo.uid
        }
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
        checkNetworkAndRequestGetMedicalList()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)


    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension MedicalHistoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count > 0 ? dataArray.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if dataArray.count > 0 {
            let cellIdentify = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! SeperatorTableViewCell
            configuageCell(cell, indexPath: indexPath)
            return cell
        } else {
            
            
            let cellIdentify = "EmptyCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! EmptyImageTableViewCell
            
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.textLabel?.text = "Chưa có hồ sơ bệnh án"
            return cell
            
            
        }
        

        
        
    }
    
    
    func configuageCell(_ cell: SeperatorTableViewCell, indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        cell.detailTextLabel?.textColor = UIColor.Text.blackMediumColor()
        
        cell.textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!
        cell.detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.small++)!
        
        cell.textLabel?.text = dataArray[indexPath.row].symptom
        cell.detailTextLabel?.text = Utility.getStringTime(dataArray[indexPath.row].createdAt, formatter: "HH:mm dd/MM/yyyy")
    
    }
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension MedicalHistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        guard dataArray.count > 0 else { return }
        
        let detailVC = MedicalDetailViewController()
        detailVC.medicalInfo = dataArray[indexPath.row]
        detailVC.delegate = self
        navigationController?.pushViewController(detailVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard dataArray.count > 0 else {  return tableView.frame.height * 2 / 3 }
       return 50
    }
    
}

//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------
extension MedicalHistoryViewController: MedicalDetailViewControllerDelegate {
    
    func deleteMedicalSuccess(_ medical: MedicalInfo) {
        guard let pos = dataArray.firstIndex(where: { $0.id == medical.id }) else { return }
        dataArray.remove(at: pos)
        table.reloadData()
    }
}

//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------
extension MedicalHistoryViewController {
    
    @objc func back(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func addMedical(_ sender: UIBarButtonItem) {
  
        let newVC = MedicalCreateViewController()
        newVC.delegate = self
        var medicalInfo = MedicalInfo(data: [:])
        medicalInfo.date = Date().timeIntervalSince1970
        medicalInfo.dateDiscovery = Date().timeIntervalSince1970
        
        newVC.typeView = .create(medicalInfo)
        navigationController?.pushViewController(newVC, animated: true)
        
    }
    

}

extension MedicalHistoryViewController: MedicalCreateViewControllerDelegate {
    
    func createMedicalSuccess(_ medical: MedicalInfo) {
        
        if let pos = dataArray.firstIndex(where: { $0.id == medical.id }) {
            dataArray[pos] = medical
        } else {
            dataArray.insert(medical, at: 0)
        }
        
        table.reloadData()
    }
}


extension MedicalHistoryViewController {
    
    //----------------------------------------
    // - MARK: GET ALBUM LIST
    //----------------------------------------
    
    internal func checkNetworkAndRequestGetMedicalList() {
        
        guard let userID = userID else { return }

        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        dataArray = []
        ref.child("medicals")
            .queryOrdered(byChild: "created_by")
            .queryEqual(toValue: userID)
            .observeSingleEvent(of: .value, with: { snapshot in
            
            for child in snapshot.children {
                
                print("child: \(child)")
                
                guard let snap = child as? DataSnapshot,
                    let data = snap.value as? [String : Any] else { continue }
                
                self.dataArray.append(MedicalInfo(data: data, id: snap.key))
      
               
            }

            self.table.reloadData()
            
            
        })
               

        
    }

//    /**
//     Request lấy danh sách bài viết thành công
//     */
//    fileprivate func getEventsRequestSuccess(_ respond: HTTPGetEventIndex.RespondType, loadType: LoadType) {
//
//        HUD.dismissHUD()
//
//        switch loadType {
//        case .newUpdate:
//            lastTimeUpdate = Date().timeIntervalSince1970
//        default:
//            allowLoadMore = respond.events.count > 0
//        }
//
//
//
//        for item in respond.events {
//
//            if let pos = events.firstIndex(where: { $0.eventID == item.eventID }) {
//                events[pos] = item
//            } else {
//                events.append(item)
//            }
//        }
//
//
//        events = events
//            .sorted { $0.receivedAt > $1.receivedAt}
//            .sorted { $0.important && !$1.important}
//
//        table.reloadData()
//
//    }
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    fileprivate func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
}


//====================================
// MARK: - SETUP
//====================================

extension MedicalHistoryViewController {
    fileprivate func setupAllSubviews() {
        
        
        
        title = "Lịch sử bệnh án"
        view.backgroundColor = .white
    
        buttonAdd = setupBackBarButton(Icon.General.Add, selector: #selector(self.addMedical(_:)), target: self)
        
        buttonBack = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
        
        
        if let userInfo = AppData.instance.myInfo, userInfo.accountType == .patient, (userID ?? "") == userInfo.uid {
           navigationItem.rightBarButtonItem = buttonAdd
        } else {
            navigationItem.leftBarButtonItem = buttonBack
        }
        
        
        


        table = setupTable(dataSource: self, cellClass: SeperatorTableViewCell.self)
        
        view.addSubview(table)
    }
    
    func setupAllConstraints() {
        
        table.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalTo(view)
            make.top.equalTo(view)
        }
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.register(cellClass, forCellReuseIdentifier: "Cell")
        table.register(EmptyImageTableViewCell.self, forCellReuseIdentifier: "EmptyCell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        return table
    }
}





