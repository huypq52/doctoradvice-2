//
//  MedicalCreateViewController.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/27/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit
import SwiftyUserDefaults
import Popover
import Photos
import DKImagePickerController
import Firebase
import SKPhotoBrowser
import Alamofire

struct PhotoUpload {
    var identity: String
    var image: UIImage
    var content: String
    var retry: Int
    
    init(identity: String,
         image: UIImage,
         content: String,
         retry: Int) {
        self.identity = identity
        self.image = image
        self.content = content
        self.retry = retry
    }
}

protocol MedicalCreateViewControllerDelegate: class {
    func createMedicalSuccess(_ medical: MedicalInfo)
}

class MedicalCreateViewController: CustomViewController {
    

    /// ENUM
    enum AlertType {
        case addImage
        
        func alertTitle() -> String {
            switch self {
            case .addImage:
                return LocalizedString("notification_general_button_selection_image", comment: "Chọn ảnh")
            }
        }
        
        func alertMessage() -> String? {
            switch self {
            case .addImage:  return nil
            }
        }
    }
    
    enum TypeView {
          case create(MedicalInfo)
          case edit(MedicalInfo)
          case detail(MedicalInfo)
      }
    
    enum Section: Int {
        case systom = 0, dateDiscovery, date, result, image
        
        var title: String {
            switch self {
            case .systom:
                return "Triệu chứng"
                
            case .dateDiscovery:
                return "Ngày phát hiện"
                
            case .date:
                return "Ngày khám"
                
            case .result:
                return "Kết quả khám"
                
            case .image:
                return "Chi tiết hồ sơ bệnh án"
    
            }
        }
    }
    

    fileprivate var back: UIBarButtonItem!
    fileprivate var send: UIBarButtonItem!
    
    
    weak var delegate: MedicalCreateViewControllerDelegate?
    
    var ref: DatabaseReference!
    lazy var storageRef: StorageReference = Storage.storage().reference().child("users")
    
    
    var typeView: TypeView = .create(MedicalInfo(data: [:])) {
        didSet {
            
            
            switch typeView {
            case .create(let info):
                medicalInfo = info
                 title = "Thêm bệnh án"
                
            case .edit(let info):
                medicalInfo = info
                title = "Sửa bệnh án"
                
            case .detail(let info):
                medicalInfo = info
                title = "Chi tiết"
            }
            
        }
    }
    
    
    /// VAR
    
    var arraySection: [Section] = [.systom, .dateDiscovery, .date, .result, .image]
    
    var medicalInfo: MedicalInfo = MedicalInfo(data: [:])
    var datePickerView: DatePickerView?

    var newPhotos: [Photo] = []
    
//    var arrayImage: [UIImage] = []
    

//    var assets: [DKAsset]? {
//        didSet {
//
//
//            arrayImage = []
//            arrayString = []
//
//            guard let assets = assets, assets.count > 0 else { table.reloadData(); return }
//
//            assets.enumerated().forEach { (arg) in
//
//
//                let (i, asset) = arg
//                asset.fetchOriginalImage(completeBlock: { image, info in
//
//                    if let image = image {
//
//                        let croppedImage = self.cropImage(image, toSize:CGSize(width: 720, height: 720))
//
//                        self.arrayImage.append(croppedImage)
//                        if let imageData = croppedImage.jpegData(compressionQuality: 0.9) {
//                            self.arrayString.append(imageData.base64EncodedString(options: .lineLength64Characters))
//                        }
//                    }
//
//                    if i == assets.count - 1 {
////                        self.table.reloadSections(IndexSet(integer: 0), with: .automatic)
//
//                        self.table.reloadData()
//                    }
//                })
//            }
//
//        }
//    }
    
    
    var arrayPhotoUpload: [PhotoUpload] = []
    var isReloadAssest: Bool = true
    
    var assets: [DKAsset]? {
        didSet {
            
            guard isReloadAssest else {
                isReloadAssest = true
                return
            }

            let oldArray = arrayPhotoUpload
            
            arrayPhotoUpload = []
        
            
            guard let assets = assets, assets.count > 0 else {
                table.reloadData()
                return
            }
            
            HUD.showHUD(onView: self.view)
            
            assets.enumerated().forEach { (i, asset) in
                
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    
                    if let image = image {
                        self.arrayPhotoUpload.append(PhotoUpload(identity: asset.localIdentifier,
                                                                 image: self.cropImage(image, toSize:CGSize(width: 1152, height: 1152)),
                                                                 content: oldArray.first(where: { $0.identity == asset.localIdentifier})?.content ?? "",
                                                                 retry: 0))
                    }
                    
                    if i == assets.count - 1 {
                        
                        if let pos = self.arraySection.firstIndex(of: .image) {
                            self.table.reloadSections(IndexSet(integer: pos), with: .automatic)
                        }
                        
                        HUD.dismissHUD()
                    }
                })
                
            }
        }
    }
    
    
    ///-----------------
    
    var currentPhoto = 0
    
    var retry = 0
    
    var maxRetry = 3
    
    var arrayString: [String] = []
    
    
    
    //-------------------------------------------
    //MARK: - SEARCH CONTROLLER
    //-------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()

        setupAllSubviews()
        view.setNeedsUpdateConstraints()

    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}


//====================================
// MARK: - TABLE DATASOURCE
//====================================

extension MedicalCreateViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arraySection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch arraySection[section] {
        case .image:
            return arrayPhotoUpload.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch arraySection[indexPath.section] {
        case .image:
            let cellIdentifier = "NewPhotoTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! NewPhotoTableViewCell
            configuaTextPhoto(cell, indexPath: indexPath)
            return cell
            
        case .result:
            let cellIdentify = "TextViewTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! TextViewTableViewCell
            configuaTextViewCell(cell, indexPath: indexPath)
            return cell
            
            
        default:
            
            let cellIdentify = "LoginTextFieldTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! LoginTextFieldTableViewCell
            configuaCell(cell, indexPath: indexPath)
            return cell
            
        }
        
        

    }
    
    func configuaTextViewCell(_ cell: TextViewTableViewCell, indexPath: IndexPath) {
        
        cell.textView.text = medicalInfo.resultContent
        cell.textView.delegate = self
        cell.textView.tag = indexPath.section
        cell.textView.placeholder = arraySection[indexPath.section].title
    }
    
    func configuaCell(_ cell: LoginTextFieldTableViewCell, indexPath: IndexPath) {
        
        
        cell.layer.shadowColor   = UIColor.black.withAlphaComponent(0.3).cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius  = 3.0
        cell.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        
        cell.textLabel?.text = arraySection[indexPath.section].title
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!
        
        cell.textLabel?.isHidden = true
        cell.seperatorStyle = .hidden
        cell.padding = 10
        cell.contentView.layer.cornerRadius = 5
        
        cell.textField.delegate = self
        cell.textField.placeholder = arraySection[indexPath.section].title
        cell.textField.tag = indexPath.row
        
        cell.imageView?.image = Icon.General.Calendar.tint(UIColor.lightGray)
        cell.imageView?.isHidden = false
        
        switch arraySection[indexPath.section] {
        case .dateDiscovery:
            cell.textField.isEnabled = false
            cell.textField.text = Utility.getStringTime(medicalInfo.dateDiscovery, formatter: "dd/MM/yyyy")
            
        case .date:
            cell.textField.isEnabled = false
            cell.textField.text = Utility.getStringTime(medicalInfo.date, formatter: "dd/MM/yyyy")
            
            
        default:
            cell.imageView?.isHidden = true
        }
        
        
        if indexPath.section == arraySection.count - 1 {
            cell.textField.returnKeyType = .done
        } else {
            cell.textField.returnKeyType = .next
        }
        
    }
    
    
    func configuaTextPhoto(_ cell: NewPhotoTableViewCell, indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.buttonDelete.addTarget(self, action: #selector(self.deletePicture(_:)), for: .touchUpInside)
        cell.buttonDelete.tag = indexPath.row
        cell.textView.tag = indexPath.section * 1000 + indexPath.row
        cell.textView.delegate = self
        cell.textView.isHidden = true
        cell.imageView?.image = arrayPhotoUpload[indexPath.row].image

        cell.layer.shadowColor   = UIColor.black.withAlphaComponent(0.3).cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius  = 3.0
        cell.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        
    }
    
}


//====================================
// MARK: - TABLE DELEGATE
//====================================

extension MedicalCreateViewController: UITableViewDelegate {
    
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch arraySection[indexPath.section] {
        case .dateDiscovery, .date:
            
            showDatePickerView(indexPath.section)
            
        default:
            break
        }
 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        switch arraySection[indexPath.section] {
        case .image:
            return UIScreen.main.bounds.width / 16 * 9
            
        case .result:
            return 150
            
        default:
            return 44
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MedicalCreateHeaderSectionView") as? MedicalCreateHeaderSectionView else { return nil }
        
        headerView.labelTitle.text = arraySection[section].title
        headerView.labelTitle.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!
        headerView.buttonAdd.tag = section
        headerView.buttonAdd.addTarget(self, action: #selector(self.selectionImage(_:)), for: .touchUpInside)
        
        switch arraySection[section] {
        case .image:
            headerView.buttonAdd.isHidden = false
        default:
            headerView.buttonAdd.isHidden = true
        }
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
}


//-------------------------------------------
//  MARK: - IMAGE PICKER DELEGATE
//-------------------------------------------

extension MedicalCreateViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        dismiss(animated: false, completion: nil)
        let croppedImage = cropImage(image, toSize:CGSize(width: 1024, height: 1024))
        
        
        arrayPhotoUpload.append(PhotoUpload(identity: UUID().uuidString, image: croppedImage, content: "", retry: 0) )
    }
    
    
    fileprivate func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        
        
        let maxSize: CGFloat = 840
        
        if image.size.width >= image.size.height { newSize = CGSize(width: maxSize, height: maxSize * image.size.height / image.size.width) }
        else { newSize = CGSize(width: maxSize * image.size.width / image.size.height, height: maxSize) }
        
        
        //        newSize = image.size
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
        
        /**
         *  Crop to size
         */
        
        //        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        //        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        ////        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        //
        //        let cropRect = CGRect(x: 0, y: 0, width: Int(size.height), height: Int(size.width))
        //        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        //
        //        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        //        return cropped
    }
}




//-------------------------------------------
//MARK: - SELECTION CENTER DELEGATE
//-------------------------------------------
extension MedicalCreateViewController: UITextFieldDelegate {
    
    /**
     Hàm validate các textfield
     
     :param: textField
     :param: range
     :param: string
     
     :returns:
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var newLength =  string.count - range.length
        
        if let name = textField.text {
            newLength += name.count
        }
        
        return (newLength > 200) ? false : true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textViewSelected?.becomeFirstResponder()
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        scrollHeight = table.rectForRow(at: IndexPath(row: 0, section: textField.tag)).maxY - (view.frame.height - 5)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        medicalInfo.symptom = textField.text ?? ""
    }
}


//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------
extension MedicalCreateViewController {
    
    @objc func back(_ sender: UIBarButtonItem) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func send(_ sender: UIButton) {
    
        view.endEditing(true)
        
        switch typeView {
        case .create(_):
            checkNetworkAndRequestNewMedical()
            
        case .edit(_):
            checkNetworkAndRequestEditMedical()
            
        default:
            break
        }
        
        
    }
    
    @objc func showDatePickerView(_ section: Int) {
        
        view.endEditing(true)
        
        datePickerView = setupDatePickerView()
        datePickerView?.tag = section
        
        switch arraySection[section] {
        case .dateDiscovery:
            datePickerView!.date = Date(timeIntervalSince1970: medicalInfo.dateDiscovery)
            
        case .date:
            datePickerView!.date = Date(timeIntervalSince1970: medicalInfo.date)
            
            
        default:
            break
        }
        
        Utility.showView(datePickerView, onView: view)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.datePickerView!.calendarView.toggleViewWithDate(self.datePickerView!.date)
         }
    }

    
    
    @objc func selectionImage(_ sender: UIButton) {
        
        showAlertController(.addImage)
        
    }
    
    @objc func deletePicture(_ sender: UIButton) {
        
        
        guard let assets = assets, assets.count > 0 else { return }
         self.assets!.remove(at: sender.tag)

        
    }
    
    /**
     Show Alert tùy theo type
     */
    func showAlertController(_ type: AlertType) {
        
        
        switch type {
        case .addImage:
            alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .actionSheet)
            //        default:
            //            alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
        }
        
        
        
        guard let alertController = alertController else { return }
        
        
        
        switch type {
        case .addImage:
            
            alertController.addAction(UIAlertAction(title: LocalizedString("genenal_button_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
            
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_library", comment: "Thư viện"), style: .default, handler: {[unowned self] _ in
                
                self.showImagePickerWithAssetType(
                    .allPhotos,
                    allowMultipleType: true,
                    sourceType: .photo,
                    allowsLandscape: true,
                    singleSelect: false
                )
                
            }))
            
            alertController.addAction(UIAlertAction(title: LocalizedString("personal_seletion_avatar_in_camera", comment: "Camera"), style: .default, handler: {[unowned self] _ in
                self.showImagePickerWithAssetType(
                    .allPhotos,
                    allowMultipleType: true,
                    sourceType: .camera,
                    allowsLandscape: true,
                    singleSelect: false
                )
            }))
            
        }
        
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            present(alertController, animated: true, completion: nil)
        } else {
            if let popoverController = alertController.popoverPresentationController {
                // show action sheet
                popoverController.sourceRect = CGRect(x: 0, y: 0, width: view.frame.width , height: 44)
                popoverController.sourceView = self.view
            }
            present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    /**
     Show controller chụp ảnh hoặc ảnh từ thư viện
     */
    fileprivate func showImagePicker(_ source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        present(picker, animated: true, completion: nil)
    }
    
    
    func showImagePickerWithAssetType(_ assetType: DKImagePickerControllerAssetType,
                                      allowMultipleType: Bool,
                                      sourceType: DKImagePickerControllerSourceType = .both,
                                      allowsLandscape: Bool,
                                      singleSelect: Bool) {
        
        let pickerController = DKImagePickerController()
        //TODO: check số lượng max
        pickerController.maxSelectableCount = 3
        
        // Custom camera
        //        pickerController.UIDelegate = CustomUIDelegate()
        //        pickerController.modalPresentationStyle = .OverCurrentContext
        
        pickerController.assetType = assetType
        pickerController.allowsLandscape = allowsLandscape
        pickerController.allowMultipleTypes = allowMultipleType
        pickerController.sourceType = sourceType
        pickerController.singleSelect = singleSelect
        
        //        pickerController.showsCancelButton = true
        //        pickerController.showsEmptyAlbums = false
        //        pickerController.defaultAssetGroup = PHAssetCollectionSubtype.SmartAlbumFavorites
        
        // Clear all the selected assets if you used the picker controller as a single instance.
        //        pickerController.defaultSelectedAssets = nil
        
//        pickerController.defaultSelectedAssets = self.assets
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            self.assets = assets
            
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            pickerController.modalPresentationStyle = .formSheet
        }
        
        self.present(pickerController, animated: true) {}
    }
    
    
    
 
  
    
}



//====================================
// MARK: - REQUEST
//====================================
extension MedicalCreateViewController {
    
    
    /// Thêm lời nhắn mới
    internal func checkNetworkAndRequestNewMedical() {
        
        guard let userInfo = AppData.instance.myInfo else { return  }
        
        guard medicalInfo.symptom.count > 0 else {
             HUD.showMessage("Bạn vui lòng nhập triệu chứng!",
                             onView: self.view,
                             position: .center)
             return
         }
        
        guard medicalInfo.resultContent.count > 0 else {
            HUD.showMessage("Bạn vui lòng nhập kết quả khám!",
                            onView: self.view,
                            position: .center)
            return
        }

        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }


         HUD.showHUD()

        let itemRef = ref.child("medicals").childByAutoId()
        
        let userItem: Parameters = [
            "id": itemRef.key ?? "",
            "symptom": medicalInfo.symptom,
            "created_by": userInfo.uid,
            "created_at": Date().timeIntervalSince1970,
            "date_discovery": medicalInfo.dateDiscovery,
            "date": medicalInfo.date,
            "result_content": medicalInfo.resultContent,
            "list_photo": medicalInfo.listPhoto]
        
        itemRef.setValue(userItem)
        
        medicalInfo.id = itemRef.key ?? ""
        
        
        
        guard arrayPhotoUpload.count > 0 else {
            
            self.delegate?.createMedicalSuccess(self.medicalInfo)
            HUD.showMessage("Thêm bệnh án thành công!") {
                _ = self.navigationController?.popViewController(animated: true)
            }
            return
        }
        
        self.currentPhoto = 0
        self.uploadImage(self.arrayPhotoUpload[self.currentPhoto].image)
            
      
    }
    
    
    /// Thêm lời nhắn mới
       internal func checkNetworkAndRequestEditMedical() {
           
    
           guard medicalInfo.symptom.count > 0 else {
                HUD.showMessage("Bạn vui lòng nhập triệu chứng!",
                                onView: self.view,
                                position: .center)
                return
            }
           
           guard medicalInfo.resultContent.count > 0 else {
               HUD.showMessage("Bạn vui lòng nhập kết quả khám!",
                               onView: self.view,
                               position: .center)
               return
           }

           /* thông báo cho người dùng không có mạng */
           guard networkReachable() else {
               HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                               comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                               onView: self.view,
                               position: .center)
               return
           }


            HUD.showHUD()

        let itemRef = ref.child("medicals").child(medicalInfo.id)
           
           let userItem: Parameters = [
               "id": medicalInfo.id,
               "symptom": medicalInfo.symptom,
               "created_by": medicalInfo.createBy,
               "created_at": medicalInfo.createdAt,
               "date_discovery": medicalInfo.dateDiscovery,
               "date": medicalInfo.date,
               "result_content": medicalInfo.resultContent,
               "list_photo": medicalInfo.listPhoto]
           
           itemRef.setValue(userItem)
           
    
           
           guard arrayPhotoUpload.count > 0 else {
               
               self.delegate?.createMedicalSuccess(self.medicalInfo)
               HUD.showMessage("Cập nhật bệnh án thành công!") {
                   _ = self.navigationController?.popViewController(animated: true)
               }
               return
           }
           
           self.currentPhoto = 0
           self.uploadImage(self.arrayPhotoUpload[self.currentPhoto].image)
               
         
       }
    
    
    
    func uploadImage(_ image: UIImage) {
        
        
         guard let userInfo = AppData.instance.myInfo else { return  }
        
        let imageData = cropImage(image, toSize:CGSize(width: 1024, height: 1024)).jpegData(compressionQuality: 0.9)
        let imagePath = userInfo.uid + "/\((Date.timeIntervalSinceReferenceDate * 1000).toString(0)).jpg"
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        
        let riversRef = storageRef.child(imagePath)
        
        HUD.showMessage(String(format: "Đang upload ảnh %d/%d", currentPhoto + 1, arrayPhotoUpload.count))
        // 6
        riversRef.putData(imageData!, metadata: metadata) { [unowned self] (metadata, error) in
            
         

            
            if let error = error {
                print("Error uploading photo: \(error)")
                
                HUD.showMessage("Gửi ảnh không thành công", onView: self.view)
                return
            }
            
 
            riversRef.downloadURL { (url, error) in

                guard let downloadURL = url else {  return }
                
                print("downloadURL.absoluteURL : \(downloadURL.absoluteString)")
                
                self.uploadPhotoSuccess(downloadURL.absoluteString)
                
//                self.sendPhotoMessage(downloadURL.absoluteString ?? self.storageRef.child(imagePath).description)
                
                
                self.currentPhoto += 1
                guard self.arrayPhotoUpload.count > self.currentPhoto else {
                    
                    self.delegate?.createMedicalSuccess(self.medicalInfo)
                           HUD.showMessage("Thêm bệnh án thành công!"){
                            _ = self.navigationController?.popViewController(animated: true)
                           }
                           return
                       }
                
                
                self.uploadImage(self.arrayPhotoUpload[self.currentPhoto].image)
            }
        }
        
    }
    
    func uploadPhotoSuccess(_ link: String) {
        
        guard let userInfo = AppData.instance.myInfo else { return  }
        
        medicalInfo.listPhoto.append(link)
        
        ref.child("medicals")
            .child(medicalInfo.id)
            .child("list_photo")
            .setValue(medicalInfo.listPhoto)
        

    
    }
    
    
    /**
     Request lấy danh sách bài viết thành công
     */
//    fileprivate func newSupportRequestSuccess(_ respond: HTTPCreateSupport.RespondType) {
//
//        newSupport = respond.support
//
//        guard arrayImage.count > 0 else {
//            HUD.showMessage(LocalizedString("notificaiton_support_send_request_success", comment: "Gửi yêu cầu hỗ trợ thành công!"))
//
//            delegate?.createSupportSuccess(respond.support)
//            return
//        }
//
//        checkNetworkAndRequestUploadPhoto(respond.support.id)
//
//    }
    
    
    
    //----------------------------------------
    // - MARK: UPLOAD PHOTO
    //----------------------------------------
    
    internal func checkNetworkAndRequestUploadPhoto(_ supportID: Int) {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)
            return
        }
        
        
        let string = String(format: LocalizedString("notification_support_uploading_image", comment: "Đang upload ảnh %d/%d"), currentPhoto + 1, arrayString.count)
        let headers = ["Authorization": "bearer \(Defaults[.Token])"]
        
        
        HUD.showHUD(string, onView: self.view) {
//            HTTPManager.instance.uploadPhoto
//                .doRequest(HTTPUploadPhoto.RequestType(objectID: supportID,
//                                                       objectType: .support,
//                                                       photo: self.arrayString[self.currentPhoto],
//                                                       description: ""), headers: headers)
//
//                .completionHandler { result in
//                    switch result {
//                    case .success(let respond):
//                        self.uploadRequestSuccess(respond, objectID: supportID)
//
//                    case .failure(let error):
//
//                        self.retry += 1
//                        if self.retry == self.maxRetry { self.uploadImageRequestFaild(error) }
//                        else { self.checkNetworkAndRequestUploadPhoto(supportID) }
//
//                    }
//            }
        }
    }
    
    /**
     Request thêm ảnh thành công
     */
//    fileprivate func uploadRequestSuccess(_ respond: HTTPUploadPhoto.RespondType, objectID: Int) {
//
//        newPhotos.append(respond.photo)
//
//        currentPhoto += 1
//        guard currentPhoto < arrayString.count else {
//
//           HUD.showMessage(LocalizedString("notificaiton_support_send_request_success", comment: "Gửi yêu cầu hỗ trợ thành công!"))
//
//
//
//            if var item = newSupport {
//                item.photos = newPhotos
//                delegate?.createSupportSuccess(item)
//            }
//
//            newPhotos = []
//            assets = []
//            table.reloadData()
//            return
//
//        }
//
//        checkNetworkAndRequestUploadPhoto(objectID)
//    }
    
    /**
     Request upload ảnh thất bại
     */
    
//    fileprivate func uploadImageRequestFaild(_ error: Error) {
//
//        HUD.showMessage(String(format: LocalizedString("notification_support_update_image_failed", comment: "Mạng chậm vui lòng thử lại sau, đã thêm %d/%d ảnh"), currentPhoto, arrayString.count))
//
//        if var item = medicalInfo {
//            item.photos = newPhotos
//            delegate?.createSupportSuccess(item)
//        }
//
//        newPhotos = []
//        assets = []
//        table.reloadData()
//        return
//
//    }
    
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    fileprivate func requestFaild(_ error: Error) {
        HUD.showMessage(Utility.getMessageFromError(error), onView: view, position: .center)
    }
    
}


extension MedicalCreateViewController {
    /**
     Lấy thông tin chiều cao của bàn phím khi hiện lên
     Để sau này tính offset cho các màn hình bé
     - parameter sender:
     */
    @objc func keyboardWillShow(_ sender: Foundation.Notification) {
        
        guard let userInfo = sender.userInfo else { return }
        guard let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else { return }
        
        
        let scrollPoint = CGPoint(x: 0.0, y: scrollHeight + frame.height)

        if scrollPoint.y > 0 {
            table.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    @objc func keyboardWillHide(_ sender: Foundation.Notification) {
        
    }
    
    func hiddenKeyboard() {
        guard let textView = textViewSelected else { return }
        textView.resignFirstResponder()
    }
}

//-------------------------------
// -MARK: SETUP VIEW
//-------------------------------
extension MedicalCreateViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        textViewSelected = textView
        
        scrollHeight = table.rectForRow(at: IndexPath(row: 0, section: textView.tag)).maxY - view.frame.height
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        switch arraySection[textView.tag] {
        case .result:
            medicalInfo.resultContent = textView.text
        default:
            break
        }
        
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count + text.count - range.length <= 5000
    }
}

//====================================
// MARK: - SETUP
//====================================

extension MedicalCreateViewController: DatePickerViewDelegate {
    
    func selectedDate(_ time: TimeInterval) {
        
        guard let dateView = datePickerView else { return }
        
        switch arraySection[dateView.tag] {
        case .dateDiscovery:
            medicalInfo.dateDiscovery = time
            
        case .date:
            medicalInfo.date = time
            
        default:
            break
        }
        table.reloadData()
        dateView.dismissView()
        
        
    }
}


//====================================
// MARK: - SETUP
//====================================

extension MedicalCreateViewController {
    fileprivate func setupAllSubviews() {
        
        

        view.backgroundColor = UIColor.Table.tableGrayColor()
        
        back = setupBackBarButton(Icon.Navigation.Back, selector: #selector(self.back(_:)), target: self)
       
        
        send = setupBarTitle(LocalizedString("general_button_send", comment: "Gửi"), selector: #selector(self.send(_:)), target: self)
        
        navigationItem.leftBarButtonItem = back
        navigationItem.rightBarButtonItem = send
        

        
        setupTable()
        setupNotificationObserver()

    }
    
    
    func setupAllConstraints() {
        
        table.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
    }
    
    /**
     setup Notification Observer
     */
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /**
     Setup Bar Button
     */
    func setupBarTitle(_ title: String, selector: Selector, target: AnyObject? = nil) -> UIBarButtonItem {
        let button = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)
        button.tintColor = .white
        return button
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable() {
        table = UITableView(frame: CGRect.zero, style: .grouped)
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.register(LoginTextFieldTableViewCell.self,
                       forCellReuseIdentifier: "LoginTextFieldTableViewCell")
        
        table.register(TextFieldTableViewCell.self,
                       forCellReuseIdentifier: "EmptyCell")
        
        table.register(NewPhotoTableViewCell.self,
                       forCellReuseIdentifier: "NewPhotoTableViewCell")
        
        table.register(TextViewTableViewCell.self,
                       forCellReuseIdentifier: "TextViewTableViewCell")
        
        table.register(MedicalCreateHeaderSectionView.self,
                       forHeaderFooterViewReuseIdentifier: "MedicalCreateHeaderSectionView")
        
        table.delegate = self
        table.dataSource = self
        table.keyboardDismissMode = .onDrag
        view.addSubview(table)
    }
    
    /**
     Setup Table
     */
    fileprivate func setupTable<T>(dataSource: T, cellClass: AnyClass, emptyClass: AnyClass? = nil) -> UITableView where T: UITableViewDataSource, T: UITableViewDelegate {
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.register(cellClass, forCellReuseIdentifier: "Cell")
        table.delegate = dataSource
        table.dataSource = dataSource
        table.isDirectionalLockEnabled = true
        table.separatorStyle = .none
        table.backgroundColor = UIColor.Table.tablePlainColor()
        return table
    }
    
    
    internal func setupDatePickerView() -> DatePickerView {
        let view = DatePickerView()
        view.delegate = self
        view.date = Date()
        return view
    }
    
    
}







