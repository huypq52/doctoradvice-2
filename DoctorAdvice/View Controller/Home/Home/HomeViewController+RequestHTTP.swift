//
//  HomeViewController+RequestHTTP.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/11/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import Firebase
import SwiftyUserDefaults

extension HomeViewController {
    
    
   

    
    // MARK: - REQUEST LOGIN
    
    internal func checkNetworkAndRequestLogin(_ userName: String, password: String) {
        
        
        Auth.auth().signIn(withEmail: userName, password: password) { authResult, error in
            
            guard let authResult = authResult else { self.logout(); return }
            self.loginRequestSuccess(authResult)
        }

    }
    
    
    /**
     Request đăng nhập thành công
     */
    func loginRequestSuccess(_ respond: AuthDataResult) {
    
        /// Check trường hợp ban đầu không cho chat, sau đồng bộ về có cho chat


        
        let itemRef = ref.child("users").child(respond.user.uid)
        
        let newMessageRefHandle = itemRef.observe(.value, with: {(snapshot) -> Void in
            print("data Login: \(snapshot)")
            
            
            guard let data = snapshot.value as? [String : Any] else { return }
            
            AppData.instance.myInfo = User(data: data)
            
            self.updateOnline()
            self.timerOnline = Timer.scheduledTimer(timeInterval: 10.seconds, target: self, selector: #selector(self.updateOnline), userInfo: nil, repeats: true)
            self.observeUsersOnline()
            
            self.checkNetworkAndRequestGetHospital()
            
        })

    }


    
    // MARK: - REQUEST LOGOUT
    func checkNetworkAndRequestLogout(_ userId: String) {
        

        HUD.showHUD(LocalizedString("notification_account_logoutting", comment: "Đăng xuất ..."), onView: self.view) {
            self.logout()
        }
    }
    
    
    
    /// Ép đăng xuất
    @objc func forceLogout(_ sender: Notification) {
        
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            logout()
            return
        }
        
        checkNetworkAndRequestLogout(AppData.instance.myInfo?.uid ?? "")
        
    }
    
    func logout() {
        
        print("Logout")

        Defaults[.IsLogin] = true
        Defaults[.TrueName] = ""
        Defaults[.BirthDay] = 0
        AppData.instance.resetData()
        
        
        // [START signinwithcustomtoken]
        
        do {
            try Auth.auth().signOut()
        } catch let error {
            print("Logout Firebase Failed \(error)")
        }
        
        

        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        if let navibar = tabBarController {
            if let items = navibar.tabBar.items {
                items.forEach { $0.badgeValue = nil }
            }
        }
        
        let naviVC: UINavigationController = UINavigationController(rootViewController: LoginViewController())
        Utility.configureAppearance(navigation: naviVC)
        
        guard let window = UIApplication.shared.keyWindow else { return }
        UIView.transition(with: window,
                          duration: 0.5,
                          options: [.transitionFlipFromLeft],
                          animations: {
                            window.rootViewController = naviVC
        }, completion: { _ in
            
            
        })

    }

    
   
    
    // MARK: - REQUEST CONTACT
    func checkNetworkAndRequestGetHospital() {
        
        /* thông báo cho người dùng không có mạng */
        guard networkReachable() else {
            HUD.showMessage(LocalizedString("notification_no_internet_connection_please_try_agian",
                                            comment: "Bạn đang offline, vui lòng kiểm tra lại kết nối."),
                            onView: self.view,
                            position: .center)

            refreshControl.endRefreshing()
            return
        }
        
      
        HUD.showHUD()
        
        
        dataArray = []
        ref.child("hospital").observeSingleEvent(of: .value, with: { snapshot in
            
            self.refreshControl.endRefreshing()
            
            for child in snapshot.children {
                
                print("child: \(child)")
                
                guard let snap = child as? DataSnapshot,
                    let data = snap.value as? [String : Any] else { continue }
                
                self.dataArray.append(Hospital(data: data))
            }
            
            HUD.dismissHUD()

            self.table.reloadData()
        })
        
        
        headerView.posts = []
        ref.child("Posts").observeSingleEvent(of: .value, with: { snapshot in
            
            self.refreshControl.endRefreshing()
            
            print("XXXXX: \(snapshot)")
            
            for child in snapshot.children {
                
                print("child: \(child)")
                
                guard let snap = child as? DataSnapshot,
                    let data = snap.value as? [String : Any] else { continue }
                
                self.headerView.posts.append(Post(data: data))
            }
            
            print("self.headerView.posts: \(self.headerView.posts.count)")
            self.headerView.updateView()
            
            self.headerView.arrayView.enumerated().forEach { (i, itemView) in
                
                itemView.tag = i
                itemView.isUserInteractionEnabled = true
                itemView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openPost(_:))))
            }
            
            HUD.dismissHUD()

            self.lastUpdate = Date().timeIntervalSince1970
            
        })
    }
    
    
    
    //----------------------------
    // MARK: - REQUEST FAILED
    //----------------------------
    /**
     Request lấy danh sách bài viết thất bại
     */
    
    func requestFaild(_ error: Error?) {
        
        guard let error = error else { HUD.dismissHUD(); return }
        
        switch error {
            
        default:
            HUD.showMessage(error.localizedDescription, onView: view, position: .center)
        }

    }

}


