//
//  HomeViewController+Delegate.swift
//  KidsOnline
//
//  Created by Pham Huy on 9/21/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit
import MXParallaxHeader
import SKPhotoBrowser
import SwiftyUserDefaults
import MessageUI


//-----------------------------
// MARK: - TABLE DATASOURCE
//-----------------------------
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let identify = "HotlineTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identify, for: indexPath) as? HotlineTableViewCell else { return UITableViewCell() }
        
        configuaHospitalCell(cell, atIndexPath: indexPath)
        
        return cell
        
            
    }
    
    
    func configuaHospitalCell(_ cell: HotlineTableViewCell, atIndexPath indexPath: IndexPath) {
        
        guard indexPath.row < dataArray.count else { return  }
        
        let hospital = dataArray[indexPath.row]
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .white
        
        cell.textLabel?.font = UIFont(name: FontType.latoMedium.., size: FontSize.normal..)!
        cell.textLabel?.textColor = UIColor.Text.blackMediumColor()
        
        cell.detailTextLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        cell.detailTextLabel?.textColor = UIColor.Text.grayMediumColor()
        
        cell.textLabel?.text = hospital.name
        cell.detailTextLabel?.text = hospital.phone
        cell.buttonCall.isHidden = hospital.phone.count == 0
        cell.buttonCall.tag = indexPath.row
        cell.buttonCall.addTarget(self, action: #selector(self.callHotLine(_:)), for: .touchUpInside)
        cell.buttonEmail.isHidden = true


    }
    
 
    
 
}


//-----------------------------
// MARK: - TABLE DATASOURCE
//-----------------------------

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       return 55
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
       return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = HeaderViewSection()
        headerView.label.text = "Đường dây nóng sẵn sàng phục vụ 24/24"
        headerView.label.textColor = UIColor.red
        headerView.label.textAlignment = .left
        headerView.bgView.backgroundColor = .clear
        headerView.padding = 0
        headerView.backgroundColor = .white
        return headerView
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
      return nil
    }
}


//====================================
// MARK: - DATEPICKER DELEGATE
//====================================

extension HomeViewController: DatePickerViewDelegate {
    func selectedDate(_ time: TimeInterval) {
        
    }
    
}

//====================================
// MARK: - HOTLINE DELEGATE
//====================================

//extension HomeViewController: HomeHotlineViewDelegate {
//    
//    func openMail(_ mail: String) {
//        
//        guard MFMailComposeViewController.canSendMail() else {
//            
//            HUD.showMessage(mail)
//            return
//        }
//
//        let mailVC = MFMailComposeViewController()
//        mailVC.mailComposeDelegate = self
//        mailVC.setToRecipients([mail])
//        present(mailVC, animated: true, completion: nil)
//    }
//    
//    func callPhone(_ phone: String) {
//        Utility.callPhoneNumberShowAlert(phone)
//    }
//    
//}

extension HomeViewController: MXParallaxHeaderDelegate {
    
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        
    }
}

extension HomeViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        switch result {
        case .cancelled:
            print("Cancel sent email")
        case .saved:
            HUD.showMessage(LocalizedString("about_label_save_mail_success", comment: "Đã lưu vào nháp thành công."), onView: self.view)
        case .sent:
            HUD.showMessage(LocalizedString("about_label_send_mail_success", comment: "Gửi email thành công."), onView: self.view)
        case .failed:
            HUD.showMessage(LocalizedString("notification_has_error_please_try_again", comment: "Có lỗi xảy ra, vui lòng thử lại sau"), onView: self.view)
            print("error: \(String(describing: error))")
            
        @unknown default:
            break
        }
    }
}


