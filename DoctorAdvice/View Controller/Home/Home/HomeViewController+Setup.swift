//
//  HomeViewController+Setup.swift
//  KidOnline
//
//  Created by Pham Huy on 3/2/19.
//  Copyright © 2018 KidOnline. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import Firebase
//import ReachabilitySwift
import Reachability
import Async
import SKPhotoBrowser

//-------------------------------------
// - MARK: - SETUP ALL SUBVIEWS
//-------------------------------------
extension HomeViewController {
    
    func setupAllSubviews() {
        
        
        
        
        setupBackground()
        logoSchool                  = setupLogoSchool()
        
    
        hotline                     = setupButtonHotline()
        

        headerView = setupHeaderView()
        
        
        setupNotification()
        setupTable()
        
        

        refreshControl = setupRefreshView()
        table.addSubview(refreshControl)
        
//        table.tableHeaderView = headerView
    
        view.backgroundColor = UIColor(rgba: "#f5f5f5")
        

        headerView.contentMode = .scaleAspectFill
        headerView.image = cropImage(Icon.General.ArrowRight,
                                     toSize: CGSize(width: UIScreen.main.bounds.width * 9 / 16 + 24,
                                                    height: UIScreen.main.bounds.width))
        
        table.parallaxHeader.view = headerView // You can set the parallax header view from the floating view
        table.parallaxHeader.height = headerView.frame.height

        table.parallaxHeader.mode = .fill
        table.parallaxHeader.minimumHeight = 0
        table.parallaxHeader.delegate = self
        
    }
    
    
    
    func setupAllConstraints() {
        table.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
 
        footerBackground.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.height.equalTo(UIScreen.main.bounds.width / 4)
            make.bottom.equalTo(view).offset(0)
        }
        
        
    }
    
    
    func setupNotification()  {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.changedStudent), name: Notification.Name(rawValue: AppNotification.ChangedStudent..), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.forceLogout(_:)), name: Notification.Name(rawValue: AppNotification.Logout..), object: nil)
//                NotificationCenter.default.addObserver(self, selector: #selector(self.reloadHome(_:)), name: Notification.Name(rawValue: AppNotification.ReloadHome..), object: nil)
        
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.observeUsersOnline), name: Notification.Name(rawValue: AppNotification.ReloadOnline..), object: nil)

    }
    
    
    
    func setupTable() {
        
        table = UITableView(frame: CGRect.zero, style: .grouped)
        table.backgroundColor = .clear
        table.register(HotlineTableViewCell.self, forCellReuseIdentifier: "HotlineTableViewCell")
        table.register(TimetableTableViewCell.self, forCellReuseIdentifier: "TimetableTableViewCell")
        table.register(SeperatorPaddingTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.register(EmptyTableViewCell.self, forCellReuseIdentifier: "EmptyTableViewCell")

        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self

        table.clipsToBounds = false
        table.layer.masksToBounds = true
        table.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        table.layer.shadowOpacity = 0.3
        table.layer.shadowRadius = 2.0
        table.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        
        table.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
    
        view.addSubview(table)
        
    }
    
    
 
    
    /// Setup Label Time ở góc phải
    
    func setupLabelTime() -> TimeNavibarView {
        let view: TimeNavibarView = TimeNavibarView()
        view.tag = 1001
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40)
        return view
    }
    

    
    
    /// Setup button Hotline góc trái
    
    func setupButtonHotline() -> UIButton {
        
        let button: UIButton = UIButton()
        button.tag = 1002
        button.setTitle(LocalizedString("function_label_support", comment: "Hỗ trợ"), for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.white, for: .normal)
        button.setImage(Icon.Login.Phone.tint(.white), for: .normal)
        button.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width / 3, height: 40)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
        button.addTarget(self, action: #selector(self.callHotLine(_:)), for: .touchUpInside)
        return button
        
    }
    
    /// Setup button Promtion
    func setupPromotionButton() -> UIImageView {
        let iconPromotion: UIImageView = UIImageView(image: Icon.Navigation.Promotion)
        iconPromotion.isUserInteractionEnabled = true
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openPromotion(_:)))
//        iconPromotion.addGestureRecognizer(tap)
        iconPromotion.tag = 1001
        iconPromotion.frame = CGRect(x: UIScreen.main.bounds.width - 10 - 40, y: 0, width: 40, height: 40)
        return iconPromotion
    }
    
    
    /// Setup Logo Scholl
    func setupLogoSchool() -> UIImageView {
        let logo: UIImageView = UIImageView(image: Icon.General.LogoKidsOnline)
        logo.tag = 1000
        logo.contentMode = .scaleAspectFit
        logo.frame = CGRect(x: (UIScreen.main.bounds.width - 100) / 2, y: 0, width: 100, height: 40)
        logo.isUserInteractionEnabled = true
        logo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnLogo(_:))))
        return logo
    }

    func setupBgHeader() -> UIImageView {
        let imageView = UIImageView(image: Icon.Home.HeaderBg.tint(UIColor.Navigation.mainColor()))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
    
    internal func setupDatePickerView() -> DatePickerView {
        let view = DatePickerView()
        view.mainColor = UIColor.Navigation.mainColor()
        view.delegate = self
        view.date = Date()
        return view
    }
    
    func setupRefreshView() -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        
        refreshControl.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60)
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        return refreshControl
    }

    
    func setupHeaderView() -> HomeHeaderView {
        let view = HomeHeaderView(frame: CGRect(x: 0,
                                                y: 0,
                                                width: UIScreen.main.bounds.width,
                                                height: UIScreen.main.bounds.width * 9 / 16 + 24))
        
        view.isUserInteractionEnabled = true
        return view
    }
    
    fileprivate func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: 1024, height: 1024 * image.size.height / image.size.width) }
        else { newSize = CGSize(width: 1024 * image.size.width / image.size.height, height: 1024) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
}

















