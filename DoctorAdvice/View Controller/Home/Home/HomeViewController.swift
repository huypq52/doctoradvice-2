//
//  HomeViewController.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/22/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults
import Firebase
import Reachability
import Async
import SKPhotoBrowser



struct Hospital {
    var name: String
    var phone: String
    
    init(data: [String : Any]) {
        self.name = data["name"] as? String ?? ""
        self.phone = data["phone"] as? String ?? ""
    }
}

class HomeViewController: CustomViewController {
    
    /// ENUM
    internal enum Section: Int {
        case article = 0, function, timetableFake, timetable
    }
    

    var datePickerView: DatePickerView!
    
//    var labelTime: TimeNavibarView!
    var hotline: UIButton!
    var logoSchool: UIImageView!
    
    var headerView: HomeHeaderView!
 
//    var hotlineView: HomeHotlineView?
    var refreshControl: UIRefreshControl!
    


    var ref: DatabaseReference!
    var guestRef: DatabaseReference!
    var messageRef: DatabaseReference!
    var listRoomName: [String] = []
    var listHandle: [(refHandle: DatabaseReference, handle: DatabaseHandle)] = []
    
    var timerOnline: Timer!
    var  browser: SKPhotoBrowser?
    
    
    var dataArray: [Hospital] = []
    
    var lastUpdate: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        ref = Database.database().reference()

        guestRef = ref.child("users")
        messageRef = ref.child("chats")
        
        
        setupAllSubviews()
        view.setNeedsUpdateConstraints()
        
//        if AppData.instance.isAutoLogin && networkReachable() {
//
//            AppData.instance.isAutoLogin = false
//            checkNetworkAndRequestLogin(Defaults[.UserName], password: Defaults[.Password])
//
//        }
//        else {
//
//            if AppData.instance.willReloadHome {
////                checkNetworkAndRequestGetHome(dateSelected, getPost: true, getTimetable: true)
//            } else {
                checkNetworkAndRequestGetHospital()
//            }
//
//        }
        
        
        observeUsersOnline()
        updateOnline()
        timerOnline = Timer.scheduledTimer(timeInterval: 10.seconds, target: self, selector: #selector(self.updateOnline), userInfo: nil, repeats: true)

        
        
  
//        guard Defaults[.NumberShowDialogNotification] < 3 else { return }
//        if let type = UIApplication.shared.currentUserNotificationSettings, UIApplication.shared.isRegisteredForRemoteNotifications, (type.types.contains(.alert) || type.types.contains(.sound) || type.types.contains(.badge)) {
//
//        } else {
//            showAlertController(.notNotificationServices)
//        }
        
        
    }
    
    @objc func updateOnline() {
        guard let info = AppData.instance.myInfo else { return }
        guestRef.child(String(format: "%@/last_update", info.uid)).setValue(Date().timeIntervalSince1970)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        updateLogo()
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    
    deinit {
        removeObserveOnline()
    }
    
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            setupAllConstraints()
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        


        table.reloadData()


        navigationController?.navigationBar.addSubview(logoSchool)
        navigationController?.navigationBar.addSubview(hotline)
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         
        guard let navigationController = navigationController else { return }
        navigationController.navigationBar.subviews.forEach {
            if $0.tag >= 1000 { $0.removeFromSuperview() }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let navigationController = navigationController else { return }
        
        navigationController.navigationBar.subviews
            .filter { $0.tag >= 1000 }
            .forEach { $0.removeFromSuperview() }
    }
    
    @objc func observeUsersOnline() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB

        removeObserveOnline()
        
        
        guard let info = AppData.instance.myInfo else { return }
        
        AppData.instance.guestInfoList = info.guestFriend.map { GuestInfo(id: $0) }
        listRoomName = Utility.getListRoomName()
        
        /*
         * Lắng nghe sự kiện online
         */
        info.guestFriend.enumerated().forEach { (i, id) in
            
            
            let handle = ref.child("users").child(id).observe(.value, with: { snapshot in
                
                guard let data = snapshot.value as? [String : Any], i < AppData.instance.guestInfoList.count else { return }
                AppData.instance.guestInfoList[i] = GuestInfo(data: data)
                
                 NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.ReloadTableContact..), object: nil, userInfo: ["id" : AppData.instance.guestInfoList[i].id])
                                    
            })
            
            listHandle.append((ref.child("users").child(id), handle: handle))
        }
        
        
        
        listRoomName.forEach { room in
            
            
            let handle = messageRef.child(room).child("messages").queryLimited(toLast:1).observe(.childAdded, with: { (snapshot) -> Void in // 1
                
                print("snapshotXXX: \(snapshot)")
                
                
                guard let channelData = snapshot.value as? [String : Any ] else { return }
                let channel = Channel(id: snapshot.key,
                                      senderID: channelData["senderId"] as? String ?? "",
                                      roomName: room,
                                      content: channelData["content"] as? String,
                                      photo: channelData["file_url"] as? String,
                                      time: (channelData["time"] as? Double) ?? 0,
                                      unread: ((channelData["unread"] as? Int) ?? 0) == 1)
                
                
                if let pos = AppData.instance.channels.firstIndex(where: { $0.roomName == channel.roomName }) {
                    if channel.time > AppData.instance.channels[pos].time {
                        AppData.instance.channels[pos] = channel
                    }
                    
                } else {
                    AppData.instance.channels.append(channel)
                }
                
                if let pos = AppData.instance.guestInfoList.firstIndex(where: { $0.id == channel.senderID }), UIApplication.shared.applicationState != .active {
                    
                    let localNotification = UILocalNotification()
                    localNotification.alertTitle = String(format: "%@ đã gửi tin nhắn cho bạn", AppData.instance.guestInfoList[pos].name)
                    
                    
                    localNotification.alertLaunchImage = AppData.instance.guestInfoList[pos].avatar
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    localNotification.applicationIconBadgeNumber += 1
                    UIApplication.shared.presentLocalNotificationNow(localNotification)
                
                }
                
                AppData.instance.channels = AppData.instance.channels.sorted { $0.time > $1.time}
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.ReloadMessage..), object: nil)
                Utility.updateNotificaitonBadge(tabbar: self.tabBarController?.tabBar)
                
            })
            
            
            listHandle.append((messageRef.child(room).child("messages"), handle: handle))
            
            
            let handleChanged = messageRef.child(room).child("messages").queryLimited(toLast:1).observe(.childChanged, with: { (snapshot) in
                
                
                guard AppData.instance.guestInfoList.count > 0 else { self.removeObserveOnline(); return }
                
                
                guard let channelData = snapshot.value as? [String : Any ] else { return }
                let channel = Channel(id: snapshot.key,
                                      senderID: channelData["senderId"] as? String ?? "",
                                      roomName: room,
                                      content: channelData["content"] as? String,
                                      photo: channelData["file_url"] as? String,
                                      time: (channelData["time"] as? Double) ?? 0,
                                      unread: ((channelData["unread"] as? Int) ?? 0) == 1)
                
                
                if let pos = AppData.instance.channels.firstIndex(where: { $0.roomName == channel.roomName }) {
                    if channel.time > AppData.instance.channels[pos].time { AppData.instance.channels[pos] = channel }
                    
                } else {
                    AppData.instance.channels.append(channel)
                }
                
                
                AppData.instance.channels = AppData.instance.channels.sorted { $0.time > $1.time}
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.ReloadMessage..), object: nil, userInfo: ["id" : channel.id])
      
                Utility.updateNotificaitonBadge(tabbar: self.tabBarController?.tabBar)
                
                
            })
            
            listHandle.append((messageRef.child(room).child("messages"), handle: handleChanged))
        }

    }
    
    func removeObserveOnline() {
        listHandle.forEach { $0.refHandle.removeObserver(withHandle:  $0.handle) }
    }
    

    @objc func handleReachabilityChanged(_ notification: Notification)
    {
        // notification.object will be a 'Reachability' object that you can query
        // for the network status.
        
       
        guard let networkReachability = notification.object as? Reachability else { return }
        
        if networkReachability.connection != .unavailable && AppData.instance.isAutoLogin {
            print("Có mạng")
            checkNetworkAndRequestLogin(Defaults[.UserName], password: Defaults[.Password])
        } else {
            print("Mất mạng")
        }

        
    }
    
}



