//
//  HomeViewController+Processor.swift
//  KidsOnline
//
//  Created by Pham Huy on 10/8/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SKPhotoBrowser
import SwiftyUserDefaults

//-------------------------------
// - MARK: SELECTOR
//-----------------------------
extension HomeViewController {
    
    /// Xem thông tin trường
    @objc func tapOnLogo(_ sender: UIGestureRecognizer) {

    }
    
    /// Show danh sách bài viết
    @objc func showPostList() {

    }

    @objc func tapOnButtonArticle(_ sender: UIButton) {
        

    }

    
    /// Bấm vào nút Hotline
    @objc func callHotLine(_ sender: UIButton) {
    
        guard sender.tag < dataArray.count else { return }
        Utility.callPhoneNumberShowAlert(dataArray[sender.tag].phone)
        
    }
    
    @objc func openPost(_ sender: UIGestureRecognizer) {
        
        guard let view = sender.view as? HomePostItemView else { return }
        
        let post = headerView.posts[view.tag]
        
        let detailVC = DetailWebViewController()
        detailVC.title  = post.title
        detailVC.urlString = post.link
        navigationController?.pushViewController(detailVC, animated: true)
        
    }
}



//-------------------------------
// - MARK: PRIVATE METHOD
//-----------------------------
extension HomeViewController {
    
    /// Hiển thị view Hotline
    @objc func showHotlineView() {
        
//        hotlineView = HomeHotlineView()
//        hotlineView?.delegate = self
//        Utility.showView(hotlineView, onView: view)
    }
    
    
    @objc func showDatePickerView() {
        
        
        
//        datePickerView = setupDatePickerView()
//        datePickerView!.maxDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + 14.days)
//        datePickerView!.date = Date(timeIntervalSince1970: headerTimetableView.selectedDate)
//        datePickerView!.calendarView.toggleViewWithDate(Date(timeIntervalSince1970: headerTimetableView.selectedDate))
//
//        Utility.showView(datePickerView, onView: view)
    }


    
    
//    func selectedPost(_ post: Post) {
        
        
        
//        guard post.link.count > 0 else {
//
//            if let content = post.content, content.count > 0 {
//                let postDetailVC = PostDetailViewController()
//                postDetailVC.post = post
//                postDetailVC.hidesBottomBarWhenPushed = true
//                navigationController?.pushViewController(postDetailVC, animated: true)
//            } else {
//                checkNetworkAndRequestGetPostDetail(post.id)
//            }
//
//            return
//        }
//
//
//        let postDetailVC =  PostNewDetailViewController()
//        postDetailVC.postID = post.id
//        postDetailVC.urlString = post.link
//        postDetailVC.schoolID = post.schoolID
//        postDetailVC.showComment = post.showComment
//        postDetailVC.canLike = post.canLike
//        postDetailVC.hidesBottomBarWhenPushed = true
//        navigationController?.pushViewController(postDetailVC, animated: true)
        
//    }
    
    func updateLogo() {
        
//        let image: UIImage = UIImage(named: "test")!
//        image.renderingMode = .
//        logoSchool.image = image
        
        logoSchool.image = Icon.General.LogoKidsOnline

//        guard let schoolInfo = (AppData.instance.schools.filter { $0.id == Defaults[.SchoolID]}).first else { return }
//        logoSchool.downloadedFrom(link: schoolInfo.logo, placeHolder: nil)
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        
        guard Date().timeIntervalSince1970 - lastUpdate > 5.minutes, networkReachable() else { refreshControl.endRefreshing(); return }
        checkNetworkAndRequestGetHospital()
    }
}





