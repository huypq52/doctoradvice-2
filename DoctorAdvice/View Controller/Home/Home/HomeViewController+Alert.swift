//
//  HomeViewController+Alert.swift
//  KidOnline
//
//  Created by Pham Huy on 11/12/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults

extension HomeViewController {
    
    enum AlertType {
        case callHotLine(String)
        
        case schoolLocked(String)
        
        case notNotificationServices

        func alertTitle() -> String {
            switch self {
            case .notNotificationServices, .schoolLocked:
                return LocalizedString("alert_title_message", comment: "Thông báo")
                
            case .callHotLine:
                return LocalizedString("alert_title_confirm", comment: "Xác nhận")
            }
        }
        
        func alertMessage() -> String {
            switch self {
            case .schoolLocked(let message):
                return message
            
            case .callHotLine:
                return LocalizedString("notificaiton_home_you_want_call_kidonline", comment: "Bạn có muốn gọi đến Hotline của KidsOnline không?")

            case .notNotificationServices:
                return LocalizedString("notificaiton_home_open_services_notification", comment: "Kidsonline cần cho phép thông báo để phục vụ bạn tốt hơn.")
            }
        }
    }
    
    
    
    /**
     Show Alert
     */
    
    internal func showAlertController(_ type: AlertType) {
        self.dismiss(animated: false, completion: nil)
        alertController = UIAlertController(title: type.alertTitle(), message: type.alertMessage(), preferredStyle: .alert)
       
        
        switch type {
        case .schoolLocked:
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: nil))

        
        case .callHotLine(let phone):
            
             alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: nil))
            
            /**
             Đồng ý
             */
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_agree", comment: "Đồng ý"), style: .default, handler: { _ in
                
                Utility.callPhoneNumber(phone)
                
                }))
            
            
            /**
             @author Hoan Pham, 16-08-13
             
             Không có dịch vụ thông báo -> vào setting
             */
        case .notNotificationServices:
            
            alertController?.addAction(
                UIAlertAction(
                    title: LocalizedString("notification_default_setting", comment: "Cài đặt"),
                    style: .default
                ) { _ in
                    if let url = URL(string: UIApplication.openSettingsURLString) {
//
                        
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    
//                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            )
            
            alertController?.addAction(UIAlertAction(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), style: .cancel, handler: { _ in
                Defaults[.NumberShowDialogNotification] += 1
            }))


        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
}
