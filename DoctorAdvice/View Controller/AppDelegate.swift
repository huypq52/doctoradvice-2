//
//  AppDelegate.swift
//  UniOnline
//
//  Created by Pham Huy on 5/11/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import Reachability
import UserNotifications
import SwiftyUserDefaults

//import CleanroomLogger
import Firebase
import FirebaseAnalytics
//import FirebaseAuth


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability: Reachability!

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        #if LOG // Hiển thị log để debug
//        FileManager.default.createAppDirectory("Log", skipBackupAttribute: true)
//        let path = FileManager.default.getDocumentDirectory().appendingPathComponent("Log")
//        Log.enable(configuration: CustomLogConfiguration(minimumSeverity: .verbose, dayToKeep: 7, filePath: path.path))
//        #endif
        
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
    
        print("XXXX")
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
//        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        /**
         *  Reachability
         */
        do {
            reachability = try Reachability.init()
            try reachability.startNotifier()
        }
        catch {
            fatalError("REACHABILITY ERROR - \(error)")
        }
        
        
        // Initialize Firebase service.
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
 
  
        setLanguage()
        
        
        if #available(iOS 13, *) {
            return true
        }
        else {
            guard let window = window else { return true }
            
            
            let navigationController: UINavigationController  = setupNavigationController()
            
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            return true
        }
        
  
    }
    
    
    func checkInvalidateToken(_ token: String) -> Bool {
        
        guard token.count > 0 else { return false }
        
        
        
        return true
        
    }
    

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        if let presentedViewController = window?.rootViewController?.presentedViewController {
            let className = String(describing: type(of: presentedViewController))
            if ["MPInlineVideoFullscreenViewController", "MPMoviePlayerViewController", "AVFullScreenViewController", "CornerKidsDetailViewController"].contains(className)
            {
                return .allButUpsideDown
            }
        }
        return .portrait
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        print("applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        print("applicationDidEnterBackground")
//
//        guard let info = AppData.instance.myInfo, AppData.instance.isCanChat else { return }
//        Database.database(url: FirebaseDatabase)
//            .reference()
//            .child("data")
//            .child(Defaults[.SchoolCode])
//            .child("chats/users")
//            .child("\(info.id - info.id % 100)/\(info.id)/lastUpdate").setValue(Date().timeIntervalSince1970 - 1.minute)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        print("applicationDidBecomeActive")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        print("applicationWillTerminate")
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Swift.Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //        print( "Device token: \(deviceToken.hexadecimalString)")
        print("Device token: \(deviceToken.hexadecimalString)")
        

        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //                print( "Received remote notification: \(userInfo) -- fetchCompletionHandler")
        
        print("Received remote notification: \(userInfo) -- fetchCompletionHandler")
        
        
       
        
    }
    
    func applicationSignificantTimeChange(_ application: UIApplication) {
        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        //        print( "Notification Setting: \(notificationSettings)")
        
        print("Notification Setting: \(notificationSettings)")
    }
    

 
}

extension AppDelegate {
    /**
     Setup Navigation
     */
    func setupNavigationController() -> UINavigationController {

        let navigationController = UINavigationController(rootViewController: LoginViewController())
        Utility.configureAppearance(navigation: navigationController)
        return navigationController
    }
    
    
   
    
    func setLanguage() {
        guard let language = LanguageValue(rawValue: Defaults[.SelectedLanguage]) else { return }
        Language.instance.setLanguage(language)
    }
    
    
    
    
    /**
     Setup Tabbar
     */
    
//    func setupTabBarController() -> UITabBarController {
//        let tabbarVC = CustomTabbarController()
//        return tabbarVC
//    }
    
}


