//
//  LoginCenterView.swift
//  GPSMobile
//
//  Created by Pham Huy on 3/25/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class LoginCenterView: UIView {
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 44, textField = 42
    }
    

    var userName: TextFieldIcon!
    var password: TextFieldIcon!
    var remember: UIButton!
    var login: UIButton!
    var forgotPassword: UIButton!
    var contentView: UIView!
    
    /// Nút ngôn ngữ
    //    var flagVN: UIButton!
    //
    //    var flagEN: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = bounds
        
        
        

        
        userName.frame  = CGRect(x: Size.padding15..,
                                 y:  Size.padding10.. / 2,
                                 width: contentView.frame.width - Size.padding15.. * 2,
                                 height: Size.textField..)
        
        password.frame  = CGRect(x: Size.padding15..,
                                 y: userName.frame.maxY + Size.padding10.. / 2,
                                 width: contentView.frame.width - Size.padding15.. * 2,
                                 height: Size.textField..)
        
        
        login.frame  = CGRect(x: Size.padding15..,
                              y: contentView.frame.height - Size.button..  - Size.padding10.. * 2,
                              width: contentView.frame.width - Size.padding15.. * 2,
                              height: Size.button..)
        
        login.layer.cornerRadius = login.frame.height / 2
        
        remember.frame  = CGRect(x: Size.padding15..,
                                 y: login.frame.minY - Size.button..  - Size.padding10.. / 2,
                                 width: contentView.frame.width / 2 - Size.padding15.. ,
                                 height: Size.button..)
        
        forgotPassword.frame = CGRect(x: contentView.frame.width / 2,
                                      y: login.frame.minY - Size.button..   - Size.padding10.. / 2 ,
                                      width: contentView.frame.width / 2 - Size.padding10..,
                                      height: Size.button..)
        
        
        //        flagVN.frame = CGRect(x: Size.padding10..,
        //                              y: bounds.height - Size.button.. * 2 -  Size.padding10.. * 3 / 2 ,
        //                              width: bounds.width / 2 - Size.padding10..,
        //                              height: Size.button..)
        //
        //        flagEN.frame = CGRect(x: bounds.width / 2,
        //                              y: bounds.height - Size.button.. * 2 -  Size.padding10.. * 3 / 2 ,
        //                              width: bounds.width / 2 - Size.padding10..,
        //                              height: Size.button..)
        
    }
}

//----------------------------
// MARK: - SETUP
//----------------------------

extension LoginCenterView {
    fileprivate func setup() {
        
        backgroundColor = .clear
        layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 0.5, height: 1.0)
        
        contentView = setupView(.white)

        
        
        userName = setupTextField("Email", icon: Icon.Contact.DetailEmail)
        password = setupTextField(LocalizedString("login_password", comment: "Mật khẩu"), icon: Icon.Login.Key, secureTextEntry: true)
        remember = setupButtonRemember(LocalizedString("login_remember_me", comment: "Ghi nhớ đăng nhập"))
        login = setupButton(LocalizedString("login_button_login_title", comment: "Đăng nhập").uppercased())
        forgotPassword = setupButton(LocalizedString("login_button_forgot_password_title", comment: "Quên mật khẩu?"),
                                     titleColor: UIColor.Navigation.mainColor(),
                                     bgColor: UIColor.clear,
                                     alignment: .right)

        
        addSubview(contentView)
   
        contentView.addSubview(userName)
        contentView.addSubview(password)
        contentView.addSubview(remember)
        contentView.addSubview(login)
        contentView.addSubview(forgotPassword)
        
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String, icon: UIImage, secureTextEntry: Bool = false ) -> TextFieldIcon {
        let textField = TextFieldIcon(textColor: UIColor.Text.blackMediumColor())
        textField.icon.image = icon.tint(UIColor.Text.grayMediumColor())
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = secureTextEntry
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal++)
        
        if secureTextEntry {
            textField.returnKeyType = .done
        } else {
            textField.returnKeyType = .next
        }
        
        return textField
    }
    
    fileprivate func setupButton(_ title: String,
                                 titleColor: UIColor = UIColor.white,
                                 bgColor: UIColor = UIColor.Navigation.mainColor(),
                                 alignment: UIControl.ContentHorizontalAlignment = .center) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)
        button.backgroundColor = bgColor
        button.contentHorizontalAlignment = alignment
        return button
    }
    
    /**
     Setup các buttons
     */
    func setupButtonRemember(_ title: String) -> UIButton {
        
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.blackMediumColor(), for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        button.tintColor = UIColor.lightGray
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 0)
        button.setImage(Icon.Login.DeselectedCheckbox.tint(UIColor.Text.grayMediumColor()), for: .normal)
        button.setImage(Icon.Login.SelectedCheckbox.tint(UIColor.Navigation.mainColor()), for: .selected)
        return button
        
    }
    
    fileprivate func setupButtonLanguage(_ title: String, image: UIImage, tag: LanguageValue, selected: Bool) -> UIButton {
        
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.grayMediumColor(), for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .selected)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        button.tag = tag..
        button.isSelected = selected
        return button
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.layer.cornerRadius = 8
        view.layer.borderColor = UIColor(rgba:"F7F7F7").cgColor
        view.layer.borderWidth = onePixel()
        view.clipsToBounds = true
        
        return view
    }
}





//----------------------------
// MARK: - SETUP TEXT FIELD ICON
//----------------------------

class TextFieldIcon: UITextField {
    
    var seperator: UIView!
    var icon: UIImageView!
    
    
    convenience init(textColor: UIColor? = nil) {
        self.init(frame: CGRect.zero)
        
        setup()
        
        self.textColor = UIColor.Text.blackMediumColor()
        
        if let color = textColor { self.textColor = color }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        seperator.frame = CGRect(x: 0, y: bounds.height - 3, width: bounds.width, height: onePixel())
        icon.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
    }
    
    fileprivate func setup() {
        
        icon = setupImageView()
        seperator = setupSeperator()
        addSubview(seperator)
        addSubview(icon)
        clearButtonMode = .whileEditing
        autocorrectionType = .no
        autocapitalizationType = .none
        
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: bounds.width + 35, height: bounds.height).insetBy(dx: 35, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: bounds.width + 15, height: bounds.height).insetBy(dx: 35, dy: 0)
    }
    
    fileprivate func setupSeperator() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }
    
    fileprivate func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .center
        return imageView
    }
    
    
}

























