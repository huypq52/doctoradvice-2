//
//  SelectionLanguageView.swift
//  KidOnline
//
//  Created by Pham Huy on 5/13/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions
import SwiftyUserDefaults

class SelectionLanguageView: UIView {
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 44, textField = 40
    }
    

    
    /// Nút ngôn ngữ
    var flagVN: UIButton!
    
    var flagEN: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        
        
        flagVN.frame = CGRect(x: Size.padding10..,
                              y: 0 ,
                              width: bounds.width / 2 - Size.padding10..,
                              height: Size.button..)
        
        flagEN.frame = CGRect(x: bounds.width / 2,
                              y: 0 ,
                              width: bounds.width / 2 - Size.padding10..,
                              height: Size.button..)
        
    }
}

//----------------------------
// MARK: - SETUP
//----------------------------

extension SelectionLanguageView {
    fileprivate func setup() {
        

        clipsToBounds = true
        backgroundColor = .clear
        

        flagVN = setupButtonLanguage("Tiếng Việt",
                                     image: Icon.Login.FlagVN,
                                     tag: .vietnamese,
                                     selected: Defaults[.SelectedLanguage] == LanguageValue.vietnamese..)
        
        flagEN = setupButtonLanguage("English",
                                     image: Icon.Login.FlagEN,
                                     tag: .english,
                                     selected: !flagVN.isSelected)
        
        
        
        addSubview(flagVN)
        addSubview(flagEN)
    }
    

    
    fileprivate func setupButtonLanguage(_ title: String, image: UIImage, tag: LanguageValue, selected: Bool) -> UIButton {
        
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.grayMediumColor(), for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .selected)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        button.tag = tag..
        button.isSelected = selected
        return button
    }
}






















