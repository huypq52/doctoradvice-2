//
//  RegisterFooterView.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/17/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit

class RegisterFooterView: UIView {
    
    enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, paddding5 = 5, button = 44
    }
    
    var padding: CGFloat = 10

    var contentView: UIView!
    var buttonLeft: UIButton!
    var buttonRight: UIButton!
    
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: padding ,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        if buttonLeft.isHidden {
            
            buttonLeft.frame = .zero
            buttonRight.frame = CGRect(x: Size.padding10..,
                                       y: Size.padding10.. / 2,
                                       width: contentView.frame.width - Size.padding10.. * 2,
                                       height: contentView.frame.height - Size.padding10..)
            
        } else {
            
            buttonLeft.frame = CGRect(x: Size.padding10..,
                                      y: Size.padding10.. / 2,
                                      width: contentView.frame.width / 2 - Size.padding10.. * 3 / 2,
                                      height: contentView.frame.height - Size.padding10..)
            
            
            buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding10.. / 2,
                                       y: Size.padding10.. / 2,
                                       width: contentView.frame.width / 2 - Size.padding10.. * 2,
                                       height: contentView.frame.height - Size.padding10..)
        }
    }
    
}

extension RegisterFooterView {

    func setupAllSubviews() {
    
        
        contentView = setupView(.clear)
        
    
        //TODO: Dịch
        buttonLeft = setupButton(title: "Hủy bỏ", titleColor: UIColor.Navigation.mainColor(), bgColor: .white)
        buttonRight = setupButton(title: "Đăng ký", titleColor: .white, bgColor: UIColor.Navigation.mainColor())
        
        addSubview(contentView)
        contentView.addSubview(buttonLeft)
        contentView.addSubview(buttonRight)
    }
    
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 8
        return view
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white,
                     bgColor: UIColor = UIColor.Navigation.mainColor()) -> UIButton {
        
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = bgColor
        
        button.layer.cornerRadius = 8
        button.layer.borderWidth = onePixel()
        button.layer.borderColor = UIColor.Navigation.mainColor().cgColor
        return button
    }

}




