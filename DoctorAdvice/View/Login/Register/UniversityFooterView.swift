//
//  UniversityFooterView.swift
//  UniOnline
//
//  Created by Pham Huy on 5/16/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
//import PHExtensions

class UniversityFooterView: UIView {
    
    enum Size: CGFloat {
        case  padding15 = 15, padding10 = 10, paddding5 = 5, button = 44, label = 25, textField = 40
    }
    

    var contentView: UIView!
    var textField: UITextField!
    var table: UITableView!
    var buttonNext: UIButton!
    
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    func setup() {
        
        
        contentView = setupView(.white)
        
        buttonNext = setupButton(image: Icon.General.ArrowRight.tint(.white))
        table = setupTable()
        textField = setupTextField(LocalizedString("search_bar_placeholder", comment: "Tìm kiếm"), icon: Icon.Login.Phone.tint(.gray))
        
        addSubview(contentView)
        addSubview(buttonNext)
        contentView.addSubview(textField)
        contentView.addSubview(table)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        buttonNext.frame = CGRect(x: bounds.width - Size.button.. - Size.padding15..,
                                  y: bounds.height - Size.button.. - Size.padding15..,
                                  width: Size.button..,
                                  height: Size.button..)
        
        contentView.frame = CGRect(x: Size.padding10.. ,
                                   y: 0, width: bounds.width - Size.padding10.. * 2,
                                   height: buttonNext.frame.minY - Size.padding10..)
        
        textField.frame = CGRect(x: Size.padding10..,
                                 y: Size.padding10.. ,
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: Size.textField.. )
        
        table.frame = CGRect(x: 0,
                             y: textField.frame.maxY + Size.padding10.. ,
                             width: contentView.frame.width,
                             height: contentView.frame.height - textField.frame.maxY - Size.padding10.. * 2)
    
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 8
        return view
    }
    
    func setupTable() -> UITableView {
        
        let table: UITableView = UITableView(frame: CGRect.zero, style: .plain)
        table.backgroundColor = .clear
        table.separatorStyle = .none
        table.allowsSelection = true
        table.register(SeperatorTableViewCell.self, forCellReuseIdentifier: "Cell")
//        table.keyboardDismissMode = .onDrag
        return table
        
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String, icon: UIImage, secureTextEntry: Bool = false ) -> TextFieldIcon {
        let textField = TextFieldIcon(textColor: UIColor.Text.blackMediumColor())
        textField.icon.image = icon
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = secureTextEntry
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal++)
        textField.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        textField.layer.borderWidth = onePixel()
        textField.layer.cornerRadius = 10
        textField.returnKeyType = .done
        textField.backgroundColor = .white
        return textField
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }

}



