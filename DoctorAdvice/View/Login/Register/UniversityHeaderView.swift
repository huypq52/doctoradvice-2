//
//  UniversityHeaderView.swift
//  UniOnline
//
//  Created by Pham Huy on 5/16/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
//import PHExtensions

class UniversityHeaderView: UIView {
    
    enum Size: CGFloat {
        case  padding15 = 15, padding10 = 10, paddding5 = 5, button = 44, label = 25
    }

    var back: UIButton!
    var labelTitle: UILabel!
    var labelSub: UILabel!
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    func setup() {
        
        back = setupButton(image: Icon.Navigation.Back.tint(.white))
        labelTitle = setupLabel("Tạo tài khoản".uppercased(),
                                textColor: .white,
                                font: UIFont(name: FontType.latoBold.., size: FontSize.large..)!,
                                alignment: .center)
        
        
        labelSub = setupLabel("Bước 1: Vui lòng chọn trường của bạn",
                                textColor: .white,
                                font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                alignment: .center)

        addSubview(back)
        addSubview(labelTitle)
        addSubview(labelSub)
        
        backgroundColor = .clear

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        
        back.frame = CGRect(x: Size.padding15..,
                             y: Size.padding15..,
                             width: Size.button..,
                             height: Size.button..)
        labelTitle.frame = CGRect(x: Size.padding15..,
                                  y: back.frame.maxY,
                                  width: bounds.width - Size.padding15.. * 2,
                                  height: Size.button.. / 2)
        
        labelSub.frame = CGRect(x: Size.padding15..,
                                y: labelTitle.frame.maxY + Size.padding10.. / 2,
                                width: bounds.width - Size.padding15.. * 2,
                                height: bounds.height - labelTitle.frame.maxY - Size.padding10.. / 2)

    }
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label: UILabel = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }

    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }
}


