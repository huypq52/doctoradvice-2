//
//  ChangedPasswordFooterView.swift
//  NguoiBA
//
//  Created by Pham Huy on 6/28/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class ChangedPasswordFooterView: UIView {
    fileprivate enum Size: CGFloat {
        case padding7 = 7, padding10 = 10, padding5 = 5, padding15 = 15, button = 44, label = 25
    }
    // buttonRight
    var buttonRight: UIButton!
    
    // buttonLeft
    var buttonLeft: UIButton!
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    fileprivate func setup() {
        
        buttonLeft = setupButton(LocalizedString("notification_default_cancel", comment: "Bỏ qua"), titleColor: UIColor.Text.grayMediumColor())
        
        buttonRight = setupButton(LocalizedString("account_label_change_password", comment: "Đổi mật khẩu"),
                                  titleColor: UIColor.Navigation.mainColor())
        
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
//        gradientLayer.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
//        buttonRight.layer.insertSublayer(gradientLayer, at: 0)
        
        addSubview(buttonLeft)
        addSubview(buttonRight)
        
    }
    
    // Set Frame
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        if buttonLeft.isHidden {
            

            let widthButton: CGFloat = Utility.widthForView(buttonRight.titleLabel?.text ?? "",
                                                            font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                                            height: 20) + 30
            
            buttonRight.frame =  CGRect(x: (bounds.width - widthButton) / 2 ,
                                        y: bounds.height - Size.padding7.. - Size.button..,
                                        width: widthButton,
                                        height: Size.button..)
        }
        else {
            
            buttonLeft.frame =  CGRect(x: Size.padding15..,
                                       y: bounds.height - Size.padding7.. - Size.button..,
                                       width: bounds.width / 2  - Size.padding15..  - Size.padding7.. / 2,
                                       height: Size.button..)
            
            buttonRight.frame =  CGRect(x: bounds.width / 2 + Size.padding7.. / 2 ,
                                        y: bounds.height - Size.padding7.. - Size.button..,
                                        width: bounds.width / 2  - Size.padding15..  - Size.padding7.. / 2,
                                        height: Size.button..)
        }
        

        

            buttonRight.layer.cornerRadius = buttonRight.frame.height / 2

        
    }
    
}


extension ChangedPasswordFooterView {
    
    fileprivate func setupButton(_ title: String, titleColor: UIColor = UIColor.Navigation.mainColor()) -> UIButton {
        let button = UIButton()
        
        button.clipsToBounds = true
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.setTitle(title.uppercased(), for: .normal)
//        button.layer.borderWidth = onePixel()

        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = titleColor
//        button.layer.borderColor = titleColor.cgColor

            

      
        return button
    }
}
