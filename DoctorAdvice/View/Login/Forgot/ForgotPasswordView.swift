//
//  ForgotPasswordView.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/21/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class ForgotPasswordView: UIView {
    
    fileprivate enum Size: CGFloat {
        case button = 44, padding10 = 10, padding5 = 5, padding7 = 7, contentWidth = 290, logo = 50
        case textFeild = 40
        
        case contentHeight = 220

        
        
    }
    
    fileprivate var contentView: UIView!

    
    var buttonSend: UIButton!
    var buttonClose: UIButton!
    var haveActivationCode: UIButton!
    var labelSub: UILabel!
    var textFieldEmail: TextFieldIcon!
  
    
    var imageLogo: UIImageView!
    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - Size.contentHeight.. ) / 2 - 75,
                                   width: Size.contentWidth..,
                                   height: Size.contentHeight..)
        
        
        imageLogo.frame = CGRect(x: Size.padding10.., y: Size.padding10..,
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: Size.logo..)
        
        buttonClose.frame = CGRect(x: contentView.frame.width - Size.button..,
                                   y: 0,
                                   width: Size.button..,
                                   height: Size.button..)
        
        let width: CGFloat = Utility.widthForView(LocalizedString("general_button_send", comment: "Gửi"),
                                                  font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                                  height: 20) + 50
        buttonSend.frame = CGRect(x: contentView.frame.width - width - 10,
                                  y: contentView.frame.height - Size.button.. - Size.padding10..,
                                  width: width,
                                  height: Size.button..)
        buttonSend.layer.cornerRadius = buttonSend.frame.height / 2
        
//        gradientLayer.frame = buttonSend.bounds
//        gradientLayer.cornerRadius = buttonSend.frame.height / 2
        
        haveActivationCode.frame = CGRect(x: Size.padding10..,
                                          y: contentView.frame.height - Size.button.. - Size.padding10..,
                                          width: buttonSend.frame.minX - Size.padding10.. * 2,
                                          height: Size.button..)
        
        
        
        
        textFieldEmail.frame = CGRect(x: Size.padding10..,
                                      y: buttonSend.frame.minY - Size.padding10.. - Size.textFeild..,
                                      width: contentView.frame.width - Size.padding10.. * 2,
                                      height: Size.textFeild..)
        


        labelSub.frame = CGRect(x: Size.padding10..,
                                y: imageLogo.frame.maxY + Size.padding10.. / 2,
                                width: contentView.frame.width - Size.padding10.. * 2,
                                height: textFieldEmail.frame.minY - imageLogo.frame.maxY - Size.padding10..)
        
        
    }
    
}


extension UIView {
    @objc func  dismissView()  {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
            
        }, completion: { _ in
            self.isHidden = true
            self.removeFromSuperview()
            
        })
    }
}


//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension ForgotPasswordView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.7)
        
        contentView = setupView(.white, cornerRadius: Size.padding10..)
        imageLogo = setupImageLogo()
        
//        textFieldSchoolCode = setupTextField(LocalizedString("login_school_code", comment: "Nhập mã trường"), icon: Icon.Login.SchoolCode.tint(UIColor.Text.grayMediumColor()))
//        textFieldSchoolCode.returnKeyType = .next
        
        
        textFieldEmail = setupTextField("Email", icon: Icon.Contact.DetailEmail.tint(UIColor.Text.grayMediumColor()))
        labelSub = setupLabel(LocalizedString("login_please_input_username", comment: "Vui lòng nhập email của bạn"),
                              textColor: UIColor.Text.blackMediumColor(),
                              font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!,
                              bgColor: UIColor.clear)
        
        
        buttonSend = setupButton(title: LocalizedString("general_button_send", comment: "Gửi").uppercased())

        
        buttonClose = setupButton(UIColor.clear, title: "", image: Icon.General.Delete.tint(UIColor.Text.blackNormalColor()))
        buttonClose.addTarget(self, action: #selector(self.dismissView), for: .touchUpInside)
        
        haveActivationCode = setupButton(.clear, title: LocalizedString("login_a_have_confirm_code", comment: "Đã có mã xác nhận"))
        haveActivationCode.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
        haveActivationCode.contentHorizontalAlignment = .left
        addSubview(contentView)
        
        
        contentView.addSubview(buttonSend)
        contentView.addSubview(labelSub)
        contentView.addSubview(textFieldEmail)
        contentView.addSubview(imageLogo)
        contentView.addSubview(buttonClose)
        contentView.addSubview(haveActivationCode)
        
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String, icon: UIImage, secureTextEntry: Bool = false ) -> TextFieldIcon {
        let textField = TextFieldIcon(textColor: UIColor.Text.blackMediumColor())
        textField.icon.image = icon.tint(UIColor.Text.grayMediumColor())
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = false
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal++)
        textField.returnKeyType = .done
        textField.textColor = UIColor.Text.blackMediumColor()
        textField.seperator.isHidden = false
        textField.clipsToBounds = true
        return textField
    }
    
    fileprivate func setupButton(_ bgColor: UIColor = UIColor.Navigation.mainColor(),
                                 title: String? = nil,
                                 image: UIImage? = nil) -> UIButton {
        
        let button = UIButton()
        if let image = image { button.setImage(image, for: UIControl.State()) }
        
        if let title = title {
            button.setTitle(title, for: UIControl.State())
        }
        
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.backgroundColor = bgColor
        return button
    }
    
    fileprivate func setupLabel(_ text: String? = nil,
                                textColor: UIColor = UIColor.white,
                                font: UIFont,
                                bgColor: UIColor = UIColor.white) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        label.backgroundColor = bgColor
        label.clipsToBounds = true
        label.text = text
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    fileprivate func setupImageLogo() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Icon.General.LogoKidsOnline
        return imageView
    }
}






