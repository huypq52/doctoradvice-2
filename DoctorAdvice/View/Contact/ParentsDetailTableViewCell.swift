//
//  ParentsDetailTableViewCell.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/25/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class ParentsDetailTableViewCell: SeperatorTableViewCell {
    
    fileprivate enum Size:CGFloat {
        case padding10 = 10, padding5 = 5, padding15 = 15, button = 44, label = 22, avatar = 80
    }
    
    var labelName: UILabel!
    var labelChild: UILabel!
    var labelAge: UILabel!
    var imageViewAvatar: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageViewAvatar.frame = CGRect(x: Size.padding15.. ,
                                    y: (bounds.height - Size.avatar..) / 2,
                                    width: Size.avatar..,
                                    height: Size.avatar..)
        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.height / 2
        
        
        labelName.frame = CGRect(x: imageViewAvatar.frame.maxX + Size.padding10..,
                                       y: bounds.height / 2 - Size.label.. * 3 / 2,
                                       width: bounds.width - imageViewAvatar.frame.maxX - Size.padding10.. * 2,
                                       height: Size.label..)
        

        
        labelChild.frame = CGRect(x: labelName.frame.minX,
                                       y: labelName.frame.maxY,
                                       width: labelName.frame.width,
                                       height: Size.label..)
        
        labelAge.frame = CGRect(x: labelName.frame.minX,
                                  y: labelChild.frame.maxY,
                                  width: labelName.frame.width,
                                  height: Size.label..)
    }
}


extension ParentsDetailTableViewCell {
    fileprivate func setupView() {
        backgroundColor = UIColor.white
        
        imageViewAvatar = setupImageView()
        labelName = setupLabel(UIFont(name: FontType.latoLight.., size: FontSize.large..)!, textColor: UIColor.Text.blackMediumColor())
        labelChild = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!, textColor: UIColor.Text.grayNormalColor())
        labelAge = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!, textColor: UIColor.Text.grayNormalColor())
        

        addSubview(imageViewAvatar)
        addSubview(labelName)
        addSubview(labelChild)
        addSubview(labelAge)

    }
    
    fileprivate func setupLabel(_ font: UIFont, textColor: UIColor) -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = font
        label.textColor = textColor
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }
    
    fileprivate func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.lightGray
        return imageView
    }
}




