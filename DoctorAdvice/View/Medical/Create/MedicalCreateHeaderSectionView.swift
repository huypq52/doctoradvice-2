//
//  MedicalCreateHeaderSectionView.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 12/27/19.
//  Copyright © 2019 Pham Huy. All rights reserved.
//


import UIKit


class MedicalCreateHeaderSectionView: UITableViewHeaderFooterView {
    
    enum Size: CGFloat {
        case button = 44, label = 25, padding15 = 15, padding10 = 10
    }
    
    var labelTitle: UILabel!
    var buttonAdd: UIButton!
    
    var padding: CGFloat = 0
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        buttonAdd.frame = CGRect(x: contentView.frame.width - Size.button.. - Size.padding10..,
                                 y: 0,
                                 width: Size.button..,
                                 height: contentView.frame.height)
        
        labelTitle.frame = CGRect(x: Size.padding10..,
                                  y: 0,
                                  width: buttonAdd.frame.minX - Size.padding10.. * 3 / 2,
                                  height: contentView.frame.height)
        
    }
}

extension MedicalCreateHeaderSectionView {
    
    
    // Setup addSubview
    func setup() {
        
        contentView.clipsToBounds = true
        
        
        labelTitle = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                                font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!)
        
        buttonAdd = setupButton(image: Icon.General.AddImage.tint(UIColor.Navigation.mainColor()))
        

        contentView.addSubview(labelTitle)
        contentView.addSubview(buttonAdd)
        
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }
}

