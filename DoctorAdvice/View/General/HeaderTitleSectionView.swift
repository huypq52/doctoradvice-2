//
//  HeaderTitleSectionView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/12/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit

class HeaderTitleSectionView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 40
    }
    
    var bgView: UIView!
    var label: UILabel!
    
    var paddingTop: CGFloat = 5 {
        didSet {
            layoutSubviews()
        }
    }
    
    var padding: CGFloat = 5
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.frame = CGRect(x: padding ,
                              y: paddingTop,
                              width: bounds.width - padding * 2 ,
                              height: bounds.height)
        
        
        
        label.frame = CGRect(x: Size.padding10.. * 3 / 2,
                             y: 5 + paddingTop,
                             width: bounds.width - Size.padding10.. * 3,
                             height: bounds.height - 10 - paddingTop)
    }
    
    
    fileprivate func setup() {
        
        clipsToBounds = true
        backgroundColor = .clear
        
        bgView = setupView(.white)
        label = setupLabel(textColor: UIColor.Text.blackMediumColor(),
                           font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!)
        
        addSubview(bgView)
        addSubview(label)
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        return view
    }
}


