//
//  HeaderDetailPhotoView.swift
//  KO4Teacher
//
//  Created by Pham Huy on 8/28/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class HeaderDetailPhotoView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 15, button = 44, buttonSmall = 40
    }
    
    fileprivate var contentView: UIView!
    var buttonAdd: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        buttonAdd.frame = bounds
        
    }
}


extension HeaderDetailPhotoView {
    func setupAllSubviews() {
        buttonAdd = setupButtonAdd()

        
        addSubview(buttonAdd)

    }
    
    func setupButtonAdd() -> UIButton {
        let button = UIButton()
        button.setTitle(LocalizedString("general_label_add_image", comment: "Thêm ảnh"), for: UIControl.State())
        button.setImage(Icon.Post.AddPicture.tint(UIColor.Navigation.mainColor()), for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.Navigation.mainColor(), for: UIControl.State())
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10)
//        button.layer.borderWidth = onePixel()
//        button.layer.borderColor = UIColor.Misc.seperatorColor().CGColor
//        button.clipsToBounds = true
//        button.layer.cornerRadius = 5
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10)
        return button
    }
    
  }
