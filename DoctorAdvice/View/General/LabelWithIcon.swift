//
//  LabelWithIcon.swift
//  TimBuyt
//
//  Created by Thành Lã on 6/26/17.
//  Copyright © 2017 timbuyt. All rights reserved.
//

import UIKit
//import PHExtensions

class LabelWithIcon: UIView {
    
    enum Size: CGFloat {
        case Padding15 = 15, Padding10 = 10, Label = 16, image = 20, button = 36, padding7 = 7
    }
    
    var label: UILabel!
    var icon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        icon.frame = CGRect(x: 0,
                            y: (bounds.height - Size.image..) / 2,
                            width: Size.image..,
                            height: Size.image..)
//        icon.layer.cornerRadius = icon.frame.height / 2
        
        label.frame = CGRect(x: icon.frame.maxX + 5,
                             y: 0,
                             width: bounds.width - (icon.frame.maxX + 5),
                             height: bounds.height)
        
        
    }
    
    func setup() {
        
        label = setTitle()
        icon = setupImageView()
        
        
        addSubview(label)
        addSubview(icon)
    }
    
    
    func setupButton() -> UIButton {
        let button = UIButton()
        
        return button
    }
    
    func setupImageView() -> UIImageView {
        let bgImage = UIImageView()
        return bgImage
    }
    
    func setTitle() -> UILabel {
        let textLabel = UILabel()
        textLabel.textAlignment = .left
        textLabel.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textLabel.textColor = UIColor.Text.blackMediumColor()
        textLabel.numberOfLines = 2
        textLabel.lineBreakMode = .byTruncatingTail

        return textLabel
    }
    
    func setupView(bgColor: UIColor = UIColor.lightGray.alpha(0.8)) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
}

