//
//  TitleContentView.swift
//  KidsOnline
//
//  Created by Pham Huy on 10/2/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SKPhotoBrowser

class TitleContentView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, label = 30
    }
    
    var contentView: UIView!
    
    fileprivate var labelSub: UILabel!
    fileprivate var labelTitle: UILabel!
    fileprivate var labelContent: UITextView!
    
    
    var data: ActivitySectionInfo! {
        didSet {
            
            labelTitle.text = data.title
            labelContent.text = data.content
            labelTitle.backgroundColor = data.bgColor
            labelSub.text = data.subTitle
            
            
            
            if data.title == LocalizedString("activity_day_label_link", comment: "Tham khảo") {
                labelContent.textColor = UIColor.Navigation.mainColor()
                labelContent.font = UIFont(name: FontType.latoItalic.., size: FontSize.normal--)!
                labelContent.isEditable = false
                labelContent.isSelectable = true
                labelContent.dataDetectorTypes = .link
            } else {
                labelContent.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
                labelContent.textColor = UIColor.Text.blackMediumColor()
            }
            
            images = data.imageList
            
            layoutSubviews()
        }
    }
    
    var viewHeight: CGFloat = 0
    
    
    
    var scroll: UIScrollView!
    
    var images: [String] = [] {
        didSet {
            
            guard images.count > 0 else {
                scroll.isHidden = true
                buttonLeft.isHidden = true
                buttonRight.isHidden = true
                return
            }
            
            scroll.isHidden = false
            buttonLeft.isHidden = false
            buttonRight.isHidden = false
        }
    }
    
    fileprivate var buttonLeft: UIButton!
    fileprivate var buttonRight: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        clipsToBounds = true
        
        contentView.frame = bounds
        
        
        let title: String = labelTitle.text ?? ""
//        let content: String = data.content ?? ""
        
        let width = Utility.widthForView(title,
            font: labelTitle.font,
            height: Size.label..)
        
        
        
        labelTitle.frame = CGRect(x: Size.padding10.. ,
                                   y: Size.padding10.. ,
                                   width: max(80, width  + 10 * 2),
                                   height: Size.label.. )
        labelTitle.layer.cornerRadius = 5
       
        labelTitle.isHidden = title.count == 0
        
        
        labelSub.frame = CGRect(x: labelTitle.frame.maxX + Size.padding10..,
                                y: Size.padding10.. / 2,
                                width: contentView.frame.width -  labelTitle.frame.maxX - Size.padding10.. * 2 ,
                                height: Size.label.. )

        
        

        
        
        
        guard images.count > 0 else {
            
            labelContent.frame = CGRect(x: Size.padding10.. ,
                                        y: labelTitle.frame.maxY + Size.padding10.. / 2 ,
                                        width: contentView.frame.width - Size.padding10.. * 2,
                                        height: contentView.frame.height - Size.padding10.. )
            
            return
        }
        
        
        labelContent.frame = CGRect(x: Size.padding10.. ,
                                    y: labelTitle.frame.maxY + Size.padding10.. / 2 ,
                                    width: contentView.frame.width - Size.padding10.. * 2,
                                    height: contentView.frame.height - Size.padding10.. - 80 )
        
        
        scroll.frame = CGRect(x: Size.padding10..,
                              y: contentView.frame.height - Size.padding10.. / 2 - 80,
                              width: contentView.frame.width - Size.padding10.. * 2,
                              height: 80)
        
        buttonLeft.frame = CGRect(x: 0,
                                  y: 0,
                                  width: 15,
                                  height: 20)
        buttonLeft.center = CGPoint(x: buttonLeft.center.x, y: scroll.center.y)
        
        buttonRight.frame = CGRect(x: contentView.frame.width -  15,
                                   y: 0,
                                   width: 15,
                                   height: 20)
        
        buttonRight.center = CGPoint(x: buttonRight.center.x, y: scroll.center.y)
        
        
        reloadImage()

        
        
        
    }
}

extension TitleContentView: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        print("url: \(URL)")
        return true
    }
}

extension TitleContentView {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        
        contentView = setupSeperator(UIColor.white)
        labelTitle = setupLabel(UIFont(name: FontType.latoBold.., size: FontSize.normal++)!,
                                 textColor: UIColor.white,
                                 bgColor: UIColor.Navigation.mainColor())
        
        labelSub = setupLabel(UIFont(name: FontType.latoBold.., size: FontSize.normal++)!,
                              textColor: UIColor.Text.blackMediumColor(),
                              alignment: .right,
                              bgColor: UIColor.clear)
        
        labelContent = setupTextView()
        
        
        contentView.addSubview(labelTitle)
        contentView.addSubview(labelContent)
        contentView.addSubview(labelSub)
        

        addSubview(contentView)
        
        
        
        scroll = setupScrollView()
        buttonLeft = setupButton(Icon.General.ArrowLeft.tint(UIColor.Text.grayMediumColor()))
        buttonRight = setupButton(Icon.General.ArrowRight.tint(UIColor.Text.grayMediumColor()))
        
        
        contentView.addSubview(scroll)
        contentView.addSubview(buttonLeft)
        contentView.addSubview(buttonRight)
        
        
    }
    
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                            textColor: UIColor = UIColor.Text.blackMediumColor(),
                            alignment: NSTextAlignment = .center,
                            bgColor: UIColor = UIColor.clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    
    fileprivate func setupTextView() -> UITextView {
        let textView = UITextView()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.isEditable = false
        textView.textAlignment = .justified
        textView.backgroundColor = UIColor.clear
        textView.delegate = self
        return textView
    }
    
    
    fileprivate func setupSeperator(_ color: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
    
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.setImage(image, for: .normal)
        return button
    }
    
    
    fileprivate func setupScrollView() -> UIScrollView {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.clipsToBounds = true
        scroll.backgroundColor = .clear
        return scroll
    }

    
}

extension TitleContentView {
    
    @objc func tapOnUIImageView(_ sender: UIGestureRecognizer) {
        
        guard let view = sender.view else { return }
//        delegate?.didSelectedPosition(view.tag)
        
        let photos: [SKPhoto] = images.map { SKPhoto.photoWithImageURL($0.getFullLink())  }
        let browser = SKPhotoBrowser(photos: photos)
        browser.initializePageIndex(view.tag)
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller

            topController.present(browser, animated: true, completion: {})
        }
        
    }
    
    func reloadImage() {
        
        

        
        for view in scroll.subviews { view.removeFromSuperview() }
        
        
        for (index, image) in images.enumerated() {
            
            
            
            let imageView = UIImageView(image: Icon.General.CoverDefault)
            imageView.contentMode = .scaleAspectFill
            imageView.tag = index
            imageView.clipsToBounds = true
            imageView.frame = CGRect(x: CGFloat(index) * scroll.frame.height + 5 / 2,
                                     y: 5,
                                     width: scroll.frame.height - 10 / 2,
                                     height: scroll.frame.height - 10)
            
            print("index: \(index) --- \(imageView.frame)")
            
            imageView.backgroundColor = UIColor.clear
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnUIImageView(_:))))
            imageView.downloadedFrom(link: image)
            scroll.addSubview(imageView)
            
            
            
            
        }
        scroll.contentSize = CGSize(width: scroll.frame.height * CGFloat(images.count), height: scroll.frame.height)
    }
    
}


