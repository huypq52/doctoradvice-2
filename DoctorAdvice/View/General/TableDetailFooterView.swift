//
//  TableDetailFooterView.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/28/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class TableDetailFooterView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 40
    }
    
    var buttonRight: UIButton!
    
    fileprivate var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: Size.padding10..,
                                   y: 0,
                                   width: bounds.width - Size.padding10..,
                                   height: bounds.height)
        
        buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding10.. / 2,
                                   y: (contentView.frame.height - Size.button.. ) / 2,
                                   width: contentView.frame.width / 2 - Size.padding10.. * 3 / 2,
                                   height: Size.button..)
    }
}

extension TableDetailFooterView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.clear
        
        contentView = setupView()
        buttonRight = setupButton()
        
        
        
        addSubview(contentView)
        contentView.addSubview(buttonRight)
        
        
    }
    
    func setupButton() -> UIButton {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        button.setTitleColor(UIColor.white, for: .normal)
        button.contentHorizontalAlignment = .left

        button.setTitle(LocalizedString("notification_default_update", comment: "Cập nhật").uppercased(), for: .normal)
        button.setImage(Icon.General.Upload.tint(UIColor.white), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: -2, left: 10, bottom: 2, right: -10)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: -18)
        
        return button
    }
    
    func setupView() -> UIView {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }
}
