//
//  QuickHelpImageView.swift
//  KidOnline
//
//  Created by Pham Huy on 5/29/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions

class QuickHelpImageView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 44
    }
    
    var contentView: UIView!
    
    var imageView: UIImageView!
    var buttonClose: UIButton!
    
    var image: UIImage? = nil {
        didSet {
            
            guard imageView != nil else { return }
            imageView.image = image
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        
        imageView.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        buttonClose.frame = CGRect(x: contentView.frame.width - Size.button.. - 10,
                                   y: 20,
                                   width: Size.button..,
                                   height: Size.button..)
        
        buttonClose.layer.cornerRadius = Size.button.. / 2
        
        
        
        
    }
}

extension QuickHelpImageView {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.black.alpha(0.6)
        
        contentView = setupSeperator(.clear)
        imageView = setupImageView()
        buttonClose = setupButton(image: Icon.General.Delete.tint(.black))
        buttonClose.clipsToBounds = true
        buttonClose.backgroundColor = UIColor.white.alpha(0.7)
        
        
        
        contentView.addSubview(imageView)
        contentView.addSubview(buttonClose)
        addSubview(contentView)
        
        
        buttonClose.addTarget(self, action: #selector(self.dismissView), for: .touchUpInside)
        
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }

    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        return imageView
    }
    
    fileprivate func setupSeperator(_ color: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
}


