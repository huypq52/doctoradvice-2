//
//  TimePickerView+Delegate.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/13/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import CVCalendar

//====================================
// MARK: - CVCALENDAR
//====================================

//====================================
// MARK: - -CVCALENDAR MENU DELEGATE
//====================================
extension DatePickerView: CVCalendarMenuViewDelegate {
    func dayLabelWeekdayInTextColor() -> UIColor {
        return UIColor.black
    }
    
    func firstWeekday() -> Weekday {
        return .sunday
    }
    
    func weekdaySymbolType() -> WeekdaySymbolType {
        return .short
    }
    
    
    func dayOfWeekFont() -> UIFont {
        return UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
    }
}

//====================================
// MARK: - -CVCALENDAR  DELEGATE
//====================================

extension DatePickerView: CVCalendarViewDelegate {
    
    /// Required method to implement!
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func shouldShowWeekdaysOut() -> Bool {
        return false
    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        
        let componentsDate = Calendar.current.dateComponents([ .year, .month, .day, .hour, .minute], from: date as Date)
        guard let dateTemp = dayView.date.convertedDate(calendar: Calendar.current) else { return }
        date = dateTemp + componentsDate.hour!.hours + componentsDate.minute!.minutes
        //        changeDateTime(date.timeIntervalSince1970)
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        self.presentedLabelDateUpdated(date)
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool {
        return false
    }
    
}

//============================================
// MARK: - -CVCALENDAR APPEARANCE DELEGATE
//============================================

extension DatePickerView: CVCalendarViewAppearanceDelegate {
    func dayLabelWeekdaySelectedTextColor() -> UIColor {
        return UIColor.white
    }
    
    func dayLabelWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.Navigation.subColor()
    }
    
    func dayLabelPresentWeekdayTextColor() -> UIColor {
        return UIColor.Navigation.mainColor()
    }
    
    func dayLabelPresentWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.Navigation.subColor()
    }
    
    func dayLabelPresentWeekdayFont() -> UIFont {
        return UIFont(name: FontType.latoBold.., size: FontSize.normal++)!
    }
    
    func dayLabelWeekdaySelectedFont() -> UIFont {
        return UIFont(name: FontType.latoBold.., size: FontSize.normal++)!
    }
    
    func dayLabelPresentWeekdaySelectedFont() -> UIFont {
        return UIFont(name: FontType.latoBold.., size: FontSize.normal++)!
    }
    
    func dayLabelWeekdayFont() -> UIFont {
        return UIFont(name: FontType.latoRegular.., size: FontSize.normal++)!
    }
    func spaceBetweenDayViews() -> CGFloat {
        return 1.0
    }
    
//    func dayLabelWeekdayHighlightedTextColor() -> UIColor {
//        return .red
//    }
    
    func dayLabelPresentWeekdaySelectedTextColor() -> UIColor {
        return UIColor.Navigation.mainColor()
    }
    

}

