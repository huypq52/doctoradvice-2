//
//  HeaderProcessView.swift
//  KO4Teacher
//
//  Created by Pham Huy on 9/1/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


 
import Foundation
import NVActivityIndicatorView
//import PHExtensions


class HeaderProcessView: UIView {
    
    fileprivate enum Size: CGFloat {
        case process = 30, padding15 = 15
    }
    
    var textLabel: UILabel!
    
    var processView: NVActivityIndicatorView = {
        let view = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 30), type: .ballSpinFadeLoader, color: UIColor.Navigation.mainColor(), padding: 2)
//        view.hidesWhenStopped = true
//        
        view.backgroundColor = .clear
        view.isHidden = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        let textWidth = Utility.widthForView(textLabel.text ?? "",
                                             font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                             height: bounds.height)
        
        processView.frame = CGRect(x: (bounds.width - textWidth) / 2 - Size.process..  - Size.padding15.. / 2,
                                   y: (bounds.height - Size.process.. ) / 2,
                                   width: Size.process..,
                                   height: Size.process..)
        
        
        textLabel.frame = CGRect(x: processView.frame.maxX + Size.padding15.. / 2,
                                  y: 0,
                                  width: textWidth,
                                  height: bounds.height)
    }
}

extension HeaderProcessView {
    
    fileprivate func setup() {
        
        textLabel = UILabel()
        textLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        textLabel?.textColor = UIColor.Text.blackMediumColor()
        textLabel?.textAlignment = .center
        
        addSubview(textLabel)
        addSubview(processView)
        
    }
}



