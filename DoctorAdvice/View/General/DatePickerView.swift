//
//  DatePickerView.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/5/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
//import PHExtensions
import CVCalendar


protocol DatePickerViewDelegate: class {
    func selectedDate(_ time: TimeInterval)
}

class DatePickerView: UIView {
    
    //==============================
    // MARK: - ENUM
    //==============================
    
    fileprivate enum Size: CGFloat {
        case rowWidth = 60, pickerView = 142, label = 38, button = 40, padding10 = 10, padding5 = 5, padding7 = 7
        case contentWidth = 310, contentHeight = 365
    }

    var calendarView: CVCalendarView!
    var menuView: CVCalendarMenuView!
    
    var contentView: UIView!
    var labelHeader: UILabel!
    
    var animationFinished = true
    var monthLabel: UILabel!
    var maxDate: Date? = nil
    var minDate: Date? = nil
    
    var date = Date() {
        didSet {
            
            setupDelegate()
        }
    }
    
    fileprivate var seperator: UIView!
    fileprivate var buttonLeft: UIButton!
    fileprivate var buttonRight: UIButton!
    fileprivate var seperatorCalendar: UIView!
    
    var backMonth: UIButton!
    var nextMonth: UIButton!
//    var backYear: UIButton!
//    var nextYear: UIButton!
    
    
    var size: CGSize = CGSize(width: 320 - Size.padding5.. * 2,
                              height: UIScreen.main.bounds.height)
    
    var mainColor: UIColor = UIColor.Navigation.mainColor() {
        didSet {
            
            labelHeader.backgroundColor = mainColor
            buttonLeft.setTitleColor(mainColor, for: .normal)
            buttonLeft.layer.borderColor = mainColor.cgColor
            buttonRight.backgroundColor = mainColor
            
            backMonth.setImage(Icon.DatePicker.BackMonth.tint(mainColor), for: .normal)
            nextMonth.setImage(Icon.DatePicker.NextMonth.tint(mainColor), for: .normal)
        }
    }
    
    weak var delegate: DatePickerViewDelegate?
    
    /**
     Độ cao của Calendar theo màn hình
     */
    fileprivate var calendarHeight: CGFloat {
        get {

                return 220

        }
    }
    
    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - Size.contentHeight..) / 2,
                                   width: Size.contentWidth..,
                                   height: Size.contentHeight..)
        
        buttonLeft.frame = CGRect(x: Size.padding10..,
                                  y: contentView.frame.height - Size.button.. - Size.padding7..,
                                  width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                  height: Size.button..)
        
        buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding7.. / 2,
                                   y: contentView.frame.height - Size.button.. - Size.padding7..,
                                   width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                   height: Size.button..)
        
        
        labelHeader.frame = CGRect(x: 0,
                                   y: 0,
                                   width: contentView.frame.width,
                                   height: Size.button..)
        

        
        
        
        monthLabel.frame = CGRect(x: 0, y: labelHeader.frame.maxY, width: contentView.frame.width, height: Size.label..)
        
        backMonth.frame = CGRect(x: 0, y: labelHeader.frame.maxY, width: Size.label.., height: Size.label..)
        
        nextMonth.frame = CGRect(x: contentView.frame.width - Size.label.., y: labelHeader.frame.maxY, width: Size.label.., height: Size.label..)
        
        
        menuView.frame = CGRect(x: 0, y: monthLabel.frame.maxY, width: contentView.frame.width, height: 20)
        
        
        seperator.frame = CGRect(x: 5, y: menuView.frame.maxY + 1, width: contentView.frame.width - 5 * 2, height: onePixel())
        
        calendarView.frame = CGRect(x: 0,
                                y: menuView.frame.maxY,
                                width: size.width,
                                height: calendarHeight)
        
        calendarView.frame.size.width = size.width
        
        
        seperatorCalendar.frame = CGRect(x: Size.padding10..,
                                         y: calendarView.frame.maxY,
                                         width: calendarView.frame.width - Size.padding10.. * 2,
                                         height: onePixel())
        
        calendarView.commitCalendarViewUpdate()
        
    }
    
    
    //-------------------------------------------
    //MARK: - PRIVATE METHOD
    //-------------------------------------------
    
    
    func reloadCalendarView() {
        
        self.contentView.subviews.forEach { if $0.tag == 1000 { $0.removeFromSuperview() } }
        calendarView = setupCalendar()
        contentView.addSubview(calendarView)
        calendarView.commitCalendarViewUpdate()
    }
    
    
    func reloadDatePickerViewWithSize(_ size: CGSize) {
        self.size = size
        reloadCalendarView()
        layoutSubviews()
    }
    
    
    func presentedLabelDateUpdated(_ date: CVDate) {
        monthLabel.text = date.globalDescription.uppercased()
    }
    
}


//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------

extension DatePickerView {
    
    
    @objc func didSelectedDate(_ sender: UIButton) {
        delegate?.selectedDate(date.timeIntervalSince1970)
    }
    

    @objc func nextMonth(_ sender: UIButton) {
       calendarView.loadNextView()
    }
    
    @objc func backMonth(_ sender: UIButton) {
        calendarView.loadPreviousView()
    }
    
    @objc func nextYear(_ sender: UIButton) {
        
        for _ in 0..<12 {
            calendarView.loadNextView()
        }
        
    }
    
    @objc func backYear(_ sender: UIButton) {
        for _ in 0..<12 {
            calendarView.loadPreviousView()
        }
    }
}

//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension DatePickerView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.7)
        contentView = setupView(.white, cornerRadius: Size.padding10..)
        seperator = setupView()
        
        
        calendarView = setupCalendar()
        menuView = setupCalendarMenu()
        monthLabel = setupLabel( CVDate(date: Date(), calendar: Calendar.current).globalDescription.uppercased() ,
                                 textColor: UIColor.black,
                                 font: UIFont(name: FontType.latoRegular.., size: FontSize.large..)!)

        

        seperatorCalendar = setupSeperator()
        
        buttonRight = setupButton(title: LocalizedString("alert_title_confirm", comment: "Xác nhận"), selector: #selector(self.didSelectedDate(_:)))
        buttonLeft = setupButton(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), selector: #selector(self.dismissView), borderColor: UIColor.Navigation.mainColor())
        
        
        
        labelHeader = setupLabel(LocalizedString("general_label_selection_date", comment: "Chọn ngày"),font: UIFont(name: FontType.latoBold.., size: FontSize.large++)!, bgColor:  mainColor)
        
//        nextYear = setupButtonArrow(Icon.DatePicker.NextYear,
//                                    tintColor: mainColor,
//                                    selector: #selector(self.nextYear(_:)))
//        
//        backYear = setupButtonArrow(Icon.DatePicker.BackYear,
//                                    tintColor: mainColor,
//                                    selector: #selector(self.backYear(_:)))
        
        nextMonth = setupButtonArrow(Icon.DatePicker.NextMonth,
                                     tintColor: mainColor,
                                     selector: #selector(self.nextMonth(_:)))
        
        backMonth = setupButtonArrow(Icon.DatePicker.BackMonth,
                                     tintColor: mainColor,
                                     selector: #selector(self.backMonth(_:)))
        
        
        addSubview(contentView)
        contentView.addSubview(calendarView)
        contentView.addSubview(menuView)
        
        contentView.addSubview(monthLabel)
        contentView.addSubview(buttonLeft)
        contentView.addSubview(buttonRight)
        contentView.addSubview(labelHeader)
        
        
        contentView.addSubview(nextMonth)
        
        contentView.addSubview(backMonth)
        
//        contentView.addSubview(nextYear)
//        
//        contentView.addSubview(backYear)
        
        //        addSubview(seperatorCalendar)
        
        contentView.addSubview(seperator)
        
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
        
        
//        calendarView.toggleViewWithDate(date)
    }
    
    func setupSeperator(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupButtonArrow(_ image: UIImage, tintColor: UIColor = UIColor.Text.grayNormalColor(), selector: Selector) -> UIButton {
        let button = UIButton()
        button.setImage(image.tint(tintColor), for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
    fileprivate func setupButton(_ bgColor: UIColor = UIColor.Navigation.mainColor(),
                             title: String? = nil,
                             image: UIImage? = nil,
                             selector: Selector, borderColor: UIColor? = nil) -> UIButton {
        
        let button = UIButton()
        if let image = image { button.setImage(image, for: .normal) }
        
        if let title = title {
            button.setTitle(title.uppercased(), for: .normal)
        }
        
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.backgroundColor = bgColor
        button.addTarget(self, action: selector, for: .touchUpInside)
        
        if let borderColor = borderColor {
            
            button.backgroundColor = UIColor.white
            button.layer.borderColor = borderColor.cgColor
            button.layer.borderWidth = onePixel()
            button.setTitleColor(borderColor, for: .normal)
        }
        
        button.clipsToBounds = true
        
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
    
    fileprivate func setupLabel(_ text: String? = nil,
                            textColor: UIColor = UIColor.white,
                            font: UIFont,
                            bgColor: UIColor = UIColor.white) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        label.backgroundColor = bgColor
        label.clipsToBounds = true
        label.text = text
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }

    
    
    func setupCalendar() -> CVCalendarView {
        
        let calendar = CVCalendarView(frame: CGRect(x: 0,
            y: 0,
            width: size.width,
            height: calendarHeight))
        calendar.tag = 1000

    
//        calendar.backgroundColor = UIColor.redColor()
//        calendar.animatorDelegate = self
//        calendar.calendarAppearanceDelegate = self
//        calendar.calendarDelegate = self
        
        return calendar
    }
    
    func setupCalendarMenu() -> CVCalendarMenuView {
        let menu = CVCalendarMenuView()
        menu.frame = CGRect(x: 0, y: 0, width: Size.contentWidth.., height: 20)
        menu.menuViewDelegate = self
        return menu
    }
    
    
    func setupDelegate() {
        
        guard calendarView != nil else { return }
        
        calendarView.animatorDelegate = self
        calendarView.calendarAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.commitCalendarViewUpdate()
    }
}


