//
//  DatePickerDialogView.swift
//  KO4Teacher
//
//  Created by Pham Huy on 5/14/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions


class DatePickerDialogView: UIView {
    
    //==============================
    // MARK: - ENUM
    //==============================
    
    fileprivate enum Size: CGFloat {
        case rowWidth = 60, pickerView = 142, label = 38, button = 40, padding10 = 10, padding5 = 5, padding7 = 7
        case contentWidth = 310, contentHeight = 280
    }
    

    var contentView: UIView!
    var labelHeader: UILabel!
    
    var datePicker: UIDatePicker!
    
    var animationFinished = true

    var date = Date()
    
    fileprivate var seperator: UIView!
    fileprivate var buttonLeft: UIButton!
    fileprivate var buttonRight: UIButton!
    fileprivate var seperatorCalendar: UIView!
    
    
    var size: CGSize = CGSize(width: 320 - Size.padding5.. * 2,
                              height: UIScreen.main.bounds.height)
    
    var mainColor: UIColor = UIColor.Navigation.mainColor()
    
    weak var delegate: DatePickerViewDelegate?
    
    /**
     Độ cao của Calendar theo màn hình
     */
    fileprivate var calendarHeight: CGFloat {
        get {
                return 220
        }
    }
    
    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - Size.contentHeight..) / 2,
                                   width: Size.contentWidth..,
                                   height: Size.contentHeight..)
        
        buttonLeft.frame = CGRect(x: Size.padding10..,
                                  y: contentView.frame.height - Size.button.. - Size.padding7..,
                                  width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                  height: Size.button..)
        
        buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding7.. / 2,
                                   y: contentView.frame.height - Size.button.. - Size.padding7..,
                                   width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                   height: Size.button..)
        
        
        labelHeader.frame = CGRect(x: 0,
                                   y: 0,
                                   width: contentView.frame.width,
                                   height: Size.button..)
        
        datePicker.frame = CGRect(x: 0,
                                  y: labelHeader.frame.maxY,
                                  width: contentView.frame.width,
                                  height: buttonRight.frame.minY - labelHeader.frame.maxY)
        
        
    }
    

    
}


//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------

extension DatePickerDialogView {
    
    
    @objc func didSelectedDate(_ sender: UIButton) {
        delegate?.selectedDate(datePicker.date.timeIntervalSince1970)
    }
}

//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension DatePickerDialogView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.7)
        contentView = setupView(UIColor.white, cornerRadius: Size.padding10..)
        seperator = setupView()
        
    
        
        seperatorCalendar = setupSeperator()
        
        buttonRight = setupButton(title: LocalizedString("alert_title_confirm", comment: "Xác nhận"), selector: #selector(self.didSelectedDate(_:)))
        
        let layerGradient = CAGradientLayer()
//        layerGradient.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
        layerGradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: Size.button..)
        buttonRight.layer.insertSublayer(layerGradient, at: 0)
        
        buttonLeft = setupButton(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"), borderColor: UIColor.Navigation.mainColor(), selector: #selector(self.hiddenView))
        
        
        
        labelHeader = setupLabel(LocalizedString("general_label_selection_date", comment: "Chọn ngày").uppercased(),
                                 font: UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!,
                                 bgColor:  mainColor)
        
        
        
        datePicker = setupDatePicker()
        
        addSubview(contentView)

        contentView.addSubview(buttonLeft)
        contentView.addSubview(buttonRight)
        contentView.addSubview(labelHeader)
        
    
        
        contentView.addSubview(seperator)
        contentView.addSubview(datePicker)
        

    }
    
    
    @objc func hiddenView() {
        
        AppData.instance.gestureRecognizerShouldReceiveTouch = false
        dismissView()
    }
    
    func setupDatePicker() -> UIDatePicker {
        let datePicker = UIDatePicker()
        datePicker.date = date
        datePicker.datePickerMode = .date
        return datePicker
    }
    
    func setupSeperator(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupButtonArrow(_ image: UIImage, tintColor: UIColor = UIColor.Text.grayNormalColor(), selector: Selector) -> UIButton {
        let button = UIButton()
        button.setImage(image.tint(tintColor), for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
    fileprivate func setupButton(_ bgColor: UIColor = UIColor.Navigation.mainColor(),
                                 title: String? = nil,
                                 image: UIImage? = nil,
                                 borderColor: UIColor? = nil,
                                 selector: Selector) -> UIButton {
        
        let button = UIButton()
        if let image = image { button.setImage(image, for: .normal) }
        
        if let title = title {
            button.setTitle(title.uppercased(), for: .normal)
        }
        
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)
        button.backgroundColor = bgColor
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = Size.button.. / 2
        
        if let color = borderColor {
            button.backgroundColor = UIColor.white
            button.setTitleColor(color, for: .normal)
            button.layer.borderColor = color.cgColor
            button.layer.borderWidth = onePixel()
        }
        
        return button
    }
    
    fileprivate func setupLabel(_ text: String? = nil,
                                textColor: UIColor = UIColor.white,
                                font: UIFont,
                                bgColor: UIColor = UIColor.white) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        label.backgroundColor = bgColor
        label.clipsToBounds = true
        label.text = text
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    
    
}


