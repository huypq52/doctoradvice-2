//
//  NilSectionView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 11/26/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit



class NilSectionView: UIView {
    
    enum Size: CGFloat {
        case padding10 = 10
    }
    
    var contentView: UIView!
    
    var padding: CGFloat = 10
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
    }
    
    
}

extension NilSectionView {
    
    
    func setupAllSubviews() {
        
        backgroundColor = .clear
        
        contentView = UIView()
        contentView.backgroundColor = .white
        addSubview(contentView)
    }
    
}












