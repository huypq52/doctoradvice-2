//
//  TimePickerView.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/3/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class TimePickerView: UIView {
    
    fileprivate enum Component: Int {
        case hour = 0, minute
    }
    
    fileprivate enum Size: CGFloat {
        case rowWidth = 60, pickerView = 162, label = 16, button = 40, padding10 = 10, padding5 = 5, padding7 = 7, contentWidth = 300, contentHeight = 240
    }
    
    
    
    fileprivate var labelDots: UILabel!
    fileprivate var labelHour: UILabel!
    fileprivate var labelMinute: UILabel!
    fileprivate var contentView: UIView!
    
    var buttonRight: UIButton!
    var buttonLeft: UIButton!
    var labelHeader: UILabel!
    var seperator: UIView!
    var pickerView: UIPickerView!
    
    var date: Date = Date() {
        didSet {
            
            let componentsDate = (Calendar.current as NSCalendar).components([ .year, .month, .day, .hour, .minute], from: date)
            
            guard rowSelected == nil else { return }
            rowSelected = (componentsDate.hour!,componentsDate.minute! / 5)
            pickerView.reloadAllComponents()
            pickerView.selectRow(componentsDate.hour!, inComponent: Component.hour.., animated: false)
            pickerView.selectRow(componentsDate.minute! / 5, inComponent: Component.minute.., animated: false)
        }
    }
    
    var mainColor: UIColor = UIColor.Navigation.mainColor() {
        didSet {
            labelDots.textColor = mainColor
            labelHeader.backgroundColor = mainColor
            buttonLeft.backgroundColor = mainColor
            buttonRight.backgroundColor = mainColor
            labelHour.textColor = mainColor
            labelMinute.textColor = mainColor
        }
    }
    
    var rowSelected: (hour: Int, minute: Int)?
    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    //-------------------------------------------
    //MARK: - PRIVATE METHOD
    //-------------------------------------------
    
    func isRowEnable(_ row: Int, component: Int) -> Bool {
        
        guard let rowSelected = rowSelected else { return false }
        
        switch component {
        case Component.hour..:
            if row == rowSelected.hour { return true }
            
        case Component.minute..:
            if row == rowSelected.minute { return true }
            
        default:
            break
        }
        return false
    }
}

//-------------------------------------------
//MARK: - PICKERVIEW DELEGATE
//-------------------------------------------

extension TimePickerView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as? UILabel
        if label == nil { label = UILabel() }
        
        switch component {
        case Component.hour..:
             label!.text = (row.toString(0).count > 1 ? "" : "0") + "\(row)"
        default:
             label!.text = ((row * 5).toString(0).count > 1 ? "" : "0") + "\(row * 5)"
        }
       
        label!.textAlignment = .center
        label!.font = UIFont(name: FontType.latoSemibold.., size: FontSize.large++)
        label!.textColor = isRowEnable(row, component: component) ? mainColor : UIColor.Text.blackMediumColor()
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return Size.rowWidth..
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Size.button..
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if rowSelected == nil { rowSelected = (0,0) }
        
        switch component {
        case Component.hour..:
            date = date + (row - rowSelected!.hour).hour
            rowSelected!.hour = row
            
        case Component.minute..:
            date = date + (row - rowSelected!.minute).minute * 5
            rowSelected!.minute = row
            
        default:
            break
        }
        
        pickerView.reloadComponent(component)
    }
}

//-------------------------------------------
//MARK: - PICKERVIEW DATASOURCE
//-------------------------------------------

extension TimePickerView: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return Component.minute++
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case Component.hour..:
            return 24
        case Component.minute..:
            return 60 / 5
        default:
            return 0
        }
    }
}


//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension TimePickerView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.7)
        
        contentView = setupView(UIColor.white, cornerRadius: Size.padding10..)
        seperator = setupView()
        pickerView = setupPickerView()
        
        buttonRight = setupButton(title: LocalizedString("notification_default_agree", comment: "Đồng ý"))
        buttonLeft = setupButton(title: LocalizedString("notification_default_cancel", comment: "Bỏ qua"))
        buttonLeft.addTarget(self, action: #selector(self.dismissView), for: .touchUpInside)
        
        

        labelHeader = setupLabel(font: UIFont(name: FontType.latoBold.., size: FontSize.large++)!, bgColor:  mainColor)
        
        labelDots = setupLabel(":",
                               textColor: mainColor,
                               font: UIFont(name: FontType.latoSemibold.., size: FontSize.large++)!,
                               bgColor: UIColor(rgba: "F6F6F6"))
        
        
        labelHour = setupLabel(LocalizedString("time_picker_default_hour", comment: "Giờ"),
                               textColor: mainColor,
                               font: UIFont(name: FontType.latoRegular.., size: FontSize.normal++)!,
                               bgColor: .clear)
        
        
        labelMinute = setupLabel(LocalizedString("time_picker_default_minute", comment: "Phút"),
                               textColor: mainColor,
                               font: UIFont(name: FontType.latoRegular.., size: FontSize.normal++)!,
                               bgColor: .clear)
    
        
        addSubview(contentView)
        
        contentView.addSubview(labelHeader)
        contentView.addSubview(buttonRight)
        contentView.addSubview(buttonLeft)
        contentView.addSubview(labelDots)
        contentView.addSubview(labelHour)
        contentView.addSubview(labelMinute)
        contentView.addSubview(pickerView)
        contentView.addSubview(seperator)
        
    }
    
    func setupPickerView() -> UIPickerView {
        let picker = UIPickerView(frame: CGRect(x: 0,
            y: 0,
            width: Size.contentWidth..,
            height: Size.contentHeight.. - Size.button.. * 2 - Size.padding10..))
        
        picker.showsSelectionIndicator = true
        picker.backgroundColor = .clear
        picker.delegate = self
        picker.dataSource = self
        return picker
    }
    
    fileprivate func setupButton(_ bgColor: UIColor = UIColor.Navigation.mainColor(),
                             title: String? = nil,
                             image: UIImage? = nil) -> UIButton {
        
        let button = UIButton()
        if let image = image { button.setImage(image, for: .normal) }
        
        if let title = title {
            button.setTitle(title.uppercased(), for: .normal)
        }
        
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.backgroundColor = bgColor
        return button
    }
    
    fileprivate func setupLabel(_ text: String? = nil,
                            textColor: UIColor = UIColor.white,
                            font: UIFont,
                            bgColor: UIColor = UIColor.white) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        label.backgroundColor = bgColor
        label.clipsToBounds = true
        label.text = text
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - Size.contentHeight.. ) / 2,
                                   width: Size.contentWidth..,
                                   height: Size.contentHeight..)
        
        buttonLeft.frame = CGRect(x: Size.padding10..,
                                  y: contentView.frame.height - Size.button.. - Size.padding7..,
                                  width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                  height: Size.button..)
        
        buttonRight.frame = CGRect(x: contentView.frame.width / 2 + Size.padding7.. / 2,
                                   y: contentView.frame.height - Size.button.. - Size.padding7..,
                                   width: contentView.frame.width / 2 - Size.padding10..  - Size.padding7.. / 2,
                                   height: Size.button..)
        
        
        labelHeader.frame = CGRect(x: 0,
                                   y: 0,
                                   width: contentView.frame.width,
                                   height: Size.button..)
        
        seperator.frame = CGRect(x: 0, y: labelHeader.frame.maxY, width: contentView.frame.width, height: onePixel())
        
        
        pickerView.frame = CGRect(x: 0,
                                  y: labelHeader.frame.maxY,
                                  width: contentView.frame.width,
                                  height: buttonLeft.frame.minY - labelHeader.frame.maxY)
        
        labelDots.frame = CGRect(x: 0,
                                 y: pickerView.frame.minY + (pickerView.frame.height - Size.button..) / 2 - 1,
                                 width: contentView.frame.width,
                                 height: Size.button.. + 2)
        
        labelHour.frame = CGRect(x: Size.padding10..,
                                 y: pickerView.frame.minY + (pickerView.frame.height - Size.button..) / 2,
                                 width: contentView.frame.width / 3,
                                 height: Size.button..)
        
        labelMinute.frame = CGRect(x: contentView.frame.width * 2 / 3 - Size.padding10..,
                                   y: pickerView.frame.minY + (pickerView.frame.height - Size.button..) / 2,
                                   width: contentView.frame.width / 3,
                                   height: Size.button..)
    }
}









