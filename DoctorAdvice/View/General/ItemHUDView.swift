//
//  ItemHUDView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 8/1/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import Foundation
import NVActivityIndicatorView
//import PHExtensions


class ItemHUDView: UIView {
    
    enum Size: CGFloat {
        case process = 58, padding15 = 15, image = 46, content = 68
    }
    
    var imageView: UIImageView!
    var processView: NVActivityIndicatorView!
    var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.content..) / 2,
                                   y: (bounds.height - Size.content..) / 2,
                                   width: Size.content..,
                                   height: Size.content..)
        
        contentView.layer.cornerRadius = contentView.frame.height / 2
        
        
        
        imageView.frame = CGRect(x: (contentView.frame.width - Size.image..) / 2,
                                 y: (contentView.frame.width - Size.image..) / 2,
                                 width: Size.image..,
                                 height: Size.image..)
        
        
        processView.frame = CGRect(x: (contentView.frame.width - Size.process..) / 2,
                                 y: (contentView.frame.width - Size.process..) / 2,
                                 width: Size.process..,
                                 height: Size.process..)
        
    }
}

extension ItemHUDView {
    
    func setup() {
        

        contentView = setupView(.white)
        imageView = setupImageView()
        processView = setupProcessView()
        
        addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(processView)
        
        
        
        layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.5
        layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        
    }
    
    
    func setupProcessView() -> NVActivityIndicatorView {
        let view = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: Size.process.., height: Size.process..),
                                           type: .circleStrokeSpin,
                                           color: UIColor.Navigation.mainColor(),
                                           padding: 0)
        view.backgroundColor = .clear
        view.isHidden = true
        view.startAnimating()
        return view
    }
    
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        
        

        
        return view
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: Icon.General.LogoKidsOnline)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    

}



