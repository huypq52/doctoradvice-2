//
//  InteractionHeaderView.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 3/14/19.
//  Copyright © 2019 OMT. All rights reserved.
//



import UIKit

class InteractionHeaderView: UICollectionReusableView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, padding5 = 5, image =  22, button = 30
    }

 
    var bgView: UIView!
    var label: UILabel!
    var buttonSub: UIButton!
    
    var padding: CGFloat = 5
    var radius: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        bgView.frame = CGRect(x: padding,
                              y: Size.padding10.. / 2,
                              width: bounds.width - padding * 2,
                              height: bounds.height - Size.padding10.. / 2 + radius)
        
        
        if buttonSub.isHidden {
            
            label.frame = CGRect(x: Size.padding10..,
                                 y: 0,
                                 width: bgView.frame.width - Size.padding10.. * 2,
                                 height: bgView.frame.height - radius)
            
            buttonSub.frame = .zero
            
        } else {
            
            
            var width = Utility.widthForView(buttonSub.titleLabel?.text ?? "",
                                             font: UIFont(name: FontType.latoMedium.., size: FontSize.small++)!,
                                             height: 20) + 20 + 21
            
            width = max(120, width)
            
            buttonSub.frame = CGRect(x: bgView.frame.width - width - Size.padding10.. ,
                                     y:  (bgView.frame.height - Size.button..) / 2,
                                     width: width,
                                     height: Size.button..)
            
            
            label.frame = CGRect(x: Size.padding10..,
                                 y: 0 ,
                                 width: buttonSub.frame.minX - Size.padding10.. * 2,
                                 height: bgView.frame.height)
            
            
            
            buttonSub.layer.cornerRadius = buttonSub.frame.height / 2
        }
        

        

    }
    
}


extension InteractionHeaderView {
    
    func setup() {
        
        clipsToBounds = true
      
        bgView = setupView(.clear)
        bgView.layer.cornerRadius = radius
        
        label = setupLabel(textColor: UIColor.Text.blackMediumColor(), font: UIFont(name: FontType.latoBold.., size: FontSize.large..)!)
        
        
        buttonSub = setupButton(image: Icon.General.Add, titleColor: UIColor.Navigation.mainColor())
        
        addSubview(bgView)
        bgView.addSubview(label)
        bgView.addSubview(buttonSub)
        
        
        buttonSub.isHidden = true

        
    }
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    
  
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoMedium.., size: FontSize.small++)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = UIColor.Navigation.mainColor().alpha(0.1)
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: -7)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: -14)
        
        
        return button
    }
}

