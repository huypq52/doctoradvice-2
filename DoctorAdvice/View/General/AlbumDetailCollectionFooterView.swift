//
//  AlbumDetailCollectionFooterView.swift
//  KidOnline
//
//  Created by Pham Huy on 12/9/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit

class AlbumDetailCollectionFooterView: UICollectionReusableView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 15, button = 44, buttonSmall = 40
    }
    
    var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        label.frame = bounds
        
    }
}


extension AlbumDetailCollectionFooterView {
    func setupAllSubviews() {
        label = setupLabel()
        addSubview(label)
    }
    
    func setupLabel() -> UILabel {
        let button = UILabel()
        
        return button
    }
    
}
