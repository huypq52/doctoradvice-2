//
//  HealthHeaderSectionView.swift
//  KidOnline
//
//  Created by Pham Huy on 7/11/19.
//  Copyright © 2019 KidOnline. All rights reserved.
//


import UIKit

class HealthHeaderSectionView: UICollectionReusableView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, padding5 = 5, image =  29, button = 30
    }
    
    
    var bgView: UIView!
    var label: UILabel!
    var buttonSub: UIButton!
    var icon: UIImageView!
    
    var padding: CGFloat = 5
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        bgView.frame = CGRect(x: padding,
                              y: Size.padding10.. / 2,
                              width: bounds.width - padding * 2,
                              height: bounds.height - Size.padding10.. / 2)
        
        icon.frame = CGRect(x: icon.isHidden ? 0 : Size.padding10..,
                            y: 0,
                            width: icon.isHidden ? 0 : Size.image..,
                            height: bgView.frame.height)
        
        if buttonSub.isHidden {
            
            label.frame = CGRect(x: icon.frame.maxX + Size.padding10..,
                                 y: 0,
                                 width: bgView.frame.width - Size.padding10.. * 2 - icon.frame.maxX,
                                 height: bgView.frame.height)
            
            buttonSub.frame = .zero
            
        } else {
            
            
            var width = Utility.widthForView(buttonSub.titleLabel?.text ?? "",
                                             font: UIFont(name: FontType.latoMedium.., size: FontSize.small--)!,
                                             height: 20) + 20 + 21
            
            width = max(40, width)
            
            buttonSub.frame = CGRect(x: bgView.frame.width - width - Size.padding10.. ,
                                     y:  (bgView.frame.height - Size.button..) / 2,
                                     width: width,
                                     height: Size.button..)
            
            
            label.frame = CGRect(x: icon.frame.maxX + Size.padding10..,
                                 y: 0 ,
                                 width: buttonSub.frame.minX - Size.padding10.. * 2 - icon.frame.maxX,
                                 height: bgView.frame.height)
            
            
            
            buttonSub.layer.cornerRadius = buttonSub.frame.height / 2
        }
        
        
        
        
    }
    
}


extension HealthHeaderSectionView {
    
    func setup() {
        
        clipsToBounds = true
        
        bgView = setupView(.clear)
        
        label = setupLabel(textColor: UIColor.Text.blackMediumColor(), font: UIFont(name: FontType.latoBold.., size: FontSize.large..)!)
        
        
        buttonSub = setupButton(image: Icon.General.Add, titleColor: UIColor.Navigation.mainColor())
        icon = setupImageView()
        
        
        addSubview(bgView)
        bgView.addSubview(label)
        bgView.addSubview(buttonSub)
        bgView.addSubview(icon)
        
        
        buttonSub.isHidden = true
        icon.isHidden = true
        
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = .clear
        button.contentHorizontalAlignment = .right
        
        
        return button
    }
}


