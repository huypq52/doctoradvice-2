//
//  IconLabel.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 10/16/17.
//  Copyright © 2017 OMT. All rights reserved.
//


import UIKit

class IconLabel: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, label = 30
    }
    
    var contentView: UIView!
    var labelTitle: UILabel!
    var iconView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        iconView.frame = CGRect(x: 0, y: 0, width: 16, height: contentView.frame.height)
        labelTitle.frame = CGRect(x: iconView.frame.maxX + 5,
                                  y: 0, width: contentView.frame.width - iconView.frame.maxX - 5 * 2,
                                  height: contentView.frame.height)
        
        
    }
}

extension IconLabel {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        contentView = setupSeperator(UIColor.white)
                
        
        labelTitle = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor.Text.blackMediumColor(),
                                bgColor: UIColor.clear)
        
        iconView = setupImageView()

        contentView.addSubview(labelTitle)
        contentView.addSubview(iconView)
        
        
        
        addSubview(contentView)
        

        
    }
    
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor = UIColor.Text.blackMediumColor(),
                                alignment: NSTextAlignment = .left,
                                bgColor: UIColor = UIColor.clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    

    func setupImageView() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    
    fileprivate func setupSeperator(_ color: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
}

