//
//  HomePostItemView.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 1/8/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//




import UIKit
import SwiftyUserDefaults


class HomePostItemView: UIView {
    
    //==============================
    // MARK: - ENUM
    //==============================
    
    fileprivate enum Size: CGFloat {
        case icon = 16, padding10 = 10, padding15 = 17, padding7 = 7, radius = 22
    }
    
    var post: Post? = nil {
        didSet {
            
            guard let group = post, labelName != nil else { return }
            
            labelName.text = group.title
            icon.downloadedFrom(link: group.cover, placeHolder: nil)
     
        }
    }

   

    var contentView: UIView!
    var labelName: UILabel!
    var icon: UIImageView!
    var gradientLayer: CAGradientLayer!

    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        contentView.frame = CGRect(x: 0,
                                   y: bounds.height - bounds.height,
                                   width: bounds.width,
                                   height: bounds.height)
        
       
        
        icon.frame = contentView.bounds
        
         gradientLayer.frame = CGRect(x: 0,
                                      y: icon.frame.height / 2,
                                      width: icon.frame.width,
                                      height: max(UIScreen.main.bounds.height * 9 / 16, icon.frame.height) )
        
        let heightName: CGFloat = Utility.heightForView(labelName.text ?? "",
                                                        font: labelName.font,
                                                        width: contentView.frame.width - Size.padding10.. * 2)
        
        labelName.frame = CGRect(x: Size.padding10..,
                                 y: contentView.frame.height - heightName - Size.padding10.. * 3 / 2,
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: heightName)
            
        
    }
    
}

//-------------------------------------------
//MARK: - SELECTOR
//-------------------------------------------

extension HomePostItemView {
    
}


//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension HomePostItemView {
    func setupAllSubviews() {
        
        backgroundColor = .clear
        isUserInteractionEnabled = true

        contentView = setupView(.white, cornerRadius: 0)
        
        labelName = setupLabel(post?.title ?? "", bgColor: .clear, alignment: .left)
        icon = setupImageView()
        
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
        gradientLayer.locations = [0.0, 1.0]
        
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.alpha(1.0).cgColor]
        icon.layer.insertSublayer(self.gradientLayer, at: 0)
   

        
        addSubview(contentView)
        contentView.addSubview(icon)
        contentView.addSubview(labelName)
        
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = .white,
                    font: UIFont = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.isUserInteractionEnabled = true
        return view
    }
    

    
}


