//
//  HomeHeaderSectionView.swift
//  KidOnline
//
//  Created by Pham Huy on 5/12/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class HomeHeaderSectionView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 33
    }
    
    var contentView: UIView!
    var bgView: UIView!
    var label: UILabel!
    var icon: UIImageView!

    var paddingTop: CGFloat = 0 {
        didSet {
            layoutSubviews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        contentView.frame = CGRect(x: Size.padding10..,
                                   y: 5,
                                   width: bounds.width - Size.padding10.. * 2,
                                   height: bounds.height + 5)
        

        
        let width: CGFloat = Utility.widthForView(label.text ?? "", font: label.font, height: 20) + 20

        bgView.frame = CGRect(x: (contentView.frame.width - width) / 2,
                              y: 5 ,
                              width: width,
                              height: Size.button..)
        
        icon.frame = CGRect(x: contentView.frame.width - 5 - 20 ,
                            y: bgView.frame.midY - 10,
                            width: 20,
                            height: 20)
        
        label.frame = CGRect(x: Size.padding10.. / 2,
                             y: 0,
                             width: bgView.frame.width - Size.padding10.. ,
                             height: bgView.frame.height)
        
      
    }
    
    
    fileprivate func setup() {
        
        contentView = setupBackgroundView(.white)
        bgView = setupBackgroundView()
        label = setupLabel()
        icon = setupImageView()

        
        addSubview(contentView)
        
        contentView.addSubview(bgView)
        bgView.addSubview(label)
        contentView.addSubview(icon)

        clipsToBounds = true
    }
    
    fileprivate func setupLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont(name: FontType.latoBold.., size: FontSize.small++)
        label.backgroundColor = UIColor.clear
        return label
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.isHidden = false
        button.contentHorizontalAlignment = .center
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
        
        button.backgroundColor = UIColor.clear
        return button
    }
    
    fileprivate func setupBackgroundView(_ bgColor: UIColor = UIColor.Navigation.mainColor(), cornerRadius: CGFloat = 8) -> UIView {
        let view = UIView()
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
        view.backgroundColor = bgColor
        return view
    }
    
    func setupImageView() -> UIImageView {
        let imageView: UIImageView = UIImageView(image: Icon.General.ArrowRight.tint(UIColor.Navigation.mainColor()))
        imageView.contentMode = .center
        return imageView
    }
}

