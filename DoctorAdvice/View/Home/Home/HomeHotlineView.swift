//
//  HomeHotlineView.swift
//  KidOnline
//
//  Created by Pham Huy on 9/2/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit
//import PHExtensions
import CVCalendar
import SwiftyUserDefaults


protocol HomeHotlineViewDelegate: class {
    func openMail(_ mail: String)
    func callPhone(_ phone: String)
}


class HomeHotlineView: UIView {
    
    //==============================
    // MARK: - ENUM
    //==============================
    
    fileprivate enum Size: CGFloat {
        case rowWidth = 60, label = 38, button = 41, padding10 = 10, padding5 = 5, padding7 = 7, headerSection = 40
        case contentWidth = 300, contentHeight = 365
    }
    

    weak var delegate: HomeHotlineViewDelegate?
    
    var bgView: UIView!
    var contentView: UIView!
    var labelHeader: UILabel!
    var buttonClose: UIButton!
    var table: UITableView!

    var size: CGSize = CGSize(width: 320 - Size.padding5.. * 2,
                              height: UIScreen.main.bounds.height)
    


    var dataArray: [GuestInfo] = []
    
    
    //-------------------------------------------
    //MARK: - INIT
    //-------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    //-------------------------------------------
    //MARK: - LAYOUT SUBVIEW
    //-------------------------------------------
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.frame = bounds
        
        var contentHeight: CGFloat = Size.button.. + 5 + 56
        
//        if let school = (AppData.instance.schools.filter { $0.id == Defaults[.SchoolID]}).first, school.phone.count > 0 {
//            contentHeight += 50 + Size.headerSection.. + 5
//        }
        
        contentHeight += CGFloat(dataArray.count) * 65.0

        contentHeight = min(contentHeight, bounds.height * 3 / 4)
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - contentHeight) / 2,
                                   width: Size.contentWidth..,
                                   height: contentHeight)
        
        
        labelHeader.frame = CGRect(x: 0,
                                   y: 0,
                                   width: contentView.frame.width,
                                   height: Size.button..)
        
        
        table.frame = CGRect(x: 0, y: labelHeader.frame.maxY, width: contentView.frame.width, height: contentView.frame.height - labelHeader.frame.maxY)

        
    }

}




//-------------------------------------------
//MARK: - SETUP VIEW
//-------------------------------------------

extension HomeHotlineView {
    func setupAllSubviews() {
        
        
        

            
//        //TODO: Dịch
//        
//        #if VINSCHOOL
//        
//        let guestInfo1 = GuestInfo(data: [:])
//        guestInfo1.name = LocalizedString("login_hotline_vinschool_one_name", comment: "Văn phòng Vinschool (Khu vực phía bắc)")
//        guestInfo1.phone = "02439753333"
//        guestInfo1.email = "info@vinschool.edu.vn"
//        guestInfo1.address = LocalizedString("login_hotline_vinschool_one_phone", comment: "0243 975 3333 (nhánh 0)")
//        dataArray.append(guestInfo1)
//        
//        let guestInfo2 = GuestInfo(data: [:])
//        guestInfo2.name = LocalizedString("login_hotline_vinschool_two_name", comment: "Văn phòng Vinschool (Khu vực phía nam)")
//        guestInfo2.phone = "02836226888"
//        guestInfo2.email = "info@vinschool.edu.vn"
//        guestInfo2.address = LocalizedString("login_hotline_vinschool_two_phone", comment: "0283 622 6888 (nhánh 0)")
//        dataArray.append(guestInfo2)
//        
//        #else
//        
//        if let school = (AppData.instance.schools.filter { $0.id == Defaults[.SchoolID]}).first, school.phone.count > 0 {
//            
//            let guestInfo = GuestInfo(data: [:])
//            guestInfo.name = school.fullName
//            guestInfo.phone = school.phone
//            guestInfo.email = school.email
//            guestInfo.address = school.phone.count > 0 ? school.phone : school.email
//            dataArray.append(guestInfo)
//            
//        }
//        
//        #endif
  
        
        let guestInfo = GuestInfo(data: [:])
        guestInfo.name = LocalizedString("login_hotline_school_online_name", comment: "Hỗ trợ kỹ thuật SchoolOnline")
        guestInfo.phone = "19000362"
        guestInfo.email = "schoolonline@omt.vn"
        guestInfo.address = LocalizedString("login_hotline_school_online_phone", comment: "1900 0362 (nhánh 2)")
        dataArray.append(guestInfo)
        
        

        
        
        backgroundColor = UIColor.black.alpha(0.7)
        contentView = setupView(.white, cornerRadius: Size.padding10..)
       
        
        labelHeader = setupLabel(LocalizedString("function_label_support", comment: "Hỗ trợ"),
                                 font: UIFont(name: FontType.latoBold.., size: FontSize.large++)!,
                                 bgColor:  UIColor.Navigation.mainColor())
        

        table = setupTableView()
        
        bgView = setupView(.clear, cornerRadius: 0)
        
        
        
        addSubview(bgView)

        
        
        addSubview(contentView)

        contentView.addSubview(labelHeader)
        contentView.addSubview(table)


        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.cancelsTouchesInView = false
        bgView.addGestureRecognizer(tap)
        

    }
    
    func setupSeperator(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupButtonArrow(_ image: UIImage, tintColor: UIColor = UIColor.Text.grayNormalColor(), selector: Selector) -> UIButton {
        let button = UIButton()
        button.setImage(image.tint(tintColor), for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
   
    fileprivate func setupLabel(_ text: String? = nil,
                                textColor: UIColor = UIColor.white,
                                font: UIFont,
                                bgColor: UIColor = UIColor.white) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        label.backgroundColor = bgColor
        label.clipsToBounds = true
        label.text = text
        return label
    }
    
    fileprivate func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor(), cornerRadius: CGFloat = 0) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    
    fileprivate func setupTableView() -> UITableView {
        let table = UITableView(frame: CGRect.zero, style: .grouped)
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.register(HotlineTableViewCell.self, forCellReuseIdentifier: "HotlineTableViewCell")
        table.backgroundColor = UIColor.white
        table.keyboardDismissMode = .onDrag
        return table
    }
    
    
    fileprivate func setupHeaderViewSection(_ title: String, paddingTop: CGFloat = 0) -> UIView {
        let label = UILabel()
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        label.textAlignment = .left
        label.textColor = UIColor.Text.grayMediumColor()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = title
        
        label.frame = CGRect(x: 10,
                             y: paddingTop,
                             width: Size.contentWidth.. - 10 * 2,
                             height: CGFloat.leastNonzeroMagnitude)
        label.sizeToFit()
        

        let headerView = UIView()
        headerView.backgroundColor = .clear
        headerView.addSubview(label)
        headerView.clipsToBounds = true
        return headerView
    }


}

extension HomeHotlineView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "HotlineTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotlineTableViewCell
        configuaCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configuaCell(_ cell: HotlineTableViewCell, indexPath: IndexPath) {
        
        guard indexPath.row < dataArray.count else { return }
        
        let guestInfo = dataArray[indexPath.row]
        
        cell.selectionStyle = .none
        cell.seperatorRightPadding = 10
        cell.seperatorStyle = .padding(10)
        cell.buttonCall.addTarget(self, action: #selector(self.call(_:)), for: .touchUpInside)
        cell.buttonCall.tag = indexPath.row
        
        cell.buttonEmail.addTarget(self, action: #selector(self.email(_:)), for: .touchUpInside)
        cell.buttonEmail.tag = indexPath.row
        
        cell.labelName.text = guestInfo.name
        cell.labelSub.text = guestInfo.address
        cell.buttonCall.isHidden = guestInfo.phone.count == 0
        cell.buttonEmail.isHidden = guestInfo.email.count == 0

        
    }
}


extension HomeHotlineView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard indexPath.row < dataArray.count else { return }
        
        let guestInfo = dataArray[indexPath.row]
        
        if guestInfo.phone.count > 0 {
            
            delegate?.callPhone(guestInfo.phone)
            
        } else if guestInfo.email.count > 0 {
            delegate?.openMail(guestInfo.email)
        }
    }
    
    
    @objc func call(_ sender: UIButton) {
        
        guard sender.tag < dataArray.count else { return }
        
        let guestInfo = dataArray[sender.tag]
        
        delegate?.callPhone(guestInfo.phone)
    }
    
    @objc func email(_ sender: UIButton) {
        
        guard sender.tag < dataArray.count else { return }
        
        let guestInfo = dataArray[sender.tag]
        
        delegate?.openMail(guestInfo.email)
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
        
        return CGFloat.leastNonzeroMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 20 + Utility.heightForView(LocalizedString("login_hotline_label_sub", comment: "Liên hệ trong giờ hành chính 8h-17h, từ thứ Hai đến thứ Sáu."),
                                          font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                          width: Size.contentWidth.. - 20)
        
//        switch section {
//        case 0:
//            return 5
//        default:
//            return onePixel()
//        }

    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return nil

    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        return setupHeaderViewSection(LocalizedString("login_hotline_label_sub", comment: "Liên hệ trong giờ hành chính 8h-17h, từ thứ Hai đến thứ Sáu."), paddingTop: 10)
        
    }
}
