//
//  HomeHeaderView.swift
//  DoctorAdvice
//
//  Created by Pham Huy on 1/8/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit

struct Post {
    
    var title: String
    var cover: String
    var link: String
    
    init(data: [String : Any]) {
        self.title = data["title"] as? String ?? ""
        self.cover = data["cover"] as? String ?? ""
        self.link = data["link"] as? String ?? ""
        
    }
}


class HomeHeaderView: UIImageView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 40
    }
    
    var scrollView: UIScrollView!
    var arrayView: [HomePostItemView] = []
    var posts: [Post] = []
    var control: UIPageControl!
    var currentOffset: CGPoint = CGPoint(x: 0, y: 0)
    var padding: CGFloat = 0
    
    var timer: Timer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        control.frame = CGRect(x: 0,
                               y: bounds.height - 30,
                               width: bounds.width,
                               height: 30)
        
        scrollView.frame = CGRect(x: padding,
                                  y: 0,
                                  width: bounds.width - padding * 2,
                                  height: control.frame.minY)
        
     
       
        arrayView.enumerated().forEach { (i, view) in
            
            view.frame = CGRect(x: CGFloat(i) * scrollView.frame.width,
                                y: 0,
                                width: scrollView.frame.width,
                                height: scrollView.frame.height)
            
        }
        
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(arrayView.count),
                                        height: scrollView.frame.height)
    }
}


extension HomeHeaderView {
    
    func setup() {
        
        
        image = Icon.General.CoverDefault
        clipsToBounds = true
        backgroundColor = .white
        
        scrollView = setupScrollView()
        control = setupControl()
        
        addSubview(scrollView)
        addSubview(control)
        
    }
    
    func updateView() {
        
        arrayView.forEach { $0.removeAllSubviews() }
        arrayView = []
        
        posts.enumerated().forEach { (i, post) in
            
            let view = setupItemView(post, tag: i)
            
            arrayView.append(view)
            scrollView.addSubview(view)
            
        }
        
        control.numberOfPages = arrayView.count
        control.currentPage = 0
        
        layoutSubviews()
        
        timer = Timer.scheduledTimer(timeInterval: 5.seconds, target: self, selector: #selector(self.autoScrollView), userInfo: nil, repeats: true)
    }
    
    
    @objc func autoScrollView() {
        
        if control.currentPage < control.numberOfPages - 1 {
            control.currentPage += 1
        } else {
            control.currentPage = 0
        }
        

        scrollViewMoveToPage(control.currentPage)
        
    }
    

    func setupScrollView() -> UIScrollView {
        let view = UIScrollView()
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.isPagingEnabled = true
        view.delegate = self
        return view
    }
    
    func setupItemView(_ post: Post, tag: Int) -> HomePostItemView {
        let view = HomePostItemView()
        view.tag = tag
        view.post = post
        return view
    }
    
    func setupControl() -> UIPageControl {
        let control = UIPageControl()
        control.hidesForSinglePage = true
        control.addTarget(self, action: #selector(self.tapOnControl(_:)), for: .touchUpInside)
        control.pageIndicatorTintColor = UIColor.Misc.seperatorColor()
        control.currentPageIndicatorTintColor = UIColor.Navigation.mainColor()
        return control
    }
    
}

extension HomeHeaderView {
    
    @objc func tapOnControl(_ sender: UIPageControl) {
            
            
    //        print("sender.currentPage: \(sender.currentPage) - \(sender.numberOfPages)")
    //
    //        if sender.currentPage < sender.numberOfPages  {
    //            sender.currentPage  += 1
    //        } else {
    //            sender.currentPage  = 0
    //        }
            
            scrollViewMoveToPage(sender.currentPage)
            
        }
    

}

//-----------------------------
// - MARK: SCROLL VIEW DELEGATE
//-----------------------------
extension HomeHeaderView: UIScrollViewDelegate {
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        currentOffset = scrollView.contentOffset
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let page = Int(floor(targetContentOffset.pointee.x / scrollView.frame.width))
        if floor(targetContentOffset.pointee.x) != currentOffset.x {
//            segment.selectedSegmentIndex = page
//            segment.sendActions(for: .valueChanged)
            
            control.currentPage = page
//            control.sendActions(for: .touchUpInside)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
//            indicator.frame.origin.x = round(scrollView.contentOffset.x / CGFloat(segment.numberOfSegments))
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let page = Int(floor(scrollView.contentOffset.x / self.scrollView.frame.width));
        if (floor(scrollView.contentOffset.x) != currentOffset.x) {
            
            scrollViewMoveToPage(page)
            
//            switch page {
//            case TablePosition.article..:
//                scrollViewWillMoveToArticleTable()
//
//
//
//            case TablePosition.promo..:
//                scrollViewwillMoveToGoogleTable()
//
//            default:
//                break
//            }
        }
    }
    
    /**
     Chuyển sang table tương ứng với selected segment
     
     - parameter page:
     */
    fileprivate func scrollViewMoveToPage(_ page: Int) {
        var rect = scrollView.bounds
        rect.origin.x = rect.width * CGFloat(page)
        rect.origin.y = 0
        scrollView.scrollRectToVisible(rect, animated: true)
        
        if timer != nil { timer.invalidate() }
        timer = Timer.scheduledTimer(timeInterval: 5.seconds, target: self, selector: #selector(self.autoScrollView), userInfo: nil, repeats: true)
    }
}

