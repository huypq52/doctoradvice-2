//
//  FooterViewSection.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/27/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class FooterViewSection: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10
    }
    
    var bgView: UIView!
    var button: UIButton!
    var padding: CGFloat = 10 {
        didSet {
            layoutSubviews()
        }
    }
    var paddingButton: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        bgView.frame = CGRect(x: padding,
                              y: -5,
                              width: bounds.width - padding * 2,
                              height: bounds.height)
        
        var width = Utility.widthForView(button.titleLabel!.text!,
                                         font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                         height: bounds.height - 5) + Size.padding10.. * 2
        
        if let imageView =  button.imageView {
            if let _ = imageView.image { width += 30 }
        }
        
        
        width = max(width, 60)
        
        button.frame = CGRect(x: bounds.width - Size.padding10.. * 2 - width,
                              y: paddingButton,
                              width: width ,
                              height: bounds.height - 5 - paddingButton)
        button.layer.cornerRadius = 5
    }
    
    
    fileprivate func setup() {
        bgView = setupBackgroundView()
        button = setupButton()
        
        addSubview(bgView)
        addSubview(button)
        
        clipsToBounds = true
        
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.setTitle(LocalizedString("general_button_show_more", comment: "Xem thêm"), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 130 / 255.0, green: 194 / 255.0, blue: 226 / 255.0, alpha: 1.0)
        button.isHidden = true
        return button
    }
    
    fileprivate func setupBackgroundView() -> UIView {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.backgroundColor = UIColor.white
        view.clipsToBounds = true
        return view
    }
    
}
