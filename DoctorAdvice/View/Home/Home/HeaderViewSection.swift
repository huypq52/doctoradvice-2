//
//  HeaderViewSection.swift
//  KidsOnline
//
//  Created by Pham Huy on 7/27/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class HeaderViewSection: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10, button = 40
    }
    
    var bgView: UIView!
    var label: UILabel!
    var button: UIButton!
    var buttonSub: UIButton!
    
    var paddingTop: CGFloat = 0 {
        didSet {
            layoutSubviews()
        }
    }
    
    var padding: CGFloat = 10
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.frame = CGRect(x: Size.padding10..,
                              y: padding,
                              width: bounds.width - Size.padding10.. * 2,
                              height: bounds.height)
        
        label.frame = CGRect(x: Size.padding10.. * 2,
                             y: padding + paddingTop,
                             width: bounds.width - Size.padding10.. * 4,
                             height: bounds.height - padding - paddingTop)
        
        button.frame = CGRect(x: bounds.width / 2 ,
                              y: padding + paddingTop,
                              width: bounds.width / 2  - Size.padding10.. * 2,
                              height: bounds.height - padding - paddingTop)
        
        buttonSub.frame = CGRect(x: bounds.width / 2 ,
                                 y: padding + paddingTop,
                                 width: bounds.width / 2  - Size.padding10.. * 2,
                                 height: bounds.height - padding - paddingTop)
        
        guard !button.isHidden else { return }
        
        
        
        if let title =  button.titleLabel {
            if let string = title.text, string.count > 0 {
                let width = Utility.widthForView(string, font: title.font, height: 30)
                label.frame.size.width = ( bounds.width - Size.padding10.. * 4 ) - width
            }
        } else {
            label.frame.size.width = ( bounds.width - Size.padding10.. * 4 ) - button.frame.height
        }
        

        
        guard !buttonSub.isHidden else { return }
        
        
        
        button.frame = CGRect(x: bounds.width - Size.padding10..  - Size.button.. ,
                                 y: padding + paddingTop,
                                 width: Size.button..,
                                 height: bounds.height - padding - paddingTop)
        button.contentHorizontalAlignment = .center
        buttonSub.contentHorizontalAlignment = .center
        
        buttonSub.frame = CGRect(x: bounds.width - Size.padding10..  - Size.button.. * 2,
                                 y: padding + paddingTop,
                                 width: Size.button..,
                                 height: bounds.height - padding - paddingTop)
        
        label.frame.size.width = ( bounds.width - Size.padding10.. * 2 ) - Size.button.. * 2
    }
    
    
    fileprivate func setup() {
        bgView = setupBackgroundView()
        label = setupLabel()
        button = setupButton()
        buttonSub = setupButton()
        
        addSubview(bgView)
        addSubview(label)
        addSubview(buttonSub)
        addSubview(button)
        
        clipsToBounds = true
        
        backgroundColor = UIColor.Table.tableGroupColor()
    }
    
    fileprivate func setupLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)
        label.backgroundColor = UIColor.clear
        return label
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.isHidden = true
        button.contentHorizontalAlignment = .right
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.clear
        return button
    }
    
    fileprivate func setupBackgroundView() -> UIView {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        return view
    }
}
