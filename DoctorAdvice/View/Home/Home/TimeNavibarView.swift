//
//  TimeNavibarView.swift
//  KO4Teacher
//
//  Created by Pham Huy on 8/31/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class TimeNavibarView: UIView {
    
    var timer: Timer!
    
    var labelTime: UILabel!
    
    fileprivate var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "EEEE\ndd/MM/yyyy"
        return formatter
    }()
    
    fileprivate var timeFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm \ndd/MM/yyyy"
        return formatter
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        labelTime.frame = CGRect(x: 5, y: 0, width: bounds.width - 15, height: bounds.height)
        
        
    }
    
    @objc func updateTime() {
        
        labelTime.text = dateFormatter.string(from: Date())
    }
}

extension TimeNavibarView {
    func setupAllSubviews() {
        labelTime = setupLabel()
        addSubview(labelTime)
        
        updateTime()
        timer = Timer.scheduledTimer(timeInterval: 1.minute, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
      
    }
    
    func setupLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..) ?? UIFont.systemFont(ofSize: FontSize.normal..)
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .right
        label.backgroundColor = UIColor.clear
        return label
    }
}
