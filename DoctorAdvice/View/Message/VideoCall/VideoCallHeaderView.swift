//
//  VideoCallHeaderView.swift
//  DoctorAdvice
//
//  Created by Nguyen Nang on 2/4/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit

class VideoCallHeaderView: UIView {
    
    enum Size: CGFloat {
        case button = 54, label = 25, padding15 = 15, padding10 = 10, avatar = 56
    }
    
    var contentView: UIView!
    var avatar: UIImageView!
    var labelName: UILabel!
    var labelSub: UILabel!
    var buttonChange: UIButton!
    
    var padding: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        avatar.frame = CGRect(x: Size.padding15..,
                              y: Size.padding15..,
                              width: Size.avatar..,
                              height: Size.avatar..)
        
        buttonChange.frame = CGRect(x: contentView.frame.width - Size.button.. - Size.padding15..,
                              y: Size.padding15..,
                              width: Size.button..,
                              height: Size.button..)
        
        buttonChange.center.y = avatar.center.y
        
        
        labelName.frame = CGRect(x: avatar.frame.maxX + Size.padding10..,
                                 y: avatar.frame.minY,
                                 width: buttonChange.frame.minX - avatar.frame.maxX - Size.padding10.. * 2,
                                 height: Size.avatar.. / 2)
        
        labelSub.frame = CGRect(x: avatar.frame.maxX + Size.padding10..,
                                y: labelName.frame.maxY,
                                width: buttonChange.frame.minX - avatar.frame.maxX - Size.padding10.. * 2,
                                height: Size.avatar.. / 2)        
   
    }
}

extension VideoCallHeaderView {
    
    
    // Setup addSubview
    func setup() {
        
        contentView = setupView(.clear)
        
        
         avatar = setupImageView()
         labelName = setupLabel(textColor: .white,
                                font: UIFont(name: FontType.latoBold.., size: FontSize.large++)!)
         
         labelSub = setupLabel(textColor: .white,
                               font: UIFont(name: FontType.latoRegular.., size: FontSize.normal..)!)
         
         
         buttonChange = setupButton(image: Icon.General.switchCamera.tint(.white),
                                    imageSelected: Icon.General.switchCamera.tint(.white))

        addSubview(contentView)
        contentView.addSubview(avatar)
        contentView.addSubview(labelName)
        contentView.addSubview(labelSub)
        contentView.addSubview(buttonChange)
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: Icon.Account.AvatarDefault)
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.white.alpha(0.8).cgColor
        imageView.layer.cornerRadius = Size.avatar.. / 2
        return imageView
    }
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white,
                     bgColor: UIColor = .clear) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = bgColor
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}


