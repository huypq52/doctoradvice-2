//
//  VideoCallDebugView.swift
//  DoctorAdvice
//
//  Created by Nguyen Nang on 2/4/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit

class VideoCallDebugView: UIView {
    
    enum Size: CGFloat {
        case button = 54, label = 25, padding15 = 15, padding10 = 10, avatar = 56
    }
    
    var contentView: UIView!
    var signalingStatusLabel: UILabel!
    var localSdpStatusLabel: UILabel!
    var localCandidatesLabel: UILabel!
    var remoteSdpStatusLabel: UILabel!
    var remoteCandidatesLabel: UILabel!
    var webRTCStatusLabel: UILabel!
    
    var padding: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
       
        signalingStatusLabel.frame = CGRect(x: Size.padding10..,
                                            y: 0,
                                            width: contentView.frame.width - Size.padding10.. * 2,
                                            height: Size.label..)
        
        localSdpStatusLabel.frame = CGRect(x: Size.padding10..,
                                           y: signalingStatusLabel.frame.maxY,
                                           width: contentView.frame.width - Size.padding10.. * 2,
                                           height: Size.label..)
        
        localCandidatesLabel.frame = CGRect(x: Size.padding10..,
                                            y: localSdpStatusLabel.frame.maxY,
                                            width: contentView.frame.width - Size.padding10.. * 2,
                                            height: Size.label..)
        
        remoteSdpStatusLabel.frame = CGRect(x: Size.padding10..,
                                            y: localCandidatesLabel.frame.maxY,
                                            width: contentView.frame.width - Size.padding10.. * 2,
                                            height: Size.label..)
        
        remoteCandidatesLabel.frame = CGRect(x: Size.padding10..,
                                             y: remoteSdpStatusLabel.frame.maxY,
                                             width: contentView.frame.width - Size.padding10.. * 2,
                                             height: Size.label..)
        
        webRTCStatusLabel.frame = CGRect(x: Size.padding10..,
                                         y: remoteCandidatesLabel.frame.maxY,
                                         width: contentView.frame.width - Size.padding10.. * 2,
                                         height: Size.label..)
   
    }
}

extension VideoCallDebugView {
    
    
    // Setup addSubview
    func setup() {
        
        contentView = setupView(.clear)
        
        
        signalingStatusLabel = setupLabel()
        localSdpStatusLabel = setupLabel()
        localCandidatesLabel = setupLabel()
        remoteSdpStatusLabel = setupLabel()
        remoteCandidatesLabel = setupLabel()
        webRTCStatusLabel = setupLabel()
        
        addSubview(contentView)
        contentView.addSubview(signalingStatusLabel)
        contentView.addSubview(localSdpStatusLabel)
        contentView.addSubview(localCandidatesLabel)
        contentView.addSubview(remoteSdpStatusLabel)
        contentView.addSubview(remoteCandidatesLabel)
        contentView.addSubview(webRTCStatusLabel)
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: Icon.Account.AvatarDefault)
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.white.alpha(0.8).cgColor
        imageView.layer.cornerRadius = Size.avatar.. / 2
        return imageView
    }
    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white,
                     bgColor: UIColor = .clear) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = bgColor
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}



