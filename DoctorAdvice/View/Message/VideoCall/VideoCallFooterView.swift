//
//  VideoCallFooterView.swift
//  DoctorAdvice
//
//  Created by Nguyen Nang on 2/4/20.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit

class VideoCallFooterView: UIView {
    
    enum Size: CGFloat {
        case button = 54, label = 25, padding15 = 15, padding10 = 10
    }
    
    var contentView: UIView!
    var buttonVideo: UIButton!
    var buttonSpeak: UIButton!
    var buttonMute: UIButton!
    var buttonEndCall: UIButton!
    var buttonAnswer: UIButton!
    
    var padding: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    
        contentView.frame = CGRect(x: padding,
                                   y: 0,
                                   width: bounds.width - padding * 2,
                                   height: bounds.height)
        
        buttonSpeak.frame = CGRect(x: Size.padding15..,
                                   y: Size.padding10..,
                                   width: Size.button..,
                                   height: Size.button..)
        
//        buttonVideo.frame = CGRect(x: Size.padding15..,
//                                   y: Size.padding10..,
//                                   width: Size.button..,
//                                   height: Size.button..)
        
        buttonMute.frame = CGRect(x: contentView.frame.width - Size.button.. - Size.padding15..,
                                  y: Size.padding10..,
                                  width: Size.button..,
                                  height: Size.button..)
        
        
        if buttonAnswer.isHidden {
            
            buttonEndCall.frame = CGRect(x: (contentView.frame.width - Size.button..) / 2,
                                         y: Size.padding10..,
                                         width: Size.button..,
                                         height: Size.button..)
            
        } else {
            
            
            buttonAnswer.frame = CGRect(x: buttonSpeak.frame.maxX + (buttonMute.frame.minX - buttonSpeak.frame.maxX - Size.button.. * 2) / 3,
                                         y: Size.padding10..,
                                         width: Size.button..,
                                         height: Size.button..)
            
            buttonEndCall.frame = CGRect(x: buttonAnswer.frame.maxX +  (buttonMute.frame.minX - buttonAnswer.frame.maxX - Size.button..) / 2,
                                         y: Size.padding10..,
                                         width: Size.button..,
                                         height: Size.button..)
            
            
            

        }
        
        
        
   
    }
}

extension VideoCallFooterView {
    
    
    // Setup addSubview
    func setup() {
        
        contentView = setupView(.clear)
        
        
         buttonEndCall = setupButton(image: Icon.Login.Phone.tint(.white),
                                     bgColor: .red)
        
        buttonAnswer = setupButton(image: Icon.Login.Phone.tint(.white),
                                    bgColor: UIColor(rgba: "#32CD32"))
         
         buttonVideo = setupButton(image: Icon.General.videoOff.tint(.white),
                                   imageSelected: Icon.General.videoOn.tint(.white),
                                   bgColor: UIColor.black.alpha(0.7))
         
         buttonMute = setupButton(image: Icon.General.micOn.tint(.white),
                                  imageSelected: Icon.General.micOff.tint(.white),
                                  bgColor: UIColor.black.alpha(0.7))
        
        buttonSpeak = setupButton(image: Icon.General.speakerOff.tint(.white),
                                 imageSelected: Icon.General.speakerOn.tint(.white),
                                 bgColor: UIColor.black.alpha(0.7))

        addSubview(contentView)
        contentView.addSubview(buttonMute)
//        contentView.addSubview(buttonVideo)
        contentView.addSubview(buttonEndCall)
        contentView.addSubview(buttonAnswer)
        contentView.addSubview(buttonSpeak)
    }
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white,
                     bgColor: UIColor = .clear) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = bgColor
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
    
    func setupView(_ bgColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
}

