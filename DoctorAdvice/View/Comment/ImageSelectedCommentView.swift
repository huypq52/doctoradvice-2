//
//  ImageSelectedCommentView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/23/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//



import UIKit


class ImageSelectedCommentView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, label = 30, imageHeight = 90, button = 40
    }
    
    var contentView: UIView!
    var imageView: UIImageView!
    var buttonDelete: UIButton!
    
    var image: UIImage? = nil {
        didSet {
            guard let image = image else {
                return
            }
            imageView.image = image
            layoutSubviews()
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        guard let image = image else { return }
        
        
        let widthImage = (image.size.width / image.size.height) * Size.imageHeight..
        
        contentView.frame = CGRect(x: Size.padding10.. / 2,
                                   y: Size.padding10.. / 2,
                                   width: min(widthImage, Size.imageHeight.. / 9 * 16) + Size.padding10.. / 2,
                                   height:  Size.imageHeight.. + Size.padding10.. / 2)
        
        buttonDelete.frame = CGRect(x: (58 - Size.button.. ) / 2,
                                    y: contentView.frame.maxY + Size.padding10.. / 2,
                                    width: Size.button..,
                                    height: Size.button..)
        
        buttonDelete.layer.cornerRadius = buttonDelete.frame.height / 2
        
        
        imageView.frame = CGRect(x: Size.padding10.. / 4,
                                 y: Size.padding10.. / 4,
                                 width: contentView.frame.width - Size.padding10.. / 2,
                                 height: contentView.frame.height - Size.padding10.. / 2)

        
    }
}

extension ImageSelectedCommentView {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        
        contentView = setupView(.white)
       
        
        imageView = setupImageView()
        buttonDelete = setupButton(image: Icon.General.Delete.tint(.red))
        
        
        addSubview(contentView)
        addSubview(buttonDelete)
        contentView.addSubview(imageView)
        
        
        layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 1.0, height: 2.0)

    }
    
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor = UIColor.Text.blackMediumColor(),
                                alignment: NSTextAlignment = .left,
                                bgColor: UIColor = UIColor.clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    

    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        return view
    }
    
    func setupImageView() -> UIImageView {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 5
        image.layer.borderWidth = onePixel()
        image.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        image.isUserInteractionEnabled = true
        return image
    }
    

    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = .white
        
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        button.layer.shadowOpacity = 0.3
        button.layer.shadowRadius = 2.0
        button.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        
        return button
    }

}

