//
//  TextInputCommentView.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/19/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import DKImagePickerController
import KMPlaceholderTextView

class TextInputCommentView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding5 = 5, button = 48, text = 38
    }
    
    
    fileprivate var contentView: UIView!
    fileprivate var seperatorTop: UIView!
    
    
    var send: UIButton!
    var addImage: UIButton!
    var textView: KMPlaceholderTextView!
    

    var imageViewSelected: ImageSelectedCommentView!
    
    var paddingBottom: CGFloat = 0
    
    
    var assets: [DKAsset]? {
        didSet {
            
            guard let assets = assets, assets.count > 0 else {
                imageSelected = nil
                return
            }
            
            for (_, asset) in assets.enumerated() {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    if let image = image {
                        self.imageSelected = self.cropImage(image, toSize:CGSize(width: 720, height: 720))
                    } else {
                        self.imageSelected = nil
                    }
                })
            }
        }
    }

    
    var imageSelected: UIImage? = nil {
        didSet {
            
            guard let image = imageSelected else {
                addImage.isSelected = false
                addImage.alpha = 1.0
                imageString = nil
                imageViewSelected.imageView.image = nil
                
                
                self.imageViewSelected.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: {
                
                    self.imageViewSelected.alpha = 0.0
                }, completion: { finish in
                    self.imageViewSelected.isHidden = true
                    
                })
                
                return
            }
            
            addImage.isSelected = true
            addImage.alpha = 0.0
            
            if let imageData = image.jpegData(compressionQuality: 0.95) {
                imageString = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            
            imageViewSelected.image = image
            self.imageViewSelected.isHidden = false
            self.imageViewSelected.alpha = 0.0
            UIView.animate(withDuration: 0.3, animations: {
                
                self.imageViewSelected.alpha = 1.0
            }, completion: nil)
            
            
        }
    }
    var imageString: String?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = bounds
        
        
        imageViewSelected.frame = CGRect(x: 0,
                                         y: bounds.height - 150,
                                         width: contentView.frame.width / 3 + 10,
                                         height: 150)
        
        seperatorTop.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: onePixel())
        
        addImage.frame = CGRect(x: Size.padding5..,
                                y: contentView.frame.height - paddingBottom - Size.button..,
                                width: Size.button..,
                                height: Size.button..)
        
        send.frame = CGRect(x: contentView.frame.width - Size.button.. - Size.padding5..,
                            y: contentView.frame.height - paddingBottom - Size.button..,
                            width: Size.button..,
                            height: Size.button..)
        
        
        if addImage.isHidden {
            
            textView.frame = CGRect(x: Size.padding10..,
                                    y: Size.padding5..,
                                    width: send.frame.minX - Size.padding10.. * 2,
                                    height: contentView.frame.height  - paddingBottom - Size.padding5.. * 2)
            
        } else {
            
            
            textView.frame = CGRect(x: addImage.frame.maxX + Size.padding10.. / 2,
                                    y: Size.padding5..,
                                    width: send.frame.minX - Size.padding10..  - addImage.frame.maxX ,
                                    height: contentView.frame.height  - paddingBottom - Size.padding5.. * 2)
            
        }
    }
}


extension TextInputCommentView {
    func setupAllSubviews() {
        
        contentView = setupView()
        seperatorTop = setupView(UIColor.Misc.seperatorColor())
        addSubview(contentView)
        addSubview(seperatorTop)
        
        textView = setupTextView()
        send = setupButton(image: Icon.General.Send.tint(UIColor.Navigation.mainColor()))
        send.setImage(Icon.General.Send.tint(UIColor.Misc.seperatorColor()), for: .disabled)
        
        addImage = setupButton(image: Icon.General.AddImage.tint(UIColor.Text.grayMediumColor()),
                               imageSelected: Icon.General.AddImage.tint(UIColor.Navigation.mainColor()))
        
        addImage.isHidden = true
        
        contentView.addSubview(textView)
        contentView.addSubview(send)
        contentView.addSubview(addImage)
        
        imageViewSelected = setupSelectedImageView(imageSelected)
        addSubview(imageViewSelected)
    }
    
    func setupView(_ bgColor: UIColor = UIColor.white) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupTextView() -> KMPlaceholderTextView {
        let textView = KMPlaceholderTextView()
        
        textView.layer.borderColor = UIColor(rgba: "#d4d7db").cgColor
        textView.layer.borderWidth = onePixel() * 2
        textView.layer.cornerRadius = 19
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.backgroundColor = UIColor(rgba: "#f6f7f9")
        textView.autocorrectionType = .no
        textView.placeholder = LocalizedString("general_label_writing_comment", comment: "Viết bình luận ...")
        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.placeholderColor = UIColor(rgba: "#8a8e96")
        
        var container = textView.textContainerInset
        container.left = 8
        container.right = 8
        container.bottom += 4
        container.top += 2
        
        textView.textContainerInset = container
        
        return textView
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        return button
    }

    
    func setupButton() -> UIButton {
        let button = UIButton()
        button.setTitle(LocalizedString("general_button_send_comment", comment: "Đăng"), for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal..)
        return button
    }
    
    func setupSelectedImageView(_ image: UIImage?) -> ImageSelectedCommentView {
        
        let view: ImageSelectedCommentView = ImageSelectedCommentView()
        view.image = image
        view.isHidden = true
        view.buttonDelete.addTarget(self, action: #selector(self.buttonDelete(_:)), for: .touchDown)
        
        return view
        
    }
    
    
    @objc func buttonDelete(_ sender: UIButton) {
        assets = nil
    }
    
    
    func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        var newSize: CGSize
        /**
         *  Resize xuống 1024 x 1024
         */
        
        
        let maxSize: CGFloat = 840
        
        if image.size.width >= image.size.height { newSize = CGSize(width: maxSize, height: maxSize * image.size.height / image.size.width) }
        else { newSize = CGSize(width: maxSize * image.size.width / image.size.height, height: maxSize) }
        
        
        //        newSize = image.size
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
        
        /**
         *  Crop to size
         */
        
        //        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        //        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        ////        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        //
        //        let cropRect = CGRect(x: 0, y: 0, width: Int(size.height), height: Int(size.width))
        //        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        //
        //        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        //        return cropped
    }
}



