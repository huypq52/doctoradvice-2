//
//  ItemCommentView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 7/23/19.
//  Copyright © 2020 Pham Huy. All rights reserved.
//


import UIKit
import WebKit
import AVFoundation

class ItemCommentView: UIView {
    
    enum Size: CGFloat {
        case padding5 = 5, padding10 = 10, image = 40, label = 20, button = 38, imageHeight = 150, buttonWidth = 60
    }
    
    var contentView: UIView!
    var webView: WKWebView = WKWebView()

    var commnet: Comment? {
        didSet {
            guard let comment = commnet else { return }
            
            labelTime.text      = Utility.stringFromPastTimeCommentToText(comment.time)
            labelComment.text   = comment.content
            labelName.text      = comment.name
            
            buttonDelete.isHidden   = !comment.canDelete
            buttonEdit.isHidden     = !comment.canEdit
            buttonReply.isHidden    = !comment.canReply
            
            imageView.isHidden = true
            imageView.image = nil
            icon.isHidden = true
            webView.removeFromSuperview()
            
            guard comment.fileAttach.count > 0 else { layoutSubviews(); return }
            
            switch comment.attachType {
            case .image:
                
                
                
                imageView.isHidden = false
                
                
                if let image = comment.imageAttach {
                    imageView.image = image
                    layoutSubviews()
                    return
                    
                }
                
                imageView?.downloadedFrom(link: comment.fileAttach, placeHolder: nil) { image in
                    
                    self.commnet?.imageAttach = image
                    self.layoutSubviews()
                }
                
            case .video:
                
                imageView.isHidden = false
                
                if let image = comment.videoImage {
                    
                    if let _ = comment.fileAttach.youtubeID {
                        icon.image = UIImage(named: "ic_youtube")
                    } else {
                        icon.image = UIImage(named: "ic_play_video")?.tint(.white)
                    }
                    
                    icon.isHidden = false
                    imageView.image = image
                    layoutSubviews()
                    return
                    
                } else {
                    imageView.image = UIImage(named: "ic_video_default")
                }
                
                
                
                if let id = comment.fileAttach.youtubeID {
                    
                    icon.isHidden = false
                    icon.image = UIImage(named: "ic_youtube")
                    imageView.downloadedFrom(link: String(format: "https://i.ytimg.com/vi/%@/hqdefault.jpg", id), placeHolder: nil) { image in

                        comment.videoImage = image
                    }
                    
                    
                }
                
                else if let url = URL(string: Utility.getVideoLink(comment.fileAttach) ) {
                    
                    self.icon.isHidden = false
                    self.icon.image = UIImage(named: "ic_play_video")?.tint(.white)
                    
                    getThumbnailImageFromVideoUrl(url: url, completion: { image in
                        
                        
                        self.imageView.image = image
                        comment.videoImage = image
                    })
                }
                
                
            default:
                break
            }
            
            layoutSubviews()
            
        }
    }
    
    var boundView: UIView!
    var labelName: UILabel!
//    var labelComment: UILabel!
    
    var labelComment: UITextView!
    var imageView: UIImageView!
    var icon: UIImageView!
    
    var labelTime: UILabel!
    var buttonReply: UIButton!
    var buttonDelete: UIButton!
    var buttonEdit: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        guard let comment = commnet else { return }

        
        labelName.frame = CGRect(x: Size.padding10..,
                                 y: Size.padding10.. / 2,
                                 width: contentView.frame.width - Size.padding10.. * 2,
                                 height: Size.label..)
        
        var commentHeight: CGFloat = 0
        
//        if comment.content.count > 0 {
//            commentHeight = Utility.heightForView(comment.content,
//                                                  font: labelComment.font,
//                                                  width: labelName.frame.width) + Size.padding5..
//        }
//
//
//        labelComment.frame = CGRect(x: labelName.frame.minX,
//                                    y: labelName.frame.maxY ,
//                                    width: labelName.frame.width,
//                                    height: commentHeight)
        
        if comment.content.count > 0 {
            commentHeight = Utility.heightForTextView(comment.content,
                                                      font: labelComment.font ?? UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                                      width: labelName.frame.width + 10) + Size.padding5..
        }
        
        
        labelComment.frame = CGRect(x: labelName.frame.minX - 5,
                                    y: labelName.frame.maxY ,
                                    width: labelName.frame.width + 10,
                                    height: commentHeight)

        
        switch comment.attachType {
        case .image:
        
            var widthImage: CGFloat = 0
            if let image = imageView.image {
                widthImage = (image.size.width / image.size.height) * Size.imageHeight..
            }
            
            
            
            imageView.frame = CGRect(x: Size.padding10..,
                                     y: labelComment.frame.maxY + Size.padding10.. ,
                                     width: min(contentView.frame.width - 20, widthImage),
                                     height: Size.imageHeight.. )
            

            icon.frame = .zero
                
        case .video:
            
            
            imageView.frame = CGRect(x: Size.padding10..,
                                     y: labelComment.frame.maxY + Size.padding10.. ,
                                     width: contentView.frame.width - 20,
                                     height: Size.imageHeight.. )
            
            webView.frame = imageView.bounds
            
            if icon.isHidden {
                icon.frame = .zero
                
            } else {
                
                let widthItem = min(UIScreen.main.bounds.width / 4, max(imageView.frame.width / 3, UIScreen.main.bounds.width / 8))
                
                icon.frame = CGRect(x: (imageView.frame.width - widthItem) / 2,
                                    y: 0,
                                    width:  widthItem,
                                    height: imageView.frame.height)
            }
            
            
        default:
            imageView.frame = .zero
            icon.frame = .zero
        }
        
        
//        var widthImage: CGFloat = 0
//        
//        if let image = imageView.image, !imageView.isHidden {
//            
//            widthImage = (image.size.width / image.size.height) * Size.imageHeight..
//            
//            imageView.frame = CGRect(x: Size.padding10..,
//                                     y: labelComment.frame.maxY + Size.padding10.. ,
//                                     width: min(contentView.frame.width - 20, widthImage),
//                                     height: Size.imageHeight.. )
//            
//            
//            if icon.isHidden {
//
//                icon.frame = .zero
//                
//            } else {
//                
//                let widthItem = min(UIScreen.main.bounds.width / 4, max(imageView.frame.width / 3, UIScreen.main.bounds.width / 8))
//                
//                icon.frame = CGRect(x: (imageView.frame.width - widthItem) / 2,
//                                    y: 0,
//                                    width:  widthItem,
//                                    height: imageView.frame.height)
//            }
//            
//
//            
//            
//        } else {
//            imageView.frame = .zero
//            icon.frame = .zero
//        }
     
        
        
        let widthName: CGFloat = Utility.widthForView(comment.name, font: labelName.font, height: 20)
        let widthComment: CGFloat = Utility.widthForTextView(comment.content, font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!, height: 20)
        
        
        let maxWidth: CGFloat = min(contentView.frame.width, 20 + max(widthName, widthComment))

        
        boundView.frame = CGRect(x: 0,
                                 y: 0,
                                 width: maxWidth,
                                 height: labelComment.frame.maxY + Size.padding10.. / 2)
        
        boundView.layer.cornerRadius = 18
        
        
        
        
        let timeWidth: CGFloat = Utility.widthForView(labelTime.text ?? "",
                                                      font: labelTime.font, height: 20)
        
        labelTime.frame = CGRect(x: Size.padding10..,
                                 y: contentView.frame.height - Size.label.. - Size.padding10.. / 2,
                                 width: min(timeWidth, contentView.frame.width / 2),
                                 height: Size.label..)
        
        buttonDelete.frame = CGRect(x: labelTime.frame.maxX + Size.padding10..,
                                   y: 0,
                                   width: comment.canDelete ? Size.buttonWidth.. : 0,
                                   height: 44)
        
        buttonDelete.center.y = labelTime.center.y
        
        
        
        buttonEdit.frame = CGRect(x: buttonDelete.frame.maxX,
                                  y: buttonDelete.frame.minY,
                                  width: comment.canEdit ? Size.buttonWidth.. : 0,
                                  height: 44)
        
        buttonReply.frame = CGRect(x: buttonEdit.frame.maxX ,
                                  y: buttonDelete.frame.minY,
                                  width: comment.canReply ? Size.buttonWidth.. : 0,
                                  height: 44)
        
    }
}


extension ItemCommentView {
    func setupAllSubviews() {
        
        contentView = setupView()
        addSubview(contentView)
        

        boundView = setupView(UIColor(rgba: "#eff1f3"))

        imageView = setupImageView()
        imageView.layer.cornerRadius = 8
        imageView.layer.borderWidth = onePixel()
        imageView.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        
        icon = setupImageView(UIImage(named: "ic_youtube"), contentMode: .scaleAspectFit)
        icon.isHidden = true

        labelName = setupLabel()
        
        
//        labelComment = setupLabel(font: UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!)
//        labelComment.lineBreakMode = .byWordWrapping
//        labelComment.numberOfLines = 0
       
        labelComment = setupTextView()
       
        contentView.addSubview(boundView)
        contentView.addSubview(labelName)
        contentView.addSubview(labelComment)
        contentView.addSubview(imageView)
        imageView.addSubview(icon)
        
        
        labelTime = setupLabel(UIColor.Text.grayMediumColor(), font: UIFont(name: FontType.latoRegular.., size: FontSize.small++)!)
        buttonReply = setupButton(LocalizedString("notification_general_button_reply", comment: "Trả lời"))
        buttonEdit = setupButton(LocalizedString("notification_general_button_edit", comment: "Sửa"))
        buttonDelete = setupButton(LocalizedString("default_label_delete", comment: "Xóa"))

        

        contentView.addSubview(labelTime)
        contentView.addSubview(buttonReply)
        contentView.addSubview(buttonDelete)
        contentView.addSubview(buttonEdit)
        

    }
    
    
    func setupView(_ bgColor: UIColor = UIColor.white) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        return view
    }
    
    func setupImageView(_ image: UIImage? = nil, contentMode: UIView.ContentMode = .scaleAspectFill) -> UIImageView {
        let image = UIImageView(image: image)
        image.contentMode = contentMode
        image.clipsToBounds = true

        image.isUserInteractionEnabled = true
        return image
    }
    
    func setupLabel(_ textColor: UIColor = UIColor.Text.blackMediumColor(),
                    alignment: NSTextAlignment = .left,
                    font: UIFont = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)!) -> UILabel {
        let label = UILabel()
        label.textColor = textColor
        label.textAlignment = alignment
        label.font = font
        label.backgroundColor = .clear
        return label
    }

    
    func setupButton(_ title: String? = nil, image: UIImage? = nil) -> UIButton {
        let button = UIButton()
        
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.grayNormalColor(), for: .normal)
        button.contentHorizontalAlignment = .center
        return button
    }
    
    func setupTextView() -> UITextView {
        let textView = UITextView()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.isEditable = false
        textView.dataDetectorTypes = .link
        textView.textAlignment = .left
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
//        textView.delegate = self
        textView.isUserInteractionEnabled = true
        return textView
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}














