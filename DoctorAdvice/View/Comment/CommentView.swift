//
//  CommentView.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/19/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class CommentView: UIView {
    
    enum Size: CGFloat {
        case padding5 = 5, padding10 = 10, image = 40, label = 20, button = 38
    }
    
    fileprivate var contentView: UIView!
    
    fileprivate var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss dd/MM/yyyy"
        return formatter
    }()
    
    var commnet: Comment? {
        didSet {
            guard let comment = commnet else { return }

            
            labelConfirm.isHidden = comment.isConfirm
            itemComment.commnet = comment
            
          
            avatar.image = Icon.Account.AvatarDefault
            if let image = comment.imageAvatar {
                avatar.image = image
                return
            }
            
            avatar.downloadedFrom(link: comment.avatar, placeHolder: Icon.Account.AvatarDefault) { image in
                self.commnet?.imageAvatar = image
            }
            
//            layoutSubviews()
        }
    }
    
    
    var labelConfirm: UILabel!
    var avatar: UIImageView!
    var itemComment: ItemCommentView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        labelConfirm.frame = CGRect(x: Size.padding10.. * 2 + Size.image..,
                                    y: 0,
                                    width: bounds.width - Size.padding10.. * 3 - Size.image..,
                                    height: labelConfirm.isHidden ? 0 : 16)
        
        contentView.frame = CGRect(x: 0,
                                   y: labelConfirm.frame.maxY,
                                   width: bounds.width,
                                   height: bounds.height - labelConfirm.frame.maxY)
        
        avatar.frame = CGRect( x: Size.padding10.. ,
                               y: Size.padding10..,
                               width: Size.image..,
                               height: Size.image..)
        
        avatar.layer.cornerRadius = Size.image.. / 2
        
        
        itemComment.frame = CGRect(x: avatar.frame.maxX + Size.padding10.. / 2,
                                   y: Size.padding10.. / 2,
                                   width: contentView.frame.width - avatar.frame.maxX - Size.padding10.. ,
                                   height: contentView.frame.height - Size.padding10..)
    }
}


extension CommentView {
    func setupAllSubviews() {
        
        contentView = setupView()
        addSubview(contentView)
        
        
        
        labelConfirm = setupLabel(UIColor.Text.grayMediumColor(),
                                  alignment: .left,
                                  font: UIFont(name: FontType.latoRegular.., size: FontSize.small..)!)
        
        labelConfirm.attributedText = Utility.getAttributeString("• ",
                                                                 subString: LocalizedString("comment_label_waiting_confirm", comment: "Đang chờ kiểm duyệt"),
                                                                 fontFirst: UIFont(name: FontType.latoBold.., size: FontSize.small++)!,
                                                                 fontSub: UIFont(name: FontType.latoRegular.., size: FontSize.small..)!,
                                                                 firstColor: UIColor.Navigation.subColor(),
                                                                 subColor: UIColor.Text.grayMediumColor())
        
        addSubview(labelConfirm)
        

        avatar = setupImageView()

        
        itemComment = setupItemCommentView(commnet)
        
        contentView.addSubview(avatar)
        contentView.addSubview(itemComment)

    }
    
    
    func setupView(_ bgColor: UIColor = UIColor.white) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupImageView() -> UIImageView {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = Icon.Account.AvatarDefault
        image.clipsToBounds = true
        return image
    }
    
    func setupLabel(_ textColor: UIColor = UIColor.Text.blackMediumColor(),
                    alignment: NSTextAlignment = .left,
                    font: UIFont = UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)!) -> UILabel {
        let label = UILabel()
        label.textColor = textColor
        label.textAlignment = alignment
        label.font = font
        label.clipsToBounds = true
        return label
    }
    
    func setupButton(_ title: String? = nil, image: UIImage? = nil) -> UIButton {
        let button = UIButton()
        
        button.titleLabel?.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
        button.contentHorizontalAlignment = .right

        button.setImage(image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 0)
        
        return button
    }
    
    func setupItemCommentView(_ comment: Comment?) -> ItemCommentView {
        let view = ItemCommentView()
        view.commnet = comment
        return view
    }
}













