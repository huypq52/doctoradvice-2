//
//  LikeView.swift
//  KidOnline
//
//  Created by Pham Huy on 12/9/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import FaveButton

class LikeView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, label = 30
    }
    
    var contentView: UIView!
    
    var isLike: Bool = false {
        didSet {
//            buttonLike.alpha = isLike ?  1.0 : 0.5
            
            if buttonLike.isSelected != isLike {
                buttonLike.isSelected = isLike
            }
            
        }
    }
    
    var buttonLike: FaveButton!
    var labelTitle: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds
        
        buttonLike.frame = CGRect(x: 5, y: (contentView.frame.height - 40 ) / 2, width: 40, height: 40)

        
        labelTitle.frame = CGRect(x: buttonLike.frame.maxX ,
                                  y: 0 ,
                                  width: contentView.frame.width - (buttonLike.frame.maxX + 5) - 5,
                                  height: contentView.frame.height )

    
        
        
    }
}

extension LikeView {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        
        contentView = setupSeperator(UIColor.white)
        labelTitle = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                                textColor: UIColor.Text.blackMediumColor(),
                                bgColor: UIColor.clear)
        

        buttonLike = setupButton(image: Icon.KidsOnlineCorner.Like)
        buttonLike.setImage(Icon.KidsOnlineCorner.Liked, for: .selected)
        
        contentView.addSubview(labelTitle)
        contentView.addSubview(buttonLike)

        
        
        addSubview(contentView)
        
        
        buttonLike.isSelected = false
        
    }
    
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                            textColor: UIColor = UIColor.Text.blackMediumColor(),
                            alignment: NSTextAlignment = .left,
                            bgColor: UIColor = UIColor.clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    
    fileprivate func setupButton(_ bgColor: UIColor = UIColor.clear,
                             title: String? = nil,
                             image: UIImage) -> FaveButton {
        
        let button = FaveButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40), faveIconNormal: image)
        button.setImage(image, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.backgroundColor = bgColor
        button.isUserInteractionEnabled = true
        button.normalColor = .clear
        button.selectedColor = .clear
        button.circleToColor = UIColor.Navigation.mainColor()
        return button
    }
    
    
    fileprivate func setupSeperator(_ color: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
}
