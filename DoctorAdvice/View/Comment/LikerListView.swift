//
//  LikerListView.swift
//  KidOnline
//
//  Created by Pham Huy on 12/10/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class LikerListView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, button = 40, contentWidth = 300
    }
    
    var labelHeader: UILabel!
    var table: UITableView!
    
    fileprivate var contentView: UIView!
    
    
    var like: Like? {
        didSet {
            
            guard table != nil else { return }
            table.reloadData()
        }
    }
    
    var mainColor: UIColor = UIColor.Navigation.mainColor() {
        didSet {
            
            contentView.backgroundColor = mainColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: 0,
                                   width: Size.contentWidth..,
                                   height: bounds.height)
        contentView.layer.cornerRadius = 5
        
        labelHeader.frame = CGRect(x: 0,
                                   y: 0,
                                   width: Size.contentWidth..,
                                   height: 35)
        
        
        var tableHeight: CGFloat = 0
        
        if let like = like {
            tableHeight = CGFloat(like.likers.count) * 60
        }
        
        tableHeight = min(bounds.height - 100, tableHeight)
        
        table.frame = CGRect(x: 0,
                             y: labelHeader.frame.maxY ,
                             width: Size.contentWidth..,
                             height: tableHeight)
        
        

        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth.. ) / 2,
                                   y: (bounds.height - table.frame.maxY) / 2 - 40,
                                   width: Size.contentWidth..,
                                   height: table.frame.maxY)
    }
}


extension LikerListView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let like = like else { return 0 }
        return like.likers.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentify = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! ContactTableViewCell
        configuareCell(cell, indexPath: indexPath)
        return cell
    }
    
    func configuareCell(_ cell: ContactTableViewCell, indexPath: IndexPath) {
        guard let like = like else { return }
        
        cell.isPadding = false
        cell.statusView.isHidden = true
        cell.buttonMessage.isHidden = true
        cell.imageView?.image = Icon.Account.AvatarDefault
        cell.textLabel?.text = like.likers[indexPath.row].fullName
        cell.imageView?.downloadedFrom(link: like.likers[indexPath.row].avatar, placeHolder: Icon.Account.AvatarDefault)
    }
    
}


extension LikerListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension LikerListView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.3)
        
        
        contentView = setupView(mainColor)
        
        
        table = setupTable()
        labelHeader = setupLabel()
        labelHeader.text = LocalizedString("general_label_number_liked", comment: "Những người thích nội dung này")
        
        addSubview(contentView)
        contentView.addSubview(labelHeader)
        contentView.addSubview(table)
        
        
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissView)))
        
    }
    
    func setupTable() -> UITableView {
        
        let table = UITableView(frame: CGRect.zero, style: .plain)
        table.backgroundColor = UIColor.white
        table.separatorStyle = .none
        table.allowsSelection = true
        table.backgroundColor = UIColor.white
        table.register(ContactTableViewCell.self, forCellReuseIdentifier: "Cell")
        table.keyboardDismissMode = .onDrag
        table.delegate = self
        table.dataSource = self
        return table
    }

    func setupLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.clear
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        label.clipsToBounds = true
        label.textAlignment = .center
        return label
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Navigation.mainColor()) -> UIView {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = bgColor
        return view
    }
}
