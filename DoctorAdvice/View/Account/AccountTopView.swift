//
//  AccountTopView.swift
//  SchoolOnline
//
//  Created by Pham Huy on 9/20/19.
//  Copyright © 2019 OMT. All rights reserved.
//



import UIKit
import SwiftyUserDefaults

class AccountTopView: UIImageView {
    
     enum Size: CGFloat {
        case padding3 = 5, padding15 = 15, padding10 = 10, avatar = 120, label = 40, button = 36
    }
    
    var cover: UIImageView!
    var bgView: UIView!
    fileprivate var shadowAvatar: UIView!
    var avatar: UIImageView!
    var buttonChangedAvatar: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bgView.frame = CGRect(x: 0,
                              y: bounds.height - Size.avatar.. / 2 - Size.button.. / 2,
                              width: bounds.width,
                              height: Size.avatar.. )
        
        cover.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bgView.frame.minY)
        
        
        shadowAvatar.frame = CGRect(x: (bounds.width - Size.avatar.. ) / 2,
                                    y: bgView.frame.minY - Size.avatar.. / 2,
                                    width: Size.avatar..,
                                    height: Size.avatar..)
        shadowAvatar.layer.cornerRadius = 12
        
        
        avatar.frame = CGRect(x: 0, y: 0, width: Size.avatar.. - onePixel() * 2, height: Size.avatar.. - onePixel() * 2)
        avatar.center = CGPoint(x: shadowAvatar.frame.width / 2, y: shadowAvatar.frame.height / 2 )
        avatar.layer.cornerRadius = 12
        

        buttonChangedAvatar.frame = CGRect(x: shadowAvatar.frame.maxX - Size.button.. * 3 / 4,
                                           y: shadowAvatar.frame.maxY - Size.button.. * 3 / 4,
                                           width: Size.button..,
                                           height: Size.button..)
  
    }
    
}

extension AccountTopView {
    fileprivate func setup() {
        

        cover = setupImageView()
        bgView = setupView(.white)
        avatar = setupAvatar()
        shadowAvatar = setupShadowAvatar()
        buttonChangedAvatar = setupButton(Icon.Navigation.Edit.tint(UIColor.Navigation.mainColor()))

        addSubview(cover)
        addSubview(bgView)
        shadowAvatar.addSubview(avatar)
        addSubview(shadowAvatar)
        addSubview(buttonChangedAvatar)
        
    
    }
    
    
    
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
    
    fileprivate func setupAvatar() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }
    
    fileprivate func setupShadowAvatar() -> UIView {
        
        let view = UIView()
        view.clipsToBounds = true
        
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 1.5
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.clipsToBounds = false
        
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    fileprivate func setupLabel(_ textColor: UIColor = UIColor.Text.blackMediumColor(),
                                font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.large..)!) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        return label
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.layer.cornerRadius = Size.button.. / 2
        button.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        button.layer.borderWidth = onePixel()
        
        button.layer.shadowColor = UIColor.black.alpha(0.3).cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowRadius = 4.0
        button.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        button.clipsToBounds = false
        return button
    }
    
    fileprivate func setupButtonLanguage(_ title: String, image: UIImage, tag: LanguageValue, selected: Bool) -> UIButton {
        
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.grayMediumColor(), for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .selected)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        button.tag = tag..
        button.isSelected = selected
        return button
    }
    
    func viewHeight() -> CGFloat {
        return Size.avatar.. / 2 + Size.padding10.. / 2 + Size.label.. + Size.label.. / 2 + Size.padding10..
    }
}
