//
//  AboutHeaderView.swift
//  KidOnline
//
//  Created by Pham Huy on 11/6/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions


class AboutHeaderView: UIView {
    
    enum Size: CGFloat {
        case logo = 90, label = 25, padding15 = 15, padding10 = 10
    }
    
    var logo: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    //    var logo: UIButton = {
    //        var button = UIButton()
    //        button.contentHorizontalAlignment = .center
    //        button.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 10)
    //        button.titleEdgeInsets = UIEdgeInsetsMake(-10, -5, 10, 5)
    //        button.userInteractionEnabled = false
    //        return button
    //        }()
    
    var version: UILabel = {
        var label = UILabel()
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.small..)
        label.textColor = UIColor.Text.blackMediumColor()
        label.textAlignment = .center
        return label
    }()
    
    var slogan: UILabel = {
        var label = UILabel()
        label.font = UIFont(name: FontType.latoBold.., size: FontSize.normal++)
        label.textAlignment = .center
        label.textColor = UIColor.Navigation.mainColor()
        return label
    }()
    
    var seperator: UIView = {
        var view = UILabel()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }()
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    func setup() {
        addSubview(logo)
        addSubview(slogan)
        addSubview(seperator)
        addSubview(version)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logo.frame = CGRect(x: 0,
                            y: Size.padding10.. ,
                            //            width: Size.Logo..,
            width: bounds.width,
            height: Size.logo..)
        
        
        
        seperator.frame = CGRect(x: Size.padding15..,
                                 y: bounds.height - onePixel(),
                                 width: bounds.width - Size.padding15.. * 2,
                                 height: onePixel())
        
        version.frame = CGRect(x: bounds.width / 4 - 5,
                               y:  logo.frame.maxY + Size.padding10.. / 4 ,
                               width: bounds.width / 2,
                               height: 20)
        
        slogan.frame = CGRect(x: Size.padding15..,
                              y: version.frame.maxY - Size.padding10.. / 2,
                              width: bounds.width - Size.padding15.. * 2,
                              height: Size.label.. + 5)
    }
}


