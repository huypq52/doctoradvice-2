//
//  SchoolInfoHeaderView.swift
//  KidOnline
//
//  Created by Pham Huy on 11/17/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//


import UIKit


class SchoolInfoHeaderView: UIImageView {
    
    enum Size: CGFloat {
        case logo = 80, label = 25, padding15 = 15, padding10 = 10
    }
    
    var logo: UIImageView!
    var conver: UIImageView!
    var bgColor: UIView!
    var labelName: UILabel!
    var contentView: UIView!
    
    
    var gradientLayer: CAGradientLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         setupAllSubviews()
    }

    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupAllSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        conver.frame = bounds
        bgColor.frame = bounds
        
        
         gradientLayer.frame = CGRect(x: 0,
//                                      y: bounds.height - ( bounds.height * 3 / 4 ),
                                      y: 0,
                                      width: bounds.width, height: bounds.height + UIScreen.main.bounds.width / 2)
        
        contentView.frame = CGRect(x: Size.padding10.. / 2,
                                   y: Size.padding10.. / 2,
                                   width: bounds.width - Size.padding10..,
                                   height: bounds.height - Size.padding10..)
        
        
        labelName.frame = CGRect(x: 0, y: contentView.frame.height - 50, width: contentView.frame.width, height: 50)
        
        
        logo.frame = CGRect(x: 0,
                            y: (labelName.frame.minY - Size.logo..) / 2 + Size.padding10.. / 2 ,
                            width: contentView.frame.width,
                            height: Size.logo..)
        
        

        

    }
}


extension SchoolInfoHeaderView {

    func setupAllSubviews() {
    
        
        conver = setupImageView(contentMode: .scaleAspectFill)
        bgColor = setupView(.clear)
        contentView = setupView(.clear)
        
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.alpha(1.0).cgColor]
        bgColor.layer.addSublayer(gradientLayer)
        
        addSubview(conver)
        addSubview(bgColor)
        addSubview(contentView)
        
        logo = setupImageView(contentMode: .scaleAspectFit)
        labelName = setupLabel("", textColor: .white, font: UIFont(name: FontType.latoBold.., size: FontSize.large..)!,
                               bgColor: .clear, alignment: .center)
        
    
        contentView.addSubview(logo)
        contentView.addSubview(labelName)
    }
    
    


    
    
    func setupLabel(_ title: String = "", textColor: UIColor = UIColor.Text.blackMediumColor(),
                    font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                    bgColor: UIColor = .clear,
                    alignment: NSTextAlignment = .left) -> UILabel {
        
        let label: UILabel = UILabel()
        label.text = title
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.font = font
        return label
    }

    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view: UIView = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    func setupImageView(contentMode: UIView.ContentMode = .scaleAspectFit) -> UIImageView {
    
        let view: UIImageView = UIImageView()
        view.contentMode = contentMode
        return view
    
    }
}


