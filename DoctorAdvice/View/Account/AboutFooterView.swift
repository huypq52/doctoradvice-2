//
//  AboutFooterView.swift
//  KidOnline
//
//  Created by Pham Huy on 11/6/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions

class AboutFooterView: UIView {
    
    enum Size: CGFloat {
        case  padding15 = 15, padding10 = 10, paddding5 = 5, button = 44, label = 25
    }
    
    var terms: UIButton = {
        var button = UIButton()
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .normal)
        button.contentHorizontalAlignment = .center
        return button
    }()
    
    
    var share: UIButton!
    var rate: UIButton!
    
    var copyRight: UILabel = {
        var label = UILabel()
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        label.textColor = UIColor.Text.grayMediumColor()
        label.textAlignment = .center
        return label
    }()
    
    var seperator: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }()
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    func setup() {
        
        share = setupButton(LocalizedString("general_label_share", comment: "Chia sẻ"), titleColor: UIColor.Text.grayMediumColor())
        
        rate = setupButton(LocalizedString("general_label_rating", comment: "Đánh giá"))
        
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
//        gradientLayer.colors = [UIColor.Gradient.firstColor().cgColor, UIColor.Gradient.secondColor().cgColor]
//        rate.layer.insertSublayer(gradientLayer, at: 0)
        
        addSubview(copyRight)
//        addSubview(share)
        addSubview(rate)
//        addSubview(terms)
//        addSubview(seperator)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        copyRight.frame = CGRect(x: 0,
                                 y: bounds.height - Size.label.. - Size.paddding5.. * 2,
                                 width: bounds.width,
                                 height: Size.label.. + Size.paddding5..)
        
        
        
        share.frame = CGRect(x: Size.padding15..,
                             y: copyRight.frame.minY - Size.button..,
                             width: bounds.width / 2 - Size.padding15.. - Size.padding10.. / 2,
                             height: Size.button..)
        
//        rate.frame = CGRect(x: bounds.width / 2  + Size.padding10.. / 2,
//                            y: copyRight.frame.minY - Size.button..,
//                            width: bounds.width / 2 - Size.padding15.. - Size.padding10.. / 2,
//                            height: Size.button..)
        
        
        let width = Utility.widthForView(rate.titleLabel?.text ?? "",
                                         font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                         height: 20) + 30
        
        rate.frame = CGRect(x:  (bounds.width - max(width, 80)) / 2 ,
                            y: copyRight.frame.minY - Size.button..,
                            width: max(width, 80),
                            height: Size.button..)
        
        terms.frame = CGRect(x: Size.padding15..,
                             y: 0,
                             width: bounds.width - Size.padding15.. * 2,
                             height: 35)
        
        seperator.frame = CGRect(x: Size.padding15..,
                                 y: 0,
                                 width: bounds.width - Size.padding15.. * 2,
                                 height: onePixel())
        
    }
    
    fileprivate func setupButton(_ title: String, titleColor: UIColor = UIColor.Navigation.mainColor()) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
//        button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        button.setTitle(title.uppercased(), for: .normal)
        button.layer.borderColor = titleColor.cgColor
        button.layer.borderWidth = onePixel()
        button.setTitleColor(titleColor, for: .normal)
        button.clipsToBounds = true
        button.layer.cornerRadius = Size.button.. / 2
        return button
    }
}

