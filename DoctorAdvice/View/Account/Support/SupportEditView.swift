//
//  SupportEditView.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 11/15/17.
//  Copyright © 2017 OMT. All rights reserved.
//



import UIKit
import KMPlaceholderTextView

class SupportEditView: UIView {
    
    enum Size: CGFloat {
        case buttonWidth = 80, cornerRadius = 8, padding10 = 10, label = 30, contentWidth = 300, contentHeight = 280, button = 40
    }
    
    var contentView: UIView!
    var labelHeader: UILabel!
    var buttonUpdate: UIButton!
    var subTitle: UILabel!
    var buttonClose: UIButton!
    
    var bgTextField: UIView!
    var textField: UITextField!
    var textView: KMPlaceholderTextView!
    
    
    
    fileprivate var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupAllSubviews()
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupAllSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = CGRect(x: (bounds.width - Size.contentWidth..) / 2 ,
                                   y: (bounds.height - Size.contentHeight..) / 2 - 20,
                                   width: Size.contentWidth..,
                                   height: Size.contentHeight..)
        
        buttonClose.frame = CGRect(x: 0 ,
                                    y: 0,
                                    width: 36,
                                    height: 36)
        
        labelHeader.frame = CGRect(x: 0 ,
                                   y: 0,
                                   width: contentView.frame.width,
                                   height: 36)
        
        bgTextField.frame = CGRect(x:  Size.padding10..  ,
                                   y: labelHeader.frame.maxY + Size.padding10..,
                                   width: contentView.frame.width - Size.padding10.. * 2,
                                   height: 36)
        
        textField.frame = CGRect(x:  Size.padding10.. / 2 ,
                                 y: 0,
                                 width: bgTextField.frame.width - Size.padding10.. ,
                                 height: bgTextField.frame.height)
        

        
        
        
        var widthButton: CGFloat = 80.0
        
        if let label = buttonUpdate.titleLabel, let text = label.text, text.count > 0 {
            widthButton = Utility.widthForView(text,
                                              font: label.font,
                                              height: Size.button..) + 10
            
        }
        

        
        buttonUpdate.frame = CGRect(x:  contentView.frame.width - widthButton - 10 ,
                                    y: contentView.frame.height - Size.button.. - 10,
                                    width: widthButton ,
                                    height: Size.button..)
        
        subTitle.frame = CGRect(x: Size.padding10..,
                                y: buttonUpdate.frame.minY,
                                width: buttonUpdate.frame.minX - Size.padding10.. * 2,
                                height: Size.button..)
        
        textView.frame = CGRect(x: Size.padding10..,
                                y: bgTextField.frame.maxY + Size.padding10.. ,
                                width: contentView.frame.width - Size.padding10.. * 2,
                                height: buttonUpdate.frame.minY - (bgTextField.frame.maxY + Size.padding10.. * 2))
    }
    
    
    @objc func hiddenKeyboard() {
        
        textField.resignFirstResponder()
        textView.resignFirstResponder()
        
    }
    
    @objc func backgroundTap() {
     
        if textField.isFirstResponder || textView.isFirstResponder {
            hiddenKeyboard()
        }
        else {
            dismissView()
        }
    }
}


extension SupportEditView: UITextFieldDelegate {
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textView.becomeFirstResponder()
        return true
    }
    
}


extension SupportEditView: UITextViewDelegate {
    
    
}


extension SupportEditView {
    func setupAllSubviews() {
        
        backgroundColor = UIColor.black.alpha(0.5)
      
        
        labelHeader = setupLabel(UIFont(name: FontType.latoSemibold.., size: FontSize.small++)!,
                                 textColor: .white,
                                 bgColor: UIColor.Navigation.mainColor())
        
        subTitle = setupLabel(UIFont(name: FontType.latoRegular.., size: FontSize.small++)!,
                               textColor: UIColor.Navigation.subColor(),
                               alignment: .left,
                               bgColor: UIColor.clear)
        
        subTitle.text = LocalizedString("support_label_edit_request_need_center_confirm", comment: "Sửa yêu cầu, trung tâm cần xác nhận lại")

  
        buttonUpdate = setupButton(title: LocalizedString("notification_default_update", comment: "Cập nhật"),
                                   titleColor: UIColor.Text.whiteNormalColor())
        
        textField = setupTextField(LocalizedString("support_title_request_support", comment: "Tiêu đề yêu cầu hỗ trợ"))
        textView = setupTextView()
        
        bgTextField = setupView(4, bgColor: UIColor.Table.tableGrayColor())
        bgTextField.layer.borderWidth = onePixel()
        bgTextField.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        contentView = setupView(5, bgColor: .white)
        buttonClose = setupButton(image: Icon.Navigation.Delete.tint(.white))
        buttonClose.backgroundColor = .clear
        buttonClose.addTarget(self, action: #selector(self.dismissView), for: .touchUpInside)
        
        addSubview(contentView)
        contentView.addSubview(textView)
        contentView.addSubview(bgTextField)
        contentView.addSubview(subTitle)
        contentView.addSubview(buttonUpdate)
        contentView.addSubview(labelHeader)
        contentView.addSubview(buttonClose)
        
        bgTextField.addSubview(textField)
        
        contentView.clipsToBounds = true
        
        
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap)))
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard))
        tap.cancelsTouchesInView = true
        contentView.addGestureRecognizer(tap)
        contentView.addGestureRecognizer(UISwipeGestureRecognizer(target: self, action: #selector(self.hiddenKeyboard)))
    }
    
    
    func setupView(_ cornerRadius: CGFloat = 0, bgColor: UIColor = .clear) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        return view
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        return imageView
    }
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                textColor: UIColor = .white,
                                alignment: NSTextAlignment = .center,
                                bgColor: UIColor = .clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    

    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor(red: 130 / 255.0, green: 194 / 255.0, blue: 226 / 255.0, alpha: 1.0)
        return button
    }

    
    func setupTextView() -> KMPlaceholderTextView {
        let textView = KMPlaceholderTextView()
        textView.backgroundColor = UIColor.Table.tableGrayColor()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .none
        textView.clipsToBounds = true
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = onePixel()
        textView.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        textView.delegate = self
        textView.textAlignment = .justified
        textView.placeholderColor = UIColor.lightGray
        textView.placeholderFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textView.placeholder =  LocalizedString("support_label_place_holder_content", comment: "Quý phụ huynh/Học sinh vui lòng điền nội dung và những vấn đề cần hỗ trợ hoặc trao đổi với nhà trường tại đây. Trân trọng cảm ơn!")
        return textView
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String) -> UITextField {
        let textField = UITextField()
        textField.textColor = UIColor.Text.blackMediumColor()
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = false
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        textField.delegate = self
        textField.backgroundColor = .clear
        textField.returnKeyType = .next
        return textField
    }
}



