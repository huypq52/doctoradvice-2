//
//  SupportDetailHeaderView.swift
//  KidOnline
//
//  Created by Pham Huy on 12/3/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class SupportDetailHeaderView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding10 = 10, padding15 = 15, button = 40, avatar = 36, label = 42
    }
    
    var labelStatus: UILabel!
    var buttonClose: UIButton!
    var contentView: UIView!
    var labelTitle: UILabel!
    var labelContent: UITextView!
    var labelNote: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = bounds

        
        let widthStatus: CGFloat = Utility.widthForView(labelStatus.text ?? "", font: labelStatus.font, height: 24) + 20
        labelStatus.frame = CGRect(x: contentView.frame.width - Size.padding10.. / 2 - widthStatus,
                                   y: Size.padding10.. / 2,
                                   width: widthStatus,
                                   height: 24)
        
        labelStatus.layer.cornerRadius = labelStatus.frame.height / 2
        
        labelTitle.frame = CGRect(x: Size.padding10.. / 2,
                                  y: 0,
                                  width: labelStatus.frame.minX - Size.padding10..,
                                  height: 40)
        
        labelContent.frame = CGRect(x: Size.padding10.. / 2 ,
                                    y: labelTitle.frame.maxY + Size.padding10.. / 2  ,
                                    width: contentView.frame.width - Size.padding10.. ,
                                    height: labelNote.frame.minY  - labelTitle.frame.maxY - Size.padding10.. )
        
       
        
        
        let widthButton: CGFloat = Utility.widthForView(buttonClose.titleLabel!.text ?? "", font: buttonClose.titleLabel!.font, height: 38) + 15
        buttonClose.frame =  CGRect(x: contentView.frame.width - Size.padding10.. / 2 - widthButton,
                                    y: contentView.frame.height - 5 - 38,
                                    width: widthButton,
                                    height: 38)
        
        labelNote.frame = CGRect(x: Size.padding10.. / 2 ,
                                 y: contentView.frame.height - 5 - 38,
                                 width: buttonClose.frame.minX - Size.padding10.. ,
                                 height: 38)
        
 
        
    }
}

extension SupportDetailHeaderView {
    
    fileprivate func setup() {
        
        backgroundColor = UIColor.clear
        
        contentView = setupSeperator(UIColor.white)
        
        labelTitle = setupLabel(UIFont(name: FontType.latoSemibold.., size: FontSize.normal++)!,
                                textColor: UIColor.Text.blackMediumColor(), alignment: .left, bgColor: .clear)
        labelNote = setupLabel(UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!,
                                 textColor: UIColor.Text.grayMediumColor(),
                                 bgColor: UIColor.clear)
        
        labelStatus = setupLabel(UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!,
                                 textColor: .white, alignment: .center, bgColor: UIColor.Navigation.mainColor())
        
        
        buttonClose = setupButton("Đóng yêu cầu")
        
        labelContent = setupTextView()

        
        contentView.addSubview(labelTitle)
        contentView.addSubview(labelContent)
        contentView.addSubview(labelNote)
        contentView.addSubview(labelStatus)
        contentView.addSubview(buttonClose)
        addSubview(contentView)
        
        
    }
    
    
    fileprivate func setupLabel(_ font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!,
                            textColor: UIColor = UIColor.Text.blackMediumColor(),
                            alignment: NSTextAlignment = .left,
                            bgColor: UIColor = UIColor.clear) -> UILabel {
        
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = textColor
        label.backgroundColor = bgColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.clipsToBounds = true
        return label
    }
    
    fileprivate func setupTextView() -> UITextView {
        let textView = UITextView()
        textView.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)!
        textView.textColor = UIColor.Text.blackMediumColor()
        textView.isEditable = false
        textView.textAlignment = .justified
        textView.backgroundColor = UIColor.clear
        textView.clipsToBounds = true
        textView.layer.cornerRadius = 5
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .none
        textView.isScrollEnabled = false
        return textView
    }
    

    
    
    fileprivate func setupSeperator(_ color: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        view.clipsToBounds = true
        return view
    }
    
    fileprivate func setupButton(_ title: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: UIControl.State())
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        button.setTitleColor(UIColor.red, for: UIControl.State())
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal--)!
        button.backgroundColor = UIColor(red: 130 / 255.0, green: 194 / 255.0, blue: 226 / 255.0, alpha: 1.0)
        button.backgroundColor = .white
        button.layer.borderWidth = onePixel()
        button.layer.borderColor = UIColor.red.cgColor
        return button
    }
    
    fileprivate func setupTextField(_ textPlaceHoder: String) -> UITextField {
        let textField = UITextField()
        textField.textColor = UIColor.Text.blackMediumColor()
        textField.placeholder = textPlaceHoder
        textField.autocorrectionType = .no
        textField.autoresizingMask = UIView.AutoresizingMask()
        textField.isSecureTextEntry = false
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        
        textField.backgroundColor = .clear
        textField.returnKeyType = .next
        return textField
    }
}





