//
//  SupportSelectionCenterView.swift
//  CenterOnlineForParent
//
//  Created by Pham Huy on 11/14/17.
//  Copyright © 2017 OMT. All rights reserved.
//


import UIKit


class SupportSelectionCenterView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding15 = 15, padding10 = 10
    }
    
    var bgView: UIView!
    var button: UIButton!
    var addImage: UIButton!
    var paddingButton: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
        bgView.frame = CGRect(x: 0,
                              y: -5,
                              width: bounds.width,
                              height: bounds.height)
        
        var width = Utility.widthForView(button.titleLabel?.text ?? "" ,
                                         font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                         height: bounds.height - 5) + Size.padding10.. * 2
        
        if let imageView =  button.imageView {
            if let _ = imageView.image { width += 30 }
        }
        
        
        width = max(width, 80)
        
        button.frame = CGRect(x: bounds.width - Size.padding10.. - width,
                              y: paddingButton,
                              width: width ,
                              height: bounds.height - 5 - paddingButton)
        
        button.layer.cornerRadius = button.frame.height / 2
        
        
        
        var widthAdd = Utility.widthForView(addImage.titleLabel?.text ?? "" ,
                                         font: UIFont(name: FontType.latoBold.., size: FontSize.normal--)!,
                                         height: bounds.height - 5) + Size.padding10.. * 5 + 30

         widthAdd = max(widthAdd, 80)
        
        addImage.frame = CGRect(x: Size.padding10.. ,
                                y: paddingButton,
                                width: min(widthAdd, button.frame.minX - Size.padding10.. * 2) ,
                                height: bounds.height - 5 - paddingButton)
        
        addImage.layer.cornerRadius = addImage.frame.height / 2
   
    }
    
    
    fileprivate func setup() {
        bgView = setupBackgroundView()
        button = setupButton(title: "Gửi", titleColor: UIColor.Navigation.mainColor())

        addImage = setupButton(title: LocalizedString("general_label_add_image", comment: "Thêm ảnh"), image: Icon.General.AddImage.tint(UIColor.Navigation.mainColor()), titleColor: UIColor.Navigation.mainColor())
        
        
        addImage.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10)
        
        addSubview(bgView)
        addSubview(button)
        addSubview(addImage)
        
        clipsToBounds = true
        
        
    }
    
    fileprivate func setupButton() -> UIButton {
        let button = UIButton()
        button.setTitle(LocalizedString("general_button_show_more", comment: "Xem thêm"), for: .normal)
        button.setTitleColor(UIColor.white, for: UIControl.State())
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 130 / 255.0, green: 194 / 255.0, blue: 226 / 255.0, alpha: 1.0)
        return button
    }
    
    
    func setupButton(title: String? = nil,
                     titleSelected: String? = nil,
                     image: UIImage? = nil,
                     imageSelected: UIImage? = nil,
                     titleColor: UIColor = .white) -> UIButton {
        let button = UIButton()
        button.setTitleColor( titleColor, for: .normal)
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.normal..)!
        button.setTitle(title, for: .normal)
        button.setTitle(titleSelected, for: .selected)
        button.setImage(image, for: .normal)
        button.setImage(imageSelected, for: .selected)
        button.clipsToBounds = true
        button.backgroundColor = .white
        
        return button
    }

    
    fileprivate func setupBackgroundView() -> UIView {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.backgroundColor = UIColor.white
        view.clipsToBounds = true
        return view
    }
    
}

