//
//  PersonalHeaderView.swift
//  NguoiBA
//
//  Created by Pham Huy on 5/18/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import PHExtensions
import SwiftyUserDefaults

class PersonalHeaderView: UIView {
    
    fileprivate enum Size: CGFloat {
        case padding3 = 5, padding15 = 15, padding10 = 10, avatar = 120, label = 36, button = 37
    }
    

    var name: UILabel!
    var date: UILabel!
    var seperator: UIView!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        name.frame = CGRect(x: Size.padding15..,
                            y: 0,
                            width: bounds.width - Size.padding15.. * 2,
                            height: Size.label..)
        
        date.frame = CGRect(x: Size.padding15..,
                            y: name.frame.maxY ,
                            width: bounds.width - Size.padding15.. * 2,
                            height: Size.label.. / 2)
        
        
        seperator.frame = CGRect(x: Size.padding15..,
                                 y: bounds.height - onePixel(),
                                 width: bounds.width - Size.padding15.. * 2,
                                 height: onePixel())
    }
    
}

extension PersonalHeaderView {
    fileprivate func setup() {
        
        name = setupLabel()
        date = setupLabel(UIColor.Text.grayMediumColor(), font: UIFont(name: FontType.latoLight.., size: FontSize.normal--)!)
        
       
        seperator = setupView(UIColor.Misc.seperatorColor())
        

        addSubview(name)
        addSubview(date)
        addSubview(seperator)
    }
    
    
    fileprivate func setupAvatar() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }
    
    fileprivate func setupShadowAvatar() -> UIView {
        
        let view = UIView()
        view.clipsToBounds = true
        
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 1.5
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.clipsToBounds = false
        
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        return view
    }
    
    fileprivate func setupLabel(_ textColor: UIColor = UIColor.Text.blackMediumColor(),
                                font: UIFont = UIFont(name: FontType.latoRegular.., size: FontSize.large..)!) -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = textColor
        label.font = font
        return label
    }
    
    fileprivate func setupButton(_ image: UIImage) -> UIButton {
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.layer.cornerRadius = Size.button.. / 2
        button.layer.borderColor = UIColor.Misc.seperatorColor().cgColor
        button.layer.borderWidth = onePixel()

        button.layer.shadowColor = UIColor.black.alpha(0.3).cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowRadius = 4.0
        button.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        button.clipsToBounds = false
        return button
    }
    
    fileprivate func setupButtonLanguage(_ title: String, image: UIImage, tag: LanguageValue, selected: Bool) -> UIButton {
        
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.Text.grayMediumColor(), for: .normal)
        button.setTitleColor(UIColor.Navigation.mainColor(), for: .selected)
        button.titleLabel?.font = UIFont(name: FontType.latoBold.., size: FontSize.normal--)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        button.tag = tag..
        button.isSelected = selected
        return button
    }
    
    func viewHeight() -> CGFloat {
        return  Size.label.. + Size.label.. / 2 + Size.padding10..
    }
}
