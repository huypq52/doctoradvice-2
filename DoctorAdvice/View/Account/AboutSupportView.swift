//
//  AboutSupportView.swift
//  KidOnline
//
//  Created by Pham Huy on 6/12/17.
//  label © 2017 KidOnline. All rights reserved.
//

import UIKit
//import PHExtensions

class AboutSupportView: UIView {
    
    enum Size: CGFloat {
        case  padding15 = 15, padding10 = 10, paddding5 = 5, button = 60, label = 30
    }
    

    
    
    var skpye: UIButton!
    var email: UIButton!
    var phone: UIButton!
    
    var label: UILabel = {
        var label = UILabel()
        label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal--)
        label.textColor = UIColor.Text.blackMediumColor()
        label.textAlignment = .center
        return label
    }()
    
    var seperator: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.Misc.seperatorColor()
        return view
    }()
    
    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // Setup addSubview
    func setup() {
        
        skpye = setupButton("Skype", titleColor: UIColor.Text.grayMediumColor(), image: Icon.About.Skype.tint(UIColor(rgba: "#1ba7e0")))
        
        email = setupButton("Email", titleColor: UIColor.Text.grayMediumColor(), image: Icon.About.Email.tint(UIColor.Navigation.mainColor()))
        
        phone = setupButton("Phone", titleColor: UIColor.Text.grayMediumColor(), image: Icon.About.Phone)
        
        label.text = LocalizedString("about_cell_title_support_online", comment: "Hỗ trợ trực tuyến")
        
        addSubview(label)
        addSubview(skpye)
        addSubview(email)
        addSubview(phone)
//        addSubview(seperator)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        seperator.frame = CGRect(x: Size.padding15..,
                                 y: 0,
                                 width: bounds.width - Size.padding15.. * 2,
                                 height: onePixel())
        
        label.frame = CGRect(x: Size.padding10..,
                                 y: Size.padding10.. ,
                                 width: bounds.width - Size.padding10.. * 2,
                                 height: Size.label.. )
        
        
        
        skpye.frame = CGRect(x: Size.padding10..,
                             y: label.frame.maxY + Size.padding10.. / 2,
                             width: (bounds.width - Size.padding10.. * 2 ) / 3 ,
                             height: Size.button.. )
        
        email.frame = CGRect(x: skpye.frame.maxX,
                             y: label.frame.maxY + Size.padding10.. / 2,
                             width: (bounds.width - Size.padding10.. * 2 ) / 3 ,
                             height: Size.button.. )
        

        phone.frame = CGRect(x: email.frame.maxX,
                             y: label.frame.maxY + Size.padding10.. / 2,
                             width: (bounds.width - Size.padding10.. * 2 ) / 3 ,
                             height: Size.button.. )

        
    }
    
    fileprivate func setupButton(_ title: String, titleColor: UIColor = UIColor.Navigation.mainColor(), image: UIImage) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(name: FontType.latoSemibold.., size: FontSize.small..)
//        button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        button.setTitle(title.uppercased(), for: .normal)
//        button.layer.borderColor = titleColor.cgColor
//        button.layer.borderWidth = onePixel()
        button.setTitleColor(titleColor, for: .normal)
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        Utility.centeredTextAndImageForButton(button)
        return button
    }
}


