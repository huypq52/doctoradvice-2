//
//  NumberFormatter.swift
//  KidsOnline
//
//  Created by Pham Huy on 10/8/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import Charts

class NumberFormatter: Foundation.NumberFormatter {
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


extension NumberFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        return LocalizedString("general_label_month_short", comment: "Thg") + " " + value.toString(0)
        return value.toString(0)
    }
}



class CmNumberFormatter: Foundation.NumberFormatter {
    
    var arrayValue: [Double] = []
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public convenience init(arrayValue: [Double]) {
        self.init()
        
        self.arrayValue = arrayValue
    }
}


extension CmNumberFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        return value.toString(0) + " cm"
        
        let pos = Int(value)
        
        if arrayValue.count > 0, pos < arrayValue.count {
            
            return arrayValue[pos].toString(0)
            
        } else {
             return value.toString(0)
        }
        
       
    }
}

class KgNumberFormatter: Foundation.NumberFormatter {
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


extension KgNumberFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return value.toString(0) + " kg"
    }
}


class RightNumberFormatter: Foundation.NumberFormatter {
    
    var arrayIndex: [(title: Int, value: Double)] = []
    var min: Double = 0
    var max: Double = 0
    
    override init() {
        super.init()
    }
    
    public convenience init(arrayIndex: [(title: Int, value: Double)],
         min: Double,
         max: Double) {
        
        self.init()
  
        self.arrayIndex = arrayIndex
        self.min = min
        self.max = max
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


extension RightNumberFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let step: Double = (max - min) / 25
        for item in arrayIndex {
            let pos = (item.value - min) / step
            if abs(pos - round(value)) <= 0.5  {
                return "\(item.title)"
            }
        }
        
        return ""
    }
}



