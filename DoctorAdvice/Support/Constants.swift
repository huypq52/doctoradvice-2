//
//  Constants.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import CleanroomLogger
import SwiftyUserDefaults

//TODO: Thay ID
let AppleID = "1089820437"
let UrlTermsVN = "http://mgpsapi.binhanh.com.vn/policy_vn.html" //Điều khoản sử dụng Tiếng Việt
let UrlTermsEN = "http://mgpsapi.binhanh.com.vn/policy_en.html" //Điều khoản sử dụng Tiếng Anh

let FirebaseDatabase = "https://schoolonlinechat.firebaseio.com/"

/**
 Enum các controller
 */
enum MainController: Int {
    case home = 0
    case message
    case contact
    case notification
    case account
}

/**
Enum Font Size cho text
*/
enum FontSize: CGFloat {
    case small  = 12
    case normal = 15
    case large  = 18
}

/*
 Các loại Font của Iphone
 */

enum FontType: String {
//    case latoRegular        = "Montserrat-Regular"
//    case latoMedium         = "Montserrat-Medium"
//    case latoSemibold       = "Montserrat-SemiBold"
//    case latoLight          = "Montserrat-Light"
//    case latoBold           = "Montserrat-Bold"
//    case latoBlackItalic    = "Montserrat-BlackItalic"
//    case latoItalic         = "Montserrat-Italic"
    
    case latoRegular        = "Lato-Regular"
    case latoMedium         = "Lato-Medium"
    case latoSemibold       = "Lato-Semibold"
    case latoLight          = "Lato-Light"
    case latoBold           = "Lato-Bold"
    case latoBlackItalic    = "Lato-BlackItalic"
    case latoItalic         = "Lato-Italic"
}




/**
Enum user default

- SelectedLanguage:             Ngôn ngữ của app | Int - Language Enum
- Username:                     Tên người dùng | String
- Email:                        Email người dùng | String
- LoggedIn:                     Xác định xem người dùng đã đăng ký hay chưa để vào Register hoặc Personal | Bool
- UserInfoSynchronized:         Thông tin người dùng đã được sync lên server hay chưa (trường hợp mất mạng ko sync đc) | Bool
- FirstRun:                     Có phải app chạy lần đầu hay ko, nếu là lần đầu sẽ xóa / set một vài giá trị mặc định | Bool
- PhoneNumberForDisplay:        Số điện thoại hiển thị cho người dùng, được format thêm " " cho dễ đọc | String
- RegisterTime:                 Thời gian cuối cùng gửi mã kích hoạt, người dùng được phép gửi lại mã kích hoạt sau 5' | Unix Time
- DatabaseVersion:              Lưu version db của client để sync với server | Int
- DefaultLocation:              Vị trí khởi tạo của map. Được cache lại từ vị trí cuối của map đặt xe | "lat;lng"
- VRConfirmResumeDisplayInfo:   thông tin hiển thị "30' - có 3 xe sẵn sàng đón bạn" trong màn hình vr confirm khi resume lại trạng thái | "số lượng;thời gian"
- ShowIntro:                    tương tự first run nhưng dành cho intro | Bool
- UpdateRequired:               yêu cầu cập nhật [ "Lock" : Int, "Version" : Int] -- lock = 0: false | lock = 1: true

- Path Avatar:                  Đường dẫn ảnh đại diện
*/

extension DefaultsKeys {
    
    static let NotFirstRunV1         = DefaultsKey<Bool>("first_run", defaultValue: false)
    
    static let NotFirstRunV2         = DefaultsKey<Bool>("first_run_v2", defaultValue: false)
    
    static let NotFirstRunV3         = DefaultsKey<Bool>("first_run_v3", defaultValue: false)
    
    static let NotFirstRunV4         = DefaultsKey<Bool>("first_run_v4", defaultValue: false)
    
    static let NotFirstRunV5         = DefaultsKey<Bool>("first_run_v5", defaultValue: false)
    
    static let NotFirstRunV6         = DefaultsKey<Bool>("first_run_v6", defaultValue: false)
    
    static let NotFirstRunV7         = DefaultsKey<Bool>("first_run_v7", defaultValue: false)
    
    static let NotFirstRunV8         = DefaultsKey<Bool>("first_run_v8", defaultValue: false)
    
    static let NotFirstRunV9         = DefaultsKey<Bool>("first_run_v9", defaultValue: false)
    
    static let ShowIntro             = DefaultsKey<Bool>("show_intro", defaultValue: false)
    
    static let SelectedLanguage      = DefaultsKey<Int>("app_language", defaultValue: LanguageValue.vietnamese.rawValue)
    static let UpdateRequired        = DefaultsKey<Bool>("update_required", defaultValue: false)
    static let DeviceKey             = DefaultsKey<String>("app_default", defaultValue: "")
    
    static let DeviceID             = DefaultsKey<String>("device_id", defaultValue: "")
    
    static let TrueName             = DefaultsKey<String>("true_name", defaultValue: "")          // Tên hiển thị
    static let NickName             = DefaultsKey<String>("nick_name", defaultValue: "")          // Tên hiển thị
    static let UserName             = DefaultsKey<String>("user_name", defaultValue: "")          // Tên đăng nhập
    static let Password             = DefaultsKey<String>("password_default", defaultValue: "")   // Mật khẩu
    static let RememberMe           = DefaultsKey<Bool>("remember_me", defaultValue: true)          // Ghi nhớ tên đăng nhập
    static let IsLogin              = DefaultsKey<Bool>("is_login", defaultValue: false)             // Đã login
    static let BirthDay             = DefaultsKey<Double>("birth_day", defaultValue: 0)          // Ngày sinh
    
    
    static let Token                = DefaultsKey<String>("token_server_response", defaultValue: "") // Token server trả về
    static let StudentID            = DefaultsKey<Int>("student_id", defaultValue: 0)               // Mã lớp
    static let ClassID              = DefaultsKey<Int?>("class_id")               // Mã lớp
    static let SchoolID             = DefaultsKey<Int>("school_id", defaultValue: 0)               // Mã trường
    static let UserID               = DefaultsKey<Int>("user_id", defaultValue: 0)  
    
    
    static var WillUpdate           = DefaultsKey<[String : Any]?>("will_update")    // ID bài viết cần show
    
    static let NotificationData     = DefaultsKey<[String : AnyObject]?>("will_update_post")    // ID bài viết cần show
    
    static let LastUpdateTime       = DefaultsKey<Double>("last_update_time", defaultValue: 0) // Thoi gian update gan nhat
    
    static let LastUpdatePopupNewsTime       = DefaultsKey<Double>("last_update_popup_news_time", defaultValue: 0) // Thoi gian update gan nhat
    
    static let WillUpdateNotification = DefaultsKey<Bool>("will_update_notificaion", defaultValue: true)    // ID bài viết cần show
    
    
    static var LastTimeLogin       = DefaultsKey<Double>("last_time_login", defaultValue: 0)    // Lần cuối đăng nhập
    
    static let DataLogin           = DefaultsKey<Data?>("data_login")    // Dữ liệu hàm login trả về
    
    static let DataHome            = DefaultsKey<Data?>("data_home")    // Dữ liệu hàm home trả về
    
    static let DataContact         = DefaultsKey<Data?>("data_contact")    // Dữ liệu hàm home trả về
    
    static let DataCornerKids      = DefaultsKey<Data?>("data_corner_kids")    // Dữ liệu hàm Corner Kids trả về
    
    static let LastCornerID        = DefaultsKey<Int?>("data_corner_id")    // Id của corner cuối lưu
    /// Biến check show help
    
    static let IsShowQuickHelpCorner = DefaultsKey<Bool>("is_show_quick_help_corner", defaultValue: false)    // ID bài viết cần show
    
    
    /// Biến đếm số lượng show yêu cầu bật notificaiton
    static let NumberShowDialogNotification = DefaultsKey<Int>("number_show_dialog_notificaiton", defaultValue: 0)
    

    
    
    /*
     Dành riêng cho bản DEV: Sử dụng server thật hay test
     */
    static let UseServerTest =       DefaultsKey<Bool>("user_server_test", defaultValue: false)
    

    
    // Lần cuối update Album
    static let AlbumLastTime        = DefaultsKey<Double>("album_update_last_time", defaultValue: 0)
    
    /// Biến xác định kiểu hiển thị album
    static let AlbumViewType        = DefaultsKey<Int>("album_view_type", defaultValue: 1)
    
    
    /// Biến xác định có hiển thị gợi ý khảo sát hay không
    static let ShowSurveySuggest     = DefaultsKey<Bool>("show_survey_suggest", defaultValue: true)
    
    

    
    /// Danh sách các màn Quick Help đã hiển thị
    static let ArrayHelpDisplayed   = DefaultsKey<[Int]>("array_help_displayed", defaultValue: [])
    
    
    /// Lưu danh sách các userID đã xóa Album cũ
    static let UserDeleteAlbum  = DefaultsKey<[Int]>("user_delete_album", defaultValue: [])
    
    
    
    /// QUÊN MẬT KHẨU
    static let OTPExpiry       = DefaultsKey<Double>("otp_expiry", defaultValue: 0)
    
    static let OTPToken        = DefaultsKey<String>("otp_token", defaultValue: "")
    
    static let SchoolOTPToken  = DefaultsKey<String?>("school_otp_token")
    
    static let RegisterTime    = DefaultsKey<Double>("register_time", defaultValue: 0)
    
    static let UserNameForgot  = DefaultsKey<String>("user_name_forgot", defaultValue: "")
    
}

 


let RealmKey = "DAUnjyfTbtP9wpxPbrQUw8tHV7h2n76EP36jxEfbGF3N3d7uvI8b32atZ9lX21Du".data(using: String.Encoding.utf8)!


#if DEV
    
var DomainKid: String {
    return Defaults[.UseServerTest] ?  "https://whoops.ko.edu.vn/" : "https://komt.kidsonline.edu.vn/"

}
    

    
#else
let DomainKid = "https://komt.kidsonline.edu.vn/" // RELEASE
    
#endif



/**
 SDT Hotline KidsOnline
 */
let KidsPhoneNumber = "1900 0362"


enum LanguageValue: Int {
    case vietnamese = 0
    case english    = 1
    
    var titlte: String {
        switch self {
        case .vietnamese:
            return "Tiếng Việt"
        case .english:
            return "English"
        }
    }
    
    var code: String {
        
        switch self {
        case .vietnamese:
            return "vi"
        case .english:
            return "en"
        }
    }
    
    var icon: UIImage {
        switch self {
        case .vietnamese:
            return Icon.Login.FlagVN
        case .english:
            return Icon.Login.FlagEN
        }
    }
}


enum AppKeyChain: String {
    case UUID         = "appuuid"
    case DatabaseKey  = "db_key"
    case DatabaseName = "db_name"
}

enum AppNotification: String {
    case LocationServiceChanged         = "location_service_changed"
    case ChangedStudent                 = "changed_student"
    case ReloadContact                  = "reload_contact"
    case ReloadMessage                 = "reload_message"
    case ReloadOnline                   = "reload_online"
  
    case Logout                         = "logout"
    
    case WillUpdate                     = "notification_will_update"
    
    case WillUpdateNotification         = "will_reload_notification"
    
    
    case ReloadTableContact             = "reload_table_contact"
    
    case ReloadItemTabbar               = "reload_item_tabbar"
    
    case ReloadWeightHeightPageView     = "reload_weight_height_page_view"
    
    
    case ScrollToTop                    = "scroll_to_top"
    
    case updateItemTimeline             = "update_item_timeline"
    
    case updateUserInfo                 = "update_user_info"
    
    
}


struct LimitedValue {
    
    let maxShowHelp: Int = 1
    
}






//// ENUM ANALYTICS
enum AnalyticsType: Int {
    case post = 1
    case dailyActivity
    case diary
    case album
    case medicineNote
    case absentRequest
    case heightWeight
    case diseaseHistory
    case generalHealth
    case registerMeal
    case registerTransport
    case registerExtracurricular
    case fee
    case teacherProfile
    case morningMessage
    case camera
    case homePage
    case comment
    case receiveNotification
    case openNotification
    case uniqueOpenNotification
    case studentMessage = 27
    
    func toInfo() -> (termID: Int, contentType: String) {
    
        switch self {
        case .post:
            return (1, "post")
            
        case .dailyActivity:
            return (2, "daily_activity")
            
        case .diary:
            return (3, "diary")
            
        case .album:
            return (4, "album")
            
        case .medicineNote:
            return (5, "medicine_note")
            
        case .absentRequest:
            return (6, "absent_request")
            
        case .heightWeight:
            return (7, "height_weight")
            
        case .diseaseHistory:
            return (8, "disease_history")
            
        case .generalHealth:
            return (9, "general_health")
            
        case .registerMeal:
            return (10, "register_meal")
            
        case .registerTransport:
            return (11, "register_transport")
            
        case .registerExtracurricular:
            return (12, "register_extracurricular")
            
        case .fee:
            return (13, "fee")
            
        case .teacherProfile:
            return (14, "teacher_profile")
            
        case .morningMessage:
            return (15, "morning_message")
            
        case .camera:
            return (16, "camera")
            
        case .homePage:
            return (17, "home_page")
            
        case .comment:
            return (18, "Comment")
            
        case .receiveNotification:
            return (19, "receive_notification")
            
        case .openNotification:
            return (20, "open_notification")
            
        case .uniqueOpenNotification:
            return (21, "unique_open_notification")
            
            
        case .studentMessage:
            return (27, "student_message")
        }
    }
    
    
}




