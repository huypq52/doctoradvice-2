//
//  Module.swift
//  KidOnline
//
//  Created by Pham Huy on 5/20/19.
//  Copyright © 2019 KidOnline. All rights reserved.
//



import UIKit

public struct TimeFormat : OptionSet {
    public let rawValue: Int
    public init(rawValue: Int) { self.rawValue = rawValue }
    
    public static let None         = TimeFormat(rawValue: 0)
    public static let Hour         = TimeFormat(rawValue: (1 << 0))
    public static let Minute       = TimeFormat(rawValue: (1 << 1))
    public static let Second       = TimeFormat(rawValue: (1 << 2))
    public static let OptionalHour = TimeFormat(rawValue: (1 << 3))
    public static let AbsoluteTime = TimeFormat(rawValue: (1 << 4))
    
}


enum Module: Int {
    
    case parentPost                     = 1  //  Tin tức - Bài viết
    case dailyActivity                  = 2  //  Hoạt động hằng ngày
    case childDiary                     = 3  //  Nhật ký cho con
    case parentAlbum                    = 4  //  Album ảnh
    case parentMedicineNote             = 5  //  Dặn thuốc
    case parentAbsentRequest            = 6  //  Đơn xin nghỉ
    case healthHistories                = 7  //  Chiều cao - cân nặng
    case diseaseHistories               = 8  //  Lịch sử bệnh
    case generalHealth                  = 9  //  Sức khỏe chung
    case mealRegister                   = 10 //  Dịch vụ- Đăng ký ăn
    case pickOff                        = 11 //  Dịch vụ- Đăng ký đưa đón
    case extracurricularRegister        = 12 //  Dịch vụ- đăng ký ngoại khóa (cũ)
    case tuitionFee                     = 13 //  Dịch vụ- Học phí
    case teacherProfile                 = 14 //  Hồ sơ giáo viên
    case parentChat                     = 15 //  Tin nhắn
    case camera                         = 16 //  Camera
    case morningMessages                = 17 //  Lời nhắn đầu ngày
    case kidsonlineCorner               = 18 //  Góc KidsOnline
    case parentSurvey                   = 19 //  Khảo sát
    case termAssessment                 = 20 //  Đánh giá định kỳ
    case parentPromotions               = 21 //  Khuyến mãi
    case advanceExtracurricularRegister = 23 //  Dịch vụ- Đăng ký ngoại khóa mới
    case parentLockCommentPost          = 24 //  Bài viết- Khóa Comment
    case parentLockCommentAlbum         = 25 //  Khóa comment album ảnh
    case diningNotification             = 26 //  Thông báo hoạt động ăn
    case studentMessage                 = 27 //  Lời nhắn cho học sinh
    case contactConnectParent           = 29 //  Kết nối danh bạ phụ huynh với phụ huynh
    case contactShow                    = 30 //  Hiện danh bạ trên app Phụ huynh
    
    
    //// Module Fake
    case health    = 100 /// Sức khỏe
    case message   = 101

    

    /**
     Icon của các module
     */
    
    var image: UIImage? {
        switch self {
            
            
        case .parentPost:                       return UIImage(named: "ic_module_post") ?? UIImage()
        case .dailyActivity:                    return UIImage(named: "ic_module_daily_activity") ?? UIImage()
        case .childDiary:                       return UIImage(named: "ic_module_") ?? UIImage()
        case .parentAlbum:                      return UIImage(named: "ic_module_album") ?? UIImage()
        case .parentMedicineNote:               return UIImage(named: "ic_module_medicine_note") ?? UIImage()
        case .parentAbsentRequest:              return UIImage(named: "ic_module_absent_request") ?? UIImage()
        case .healthHistories:                  return UIImage(named: "ic_module_health") ?? UIImage()
        case .diseaseHistories:                 return UIImage(named: "ic_module_health") ?? UIImage()
        case .generalHealth:                    return UIImage(named: "ic_module_health") ?? UIImage()
        case .mealRegister:                     return UIImage(named: "ic_module_services") ?? UIImage()
        case .pickOff:                          return UIImage(named: "ic_module_pick_off") ?? UIImage()
        case .extracurricularRegister:          return UIImage(named: "ic_module_extracurricular") ?? UIImage()
        case .tuitionFee:                       return UIImage(named: "ic_module_learn_fee") ?? UIImage()
        case .teacherProfile:                   return UIImage(named: "ic_module_") ?? UIImage()
        case .parentChat:                       return UIImage(named: "ic_module_feedback") ?? UIImage()
        case .camera:                           return UIImage(named: "ic_module_") ?? UIImage()
        case .morningMessages:                  return UIImage(named: "ic_module_morning_message") ?? UIImage()
        case .kidsonlineCorner:                 return UIImage(named: "ic_module_") ?? UIImage()
        case .parentSurvey:                     return UIImage(named: "ic_module_survey") ?? UIImage()
        case .termAssessment:                   return UIImage(named: "ic_module_term") ?? UIImage()
        case .parentPromotions:                 return UIImage(named: "ic_module_term") ?? UIImage()
        case .advanceExtracurricularRegister:   return UIImage(named: "ic_module_extracurricular") ?? UIImage()
        case .parentLockCommentPost:            return UIImage(named: "ic_module_") ?? UIImage()
        case .parentLockCommentAlbum:           return UIImage(named: "ic_module_") ?? UIImage()
        case .diningNotification:               return UIImage(named: "ic_module_") ?? UIImage()
        case .studentMessage:                   return UIImage(named: "ic_module_feedback") ?? UIImage()
            

        case .contactConnectParent:             return UIImage(named: "ic_module_contact") ?? UIImage()
        case .contactShow:                      return UIImage(named: "ic_module_contact") ?? UIImage()
            
            
        /// Module Fake
            
        case .health:                  return UIImage(named: "ic_module_health") ?? UIImage()
        case .message:                 return UIImage(named: "ic_module_morning_message") ?? UIImage()
        }
    }
    
    
    var imageMenu: UIImage? {
        
        var imageTemp: UIImage? = nil
        
        switch self {
        case .contactShow:
            imageTemp = UIImage(named: "ic_menu_contact")
            
        default:
            break
        }
        
        return imageTemp ?? self.image
    }

    
    
    var title: String {
        
//        if let module = AppData.instance.modules.first(where: { $0.module == self.. }) {
//            return module.label
//        }
        
        //TODO: dịch
        
        switch self {
        case .parentPost:
            return "Tin tức - Bài viết"
            
        case .dailyActivity:
            return "Hoạt động hàng ngày"
            
        case .childDiary:
            return "Nhật ký cho con"
            
        case .parentAlbum:
            return "Album ảnh"
            
        case .parentMedicineNote:
            return "Dặn thuốc"
            
        case .parentAbsentRequest:
            return "Đơn xin nghỉ"
            
        case .healthHistories:
            return LocalizedString("timeline_label_weight_height", comment: "Chiều cao - cân nặng")
            
        case .diseaseHistories:
            return "Lịch sử bệnh"
            
        case .generalHealth:
            return "Sức khỏe chung"
            
        case .mealRegister:
            return "Đăng ký ăn"
            
        case .pickOff:
            return "Đăng ký đưa đón"
            
        case .extracurricularRegister:
            return "Đăng ký ngoại khóa (cũ)"
            
        case .tuitionFee:
            return "Học phí"
            
        case .teacherProfile:
            return "Hồ sơ giáo viên"
            
        case .parentChat:
            return "Tin nhắn"
            
        case .camera:
            return "Camera"
            
        case .morningMessages:
            return "Lời nhắn"
            
        case .kidsonlineCorner:
            return "Góc KidsOnline"
            
        case .parentSurvey:
            return "Khảo sát"
            
        case .termAssessment:
            return "Đánh giá định kỳ"
            
        case .parentPromotions:
            return "Khuyến mãi"
            
        case .advanceExtracurricularRegister:
            return "Đăng ký ngoại khóa mới"
            
        case .parentLockCommentPost:
            return "Bài viết- Khóa Comment"
            
        case .parentLockCommentAlbum:
            return "Khóa comment album ảnh"
            
        case .diningNotification:
            return "Thông báo hoạt động ăn"
            
        case .studentMessage:
            return "Lời nhắn cho học sinh"
            
        case .contactConnectParent:
            return "Danh bạ"
            
        case .contactShow:
            return "Danh bạ" ///"Hiện danh bạ trên app Phụ huynh"
            
        /// Module Fake
        case .health:
            return "Sức khỏe"
        
        case .message:
            return "Lời nhắn"

        }
    }
}

