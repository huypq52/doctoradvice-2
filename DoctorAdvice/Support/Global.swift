//
//  Global.swift
//  G5TaxiUser
//
//  Created by NangND on 9/8/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//
////import CleanroomLogger
import UIKit
//import CleanroomLogger


func crashApp(message:String) -> Never  {
    let log = "CRASH - " + message
//    print( log)
    fatalError(log)
}

func onePixel() -> CGFloat {
    return 1 / UIScreen.main.scale
}

func networkReachable() -> Bool {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    return delegate.reachability.connection != .unavailable
}

func getBuildVersion() -> Int {
    return Int((Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String))!
}

/*
enum SystemVersion {
    case iOS7
    case iOS8AndAbove
}

func systemVersion() -> SystemVersion {
    switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
    case .OrderedSame, .OrderedDescending:
        return .iOS8AndAbove
    case .OrderedAscending:
        return .iOS7
    }
}
*/

//func arc4random <T: IntegerLiteralConvertible> (type: T.Type) -> T {
//    var r: T = 0
//    arc4random_buf(&r, UInt(sizeof(T)))
//    return r
//}
