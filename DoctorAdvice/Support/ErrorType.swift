//
//  ErrorType.swift
//  PhuongTrangTaxi
//
//  Created by Hoan Pham on 12/11/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation

public enum HTTPError: Error {
    
    case unknown
    /// Hoạt động ăn
    
    case updateRequired
    case notAttachStudent // "Tài khoản chưa được gán con"
    
    case notValidEncryptedValue     ///  24381  Dữ liệu mã hóa sai cấu trúc quy định
    
    case missingParameters        /// 2 Thiếu dữ liệu gửi lên
    case unauthorized        /// 3 Bạn không phải là phụ huynh của trường
    case requestUnsuccessfully        /// 4  Yêu cầu thực thi không thành công
    case generalError        /// 5 Lỗi chung - Hiển thị thông báo theo key data của dữ liệu trả về
    case actionUnsuccessfully        /// 6 Vui lòng thực hiện lại
    
    
    case notFoundUserFromToken    ///101 Không tìm thấy người dùng từ token
    case tokenExpired    ///102 Token hết hạn
    case invalidToken    ///103 Token khônghợp lệ
    case tokenException    ///104 Lỗi token (Token Exception)
    case tokenAbsent    ///105 Không tìm thấy token trong request
    case invalidTimestamp    ///201 Timestamp không hợp lệ (parse bị lỗi)
    case invalidDateValue    ///202 Định dạng ngày không hợp lệ
    case dateIsOffDay    ///203 Hôm nay là ngày nghỉ của con, bố mẹ vui lòng chọn ngày khác để xem
    case dateIsHolidayDate    ///204 Hôm nay là ngày nghỉ lễ, bố mẹ vui lòng chọn ngày khác để xem
    case fromDateGreaterThanToDate    ///205 Ngày bắt đầu phải nhỏ hơn ngày kết thúc, vui lòng chọn lại!
    case dateCanNotGreaterThanToDay    ///206 Ngày kết thúc phải bằng ngày hiện tại, vui lòng chọn lại!
    case invalidDateRange    ///207 Phạm vi của ngày quá lớn. Vui lòng chọn khoảng cách bé hơn
    case dateInThePast    ///208 Ngày trong quá khứ không hợp lệ
    case lockedUser    ///301 Tài khoản của bạn đã bị khóa vui lòng liên hệ hotline KidsOnline 1900.0362 để được hỗ trợ!
    case notHaveGuardianRole    ///302 Bạn không phải là phụ huynh của trường
    case wrongPassword    ///303 Mật khẩu không hợp lệ
    case notExistsUser    ///304 Tài khoản của bạn hiện không tồn tại, vui lòng liên hệ BQT nhà trường để được hỗ trợ
    case notFoundClass    ///401 Bé chưa được sắp lớp, vui lòng liên hệ BQT nhà trường để được hỗ trợ
    case notFoundSchool    ///402 Không tìm thấy trường
    case notFoundUsername    ///403 Không tìm thấy tên tài khoản
    case notFoundStudent    ///404 không tìm thấy học sinh
    case notFoundAbsentRequest    ///405 Không tìm thấy đơn xin nghỉ
    case notFoundMedicineNote    ///406 Không tìm thấy đơn dặn thuốc
    case notFoundMessage    ///407 Không tìm thấy lời nhắn
    case notFoundPhotoType    ///408 Không tìm thấy kiểu của ảnh
    case notFoundPhoto    ///409 Không tìm thấy đối tượng của ảnh
    case notFoundAlbum    ///411 Không tìm thấy album ảnh
    case notFoundItem    ///412 Không tìm thấy đối tượng hoặc đối tượng đã bị xóa
    case notFoundItemComment    ///413 Không tìm thấy đối tượng của comment
    case notFoundItemParentComment    ///414 Không tìm thấy comment cha của phản hồi (khi phản hồi comment)
    case notFoundSurvey    ///415 Không tìm thấy bài khảo sát
    case notFoundStudentTransportTicketDay    ///416 Không tìm thấy đơn đưa đón của học sinh này
    case notFoundStudentTransport    ///417 Không tìm thấy người đưa đón này
    case notFoundHeightWeightRecord    ///418 Không tìm thấy bản ghi chiều cao - cân nặng của học sinh
    case notFoundCornerCategory    ///419 Không tìm thấy chuyên mục trong góc tin tức
    
    case notFoundTuitionNotice       /// 423  Không tìm thấy thông báo phí của học sinh
    case notFoundTransaction       /// 424 Không tìm thấy giao dịch này
    
    case studentNoClass    ///501 Học sinh không có lớp
    case studentAbsenceInDate    ///502 Hôm nay con nghỉ học, bố mẹ vui lòng chọn ngày khác để xem
    case studentHaveNotJoinedClass    ///503 Học sinh chưa được phân lớp trong ngày đã chọn
    
    
    case duplicateAbsentRequest    ///601 Đã có đơn xin nghỉ bị trùng với ngày đã chọn
    case ticketInPastCanNotDelete    ///602 Đơn đã xử lý trong quá khứ không được phép xóa
    case duplicateMedicineNote    ///603 Đã có đơn dặn thuốc trùng với ngày đã chọn
    case duplicateMorningMessage    ///604 Đã có lời nhắn trùng với ngày đã chọn
    case cannotUpdateTicketInPast    ///605 Không được phép cập nhật đơn đã hết hạn sử dụng
    
    case contentNotNull       /// 606 Nội dung lời nhắn không được để trống
    case submitWrongApplication       /// 607 Gửi đơn sai quy định
    
    
    
    case uploadError    ///703 Tải ảnh lỗi
    case deletePhotoUnsuccessfully    ///704 Xóa ảnh không thành công
    case photoNotBelongsToItem    ///706 Ảnh không thuộc về đối tượng đã gửi
    case rotatePhotoUnsuccessfully    ///707 Xoay ảnh không thành công
    case updatePhotoDescriptionUnsuccessfully    ///708 Cập nhật tiêu đề ảnh không thành công
    
    
    case invalidAlbum    ///801 Thiếu mã album ảnh
    
    case invalidLikeCommentType    ///902 Kiểu like / comment không hợp lệ
    case invalidCommentContent    ///903 Nội dung comment không hợp lệ (thường do để trống)
    case likeCommentActionUnsuccessfully    ///904 Thao tác không thành công khi like / comment
    case notAuthorizedToComment    ///905 Không có quyền thao tác với comemnt
    case userHasLikedContent    ///908 Người dùng đã thích nội dung (khi gửi like với ndung đã like)
    case userHaveNotLikedContent    ///909 Người dùng chưa thích nội dung (khi unline với ndung chưa like)
    
    
    case canNotAnswerSurvey    ///1001 Không trả lời được khảo sát này
    
    case invalidAccountName    ///1101 Vui lòng nhập tên
    case invalidEmail    ///1102 Email không đúng định dạng
    case existUsingEmail    ///1103 Email này đã được sử dụng
    case invalidDob    ///1104 Ngày sinh không đúng định dạng
    case invalidAvatar    ///1105 Ảnh đại diện không đúng định dạng
    case invalidPhone    ///1106 Số điện thoại không đúng định dạng
    case notCorrectOldPassword    ///1107 Mật khẩu cũ không chính xác
    
    
    case otpNotFound    ///1120 Không tìm thấy mã OTP này
    case otpWasUsed    ///1121 OTP đã được sử dụng
    case otpWasExpired    ///1122 OTP đã hết hạn
    case creatingOtpWasLimited    ///1123 Đã hết lượt lấy OTP trong hôm nay, vui lòng liên hệ tổng đài
    case notFoundAccountByPhone /// 1124  Không tìm thấy tài khoản với số điện thoại bạn nhập
    
    case notFoundAccountByUsername       ///  1125 Không tìm thấy tài khoản này
    case accountNotHasPhoneNumber        /// 1126 Tài khoản chưa đăng ký số điện thoại để gửi mã OTP. Vui lòng liên hệ với tổng đài
    
    case  invalidPassword        /// 1127 Mật khẩu không hợp lệ
    
    
    case noHeightWeightRecords    ///1201 Học sinh không có thông tin chiều cao - cân nặng
    case invalidHeightWeightValue    ///1202 Giá trị cheièu cao - cân nặng không hợp lệ (cân nặng > chiều cao)
    case hasRecordHeightWeightOnDate    ///1203 Học sinh đã có bản ghi chiều cao - cân nặng trong ngày
    
    case noTimelineItem    ///1301 Hiện tại không có item nào trên timeline
    
    case exitStudentTransportTicket    ///1401 Đơn đưa đón của hs đã có
    case notCreatedInThePast    ///1402 Không được tạo đơn đưa đón trong quá khứ
    
    
    case promotionNotFound    ///1501 Không tìm thấy chương trình quà tặng này
    case promotionValidation    ///1502 Thiếu thông tin xử lý hành động này
    case hasCode    ///1503 Bạn đã nhận mã chương trình này trước đó
    case overCode    ///1504 Hệ thống đã hết mã
    case notActive    ///1505 Tài khoản của bạn chưa được kích hoạt
    case notCommentPost    ///1506 Bạn chưa có bình luận vào bài viết nào trong khoảng thời gian này
    case notCommentAlbum    ///1507 Bạn chưa có bình luận vào album ảnh nào trong khoảng thời gian này
    case notLikePost    ///1508 Bạn chưa bấm thích bài viết nào trong khoảng thời gian này
    case notLikeAlbum    ///1509 Bạn chưa bấm thích album ảnh nào trong khoảng thời gian này
    case notSendMorningMessage    ///1510 Bạn chưa gửi lời nhắn đầu ngày nào trong khoảng thời gian này
    case notSendThanksMessage    ///1511 Bạn chưa gửi lời cảm ơn nào trong khoảng thời gian này
    case notSendMessage    ///1512 Bạn chưa gửi lời nhắn nào trong khoảng thời gian này
    case locked    ///1513 Tài khoản hiện tại không khả dụng để nhận mã
    case notActiveInRange    ///1514 Tài khoản chưa kích hoạt trong thời gian diễn ra chương trình
    case noUserIds    ///1515 Học sinh không có phụ huynh hoặc mã phụ huynh không hợp lệ
    case promotionValidationForm    ///1516 Thông tin bạn cung cấp không hợp lệ
    case invalidPhoneAndEmail    ///1517 Chưa nhập địa chỉ email và số điện thoại
    case invalidName    ///1518 Chưa nhập tên người dùng
    case conflictPhoneOrEmail    ///1519 Số điện thoại hoặc email khác với thông tin trong tài khoản
    
    case extracurricularRegistered  /// 1601 Ngoại khóa đã đăng ký
    
    
    
    
    
    case canNotPayment        /// 1701 Không được thực hiện giao dịch này
    case transactionIsDismissed        /// 1702 Giao dịch đã bị hủy
    case transactionIsSuccessfully      /// 1703  Giao dịch thành công
    case transactionIsFailed        /// 1704 Giao dịch thất bại
    case transactionWasHandled       /// 1705 Giao dịch này đã được thực hiện trước đó
    
    
}





