//
//  Extension.swift
//  NguoiBA
//
//  Created by Pham Huy on 5/18/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
//import RealmSwift
import SwiftyUserDefaults
import Alamofire
import Kingfisher


extension Bool {
    mutating func toggle() {
        self = !self
    }
}

//extension Results {
//    func toArray() -> [Results.Iterator.Element] {
//        return Array(self)
//    }
//}

extension Data {
    public var hexadecimalString: String {
        return UnsafeBufferPointer<UInt8>(start: (self as NSData).bytes.bindMemory(to: UInt8.self, capacity: self.count), count: self.count)
            .map { String(format: "%02X", $0) }
            .joined(separator: "")
        
    }
    
    
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Date {
    var startOfWeek: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
}


extension Date {
//    var startOfWeek: Date? {
//        let gregorian = Calendar(identifier: .gregorian)
//        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
//        return gregorian.date(byAdding: .day, value: 1, to: sunday)
//    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}


extension Double {
    /**
     Lấy string từ Time
     */
    public func stringFrom(_ formatter: DateFormatter) -> String {
        return formatter.string(from: Date(timeIntervalSince1970: self))
    }
    
}

extension UIScrollView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if AppData.instance.gestureRecognizerShouldReceiveTouch { return true }
        
        let location = touch.location(in: self)
        var rect = self.bounds
        rect.origin.x += 25
        return rect.contains(location)
    }
}


extension UIImage {
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }

    
}

extension UIImageView {
    func downloadedFrom(url: URL,
                        size: CGSize? = nil,
                        tag: Int = 0,
                        contentMode mode: UIView.ContentMode? = nil,
                         placeHolder: UIImage? = nil,
                        completionHandler: ((UIImage) -> Swift.Void)?) {


        
        let modifier = AnyModifier { request in
            var r = request
            // replace "Access-Token" with the field name you need, it's just an example
            r.setValue("bearer \(Defaults[.Token])", forHTTPHeaderField: "Authorization")
            return r
        }
        
        // Downsampling
        
        
        var options: KingfisherOptionsInfo? = [.requestModifier(modifier),
                                               .transition(.fade(0.25))]
        
        
        
        if let size = size {
            let processor = ResizingImageProcessor(referenceSize: size, mode: .aspectFill)
            options?.append(.processor(processor))
        }
    

        self.kf.setImage(with: url,
                         placeholder: placeHolder ?? Icon.General.CoverDefault,
                         options: options)
        { (image, error, cache, url) in
            
            guard let image = image, let completion = completionHandler else { return }
            completion(image)
    
        }
    }
    

    func downloadedFrom(link: String,
                        size: CGSize? = nil,
                        tag: Int = 0,
                        contentMode mode: UIView.ContentMode? = nil,
                        placeHolder: UIImage?,
                        completionHandler: ((UIImage) -> Swift.Void)? = nil) {

        guard let url = link.getURL() else { return }
        
        downloadedFrom(url: url, size: size, tag: tag, contentMode: mode, placeHolder: placeHolder, completionHandler: completionHandler)
    }
    
    
    
    func getSizeScaleWith(_ image: UIImage) -> CGSize {
        
        
        guard image.size.width > self.frame.width || image.size.height > self.frame.height else {
            return image.size
        }
        
        guard self.frame.width != 0 && self.frame.height != 0 else { return self.frame.size }
        
        
        var newSize: CGSize
        
        let maxSize: CGFloat = UIScreen.main.bounds.width - 20
        
        /**
         *  Resize xuống 720 x 720
         */
        if image.size.width >= image.size.height {
            newSize = CGSize(width: maxSize,
                             height: maxSize * image.size.height / image.size.width)
            
        } else {
            newSize = CGSize(width: maxSize * image.size.width / image.size.height,
                             height: maxSize)
        }
        
        
        return newSize
    }
}

extension UIButton {
    
    
    func downloadedFrom(url: URL,
                        contentMode mode: UIView.ContentMode? = nil,
                        state: UIControl.State = .normal,
                        completionHandler: ((UIImage) -> Swift.Void)?) {
        
        if let modeNew = mode {
            self.contentMode = modeNew
        }
        
        let cache = URLCache.shared
        if let data = cache.cachedResponse(for: URLRequest(url: url)),let image = UIImage(data: data.data) {
            self.setImage(image, for: state)
            guard let completion = completionHandler else { return }
            completion(image)
            return
        }
        

        //        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
        
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = ["Authorization": "bearer \(Defaults[.Token])"]
        

        
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (responseData, responseUrl, error) -> Void in
            guard let data = responseData, data.count > 0 else { return }
            DispatchQueue.main.async(execute: { () -> Void in
                if let image = UIImage(data: data) {
                    
                    let transition:CATransition = CATransition()
                    transition.duration = 0.3
                    transition.type = CATransitionType.fade
                    self.imageView?.layer.add(transition, forKey: nil)
                    self.setImage(image, for: state)
                    
                    guard let completion = completionHandler else { return }
                    completion(image)
                }
                
                let respond = CachedURLResponse(response: responseUrl ?? URLResponse(), data: data)
                cache.storeCachedResponse(respond, for: URLRequest(url: url))
                
            })
        })
        task.resume()
    }
    
    
    func downloadedFrom(link: String,
                        contentMode mode: UIView.ContentMode? = nil,
                        state: UIControl.State = .normal,
                        completionHandler: ((UIImage) -> Swift.Void)? = nil) {
        

        guard let url = link.getURL() else { return }
    
        downloadedFrom(url: url, contentMode: mode, state: state, completionHandler: completionHandler)
    }
}


extension String {
    
    
    var toAttributedString: NSAttributedString? {
        
        
        ///TODO: check nếu ios 8 thì bỏ dòng encoding
        
        
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(FontSize.normal.rawValue - 1)\">%@</span>", self)
        
        guard let data = modifiedFont.data(using: .utf8, allowLossyConversion: false) else { return nil }
        do {
//            return try NSAttributedString(data: data,
//                                          options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
//                                          documentAttributes: nil)
//            
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey("CharacterEncoding") : String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
            
        } catch {
            return nil
        }
        
    }
    
    
    /// Lấy link Full
    func getURL() -> URL? {
        
        var fullLink: String = self.hasPrefix("http") ? self : DomainKid + self
        
        fullLink = fullLink
            .replacingOccurrences(of: " ", with: "%20")
            .replacingOccurrences(of: "\\", with: "/")
        
        return URL(string: fullLink)
    }
    
    
    func getFullLink() -> String {
        
        var fullLink: String = self.hasPrefix("http") ? self : DomainKid + self
        
        fullLink = fullLink
            .replacingOccurrences(of: " ", with: "%20")
            .replacingOccurrences(of: "\\", with: "/")
        
        return fullLink
    }
    
    
}


extension URL {
    
    /// Lấy Request Full
    
    var requestToken: URLRequest {
  
        var request = URLRequest(url: self)
        request.allHTTPHeaderFields = ["Authorization": "bearer \(Defaults[.Token])"]
        return request
    }
}

public extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}
