//
//  Utilities.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//


import UIKit
//import CleanroomLogger
import SwiftyUserDefaults
import Firebase
import Foundation
import JSQMessagesViewController
//import CleanroomLogger
//import RealmSwift
//import Realm

struct Utility {
    
    /**
     Tạo folder cho app
     */
//    static func createAppDirectory(_ name: String, skipBackupAttribute: Bool = true) {
//        let docPaths = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
//        let dbPath = docPaths[0].stringByAppendingPathComponent(name)
//        if !FileManager.default.fileExists(atPath: dbPath) {
//            do {
//                try FileManager.default.createDirectory(atPath: dbPath, withIntermediateDirectories: false, attributes: nil)
//            }
//            catch { }
//            
//            if skipBackupAttribute {
//                do {
//                    try (URL(fileURLWithPath: dbPath) as NSURL).setResourceValue(NSNumber(value: true as Bool), forKey: URLResourceKey.isExcludedFromBackupKey)
//                }
//                catch { }
//            }
//        }
//    }
    
    static func currentLanguage() -> LanguageValue {
        if Defaults[.SelectedLanguage] == LanguageValue.vietnamese.. {
            return .vietnamese
        } else {
            return .english
        }
    }
    /**
    Chỉnh lại giao diện navigation bar
    
    - parameter navController:
    */
    static func configureAppearance(navigation navController: UINavigationController, tintColor: UIColor = UIColor.Navigation.mainColor()) {
        
//        navController.navigationBar.titleTextAttributes = [
//            NSAttributedString.Key.font: UIFont(name: FontType.latoSemibold.., size: FontSize.large..)!,
//            NSAttributedString.Key.foregroundColor: UIColor.white]
//        navController.navigationBar.barTintColor = tintColor
//
//
//        navController.navigationBar.barStyle = .default
//        navController.navigationBar.shadowImage = UIImage()
//        navController.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
//        navController.navigationBar.isTranslucent = false
        

        
        navController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: FontType.latoSemibold.., size: FontSize.large..)!,
            NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navController.navigationBar.barTintColor = tintColor
        
        
        navController.navigationBar.barStyle = .black
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        navController.navigationBar.isTranslucent = false
        
    }
    
    /**
     Hiển thị 1 view full màn hình
     */
    static func showView(_ contentView: UIView?, onView supperView: UIView) {
        
        guard let view = contentView else { return }
        
        view.alpha = 0.0
        if let window = UIApplication.shared.keyWindow, let controller = window.rootViewController  {
            
            view.frame = CGRect(origin: CGPoint.zero, size: UIScreen.main.bounds.size)
            controller.view.addSubview(view)
        } else {
            view.frame = CGRect(origin: CGPoint.zero, size: supperView.frame.size)
            supperView.addSubview(view)
        }
        
        UIView.animate(withDuration: 0.3, animations: { view.alpha = 1.0 })
    }
    
    
    /**
    Căn giữa cho text và image in button
    */
    
    static func centeredTextAndImageForButton(_ button: UIButton, spacing: CGFloat = 3.0) {
     
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: button.titleLabel?.text ?? "")
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: button.titleLabel!.font as Any])
        button.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
    }
    
    /**
     Căn Phải image in button
     */
    
    static func rightedTextAndImageForButton(_ button: UIButton) {
        let spacing: CGFloat = 10.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsets(top: 0,
                                              left: -(imageSize.width + spacing * 4),
                                              bottom: 0,
                                              right: imageSize.width + spacing)
        
        
        let labelString = NSString(string: button.titleLabel!.text ?? "")
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: button.titleLabel!.font as Any])
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: titleSize.width , bottom: 0, right: -titleSize.width )
    }


    
    /**
    Tính độ cao cho text
    */
    static func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    /**
     Tính độ cao cho textView
     */
    static func heightForTextView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .justified
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    /**
     Tính độ cao cho textView Attribulte
     */
    static func heightForTextViewAttributed(_ text: NSAttributedString, width:CGFloat) -> CGFloat{
        let label:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .justified
        //        label.font = font
        label.attributedText = text
        label.sizeToFit()
        return label.frame.height
    }
    
    /**
     Tính độ cao cho textView Attribulte
     */
    static func heightForTextAttributed(_ text: NSAttributedString, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.textAlignment = .left
        //        label.font = font
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.attributedText = text
        label.sizeToFit()
        return label.frame.height
    
    }
    
    
    /**
    Tính chiều rộng của text
    */
    static func widthForView(_ text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0,  width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    
    
    /**
     Tính độ rộng cho textView Attribulte
     */
    static func widthForAttributed(_ text: NSAttributedString, height: CGFloat) -> CGFloat{
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.textAlignment = .right
        //        label.font = font
        label.attributedText = text
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        return label.frame.width
    }
    
    
    /**
    *  Kiểm tra số điện thoại
    *
    *  Phải có đầu số trùng với numberArray
    *  Nếu bắt đầu bằng 1 thì phải dài 10 ký tự
    *  Nếu bắt đầu bằng 9 thì dài 9 ký tự
    */
    
    static func validatePhoneNumber(_ phoneNumber: String) -> Bool {
        let numberArray = ["12","162","163","164","165","166","167","168","169","186","188","199","86", "88", "89"]
        var string = phoneNumber.trimmingCharacters(in: CharacterSet.whitespaces)
            .replacingOccurrences(of: " ", with: "")
        
        
        if string.hasPrefix("0") {
            string =  String(string[string.index(string.startIndex, offsetBy: 1) ..< string.endIndex])
        } else if string.hasPrefix("84") {
            string =  String(string[string.index(string.startIndex, offsetBy: 2) ..< string.endIndex])
        } else if string.hasPrefix("+84") {
            string =  String(string[string.index(string.startIndex, offsetBy: 3) ..< string.endIndex])
        }
        
        if string.hasPrefix("9") && string.count == 9 { return true }
        for number in numberArray {
            if string.hasPrefix(number) && string.count == 10 { return true }
        }
        return false
    }
    
    /// Các đầu số di động VN | tính đến 4/2015
    static let phoneHeader: [(Int, String)] = {
        let numberNineArray = ["9","86","88","89"].map { (0, $0) } //  9 số
        let numberTenArray = ["09","086","088","089","12","162","163","164","165","166","167","168","169","186","188","199"].map { (1, $0) } // 10 số
        let numberElevenArray = ["849","8486","8488","8489","012","0162","0163","0164","0165","0166","0167","0168","0169","0186","0188","0199"].map { (2, $0) } // 11 số
        let numberTwelveArray = ["+849","+8486","+8488","+8489","8412","84162","84163","84164","84165","84166","84167","84168","84169","84186","84188","84199"].map { (3, $0) } // 12 số
        
        let numberThirteenArray = ["+8412","+84162","+84163","+84164","+84165","+84166","+84167","+84168","+84169","+84186","+84188","+84199"].map { (4, $0) } // 13 số
        var total = [(Int, String)]()
        [numberNineArray, numberTenArray, numberElevenArray, numberTwelveArray, numberThirteenArray].forEach { array in
            array.forEach { item in total.append(item) }
        }
        return total
        }()
    
    
    /**
    Format lại định dạng số điện thoại để gửi lên server: bắt đầu bằng số 0
    
    - parameter phoneNumber:
    
    - returns:
    */
    static func formatPhoneNumberForServer(_ phoneNumber: String) -> String {
        var phone = phoneNumber.trimmingCharacters(in: CharacterSet.whitespaces)
            .replacingOccurrences(of: " ", with: "")
        
        if phone.hasPrefix("0") {
            phone = String(phone[phone.index(phone.startIndex, offsetBy: 1) ..< phone.endIndex])
        }
        if phone.hasPrefix("84") {
            phone = String(phone[phone.index(phone.startIndex, offsetBy: 2) ..< phone.endIndex])
        }
        
        if phone.hasPrefix("+84") {
            phone = String(phone[phone.index(phone.startIndex, offsetBy: 3) ..< phone.endIndex])
        }
        return "0" + phone
    }
    
    /**
    Format lại định dạng số điện thoại cho dễ đọc
    
    - parameter textField:
    - parameter range:
    - parameter string:
    
    - returns:
    */
    static func formatPhoneNumberForDisplay(_ textField: UITextField, range: NSRange, string: NSString) -> Bool {
        guard let phoneNumber = textField.text, phoneNumber.count >= 0 else { return false }
        
        
        return true
//        // Khi xóa nếu là khoảng trắng thì xóa thêm số tiếp theo
//        if phoneNumber.count >= 2 && string.length == 0 {
//            let rangeOfLastChar = [phoneNumber.index(phoneNumber.endIndex, offsetBy: -1) ..< phoneNumber.endIndex]
//
//            if phoneNumber[phoneNumber.index(phoneNumber.endIndex, offsetBy: -1) ..< phoneNumber.endIndex] == " " {
//                textField.text = phoneNumber.replacingCharacters(in: rangeOfLastChar, with: "")
//                return true
//            }
//        }
//
//        // Chèn thêm khoảng trắng cho dãy số
//        loop: for (addition, numberHeader) in Utility.phoneHeader {
//            if phoneNumber.hasPrefix(numberHeader) && string.length > 0 {
//                let pos = [2, 6, 9].map{ $0 + addition }.takeFirst { $0 == phoneNumber.count }
//                if let _ = pos {
//                    textField.text = phoneNumber + " "
//                    break loop
//                }
//            }
//        }
//
//        let newLength = phoneNumber.count + string.length - range.length
//
//
//        for (addition, numberHeader) in Utility.phoneHeader {
//            if phoneNumber.hasPrefix(numberHeader) {
//                return (newLength > 12 + addition) ? false : true
//            }
//        }
//        return (newLength > 12) ? false : true
    }
    
    /**
    Kiểm tra định dạng email
    
    - parameter string:
    
    - returns:
    */
    static func validateEmail(_ email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }


    /**
     Convert thời gian theo dạng đếm ngược
     */
    public static func stringFromCountDownTimeToText(_ time: TimeInterval) -> String {
//        let deltaTime  = time - Date().timeIntervalSince1970
//
//        let dateTimeFormatter = DateFormatter().config {
//            $0.timeZone = NSTimeZone(name: "0") as TimeZone?
//            switch Utility.currentLanguage() {
//            case .vietnamese:
//                $0.dateFormat = "HH:mm:ss"
//            case .english:
//                $0.dateFormat = "HH:mm:ss"
//            }
//        }
//
//        var string = String()
//        switch deltaTime {
//
//        case 0..<1.days:
//            string = dateTimeFormatter.string(from: Date(timeIntervalSince1970: deltaTime - 8.hour))
//
//        case 1.days..<3.days:
//            string = (deltaTime / 1.days).toString(0) + " " + (deltaTime > 2.day ?
//                LocalizedString("time_default_days", comment: "ngày") :
//                LocalizedString("time_default_day", comment: "ngày")) + " - " + dateTimeFormatter.string(from: Date(timeIntervalSince1970: deltaTime.truncatingRemainder(dividingBy: 1.day) - 8.hour))
//
//
//
//
//        case 3.days..<1.years:
//            string = (deltaTime / 1.days).toString(0) + " " + (deltaTime > 2.day ?
//                LocalizedString("time_default_days", comment: "ngày") :
//                LocalizedString("time_default_day", comment: "ngày"))
//        case 1.year..<100.years:
//            string = (deltaTime / 1.years).toString(0) + " " + (deltaTime > 2.hour ?
//                LocalizedString("time_default_years", comment: "năm") :
//                LocalizedString("time_default_year", comment: "năm"))
//        default:
//            break
//        }
//
//        switch Utility.currentLanguage() {
//        case .vietnamese:
//            return "Còn " + string
//        case .english:
//            return string + " left"
//        }
//
        
        return ""
    }
    
       
    /**
     Convert thời gian theo dạn quá khứ
     */
    static func stringFromPastTimeToText(_ time: TimeInterval, isLimit: Bool = false) -> String {
        let deltaTime  = max(0, Date().timeIntervalSince1970 - time)
        
        if isLimit, deltaTime > 1.days {
        
            return Utility.getTimeFormatter("HH:mm dd/MM/yyyy").string(from: Date(timeIntervalSince1970: time))
            
        }
        
        var string = String()
        switch deltaTime {
        case 0..<10:
            string = LocalizedString("time_default_few", comment: "vài") + " " + LocalizedString("time_default_seconds", comment: "giây")
        case 10..<1.minutes:
            string = deltaTime.toString(0) + " " + LocalizedString("time_default_seconds", comment: "giây")
        case 1.minutes..<1.hours:
            string =  (deltaTime / 1.minutes).toString(0) + " " + (deltaTime > 2.minute ?
                LocalizedString("time_default_minutes", comment: "phút") :
                LocalizedString("time_default_minute", comment: "phút"))
        case 1.hours..<1.days:
            string = (deltaTime / 1.hours).toString(0) + " " + (deltaTime > 2.hour ?
                LocalizedString("time_default_hours",comment: "giờ") :
                LocalizedString("time_default_hour", comment: "giờ"))
        case 1.days..<7.days:
            string = (deltaTime / 1.days).toString(0) + " " + (deltaTime > 2.day ?
                LocalizedString("time_default_days", comment: "ngày") :
                LocalizedString("time_default_day", comment: "ngày"))
        case 7.days..<31.days:
            string = (deltaTime / 7.days).toString(0) + " " + (deltaTime > 14.days ?
                LocalizedString("time_default_weeks", comment: "tuần") :
                LocalizedString("time_default_week", comment: "tuần"))
        case 31.days..<1.years:
            string = (deltaTime / 31.days).toString(0) + " " + (deltaTime > 62.days ?
                LocalizedString("time_default_months", comment: "tháng") :
                LocalizedString("time_default_month", comment: "tháng"))
        case 1.year..<100.years:
            string = (deltaTime / 1.years).toString(0) + " " + (deltaTime > 2.years ?
                LocalizedString("time_default_years", comment: "năm") :
                LocalizedString("time_default_year", comment: "năm"))
        default:
            return ""
        }
        string += " " + LocalizedString("time_default_before", comment: "trước")
        return string
    }

    
    
    /*
    Format khoảng cách
    */
    static func stringFromConvertDistanceToText(_ distance: Double, metersAccuracy: Bool = true) -> String {
        if distance < 1000 && metersAccuracy {
            return distance.toString(0) + " " + "m"
        } else {
            return (distance / 1000).toString(1) + " " + "Km"
        }
    }
    

     /**
     Thêm dấu "," vào giá tiền
     */
    static func convertMoneyToString(_ money: Double, seperatorString: String) -> String {
        var moneyString = money.toString(0)
        let numberLoop = (moneyString.count - 1) / 3
        for i in 0 ..< numberLoop {
            moneyString = Utility.insert(moneyString, insertedString: seperatorString, index: moneyString.count - 3 * (i + 1) - i)
        }
        return moneyString
    }
    
    
    static func stringMinuteFromTimeInterval(_ duration: TimeInterval) -> String {
        
        guard duration > 0 else { return "0" + " " + LocalizedString("time_default_minute", comment: "phút") }
        guard duration > 30 else { return LocalizedString("time_default_smaller_a_minute", comment: "Dưới 1 phút")  }
        
        var hour, minute, second: Int
        hour = Int(duration / 1.hour)
        minute = (Int(duration) - (hour * Int(1.hour))) / Int(1.minute)
        second = Int(duration) - (hour * Int(1.hour)) - (minute * Int(1.minute))
        
        if second >= 30 { minute += 1 }
        if minute == 60 { hour += 1; minute = 0 }
        
        
        var stringTime = ""
        if hour > 0 { stringTime += "\(hour) " + (hour > 1 ? LocalizedString("time_default_hours", comment: "giờ") : LocalizedString("time_default_hour", comment: "giờ") )}
        if minute > 0 { stringTime += " \(minute) " +  (minute > 1 ? LocalizedString("time_default_minutes", comment: "phút") : LocalizedString("time_default_minute", comment: "phút")) }
        return stringTime
    }
    
    /**
     Insert trong String
     */
    fileprivate static func insert(_ source: String, insertedString: String, index: Int) -> String {
        return  String(source.prefix(index)) +
            insertedString +
            String(source.suffix(source.count - index))
    }

    

    /**
     Gọi điện thoại
     */
    static func callPhoneNumber(_ phoneNumber: String) {
        let phone = phoneNumber
            .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ".", with: "")
        
        guard !validatePhone(value: phone), phone.count > 0 else { return }
        
        #if targetEnvironment(simulator)
//            log("on simulator - not calling")
        #else
            let tel = "tel://" + phone
        guard let url = URL(string: tel) else { return }
        UIApplication.shared.openURL(url)
        #endif
    }
    
    
    static func callPhoneNumberShowAlert(_ phoneNumber: String) {
        let phone = phoneNumber
            .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: ".", with: "")
        
        guard !validatePhone(value: phone), phone.count > 0 else { return }
      
        
        #if targetEnvironment(simulator)
//            log("on simulator - not calling")
        #else
            let tel = "telprompt://" + phone
        
        guard let url = URL(string: tel) else { return }
        UIApplication.shared.openURL(url)
        #endif
    }
    
    static func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    
    static func stringNormalFromServer(_ string: String) -> String {
        var newString: NSString = NSString(string: string)
        newString = newString.replacingOccurrences(of: "##s", with: "%@") as NSString
        newString = newString.replacingOccurrences(of: "%s", with: "%@") as NSString
        
        return newString as String
    }

    
    /**
     Đổi "đ" -> "d"
     */
    
    static func stringFromReplaceTheD(_ string: String) -> String {
//        let charSet = CharacterSet(charactersIn: "đ")
        var newString = NSString(format: "%@", string)
//        if newString.rangeOfCharacter(from: charSet).location != NSNotFound {
//            return string.lowercased().replacingOccurrences(of: "đ", with: "d")
//        }
        
        newString = newString.lowercased.replacingOccurrences(of: "đ", with: "d") as NSString
        
        return newString as String
    }

    
    /**
     *  Loại bỏ dấu câu, dấu của từ, chuyển thành chữ thường
     *  Phục vụ mục đích tạo ra search text để insert vào DB
     */
    
    static func normalizeString(_ string: String) -> String {
        
        guard let data = Utility.stringFromReplaceTheD(string).lowercased().data(using: String.Encoding.ascii, allowLossyConversion: true) else { return ""}
        
        guard let newString = String(data: data, encoding: String.Encoding.ascii) else { return "" }
        
        
        var charSet = NSMutableCharacterSet.letter()
        charSet.formUnion(with: CharacterSet.decimalDigits)
        charSet.addCharacters(in: " ")
        charSet = (charSet.inverted as NSCharacterSet).copy() as! NSMutableCharacterSet
        
        return newString.components(separatedBy: charSet as CharacterSet).joined(separator: "")
    }
    
    


    
    static func getListRoomName() -> [String] {
        return AppData.instance.guestInfoList.map { Utility.getRoomNameWithID($0.id) }
    }
    
    /**
     Lấy mã phòng từ id đẩy vào
     */
    static func getRoomNameWithID(_ id: String) -> String {
        guard let info = AppData.instance.myInfo else { return "" }
        
        if info.uid > id {
            return String(format: "%@/%@_%@", id, id, info.uid)
        } else {
            return String(format: "%@/%@_%@", info.uid, info.uid, id)
        }
        
//        let minID = min(id, info.id)
//        let maxID = max(id, info.id)
//
//        return String(format: "%d/%d/%d_%d", minID - (minID % 100), maxID - (maxID % 100), minID, maxID)
    }
    
    
    static func getIdFromRoomName(_ roomName: String) -> String {
        
        let componentsRoom = roomName.components(separatedBy: "/")
        guard componentsRoom.count > 0 else { return "" }
        
        let components = componentsRoom[componentsRoom.count - 1].components(separatedBy: "_")
        
        guard let info = AppData.instance.myInfo, components.count > 1 else { return "" }
        
        if components[0] != info.uid { return components[0]}
        else {return components[1] }
    }

    
    static func getMessagesFromRoomName(_ roomName: String) -> [JSQMessage] {
        guard let item = (AppData.instance.messageData.filter { $0.roomName == roomName }).first else { return [] }
        return item.messages
    }
    
    static func updateMessageDataWith(_ roomName: String, messages: [JSQMessage]) {
 
        if let pos = AppData.instance.messageData.firstIndex(where: { $0.roomName == roomName }) {
            AppData.instance.messageData[pos].messages = messages
        } else {
            AppData.instance.messageData.append((roomName: roomName, messages: messages))
        }
    }
    
    static func checkMessageInDataMessage(_ messages: [JSQMessage], message: JSQMessage) -> Bool {
        
        for messageTemp in messages {
            if messageTemp == message { return true }
        }
        return false
    }
    
    static func getUserInfoFromID(_ id: String) -> Any? {
        return (AppData.instance.guestInfoList.filter { $0.id == id }).first
    }
    

    
    /**
     Attribute Stirng
     */
    
    static func getAttributeString(_ firstString: String,
                                   subString: String,
                                   fontFirst: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!,
                                   fontSub: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!,
                                   firstColor: UIColor,
                                   subColor: UIColor) -> NSMutableAttributedString {
        
        let stringContact = firstString + subString
        
        let mutableStringTerms =  NSMutableAttributedString(string: stringContact,
                                                            attributes:
            [NSAttributedString.Key.font: fontFirst,
             NSAttributedString.Key.foregroundColor: firstColor])
        
        
        mutableStringTerms.addAttributes([NSAttributedString.Key.foregroundColor: subColor,
                                          NSAttributedString.Key.font: fontSub],
                                         range: (stringContact as NSString).range(of: subString))
        return mutableStringTerms
    }
    
    
    static func getAttributeWithMainString(_ mainString: String,
                                           subString: String,
                                           fontFirst: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!,
                                           fontSub: UIFont = UIFont(name: FontType.latoBold.., size: FontSize.normal..)!,
                                           firstColor: UIColor,
                                           subColor: UIColor) -> NSMutableAttributedString {
        
        
        let mutableStringTerms =  NSMutableAttributedString(string: mainString,
                                                            attributes:
            [NSAttributedString.Key.font: fontFirst,
             NSAttributedString.Key.foregroundColor: firstColor])
        
        
        mutableStringTerms.addAttributes([NSAttributedString.Key.foregroundColor: subColor,
                                          NSAttributedString.Key.font: fontSub],
                                         range: (mainString as NSString).range(of: subString))
        return mutableStringTerms
    }
    
    
    
    //Lấy Attributes
    
    static func getStringAttributeWith(_ mainString: String,
                                       subStrings: [String],
                                       mainFont: UIFont,
                                       subFonts: [UIFont],
                                       mainColor: UIColor,
                                       subColors: [UIColor]) -> NSMutableAttributedString {
        
        
        let mutableString =  NSMutableAttributedString(string: mainString,
                                                       attributes: [NSAttributedString.Key.font: mainFont,
                                                                    NSAttributedString.Key.foregroundColor: mainColor])
        
        
        subStrings.enumerated().forEach { (i, subString) in
            
            var subColor: UIColor = mainColor
            var subFont: UIFont = mainFont
            
            if i < subColors.count {
                subColor = subColors[i]
                
            } else if let color = subColors.first {
                subColor = color
            }
            
            if i < subFonts.count {
                subFont = subFonts[i]
                
            } else if let font = subFonts.first {
                subFont = font
            }
            
            
            mutableString.addAttributes([NSAttributedString.Key.foregroundColor: subColor,
                                         NSAttributedString.Key.font: subFont],
                                        range: (mainString as NSString).range(of: subString))
        }
        
        return mutableString
    }
    
    /**
    Lấy thông tin lỗi từ status
     */
    
    static func getErrorInfoFromStatus(_ status: Int) -> (error: Error, message: String) {
        switch status {

            
            
            
        case 24381:
            return (HTTPError.notValidEncryptedValue, "Dữ liệu mã hóa sai cấu trúc quy định")
            
        case -1:
            return (HTTPError.notAttachStudent, "Tài khoản chưa được gán con")
            
        case 2:
            return (HTTPError.missingParameters, "Thiếu dữ liệu gửi lên")
        case 3:
            return (HTTPError.unauthorized, "Bạn không phải là phụ huynh của trường")
        case 4:
            return (HTTPError.requestUnsuccessfully, "Yêu cầu thực thi không thành công")
        case 5:
            return (HTTPError.generalError, "Lỗi chung - Hiển thị thông báo theo key data của dữ liệu trả về")
        case 6:
            return (HTTPError.actionUnsuccessfully, "Vui lòng thực hiện lại")
            
        case 101:
            return (HTTPError.notFoundUserFromToken, "Không tìm thấy người dùng từ token")
        case 102:
            return (HTTPError.tokenExpired, "Token hết hạn")
        case 103:
            return (HTTPError.invalidToken, "Token khônghợp lệ")
        case 104:
            return (HTTPError.tokenException, "Lỗi token (Token Exception)")
        case 105:
            return (HTTPError.tokenAbsent, "Không tìm thấy token trong request")
        case 201:
            return (HTTPError.invalidTimestamp, "Timestamp không hợp lệ (parse bị lỗi)")
        case 202:
            return (HTTPError.invalidDateValue, "Định dạng ngày không hợp lệ")
        case 203:
            return (HTTPError.dateIsOffDay, "Hôm nay là ngày nghỉ của con, bố mẹ vui lòng chọn ngày khác để xem")
        case 204:
            return (HTTPError.dateIsHolidayDate, "Hôm nay là ngày nghỉ lễ, bố mẹ vui lòng chọn ngày khác để xem")
        case 205:
            return (HTTPError.fromDateGreaterThanToDate, "Ngày bắt đầu phải nhỏ hơn ngày kết thúc, vui lòng chọn lại!")
        case 206:
            return (HTTPError.dateCanNotGreaterThanToDay, "Ngày kết thúc phải bằng ngày hiện tại, vui lòng chọn lại!")
        case 207:
            return (HTTPError.invalidDateRange, "Phạm vi của ngày quá lớn. Vui lòng chọn khoảng cách bé hơn")
        case 208:
            return (HTTPError.dateInThePast, "Ngày trong quá khứ không hợp lệ")
        case 301:
            return (HTTPError.lockedUser, "Tài khoản của bạn đã bị khóa vui lòng liên hệ hotline KidsOnline 1900.0362 để được hỗ trợ!")
        case 302:
            return (HTTPError.notHaveGuardianRole, "Bạn không phải là phụ huynh của trường")
            
        case 303:
            return (HTTPError.wrongPassword, "Sai mật khẩu")

        case 304:
            return (HTTPError.notExistsUser, "Tài khoản của bạn hiện không tồn tại, vui lòng liên hệ BQT nhà trường để được hỗ trợ")
        case 401:
            return (HTTPError.notFoundClass, "Bé chưa được sắp lớp, vui lòng liên hệ BQT nhà trường để được hỗ trợ")
        case 402:
            return (HTTPError.notFoundSchool, "Không tìm thấy trường")
        case 403:
            return (HTTPError.notFoundUsername, "Không tìm thấy tên tài khoản")
        case 404:
            return (HTTPError.notFoundStudent, "không tìm thấy học sinh")
        case 405:
            return (HTTPError.notFoundAbsentRequest, "Không tìm thấy đơn xin nghỉ")
        case 406:
            return (HTTPError.notFoundMedicineNote, "Không tìm thấy đơn dặn thuốc")
        case 407:
            return (HTTPError.notFoundMessage, "Không tìm thấy lời nhắn")
        case 408:
            return (HTTPError.notFoundPhotoType, "Không tìm thấy kiểu của ảnh")
        case 409:
            return (HTTPError.notFoundPhoto, "Không tìm thấy đối tượng của ảnh")
        case 411:
            return (HTTPError.notFoundAlbum, "Không tìm thấy album ảnh")
        case 412:
            return (HTTPError.notFoundItem, "Không tìm thấy đối tượng hoặc đối tượng đã bị xóa")
        case 413:
            return (HTTPError.notFoundItemComment, "Không tìm thấy đối tượng của comment")
        case 414:
            return (HTTPError.notFoundItemParentComment, "Không tìm thấy comment cha của phản hồi (khi phản hồi comment)")
        case 415:
            return (HTTPError.notFoundSurvey, "Không tìm thấy bài khảo sát")
        case 416:
            return (HTTPError.notFoundStudentTransportTicketDay, "Không tìm thấy đơn đưa đón của học sinh này")
        case 417:
            return (HTTPError.notFoundStudentTransport, "Không tìm thấy người đưa đón này")
        case 418:
            return (HTTPError.notFoundHeightWeightRecord, "Không tìm thấy bản ghi chiều cao - cân nặng của học sinh")
        case 419:
            return (HTTPError.notFoundCornerCategory, "Không tìm thấy chuyên mục trong góc tin tức")
            
            
        case 423:
            return (HTTPError.notFoundTuitionNotice, "Không tìm thấy thông báo phí của học sinh")
            
        case 424:
            return (HTTPError.notFoundTransaction, "Không tìm thấy giao dịch này")
            
            
        case 501:
            return (HTTPError.studentNoClass, "Học sinh không có lớp")
        case 502:
            return (HTTPError.studentAbsenceInDate, "Hôm nay con nghỉ học, bố mẹ vui lòng chọn ngày khác để xem")
        case 503:
            return (HTTPError.studentHaveNotJoinedClass, "Học sinh chưa được phân lớp trong ngày đã chọn")
        case 601:
            return (HTTPError.duplicateAbsentRequest, "Đã có đơn xin nghỉ bị trùng với ngày đã chọn")
        case 602:
            return (HTTPError.ticketInPastCanNotDelete, "Đơn đã xử lý trong quá khứ không được phép xóa")
        case 603:
            return (HTTPError.duplicateMedicineNote, "Đã có đơn dặn thuốc trùng với ngày đã chọn")
        case 604:
            return (HTTPError.duplicateMorningMessage, "Đã có lời nhắn trùng với ngày đã chọn")
        case 605:
            return (HTTPError.cannotUpdateTicketInPast, "Không được phép cập nhật đơn đã hết hạn sử dụng")
            
        case 606:
            return (HTTPError.contentNotNull, "Nội dung lời nhắn không được để trống")
            
        case 607:
            return (HTTPError.submitWrongApplication, "Gửi đơn sai quy định")
            

            
        case 703:
            return (HTTPError.uploadError, "Tải ảnh lỗi")
        case 704:
            return (HTTPError.deletePhotoUnsuccessfully, "Xóa ảnh không thành công")
        case 706:
            return (HTTPError.photoNotBelongsToItem, "Ảnh không thuộc về đối tượng đã gửi")
        case 707:
            return (HTTPError.rotatePhotoUnsuccessfully, "Xoay ảnh không thành công")
        case 708:
            return (HTTPError.updatePhotoDescriptionUnsuccessfully, "Cập nhật tiêu đề ảnh không thành công")
        case 801:
            return (HTTPError.invalidAlbum, "Thiếu mã album ảnh")
        case 902:
            return (HTTPError.invalidLikeCommentType, "Kiểu like / comment không hợp lệ")
        case 903:
            return (HTTPError.invalidCommentContent, "Nội dung comment không hợp lệ (thường do để trống)")
        case 904:
            return (HTTPError.likeCommentActionUnsuccessfully, "Thao tác không thành công khi like / comment")
        case 905:
            return (HTTPError.notAuthorizedToComment, "Không có quyền thao tác với comemnt")
        case 908:
            return (HTTPError.userHasLikedContent, "Người dùng đã thích nội dung (khi gửi like với ndung đã like)")
        case 909:
            return (HTTPError.userHaveNotLikedContent, "Người dùng chưa thích nội dung (khi unline với ndung chưa like)")
        case 1001:
            return (HTTPError.canNotAnswerSurvey, "Không trả lời được khảo sát này")
        case 1101:
            return (HTTPError.invalidAccountName, "Vui lòng nhập tên")
        case 1102:
            return (HTTPError.invalidEmail, "Email không đúng định dạng")
        case 1103:
            return (HTTPError.existUsingEmail, "Email này đã được sử dụng")
        case 1104:
            return (HTTPError.invalidDob, "Ngày sinh không đúng định dạng")
        case 1105:
            return (HTTPError.invalidAvatar, "Ảnh đại diện không đúng định dạng")
        case 1106:
            return (HTTPError.invalidPhone, "Số điện thoại không đúng định dạng")
        case 1107:
            return (HTTPError.notCorrectOldPassword, "Mật khẩu cũ không chính xác")
        case 1120:
            return (HTTPError.otpNotFound, "Không tìm thấy mã OTP này")
        case 1121:
            return (HTTPError.otpWasUsed, "OTP đã được sử dụng")
        case 1122:
            return (HTTPError.otpWasExpired, "OTP đã hết hạn")
        case 1123:
            return (HTTPError.creatingOtpWasLimited, "Đã hết lượt lấy OTP trong hôm nay, vui lòng liên hệ tổng đài")
            
        case 1124:
            return (HTTPError.notFoundAccountByPhone, "Số điện thoại bạn vừa nhập, không tồn tại trên hệ thống!")

            
        case 1125:
            return (HTTPError.notFoundAccountByUsername, "Không tìm thấy tài khoản này")
            
        case 1126:
            return (HTTPError.accountNotHasPhoneNumber, "Tài khoản chưa đăng ký số điện thoại để gửi mã OTP. Vui lòng liên hệ với tổng đài")
            
        case 1127:
            return (HTTPError.invalidPassword, "Mật khẩu không hợp lệ")
            
        case 1201:
            return (HTTPError.noHeightWeightRecords, "Học sinh không có thông tin chiều cao - cân nặng")
        case 1202:
            return (HTTPError.invalidHeightWeightValue, "Giá trị cheièu cao - cân nặng không hợp lệ (cân nặng > chiều cao)")
        case 1203:
            return (HTTPError.hasRecordHeightWeightOnDate, "Học sinh đã có bản ghi chiều cao - cân nặng trong ngày")
        case 1301:
            return (HTTPError.noTimelineItem, "Hiện tại không có item nào trên timeline")
        case 1401:
            return (HTTPError.exitStudentTransportTicket, "Đơn đưa đón của hs đã có")
        case 1402:
            return (HTTPError.notCreatedInThePast, "Không được tạo đơn đưa đón trong quá khứ")
        case 1501:
            return (HTTPError.promotionNotFound, "Không tìm thấy chương trình quà tặng này")
        case 1502:
            return (HTTPError.promotionValidation, "Thiếu thông tin xử lý hành động này")
        case 1503:
            return (HTTPError.hasCode, "Bạn đã nhận mã chương trình này trước đó")
        case 1504:
            return (HTTPError.overCode, "Hệ thống đã hết mã")
        case 1505:
            return (HTTPError.notActive, "Tài khoản của bạn chưa được kích hoạt")
        case 1506:
            return (HTTPError.notCommentPost, "Bạn chưa có bình luận vào bài viết nào trong khoảng thời gian này")
        case 1507:
            return (HTTPError.notCommentAlbum, "Bạn chưa có bình luận vào album ảnh nào trong khoảng thời gian này")
        case 1508:
            return (HTTPError.notLikePost, "Bạn chưa bấm thích bài viết nào trong khoảng thời gian này")
        case 1509:
            return (HTTPError.notLikeAlbum, "Bạn chưa bấm thích album ảnh nào trong khoảng thời gian này")
        case 1510:
            return (HTTPError.notSendMorningMessage, "Bạn chưa gửi lời nhắn đầu ngày nào trong khoảng thời gian này")
        case 1511:
            return (HTTPError.notSendThanksMessage, "Bạn chưa gửi lời cảm ơn nào trong khoảng thời gian này")
        case 1512:
            return (HTTPError.notSendMessage, "Bạn chưa gửi lời nhắn nào trong khoảng thời gian này")
        case 1513:
            return (HTTPError.locked, "Tài khoản hiện tại không khả dụng để nhận mã")
        case 1514:
            return (HTTPError.notActiveInRange, "Tài khoản chưa kích hoạt trong thời gian diễn ra chương trình")
        case 1515:
            return (HTTPError.noUserIds, "Học sinh không có phụ huynh hoặc mã phụ huynh không hợp lệ")
        case 1516:
            return (HTTPError.promotionValidationForm, "Thông tin bạn cung cấp không hợp lệ")
        case 1517:
            return (HTTPError.invalidPhoneAndEmail, "Chưa nhập địa chỉ email và số điện thoại")
        case 1518:
            return (HTTPError.invalidName, "Chưa nhập tên người dùng")
        case 1519:
            return (HTTPError.conflictPhoneOrEmail, "Số điện thoại hoặc email khác với thông tin trong tài khoản")

      
        case 1601:
            return (HTTPError.extracurricularRegistered, "Bạn đã đăng ký ngoại khóa này")
            
            
            
        case 1701:
            return (HTTPError.canNotPayment, "Không được thực hiện giao dịch này")
            
        case 1702:
            return (HTTPError.transactionIsDismissed, "Giao dịch đã bị hủy")
            
        case 1703:
            return (HTTPError.transactionIsSuccessfully, "Giao dịch thành công")
            
        case 1704:
            return (HTTPError.transactionIsFailed, "Giao dịch thất bại")
            
        case 1705:
            return (HTTPError.transactionWasHandled, "Giao dịch này đã được thực hiện trước đó")
            
        default:
            return (HTTPError.unknown, "Có lỗi xảy ra, vui lòng thử lại sau!")
        }
    }
    
    static func getMessageFromError(_ error: Error) -> String {
        
        switch error {
        case HTTPError.tokenAbsent, HTTPError.tokenExpired, HTTPError.invalidToken, HTTPError.tokenException, HTTPError.notFoundUserFromToken:
            NotificationCenter.default.post(name: Notification.Name(rawValue: AppNotification.Logout..), object: nil)
            
        default:
            break
            
        }
        
        switch error {

        case HTTPError.notValidEncryptedValue:
            return LocalizedString("notification_error_notValidEncryptedValue", comment: "Dữ liệu mã hóa sai cấu trúc quy định")
            
        case HTTPError.notAttachStudent:
            return LocalizedString("notification_error_notAttachStudent", comment: "Tài khoản chưa được gán con")
            
        case HTTPError.missingParameters:
            return LocalizedString("notification_error_missingParameters", comment: "Thiếu dữ liệu gửi lên")
        case HTTPError.unauthorized:
            return LocalizedString("notification_error_unauthorized", comment: "Bạn không phải là phụ huynh của trường")
        case HTTPError.requestUnsuccessfully:
            return LocalizedString("notification_error_requestUnsuccessfully", comment: "Yêu cầu thực thi không thành công")
            
            //TODO: Check
//        case HTTPError.generalError:
//            return LocalizedString("notification_error_generalError", comment: "Lỗi chung - Hiển thị thông báo theo key data của dữ liệu trả về")
        case HTTPError.actionUnsuccessfully:
            return LocalizedString("notification_error_actionUnsuccessfully", comment: "Vui lòng thực hiện lại")
            
        case HTTPError.notFoundUserFromToken:
            return LocalizedString("notification_error_notFoundUserFromToken", comment: "Không tìm thấy người dùng từ token")
        case HTTPError.tokenExpired:
            return LocalizedString("notification_error_tokenExpired", comment: "Token hết hạn")
        case HTTPError.invalidToken:
            return LocalizedString("notification_error_invalidToken", comment: "Token khônghợp lệ")
        case HTTPError.tokenException:
            return LocalizedString("notification_error_tokenException", comment: "Lỗi token (Token Exception)")
        case HTTPError.tokenAbsent:
            return LocalizedString("notification_error_tokenAbsent", comment: "Không tìm thấy token trong request")
        case HTTPError.invalidTimestamp:
            return LocalizedString("notification_error_invalidTimestamp", comment: "Timestamp không hợp lệ (parse bị lỗi)")
        case HTTPError.invalidDateValue:
            return LocalizedString("notification_error_invalidDateValue", comment: "Định dạng ngày không hợp lệ")
        case HTTPError.dateIsOffDay:
            return LocalizedString("notification_error_dateIsOffDay", comment: "Hôm nay là ngày nghỉ của con, bố mẹ vui lòng chọn ngày khác để xem")
        case HTTPError.dateIsHolidayDate:
            return LocalizedString("notification_error_dateIsHolidayDate", comment: "Hôm nay là ngày nghỉ lễ, bố mẹ vui lòng chọn ngày khác để xem")
        case HTTPError.fromDateGreaterThanToDate:
            return LocalizedString("notification_error_fromDateGreaterThanToDate", comment: "Ngày bắt đầu phải nhỏ hơn ngày kết thúc, vui lòng chọn lại!")
        case HTTPError.dateCanNotGreaterThanToDay:
            return LocalizedString("notification_error_dateCanNotGreaterThanToDay", comment: "Ngày kết thúc phải bằng ngày hiện tại, vui lòng chọn lại!")
        case HTTPError.invalidDateRange:
            return LocalizedString("notification_error_invalidDateRange", comment: "Phạm vi của ngày quá lớn. Vui lòng chọn khoảng cách bé hơn")
        case HTTPError.dateInThePast:
            return LocalizedString("notification_error_dateInThePast", comment: "Ngày trong quá khứ không hợp lệ")
        case HTTPError.lockedUser:
            return LocalizedString("notification_error_lockedUser", comment: "Tài khoản của bạn đã bị khóa vui lòng liên hệ hotline KidsOnline 1900.0362 để được hỗ trợ!")
        case HTTPError.notHaveGuardianRole:
            return LocalizedString("notification_error_notHaveGuardianRole", comment: "Bạn không phải là phụ huynh của trường")
        case HTTPError.wrongPassword:

            return LocalizedString("notification_error_wrong_password", comment: "Mật khẩu sai")
            
            
        case HTTPError.notExistsUser:
            return LocalizedString("notification_error_notExistsUser", comment: "Tài khoản của bạn hiện không tồn tại, vui lòng liên hệ BQT nhà trường để được hỗ trợ")
        case HTTPError.notFoundClass:
            return LocalizedString("notification_error_notFoundClass", comment: "Bé chưa được sắp lớp, vui lòng liên hệ BQT nhà trường để được hỗ trợ")
        case HTTPError.notFoundSchool:
            return LocalizedString("notification_error_notFoundSchool", comment: "Không tìm thấy trường")
        case HTTPError.notFoundUsername:
            return LocalizedString("notification_error_notFoundUsername", comment: "Không tìm thấy tên tài khoản")
        case HTTPError.notFoundStudent:
            return LocalizedString("notification_error_notFoundStudent", comment: "không tìm thấy học sinh")
        case HTTPError.notFoundAbsentRequest:
            return LocalizedString("notification_error_notFoundAbsentRequest", comment: "Không tìm thấy đơn xin nghỉ")
        case HTTPError.notFoundMedicineNote:
            return LocalizedString("notification_error_notFoundMedicineNote", comment: "Không tìm thấy đơn dặn thuốc")
        case HTTPError.notFoundMessage:
            return LocalizedString("notification_error_notFoundMessage", comment: "Không tìm thấy lời nhắn")
        case HTTPError.notFoundPhotoType:
            return LocalizedString("notification_error_notFoundPhotoType", comment: "Không tìm thấy kiểu của ảnh")
        case HTTPError.notFoundPhoto:
            return LocalizedString("notification_error_notFoundPhoto", comment: "Không tìm thấy đối tượng của ảnh")
        case HTTPError.notFoundAlbum:
            return LocalizedString("notification_error_notFoundAlbum", comment: "Không tìm thấy album ảnh")
        case HTTPError.notFoundItem:
            return LocalizedString("notification_error_notFoundItem", comment: "Không tìm thấy đối tượng hoặc đối tượng đã bị xóa")
        case HTTPError.notFoundItemComment:
            return LocalizedString("notification_error_notFoundItemComment", comment: "Không tìm thấy đối tượng của comment")
        case HTTPError.notFoundItemParentComment:
            return LocalizedString("notification_error_notFoundItemParentComment", comment: "Không tìm thấy comment cha của phản hồi (khi phản hồi comment)")
        case HTTPError.notFoundSurvey:
            return LocalizedString("notification_error_notFoundSurvey", comment: "Không tìm thấy bài khảo sát")
        case HTTPError.notFoundStudentTransportTicketDay:
            return LocalizedString("notification_error_notFoundStudentTransportTicketDay", comment: "Không tìm thấy đơn đưa đón của học sinh này")
        case HTTPError.notFoundStudentTransport:
            return LocalizedString("notification_error_notFoundStudentTransport", comment: "Không tìm thấy người đưa đón này")
        case HTTPError.notFoundHeightWeightRecord:
            return LocalizedString("notification_error_notFoundHeightWeightRecord", comment: "Không tìm thấy bản ghi chiều cao - cân nặng của học sinh")
        case HTTPError.notFoundCornerCategory:
            return LocalizedString("notification_error_notFoundCornerCategory", comment: "Không tìm thấy chuyên mục trong góc tin tức")
            
            
            
        case HTTPError.notFoundTuitionNotice:
            return LocalizedString("notification_error_notFoundTuitionNotice", comment: "Không tìm thấy thông báo phí của học sinh")
            
        case HTTPError.notFoundTransaction:
            return LocalizedString("notification_error_notFoundTransaction", comment: "Không tìm thấy giao dịch này")
            
            
            
        case HTTPError.studentNoClass:
            return LocalizedString("notification_error_studentNoClass", comment: "Học sinh không có lớp")
        case HTTPError.studentAbsenceInDate:
            return LocalizedString("notification_error_studentAbsenceInDate", comment: "Hôm nay con nghỉ học, bố mẹ vui lòng chọn ngày khác để xem")
        case HTTPError.studentHaveNotJoinedClass:
            return LocalizedString("notification_error_studentHaveNotJoinedClass", comment: "Học sinh chưa được phân lớp trong ngày đã chọn")
        case HTTPError.duplicateAbsentRequest:
            return LocalizedString("notification_error_duplicateAbsentRequest", comment: "Đã có đơn xin nghỉ bị trùng với ngày đã chọn")
        case HTTPError.ticketInPastCanNotDelete:
            return LocalizedString("notification_error_ticketInPastCanNotDelete", comment: "Đơn đã xử lý trong quá khứ không được phép xóa")
        case HTTPError.duplicateMedicineNote:
            return LocalizedString("notification_error_duplicateMedicineNote", comment: "Đã có đơn dặn thuốc trùng với ngày đã chọn")
        case HTTPError.duplicateMorningMessage:
            return  LocalizedString("notification_error_duplicateMorningMessage", comment: "Đã có lời nhắn trùng với ngày đã chọn")
        case HTTPError.cannotUpdateTicketInPast:
            return  LocalizedString("notification_error_cannotUpdateTicketInPast", comment: "Không được phép cập nhật đơn đã hết hạn sử dụng")
            
        case HTTPError.contentNotNull:
            return LocalizedString("notification_error_contentNotNull", comment: "Nội dung lời nhắn không được để trống")
            
        case HTTPError.submitWrongApplication:
            return LocalizedString("notification_error_submitWrongApplication", comment: "Gửi đơn sai quy định")
            
            
        case HTTPError.uploadError:
            return LocalizedString("notification_error_uploadError", comment: "Tải ảnh lỗi")
        case HTTPError.deletePhotoUnsuccessfully:
            return LocalizedString("notification_error_deletePhotoUnsuccessfully", comment: "Xóa ảnh không thành công")
        case HTTPError.photoNotBelongsToItem:
            return LocalizedString("notification_error_photoNotBelongsToItem", comment: "Ảnh không thuộc về đối tượng đã gửi")
        case HTTPError.rotatePhotoUnsuccessfully:
            return LocalizedString("notification_error_rotatePhotoUnsuccessfully", comment: "Xoay ảnh không thành công")
        case HTTPError.updatePhotoDescriptionUnsuccessfully:
            return LocalizedString("notification_error_updatePhotoDescriptionUnsuccessfully", comment: "Cập nhật tiêu đề ảnh không thành công")
        case HTTPError.invalidAlbum:
            return LocalizedString("notification_error_invalidAlbum", comment: "Thiếu mã album ảnh")
        case HTTPError.invalidLikeCommentType:
            return LocalizedString("notification_error_invalidLikeCommentType", comment: "Kiểu like / comment không hợp lệ")
        case HTTPError.invalidCommentContent:
            return LocalizedString("notification_error_invalidCommentContent", comment: "Nội dung comment không hợp lệ (thường do để trống)")
        case HTTPError.likeCommentActionUnsuccessfully:
            return LocalizedString("notification_error_likeCommentActionUnsuccessfully", comment: "Thao tác không thành công khi like / comment")
        case HTTPError.notAuthorizedToComment:
            return LocalizedString("notification_error_notAuthorizedToComment", comment: "Không có quyền thao tác với comemnt")
        case HTTPError.userHasLikedContent:
            return LocalizedString("notification_error_userHasLikedContent", comment: "Người dùng đã thích nội dung (khi gửi like với ndung đã like)")
        case HTTPError.userHaveNotLikedContent:
            return LocalizedString("notification_error_userHaveNotLikedContent", comment: "Người dùng chưa thích nội dung (khi unline với ndung chưa like)")
        case HTTPError.canNotAnswerSurvey:
            return LocalizedString("notification_error_canNotAnswerSurvey", comment: "Không trả lời được khảo sát này")
        case HTTPError.invalidAccountName:
            return LocalizedString("notification_error_LocalizedString", comment: "Vui lòng nhập tên")
        case HTTPError.invalidEmail:
            return LocalizedString("notification_error_invalidEmail", comment: "Email không đúng định dạng")
        case HTTPError.existUsingEmail:
            return LocalizedString("notification_error_existUsingEmail", comment: "Email này đã được sử dụng")
        case HTTPError.invalidDob:
            return LocalizedString("notification_error_invalidDob", comment: "Ngày sinh không đúng định dạng")
        case HTTPError.invalidAvatar:
            return LocalizedString("notification_error_invalidAvatar", comment: "Ảnh đại diện không đúng định dạng")
        case HTTPError.invalidPhone:
            return LocalizedString("notification_error_invalidPhone", comment: "Số điện thoại không đúng định dạng")
        case HTTPError.notCorrectOldPassword:
            return LocalizedString("notification_error_notCorrectOldPassword", comment: "Mật khẩu cũ không chính xác")
        case HTTPError.otpNotFound:
            return LocalizedString("notification_error_otpNotFound", comment: "Không tìm thấy mã OTP này")
        case HTTPError.otpWasUsed:
            return LocalizedString("notification_error_otpWasUsed", comment: "OTP đã được sử dụng")
        case HTTPError.otpWasExpired:
            return LocalizedString("notification_error_otpWasExpired", comment: "OTP đã hết hạn")
        case HTTPError.creatingOtpWasLimited:
            return LocalizedString("notification_error_creatingOtpWasLimited", comment: "Đã hết lượt lấy OTP trong hôm nay, vui lòng liên hệ tổng đài")
            
        case HTTPError.notFoundAccountByPhone:
            return LocalizedString("notification_error_notFoundAccountByPhone", comment: "Số điện thoại bạn vừa nhập, không tồn tại trên hệ thống!")
            
        case HTTPError.notFoundAccountByUsername:
            return LocalizedString("notification_error_notFoundAccountByUsername", comment: "Không tìm thấy tài khoản này")
            
        case HTTPError.accountNotHasPhoneNumber:
            
            return LocalizedString("notification_error_accountNotHasPhoneNumber", comment: "Tài khoản chưa đăng ký số điện thoại để gửi mã OTP. Vui lòng liên hệ với tổng đài")
            

        case HTTPError.invalidPassword:
            return LocalizedString("notification_error_invalidPassword", comment: "Mật khẩu không hợp lệ")
            
        case HTTPError.noHeightWeightRecords:
            return LocalizedString("notification_error_noHeightWeightRecords", comment: "Học sinh không có thông tin chiều cao - cân nặng")
        case HTTPError.invalidHeightWeightValue:
            return LocalizedString("notification_error_invalidHeightWeightValue", comment: "Giá trị cheièu cao - cân nặng không hợp lệ (cân nặng > chiều cao)")
        case HTTPError.hasRecordHeightWeightOnDate:
            return LocalizedString("notification_error_hasRecordHeightWeightOnDate", comment: "Học sinh đã có bản ghi chiều cao - cân nặng trong ngày")
        case HTTPError.noTimelineItem:
            return LocalizedString("notification_error_noTimelineItem", comment: "Hiện tại không có item nào trên timeline")
        case HTTPError.exitStudentTransportTicket:
            return LocalizedString("notification_error_exitStudentTransportTicket", comment: "Đơn đưa đón của hs đã có")
        case HTTPError.notCreatedInThePast:
            return LocalizedString("notification_error_notCreatedInThePast", comment: "Không được tạo đơn đưa đón trong quá khứ")
        case HTTPError.promotionNotFound:
            return LocalizedString("notification_error_promotionNotFound", comment: "Không tìm thấy chương trình quà tặng này")
        case HTTPError.promotionValidation:
            return LocalizedString("notification_error_promotionValidation", comment: "Thiếu thông tin xử lý hành động này")
        case HTTPError.hasCode:
            return LocalizedString("notification_error_hasCode", comment: "Bạn đã nhận mã chương trình này trước đó")
        case HTTPError.overCode:
            return LocalizedString("notification_error_overCode", comment: "Hệ thống đã hết mã")
        case HTTPError.notActive:
            return LocalizedString("notification_error_notActive", comment: "Tài khoản của bạn chưa được kích hoạt")
        case HTTPError.notCommentPost:
            return LocalizedString("notification_error_notCommentPost", comment: "Bạn chưa có bình luận vào bài viết nào trong khoảng thời gian này")
        case HTTPError.notCommentAlbum:
            return LocalizedString("notification_error_notCommentAlbum", comment: "Bạn chưa có bình luận vào album ảnh nào trong khoảng thời gian này")
        case HTTPError.notLikePost:
            return LocalizedString("notification_error_notLikePost", comment: "Bạn chưa bấm thích bài viết nào trong khoảng thời gian này")
        case HTTPError.notLikeAlbum:
            return LocalizedString("notification_error_notLikeAlbum", comment: "Bạn chưa bấm thích album ảnh nào trong khoảng thời gian này")
        case HTTPError.notSendMorningMessage:
            return LocalizedString("notification_error_notSendMorningMessage", comment: "Bạn chưa gửi lời nhắn đầu ngày nào trong khoảng thời gian này")
        case HTTPError.notSendThanksMessage:
            return LocalizedString("notification_error_notSendThanksMessage", comment: "Bạn chưa gửi lời cảm ơn nào trong khoảng thời gian này")
        case HTTPError.notSendMessage:
            return LocalizedString("notification_error_notSendMessage", comment: "Bạn chưa gửi lời nhắn nào trong khoảng thời gian này")
        case HTTPError.locked:
            return LocalizedString("notification_error_locked", comment: "Tài khoản hiện tại không khả dụng để nhận mã")
        case HTTPError.notActiveInRange:
            return LocalizedString("notification_error_notActiveInRange", comment: "Tài khoản chưa kích hoạt trong thời gian diễn ra chương trình")
        case HTTPError.noUserIds:
            return LocalizedString("notification_error_noUserIds", comment: "Học sinh không có phụ huynh hoặc mã phụ huynh không hợp lệ")
        case HTTPError.promotionValidationForm:
            return LocalizedString("notification_error_promotionValidationForm", comment: "Thông tin bạn cung cấp không hợp lệ")
        case HTTPError.invalidPhoneAndEmail:
            return LocalizedString("notification_error_invalidPhoneAndEmail", comment: "Chưa nhập địa chỉ email và số điện thoại")
        case HTTPError.invalidName:
            return LocalizedString("notification_error_invalidName", comment: "Chưa nhập tên người dùng")
        case HTTPError.conflictPhoneOrEmail:
            return LocalizedString("notification_error_conflictPhoneOrEmail", comment: "Số điện thoại hoặc email khác với thông tin trong tài khoản")
            
            
        case HTTPError.extracurricularRegistered:
            return  LocalizedString("notification_error_extracurricularRegistered", comment: "Bạn đã đăng ký ngoại khóa này")
            
            
        case HTTPError.canNotPayment:
            return  LocalizedString("notification_error_canNotPayment", comment: "Không được thực hiện giao dịch này")
            
        case HTTPError.transactionIsDismissed:
            return  LocalizedString("notification_error_transactionIsDismissed", comment: "Giao dịch đã bị hủy")
            
        case HTTPError.transactionIsSuccessfully:
            return  LocalizedString("notification_error_transactionIsSuccessfully", comment: "Giao dịch thành công")
            
        case HTTPError.transactionIsFailed:
            return  LocalizedString("notification_error_transactionIsFailed", comment: "Giao dịch thất bại")
            
        case HTTPError.transactionWasHandled:
            return  LocalizedString("notification_error_transactionWasHandled", comment: "Giao dịch này đã được thực hiện trước đó")

        default:
            return LocalizedString("notification_login_failed", comment: "Không thể kết nối đến server.")
        }
        
    }
    
    
    /**
     Hàm faddingView
     */
    static func setVisibilityOf(_ view: UIView, to visible: Bool, duration: TimeInterval = 0.3.second, completion: ((Bool) -> Void)? = nil) {
        
        if visible { view.isHidden = false }
        UIView.animate(withDuration: duration,
                       animations: {
                        view.alpha = visible ? 1.0 : 0.0
        },
                       completion: { finished in
                        view.isHidden = !visible
                        if let completion = completion { completion(true) }
        })
    }
    
    
    
    /// - MARK : Send FireBase
    
    static func sendLogEvent(_ eventType: AnalyticsType) {
    
        Analytics.logEvent("select_content",
                                      parameters: ["content_type" : eventType.toInfo().contentType as NSObject,
                                        "item_id" : eventType.toInfo().termID as NSObject])
    }
    
    
    /**
     Lấy string từ Time
     */
    static func stringFromTime(_ time: Double, formatter: DateFormatter) -> String {
        if Defaults[.SelectedLanguage] == LanguageValue.vietnamese.. {
            formatter.locale = Locale(identifier: "vi_VN")
        } else {
            formatter.locale = Locale(identifier: "en_US_POSIX")
        }
        return formatter.string(from: Date(timeIntervalSince1970: time))
    }
    
    /// DATE TIME FORMATTER
    static func getTimeFormatter(_ string: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = string
        if Defaults[.SelectedLanguage] == LanguageValue.vietnamese.. {
            formatter.locale = Locale(identifier: "vi_VN")
        } else {
            formatter.locale = Locale(identifier: "en_US_POSIX")
        }
        
        return formatter
    }
    
    /// DATE TIME FORMATTER
    static func getStringTime(_ time: Double, formatter: String) -> String {
        return getTimeFormatter(formatter).string(from: Date(timeIntervalSince1970: time))
    }
    
    
    /*
     Cập nhật số thông báo của app
     */
    
    static func updateNotificaitonBadge(tabbar: UITabBar?) {
        
        guard let tab = tabbar, let items = tab.items else { return }


        let notificaiton = (AppData.instance.channels.filter { $0.unread == true && $0.senderID != (AppData.instance.myInfo?.uid ?? "") }).count
        
        UIApplication.shared.applicationIconBadgeNumber = notificaiton

        if let position = AppData.instance.tabbarItems.firstIndex(where: { $0 == .message }) {

            if let item = (items.filter { $0.tag == position }).first {

                if notificaiton > 0 {
                    item.badgeValue = notificaiton > 99 ? "99+" : String(notificaiton)
                } else {
                    item.badgeValue = nil
                }
            }
        }
    }
    
    
    /**
     Kiểm tra xem đã có thể gửi lại mã kích hoạt hay chưa (quá 5' từ lần kích hoạt cuối)
     
     - returns:
     */
    public static func isVerifyCodeResendable() -> Bool {
        return Defaults[.RegisterTime].seconds + 5.minutes < Date().timeIntervalSince1970
    }
    
    /**
     Lấy thời gian còn lại phải chờ để có thể gửi lại mã kích hoạt
     
     - returns:
     */
    public static func verifyCodeResendableTimeLeft() -> TimeInterval {
        let currentTime = Date().timeIntervalSince1970
        let regTime = Defaults[.RegisterTime]
        if regTime.seconds + 5.minutes >= currentTime {
            return regTime.seconds + 5.minutes - currentTime
        }
        return 0
    }
    
    public static func stringFromTimeInterval(_ duration: TimeInterval, format: TimeFormat) -> String {
        if duration < 0 {
            return LocalizedString("time_default_smaller_a_minute", comment: "Dưới 1 phút")
        }
        
        if duration <= 60 && !format.contains(.AbsoluteTime) {
            return LocalizedString("time_default_smaller_a_minute", comment: "Dưới 1 phút")
        }
        
        var string = String()
        
        let hour: Int = Int(duration) / 3600
        let minute: Int = (Int(duration) - (hour * 3600)) / 60
        let second: Int = Int(duration) - hour * 3600 - minute * 60
        
        if format.contains(.Hour) || (format.contains(.OptionalHour) && hour > 0) { string += hour.toString(2) }
        
        
        if format.contains(.Minute) {
            if format.contains(.Hour) || (format.contains(.OptionalHour) && hour > 0) { string += ":" }
            string += minute.toString(2)
        }
        
        if format.contains(.Second) {
            if format.contains(.Minute) { string += ":" }
            string += second.toString(2)
        }
        
        
        return string
    }
    
  
}


extension Utility {
//    static func saveImageToFile(_ image: UIImage) {
//        let filePath = appDirectory().stringByAppendingPathComponent("profile_picture.png")
//        try? UIImageJPEGRepresentation(image, 0.7)!.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
//        addSkipBackupAttributeToItem("profile_picture.png")
//    }
//    
//    static func retriveImageFromFile() -> UIImage? {
//        let filePath = appDirectory().stringByAppendingPathComponent("profile_picture.png")
//        return UIImage(contentsOfFile: filePath)
//    }
//    
//    static func removeImageFile() {
//        deleteFile("profile_picture.png")
//    }
//    
//    static func copyFileToAppDirectory(_ name: String) {
//        guard !isFileExist(name) else { return }
//        
//        let destination = appDirectory().stringByAppendingPathComponent(name)
//        
//        if let file = Bundle.main.path(forResource: name, ofType: nil) {
//            do {
//                try FileManager.default.copyItem(atPath: file, toPath: destination)
////                Log.message(.Verbose, message: "Copy file \(name) to \(appDirectory())")
//            }
//            catch {
////                print( "Copy file error - \(error)")
//            }
//            
//        }
//    }
//    
//    fileprivate static func deleteFile(_ fileName: String) {
//        if isFileExist(fileName) {
//            do {
//                try FileManager.default.removeItem(atPath: appDirectory().stringByAppendingPathComponent(fileName))
//            } catch {
//            }
//            log("delete file \(fileName)")
//        }
//    }
//    
//    fileprivate static func isFileExist(_ fileName: String) -> Bool {
//        let file = appDirectory().stringByAppendingPathComponent(fileName)
//        return FileManager.default.fileExists(atPath: file)
//    }
    
    static func appDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
//    fileprivate static func addSkipBackupAttributeToItem(_ fileName: String) -> Bool {
//        if isFileExist(fileName) {
//            return addSkipBackupAttributeToItem(URL(fileURLWithPath: appDirectory().stringByAppendingPathComponent(fileName)))
//        }
//        log("set skip backup attribute failed")
//        return false
//    }
    

    
    /*
    
    static func getDataLogin(dataInfo: Any?) -> HTTPLogin.RespondType? {
        
        do {
            
            guard let item = dataInfo as? Data else { return nil }
            guard let json = try JSONSerialization.jsonObject(with: item , options: []) as? [String: AnyObject] else { return  nil }
            guard let data = json["data"] as? [String: AnyObject] else { return nil }
            

            guard let students = data["students"] as? [[String : Any]], students.count > 0 else { return nil  }
            guard let _ = data["user"] as? [String : Any] else { return nil  }
            
            return HTTPLogin.RespondType(data: data)
        
        }
        catch {
            return nil
        }
    }
 
 */
    
    /*
    static func getDataHome(dataInfo: Any?) -> HTTPHome.RespondType? {

        do {
            
            guard let item = dataInfo as? Data else { return nil }
            guard let json = try JSONSerialization.jsonObject(with: item , options: []) as? [String: AnyObject] else { return  nil }
            guard let data = json["data"] as? [String: AnyObject] else { return nil }
            
            return HTTPHome.RespondType(data: data)
        }
        catch {
            return nil
        }
        
    }
 */
    
    /*
    static func getDataContact(dataInfo: Any?) -> [GuestInfo] {
        
        do {
            
            guard let item = dataInfo as? Data else { return [] }
            guard let json = try JSONSerialization.jsonObject(with: item , options: []) as? [String: AnyObject] else { return  [] }
            guard let data = json["data"] as? [String: AnyObject] else { return [] }
            guard let contacts = data["contacts"] as? [[String: AnyObject]] else { return [] }
            
            let guestInfoList = contacts.map { contact in
                
                GuestInfo(id: (contact["id"] as? Int) ?? 0,
                          name: (contact["name"] as? String) ?? "",
                          avatar: (contact["avatar"] as? String) ?? "",
                          type: (contact["type_app"] as? Int) ?? 0,
                          imageAvatar: nil,
                          introduction: (contact["introduction"] as? String) ?? "",
                          phone: (contact["phone"] as? String) ?? "",
                          email: (contact["email"] as? String) ?? "",
                          address: (contact["address"] as? String) ?? "",
                          schoolName: (contact["schoolName"] as? String) ?? "",
                          schoolID: (contact["schoolID"] as? String) ?? "",
                          lastUpdate: 0,
                          isNewMessage: false)
            }
            
            return guestInfoList
        }
        catch {
            return []
        }
    }
    
*/

    
    static func cropImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        
        
        guard image.size.height > size.height || image.size.width > size.width else { return image }

        var newSize: CGSize
        /**
         *  Resize xuống 720 x 720
         */
        if image.size.width >= image.size.height { newSize = CGSize(width: size.width, height: size.width * image.size.height / image.size.width) }
        else { newSize = CGSize(width: size.height * image.size.width / image.size.height, height: size.height) }
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        /**
         *  Crop to size
         */
        
        let x = (((newImage?.cgImage)?.width)! - Int(size.width)) / 2
        let y = (((newImage?.cgImage)?.height)! - Int(size.height)) / 2
        let cropRect = CGRect(x: x, y: y, width: Int(size.height), height: Int(size.width))
        let imageRef = (newImage?.cgImage)?.cropping(to: cropRect)
        
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: (newImage?.imageOrientation)!)
        return cropped
    }
}




extension Utility {
    
    
    /*
    static func realmConfiguration(_ dbName: String) {
        
        
        /// Version = 1: 11/3/2017 - 3.0.5
        /// Version = 2: 18/3/2017 - 3.0.6
        
        /// Version = 3: 19/10/2017 - 3.1.2
        
        /// Version = 4: 17/08/2018 - 3.2.6
        
        /// Version = 6: 23/08/2018 - 3.2.7
        
        /// Version = 7: 15/11/2018 - 3.2.10: Thêm popup_content
        
        /// Version = 8: 29/03/2018
        
        
        /// Version = 9: 31/05/2019: Thêm students in Account
        
        /// Version = 10: 20/06/2019: Thêm contentComment in albums
        
         /// Version = 11: 26/07/2019: Thêm item Type trong Notification
        
        /// Version = 13: 12/09/2019: Thêm item list id đã đọc + xóa trong Notification
        
        
        /// Version = 14: 13/09/2019: Thêm canLike, displayLike, displayComment trong Album
        
        /// Version = 15: 15/09/2019: Thêm canLike, displayLike, displayComment trong Realm Photo
        
        /// Version = 16: 16/09/2019: Thêm newComments trong Realm Album
        
        
         /// Version = 17: 16/09/2019: Xóa Realm Comment, newComments trong Realm Album
         /// Version = 17: 16/09/2019: Thêm Realm Higghlight Comment, comments trong Realm Album
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            fileURL: FileManager.default.getDocumentDirectory().appendingPathComponent("\(dbName).realm"),
            encryptionKey: RealmKey,
            schemaVersion: 17,
            migrationBlock: { (migration, oldSchemaVersion) in
                
                /// Thay đổi của RealmNotifications
                migration.enumerateObjects(ofType: RealmNotification.className()) { oldObject, newObject in
                    if oldSchemaVersion < 1 {
                        newObject!["action"] = 1
                    }
                    
                    if oldSchemaVersion < 3 {
                        newObject!["url"] = ""
                        newObject!["subTitle"] = ""
                    }
                    
                    if oldSchemaVersion < 4 {
                        newObject!["message"] = ""
                    }
                    
                    if oldSchemaVersion < 7 {
                        newObject!["popupContent"] = ""
                    }
                    
                    if oldSchemaVersion < 8 {
                        newObject!["titleEN"] = ""
                        newObject!["subTitleEN"] = ""
                    }
                    
                    if oldSchemaVersion < 10 {
                        newObject!["itemType"] = 0
                    }
                }
                
               
                
                /// Bổ sung RealmAccount: 18/3/2017: Version 3
                
                migration.enumerateObjects(ofType: RealmAccount.className()) { oldObject, newObject in
                    
                    if oldSchemaVersion < 4 {
                        newObject!["lastUpdatePopupNews"] = 0
                    }
                    
                    if oldSchemaVersion < 9 {
                        newObject!["students"] = []
                    }
                    
                    /// Thêm loại type cho noti chat
                    if oldSchemaVersion < 13 {
                        newObject!["listIDNotificationDelete"] = []
                        newObject!["listIDNotificationRead"] = []
                    }
                    
                }
                
                if oldSchemaVersion < 2 { }
                
                /// Bổ sung RealmPopupNews: 17/08/2018: Version 4
                migration.enumerateObjects(ofType: RealmPopupNews.className()) { oldObject, newObject in
                    
                    if oldSchemaVersion < 6 {
                        newObject!["isDismiss"] = 0
                        newObject!["delayShow"] = 0
                        newObject!["clicked"] = 0
                    }
                }
                
                
                /// Bổ sung contentComment: 20/06/2019: Version 10
                migration.enumerateObjects(ofType: RealmAlbum.className()) { oldObject, newObject in
                    
                    if oldSchemaVersion < 10 {
                        newObject!["contentComment"] = ""
                        newObject!["author"] = ""
                        newObject!["authorAvatar"] = ""
                    }
                    
                    if oldSchemaVersion < 11 {
                        newObject!["filterType"] = []
                    }
                    
                    
                    if oldSchemaVersion < 14 {
                        newObject!["canLike"] = true
                        newObject!["displayComment"] = true
                        newObject!["displayLike"] = true
                    }
                    
//                    if oldSchemaVersion < 16 {
//                        newObject!["newComments"] = []
//
//                    }
                    
                    
                    if oldSchemaVersion < 17 {
                        newObject!["comments"] = []
                        
                    }
                    
                    
                }
                
                /// Bổ sung can Like: 15/09/2019: Version 15
                migration.enumerateObjects(ofType: RealmPhoto.className()) { oldObject, newObject in
                    

                    
                    if oldSchemaVersion < 15 {
                        newObject!["canLike"] = true
                        newObject!["displayComment"] = true
                        newObject!["displayLike"] = true
                    }
                    
                }
                
        },
//            deleteRealmIfMigrationNeeded: false
            deleteRealmIfMigrationNeeded: true
        )
        
        print( "Realm link: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL?.absoluteString))")
        print( "RealmKey: \(RealmKey.hexadecimalString)")
        
    }
 
 */
}




