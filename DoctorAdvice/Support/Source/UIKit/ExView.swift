//
//  ExView.swift
//  PHExtensions
//
//  Created by Hoan Pham on 7/16/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import UIKit

public extension UIView {
    
    var fWidth: CGFloat {
        get { return self.frame.size.width }
        set { self.frame.size.width = newValue }
    }
    
    var fHeight: CGFloat {
        get { return self.frame.size.height }
        set { self.frame.size.height = newValue }
    }
    
    var fTop: CGFloat {
        get { return self.frame.origin.y }
        set { self.frame.origin.y = newValue }
    }
    var fRight: CGFloat {
        get { return self.frame.origin.x + self.fWidth }
        set { self.frame.origin.x = newValue - self.fWidth }
    }
    var fBottom: CGFloat {
        get { return self.frame.origin.y + self.fHeight }
        set { self.frame.origin.y = newValue - self.fHeight }
    }
    var fLeft: CGFloat {
        get { return self.frame.origin.x }
        set { self.frame.origin.x = newValue }
    }
    
    var fCenterX: CGFloat{
        get { return self.center.x }
        set { self.center = CGPoint(x: newValue, y: self.fCenterY) }
    }
    var fCenterY: CGFloat {
        get { return self.center.y }
        set { self.center = CGPoint(x: self.fCenterX, y: newValue) }
    }
    
    var fOrigin: CGPoint {
        set { self.frame.origin = newValue }
        get { return self.frame.origin }
    }
    var fSize: CGSize {
        set { self.frame.size = newValue }
        get { return self.frame.size }
    }
}

public extension UIView {
    
    var bWidth: CGFloat {
        get { return self.bounds.size.width }
        set { self.bounds.size.width = newValue }
    }
    
    var bHeight: CGFloat {
        get { return self.bounds.size.height }
        set { self.bounds.size.height = newValue }
    }
    
    var bTop: CGFloat {
        get { return self.bounds.origin.y }
        set { self.bounds.origin.y = newValue }
    }
    var bRight: CGFloat {
        get { return self.bounds.origin.x + self.bWidth }
        set { self.bounds.origin.x = newValue - self.bWidth }
    }
    var bBottom: CGFloat {
        get { return self.bounds.origin.y + self.bHeight }
        set { self.bounds.origin.y = newValue - self.bHeight }
    }
    var bLeft: CGFloat {
        get { return self.bounds.origin.x }
        set { self.bounds.origin.x = newValue }
    }
    
    var bCenterX: CGFloat{
        get { return self.center.x }
        set { self.center = CGPoint(x: newValue, y: self.bCenterY) }
    }
    var bCenterY: CGFloat {
        get { return self.center.y }
        set { self.center = CGPoint(x: self.bCenterX, y: newValue) }
    }
    
    var bOrigin: CGPoint {
        set { self.bounds.origin = newValue }
        get { return self.bounds.origin }
    }
    var bSize: CGSize {
        set { self.bounds.size = newValue }
        get { return self.bounds.size }
    }
}
