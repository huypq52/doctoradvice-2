//
//  ExFileManager.swift
//  PHSelf
//
//  Created by Hoan Pham on 12/26/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import CleanroomLogger

public extension FileManager {
    func createAppDirectory(_ name: String, skipBackupAttribute: Bool) {
        
        let dbPath = urls(for: .documentDirectory, in: .allDomainsMask)[0].appendingPathComponent(name)
        guard !fileExists(atPath: dbPath.path) else { return }
        
        do {
            try createDirectory(atPath: dbPath.path, withIntermediateDirectories: false, attributes: nil)
            
            if skipBackupAttribute {
                addSkipBackupAttributeToItem(dbPath)
            }
        }
        catch {
//            log("\(type(of: self)) error: \(error)")
        }
    }
    
    func saveImageToFile(_ image: UIImage, name: String) {
        let filePath = getDocumentDirectory().appendingPathComponent(name)
        try? image.jpegData(compressionQuality: 0.9)!.write(to: filePath, options: [.atomic])
        addSkipBackupAttributeToItem("profile_picture.png")
    }
    
    func retriveImageFromFile(_ name: String) -> UIImage? {
        let filePath = getDocumentDirectory().appendingPathComponent(name)
        return UIImage(contentsOfFile: filePath.path)
    }
    
    func removeImageFile(_ name: String) {
        deleteFile(name)
    }
    
    func deleteFile(_ fileName: String) {
        guard fileExistence(name: fileName) else { return }
        do {
            try removeItem(at: getDocumentDirectory().appendingPathComponent(fileName))
//            log("\(type(of: self)) delete file \(fileName)")
        } catch {
//            log("\(type(of: self)) error: \(error)")
        }
    }
    
    func fileExistence(name fileName: String) -> Bool {
        let path = getDocumentDirectory().appendingPathComponent(fileName)
        
        return fileExists(atPath: path.path)
    }
    
    func getDocumentDirectory() -> URL {
        return urls(for: .documentDirectory, in: .allDomainsMask)[0]
    }
    
    @discardableResult
    fileprivate func addSkipBackupAttributeToItem(_ fileName: String) -> Bool {
        if fileExistence(name: fileName) {
            return addSkipBackupAttributeToItem(getDocumentDirectory().appendingPathComponent(fileName))

        }
//        log("\(type(of: self)) set skip backup attribute failed")
        return false
    }
    
    @discardableResult
    fileprivate func addSkipBackupAttributeToItem(_ url: URL) -> Bool {
        
        guard fileExists(atPath: url.path) else {
//            log("\(type(of: self)) set skip backup attribute failed - file not exist")
            return false
        }
       
        do {
//            url.setResourceValues(URLResourceValues.init())
            try (url as NSURL).setResourceValue(NSNumber(value: true as Bool),forKey: URLResourceKey.isExcludedFromBackupKey)
            return true
        } catch {
//            log("\(type(of: self)) Error excluding \(url.lastPathComponent) from backup \(error)")
            return false
        }
 
    }
    
}
