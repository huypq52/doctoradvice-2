//
//  ExNavigationController.swift
//  PHUtility
//
//  Created by Hoan Pham on 12/26/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import CleanroomLogger
public extension UINavigationController {
    
    func containController<T>(_: T.Type) -> Bool {
        if (viewControllers.takeFirst { $0 is T }) != nil {
            return true
        }
        return false
    }
    
    func getController<T>(_ reversedSearch: Bool, _: T.Type) -> T? {
        if let controller = (Array(reversedSearch ? viewControllers.reversed() : viewControllers).takeFirst { $0 is T }) , controller is T {
            return controller as? T
        }
        return nil
        
    }
    
    func removeController<T>(_: T.Type) {
        viewControllers = viewControllers.filter { !($0 is T) }
    }
    
    func removeController<T, V>(_: T.Type, _: V.Type) {
        viewControllers = viewControllers.filter { !($0 is T) && !($0 is V) }
    }
    
    func removeController<T, U, V, K, Y>(_: T.Type, _: V.Type, _: U.Type, _: K.Type, _: Y.Type) {
        viewControllers = viewControllers.filter { !($0 is T || $0 is V || $0 is U  || $0 is K || $0 is Y) }
    }
    
    func removeController<T, U, V, K, Y, X, W>(_: T.Type, _: V.Type, _: U.Type, _: K.Type, _: Y.Type, _: X.Type, _: W.Type) {
        viewControllers = viewControllers.filter { !($0 is T || $0 is V || $0 is U  || $0 is K || $0 is Y || $0 is X || $0 is W) }
    }
    
    func removeAllControllersExcept<T>(_: T.Type) {
        viewControllers = viewControllers.filter { $0 is T }
    }
    
    func removeAllControllersExcept<T, V>(_: T.Type, _: V.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V }
    }
    
    func removeAllControllersExcept<T, U, V>(_: T.Type, _: V.Type, _: U.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V || $0 is U }
    }
    
    func removeAllControllersExcept<T, U, V, K>(_: T.Type, _: V.Type, _: U.Type, _: K.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V || $0 is U  || $0 is K }
    }
    
    func removeAllControllersExcept<T, U, V, K, Y>(_: T.Type, _: V.Type, _: U.Type, _: K.Type, _: Y.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V || $0 is U  || $0 is K || $0 is Y }
    }
    
    func removeAllControllersExcept<T, U, V, K, Y, X>(_: T.Type, _: V.Type, _: U.Type, _: K.Type, _: Y.Type, _: X.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V || $0 is U  || $0 is K || $0 is Y || $0 is X}
    }
    
    func removeAllControllersExcept<T, U, V, K, Y, X, W>(_: T.Type, _: V.Type, _: U.Type, _: K.Type, _: Y.Type, _: X.Type, _: W.Type) {
        viewControllers = viewControllers.filter { $0 is T || $0 is V || $0 is U  || $0 is K || $0 is Y || $0 is X || $0 is W }
    }

}
