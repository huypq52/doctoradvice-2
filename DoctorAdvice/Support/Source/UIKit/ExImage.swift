//
//  ExImage.swift
//  STaxi
//
//  Created by Hoan Pham on 4/14/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import CleanroomLogger


extension UIImage {
    
    public func tint(_ color:UIColor) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        color.set()
        UIRectFill(rect)
        draw(in: rect, blendMode: CGBlendMode.destinationIn, alpha: CGFloat(1.0))
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIImageView {
    
    public func tint(_ color: UIColor) -> UIImageView {
        let template = image?.withRenderingMode(.alwaysTemplate)
        
        let imageView = UIImageView(image: template)
        imageView.tintColor = color
        return imageView
    }
}
