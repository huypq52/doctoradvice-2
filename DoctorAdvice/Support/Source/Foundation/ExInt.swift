//
//  ExInt.swift
//  STaxi
//
//  Created by Hoan Pham on 7/1/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import Foundation

extension Int {
    public func toString(_ length: Int) -> String {
        return String(format: "%.\(length)d", self)
    }
}


extension Int {
    
    public static func random(_ range: Range<Int>) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0   // allow negative ranges
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
    
    public static func random(_ min: Int = 0, max: Int) -> Int {
        return Int(arc4random_uniform(UInt32((max - min) + 1))) + min
    }
  
}
extension Int {
    
    
    public var years:	TimeInterval { return 365 * self.days }
    
    public var year:	TimeInterval { return self.years }
    
    public var days:	TimeInterval { return 24 * self.hours }
    
    public var day:		TimeInterval { return self.days }
    
    public var hours:	TimeInterval { return 60 * self.minutes }
    
    public var hour:	TimeInterval { return self.hours }
    
    public var minutes:	TimeInterval { return 60 * self.seconds }
    
    public var minute:	TimeInterval { return self.minutes }
    
    public var seconds: TimeInterval { return TimeInterval(self) }
    
    public var second:	TimeInterval { return self.seconds }
}
