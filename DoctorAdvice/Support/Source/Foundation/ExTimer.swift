//
//  ExTimer.swift
//  STaxi
//
//  Created by Hoan Pham on 4/8/15.
//  Copyright (c) 2015 Hoan Pham. All rights reserved.
//

import Foundation

private class ScheduledTimerActor {
    var block: (Timer?) -> Void
    var timer: Timer!
    
    init(_ block: @escaping (Timer?) -> Void) {
        self.block = block
    }
    
    @objc func fire() {
        block(timer)
    }
}

private class RepeatTimerActor {
    var block: (Int, Timer?) -> Void
    var timer: Timer!
    var firedTimes: Int = 1
    
    init(_ block: @escaping (Int, Timer?) -> Void) {
        self.block = block
    }
    
    @objc func fire() {
        block(firedTimes, timer)
        firedTimes += 1
    }
}

public extension Timer {
    // NOTE: `new` class functions are a workaround for a crashing bug when using convenience initializers (18720947)
    
    /// Create a timer that will call `block` once after the specified time.
    ///
    /// **Note:** the timer won't fire until it's scheduled on the run loop.
    /// Use `NSTimer.after` to create and schedule a timer in one step.
    
    class func new(after interval: Foundation.TimeInterval, _ block: @escaping (Timer?) -> Void) -> Timer {
        let timer: Timer!
        let actor = ScheduledTimerActor(block)
        timer = self.init(timeInterval: interval, target: actor, selector: #selector(ScheduledTimerActor.fire), userInfo: nil, repeats: false)
        actor.timer = timer
        return timer
    }
    
    /// Create a timer that will call `block` repeatedly in specified time intervals.
    ///
    /// **Note:** the timer won't fire until it's scheduled on the run loop.
    /// Use `NSTimer.every` to create and schedule a timer in one step.
    
    class func new(every interval: Foundation.TimeInterval, _ block: @escaping (Int, Timer?) -> Void) -> Timer {
        let timer: Timer!
        let actor = RepeatTimerActor(block)
        timer = self.init(timeInterval: interval, target: actor, selector: #selector(ScheduledTimerActor.fire), userInfo: nil, repeats: true)
        actor.timer = timer
        return timer
    }
    
    /// Create and schedule a timer that will call `block` once after the specified time.
    
    @discardableResult
    class func after(_ interval: Foundation.TimeInterval, _ block: @escaping (Timer?) -> Void) -> Timer {
        let timer = Timer.new(after: interval, block)
        timer.start()
        return timer
    }
    
    /// Create and schedule a timer that will call `block` repeatedly in specified time intervals.
    @discardableResult
    class func every(_ interval: Foundation.TimeInterval, _ block: @escaping (Int, Timer?) -> Void) -> Timer {
        let timer = Timer.new(every: interval, block)
        timer.start()
        return timer
    }
    
    /// Schedule this timer on the run loop
    ///
    /// By default, the timer is scheduled on the current run loop for the default mode.
    /// Specify `runLoop` or `modes` to override these defaults.
    
    func start(_ runLoop: RunLoop = RunLoop.current, modes: String...) {
        let modes = modes.count != 0 ? modes : [RunLoop.Mode.default.rawValue]
        
        for mode in modes {
            runLoop.add(self, forMode: RunLoop.Mode(rawValue: mode))
        }
    }
}
