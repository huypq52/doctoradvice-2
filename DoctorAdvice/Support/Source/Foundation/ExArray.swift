//
//  ExArray.swift
//  Staxi
//
//  Created by Hoan Pham on 8/12/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation


extension Array where Element : Equatable {
    /**
    Difference of self and the input arrays.
    
    :param: values Arrays to subtract
    :returns: Difference of self and the input arrays
    */
    public func difference <T: Equatable> (_ values: [T]...) -> [T] {
        
        var result = [T]()
        
        elements: for e in self {
            if let element = e as? T {
                for value in values {
                    //  if a value is in both self and one of the values arrays
                    //  jump to the next iteration of the outer loop
                    if value.contains(element) {
                        continue elements
                    }
                }
                
                //  element it's only in self
                result.append(element)
            }
        }
        
        return result
        
    }
}

extension Array {
    public func difference<T>(_ input: [T], condition: (Element, T) -> Bool) -> [Element] {
        var result = [Element]()
        for item1 in self {
            for item2 in input {
                if condition(item1, item2) { result.append(item1); break }
            }
        }
        return result
    }
    
    
}
/*
    /**
    Intersection of self and the input arrays.
    
    :param: values Arrays to intersect
    :returns: Array of unique values contained in all the dictionaries and self
    */
    func intersection <U: Equatable> (values: [U]...) -> Array {
        
        var result = self
        var intersection = Array()
        
        for (i, value) in values.enumerate() {
            
            //  the intersection is computed by intersecting a couple per loop:
            //  self n values[0], (self n values[0]) n values[1], ...
            if (i > 0) {
                result = intersection
                intersection = Array()
            }
            
            //  find common elements and save them in first set
            //  to intersect in the next loop
            value.each { (item: U) -> Void in
                if result.contains(item) {
                    intersection.append(item as! Element)
                }
            }
            
        }
        
        return intersection
        
    }
    
    /**
    Union of self and the input arrays.
    
    :param: values Arrays
    :returns: Union array of unique values
    */
    func union <U: Equatable> (values: [U]...) -> Array {
        
        var result = self
        
        for array in values {
            for value in array {
                if !result.contains(value) {
                    result.append(value as! Element)
                }
            }
        }
        
        return result
        
    }
}
*/

//extension CollectionType where Generator.Element : Equatable {
//    func contains <T: Equatable> (items: T...) -> Bool {
//        return items.all { self.indexOf($0) >= 0 }
//    }
//    
//    
//}

extension Sequence {
//    func map<T>(@noescape transform: (Self.Generator.Element) throws -> T) rethrows -> [T]
//    func filter(@noescape includeElement: (Self.Generator.Element) throws -> Bool) rethrows -> [Self.Generator.Element]
    public func takeFirst(_ condition: (Self.Iterator.Element) -> Bool) -> Self.Iterator.Element? {
        for item in self {
            if condition(item) { return item }
        }
        return nil
    }
    
    public func takeFirst<T>(_ input: [T], condition: (Self.Iterator.Element, T) -> Bool) -> Self.Iterator.Element? {
        for item1 in self {
            for item2 in input {
                if condition(item1, item2) { return item1 }
            }
        }
        return nil
    }
    
    public func each(f: (Self.Iterator.Element) throws -> Void) rethrows -> Self {
        for item in self { try f(item) }
        return self
        
    }
    
    public func eachWithIndex(f: (Int, Self.Iterator.Element) throws -> Void) rethrows -> Self {
        for (i, o) in self.enumerated() { try f(i, o) }
        return self
    }
    
//    func partitionBy<T, U: Equatable>(array: [T], function: (T) -> U) -> [[T]] {
//        var result = [[T]]()
//        var lastValue: U? = .None
//        
//        for item in array {
//            let value = function(item)
//            
//            if let lastValue = lastValue where value == lastValue {
//                result[result.count-1].append(item)
//            } else {
//                result.append([item])
//                lastValue = value
//            }
//        }
//        return result
//    }
    
    
}

extension Array {
    public func max <U: Comparable> () -> U {
        
        return map {
            return $0 as! U
            }.max()!
        
    }
    
    public func min <U: Comparable> () -> U {
        
        return map {
            return $0 as! U
            }.min()!
        
    }
    
    /**
    Applies cond to each element in array, splitting it each time cond returns a new value.
    
    :param: cond Function which takes an element and produces an equatable result.
    :returns: Array partitioned in order, splitting via results of cond.
    */
    public func partitionBy <T: Equatable> (_ cond: (Element) -> T) -> [Array] {
        var result = [Array]()
        var lastValue: T? = nil
        
        for item in self {
            let value = cond(item)
            
            if value == lastValue {
                let index: Int = result.count - 1
                result[index] += [item]
            } else {
                result.append([item])
                lastValue = value
            }
        }
        
        return result
    }
    
    
    /**
    Checks if self contains a list of items.
    
    :param: items Items to search for
    :returns: true if self contains all the items
    */
//    func contains <T: Equatable> (items: T...) -> Bool {
//        return items.all {
//            do {
//                try self.indexOf($0) >= 0
//            } catch {
//                return false
//            }
//            
//            try! self.indexOf($0) >= 0
//        
//        }
//    }
    
    public func all (_ test: (Element) -> Bool) -> Bool {
        for item in self {
            if !test(item) {
                return false
            }
        }
        
        return true
    }
    
    
    
}

/**
Remove an element from the array
*/
public func - <T: Equatable> (first: [T], second: T) -> [T] {
    return first - [second]
}

/**
Difference operator
*/
public func - <T: Equatable> (first: [T], second: [T]) -> [T] {
    return first.difference(second)
}
