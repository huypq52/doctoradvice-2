//
//  LazyInitSugar.swift
//  PHSelf
//
//  Created by Hoan Pham on 1/4/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import Foundation

public protocol LazyInitSugarSyntax { }

extension LazyInitSugarSyntax {
	public func config(_ block: (Self) -> Void) -> Self {
		block(self)
		return self
	}
}

extension NSObject: LazyInitSugarSyntax { }
