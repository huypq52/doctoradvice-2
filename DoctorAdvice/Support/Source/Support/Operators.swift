//
//  Operators.swift
//  PHUtility
//
//  Created by Hoan Pham on 12/26/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation


public postfix func ++ <T: RawRepresentable, V: BinaryFloatingPoint>(lhs: T) -> V {
    return (lhs.rawValue as! V) + 1.0
}

public postfix func -- <T: RawRepresentable, V: BinaryFloatingPoint>(lhs: T) -> V {
    return (lhs.rawValue as! V) - 1.0
}
 

public postfix func ++ <T: RawRepresentable, V: BinaryInteger>(lhs: T) -> V {
    return (lhs.rawValue as! V) + 1
}

public postfix func -- <T: RawRepresentable, V: BinaryInteger>(lhs: T) -> V {
    return (lhs.rawValue as! V) - 1
}

postfix operator ..

public postfix func ..<T: RawRepresentable> (lhs: T) -> T.RawValue {
    return lhs.rawValue
}


