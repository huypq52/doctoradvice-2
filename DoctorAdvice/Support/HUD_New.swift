//
//  HUD_New.swift
//  KidOnline
//
//  Created by Pham Huy on 3/8/19.
//  Copyright © 2018 KidOnline. All rights reserved.
//


import UIKit
import MBProgressHUD
//import PHExtensions
import JGProgressHUD
//import CleanroomLogger
import NVActivityIndicatorView

/*
 class JGProgressHUDGifIndicatorView: JGProgressHUDIndicatorView {
 init(gifView: UIImageView) {
 super.init(contentView: gifView)
 }
 
 required init?(coder aDecoder: NSCoder) {
 fatalError("init(coder:) has not been implemented")
 }
 
 }
 */

class HUD {
    
    enum HUDType {
        case none
        case error, success
        case pie, ring
        case indeterminate
        case image(UIImage)
        case gif(UIImageView)
    }
    
    fileprivate enum LastHUD {
        case none
        case hud
        case message
    }
    
    static var style: MBProgressHUDBackgroundStyle = .solidColor
    fileprivate static var hud: MBProgressHUD = MBProgressHUD()
    fileprivate static var lastest: LastHUD = .none
    
    //    private static func createIndicatorView(type: MBProgressHUDMode) -> JGProgressHUDIndicatorView {
    //        switch type {
    //        case .None:
    //            return JGProgressHUDIndicatorView()
    //        case .Error:
    //            return JGProgressHUDErrorIndicatorView()
    //        case .Success:
    //            return JGProgressHUDSuccessIndicatorView()
    //        case .Pie:
    //            return JGProgressHUDPieIndicatorView(HUDStyle: style)
    //        case .Ring:
    //            return JGProgressHUDRingIndicatorView(HUDStyle: style)
    //        case .Indeterminate:
    //            return JGProgressHUDIndeterminateIndicatorView(HUDStyle: style)
    //        case .Image(let image):
    //            return JGProgressHUDImageIndicatorView(image: image)
    //        case .Gif(let view):
    //            return JGProgressHUDGifIndicatorView(gifView: view)
    //        }
    //    }
    
    
    //        static func showMessage(_ text: String, type: HUDType = .none, onView view: UIView? = nil, duration: TimeInterval = 3.second, position: JGProgressHUDPosition = .bottomCenter,  disableUserInteraction: Bool = false, insets: UIEdgeInsets? = nil, completion: (() -> Void)? = nil) {
    
    static func showMessage(_ text: String,
                            type: HUDType = .none,
                            onView view: UIView? = nil,
                            duration: TimeInterval = 3.second,
                            position: JGProgressHUDPosition = .bottomCenter,
                            disableUserInteraction: Bool = false,
                            offset: CGPoint = CGPoint(x: 0, y: 0),
                            margin: CGFloat = 15,
                            insets: UIEdgeInsets? = nil, completion: (() -> Void)? = nil) {
        //        if JGProgressHUD.allProgressHUDsInView(view).count > 0 && lastest == .Message { return }
        
        
        
        self.dismissHUD()
        
        if let view = view {
            hud = MBProgressHUD.showAdded(to: view, animated: true)
            
        } else {
            guard let view = UIApplication.shared.keyWindow?.rootViewController?.view else { return }
            hud = MBProgressHUD.showAdded(to: view, animated: true)
        }
        
        switch type {
        case .none:
            hud.mode = .text
        default:
            hud.mode = .indeterminate
        }
        
        hud.label.numberOfLines = 0
        hud.label.lineBreakMode = .byWordWrapping
        hud.label.text = text
        hud.label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        
        hud.minShowTime = 0.3.second
        
        hud.isUserInteractionEnabled = disableUserInteraction
        
        //        hud.interactionType = disableUserInteraction ? .BlockAllTouches : .BlockNoTouches
        //        hud.margin = UIEdgeInsets(top: 10, left: 15, bottom: 15, right: 15)
        
        //        var paddingBottom: CGFloat
        //        switch Device.size() {
        //        case .Screen3_5Inch, .Screen4Inch:
        //            paddingBottom = 150
        //        default:
        //            paddingBottom = 180
        //        }
        
        //        hud.marginInsets = insets ?? UIEdgeInsets(top: 5, left: 20, bottom: paddingBottom, right: 20)
        
        //        dismissHUD()
        
        
        hud.margin = margin
        hud.offset = offset
        hud.hide(animated: true, afterDelay: duration)
        lastest = .message
        hud.bezelView.style = .solidColor
        hud.label.textColor = .white
        hud.bezelView.backgroundColor = UIColor.black.alpha(0.75)
        
        
        
        
        
        Timer.after(duration) { _ in
            HUD.lastest = .none
            if let completion = completion { completion() }
        }
    }
    
    
    //    static func showHUD(_ text: String? = nil, onView view: UIView? = nil, type: HUDType = .indeterminate, disableUserInteraction: Bool = true, position: JGProgressHUDPosition = .center, delay: TimeInterval = 0.5.second, action: (() -> Void)? = nil) {
    
    static func showHUD(_ text: String? = nil, onView view: UIView? = nil, type: MBProgressHUDMode = .indeterminate, disableUserInteraction: Bool = true, position: JGProgressHUDPosition = .center, delay: TimeInterval = 0.5.second, action: (() -> Void)? = nil) {
        //        if JGProgressHUD.allProgressHUDsInView(view).count > 0 && lastest == .HUD { return }
        
        
        
        self.dismissHUD()
        
        if let view = view {
            hud = MBProgressHUD.showAdded(to: view, animated: true)
            
        } else {
            guard let view = UIApplication.shared.keyWindow?.rootViewController?.view else { return }
            hud = MBProgressHUD.showAdded(to: view, animated: true)
        }
        
        
        if let text = text , text.count > 0 { hud.label.text = text } else { hud.label.text = nil }
        
        hud.label.font = UIFont(name: FontType.latoRegular.., size: FontSize.normal..)
        hud.minShowTime = 0.3.second
        
        
        switch type {
        case .text :
            hud.mode = .text
            
            hud.isUserInteractionEnabled = disableUserInteraction
            hud.bezelView.style = .solidColor
            hud.label.textColor = .white
            hud.bezelView.backgroundColor = UIColor.black.alpha(0.75)
            hud.contentColor = .white
            
            //            #if VINSCHOOL
            
        case .indeterminate where text == nil :
            
            
            hud.mode = .customView
            
            hud.isUserInteractionEnabled = disableUserInteraction
            hud.customView = ItemHUDView()
            
            hud.isSquare = true
            hud.bezelView.style = .solidColor
            hud.backgroundColor = .clear
            hud.backgroundView.backgroundColor = .clear
            hud.bezelView.backgroundColor = .clear
            hud.label.text = "XXXXXXXX"
            hud.label.textColor = .clear
            
            //        case .indeterminate where text != nil:
            //
            //            var message: String = ""
            //
            //            if let string = text {
            //
            //                if string.count >= 8 {
            //                    message += string
            //                } else {
            //
            //                    for _ in 0..<((8 - string.count)/2) {
            //                        message += "  "
            //                    }
            //
            //                    message += string
            //
            //                    for _ in 0..<((8 - string.count)/2) {
            //                        message += "  "
            //                    }
            //                }
            //            } else {
            //                message = "               "
            //            }
            //
            //
            //            hud.isUserInteractionEnabled = disableUserInteraction
            //            hud.label.text = message
            //            hud.label.textColor = UIColor.Text.blackMediumColor()
            //            hud.mode = .customView
            //
            //            let itemView = ItemHUDView()
            //            itemView.layer.shadowOpacity = 0.0
            //            itemView.layer.shadowColor = UIColor.clear.cgColor
            //            hud.customView = itemView
            //
            ////            hud.customView = UIImageView(image: Icon.General.Calendar.tint(.blue))
            //            hud.label.text = "Nắng mai sẽ hôn lên môi em"
            //
            //            hud.isSquare = true
            //            hud.bezelView.style = .solidColor
            //            hud.backgroundColor = .clear
            //            hud.backgroundView.backgroundColor = .clear
            //            hud.bezelView.backgroundColor = .white
            //
            //            hud.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
            //            hud.layer.shadowOpacity = 0.5
            //            hud.layer.shadowRadius = 3.5
            //            hud.layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
            
            
            
            
            
        default:
            hud.mode = .indeterminate
            
            hud.isUserInteractionEnabled = disableUserInteraction
            hud.bezelView.style = .solidColor
            hud.label.textColor = .white
            hud.bezelView.backgroundColor = UIColor.black.alpha(0.75)
            hud.contentColor = .white
            
            
        }
        
        
        Timer.after(delay) { _ in
            HUD.lastest = .none
            if let action = action { action() }
        }
        
    }
    
    
    static func dismissHUD(animated: Bool = true, view: UIView? = nil, completion:(() -> Void)? = nil) {
        
        //        if let view = view {
        //            MBProgressHUD.hide(for: view, animated: true)
        //
        //        } else {
        //            guard let view = UIApplication.shared.keyWindow?.rootViewController?.view else { return }
        //            MBProgressHUD.hide(for: view, animated: true)
        //        }
        
        hud.hide(animated: animated)
        if let completion = completion { completion() }
    }
}


class ItemHUDView: UIView {
    
    enum Size: CGFloat {
        case process = 58, padding15 = 15, image = 46, content = 68
    }
    
    var imageView: UIImageView!
    var processView: NVActivityIndicatorView!
    var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        contentView.frame = CGRect(x: (bounds.width - Size.content..) / 2,
                                   y: (bounds.height - Size.content..) / 2,
                                   width: Size.content..,
                                   height: Size.content..)
        
        contentView.layer.cornerRadius = contentView.frame.height / 2
        
        
        
        imageView.frame = CGRect(x: (contentView.frame.width - Size.image..) / 2,
                                 y: (contentView.frame.width - Size.image..) / 2,
                                 width: Size.image..,
                                 height: Size.image..)
        
        
        processView.frame = CGRect(x: (contentView.frame.width - Size.process..) / 2,
                                   y: (contentView.frame.width - Size.process..) / 2,
                                   width: Size.process..,
                                   height: Size.process..)
        
    }
}

extension ItemHUDView {
    
    func setup() {
        
        
        contentView = setupView(.white)
        imageView = setupImageView()
        processView = setupProcessView()
        
        addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(processView)
        
        
        
        layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.5
        layer.shadowOffset = CGSize(width: -1.0, height: 2.0)
        
    }
    
    
    func setupProcessView() -> NVActivityIndicatorView {
        let view = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: Size.process.., height: Size.process..),
                                           type: .circleStrokeSpin,
                                           color: UIColor.Navigation.mainColor(),
                                           padding: 0)
        view.backgroundColor = .clear
        view.isHidden = true
        view.startAnimating()
        return view
    }
    
    
    func setupView(_ bgColor: UIColor = UIColor.Misc.seperatorColor()) -> UIView {
        let view = UIView()
        view.backgroundColor = bgColor
        view.clipsToBounds = true
        
        
        
        
        return view
    }
    
    func setupImageView() -> UIImageView {
        let imageView = UIImageView(image: Icon.General.LogoKidsOnline)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    
}



