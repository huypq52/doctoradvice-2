//
//  Image.swift
//  G5TaxiUser
//
//  Created by NangND on 9/7/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import Foundation
import UIKit
//import PHExtensions

struct Icon {
    
    /// Icon các Items cho Note
    struct General {
        
        static var CoverDefault: UIImage { return UIImage(named: "CoverDefault") ?? UIImage() }
        
        static var ArrowLeft: UIImage { return UIImage(named: "ArrowLeft") ?? UIImage() }
        
        static var ArrowRight: UIImage { return UIImage(named: "ArrowRight") ?? UIImage() }
        
        static var ArrowDown: UIImage { return UIImage(named: "ArrowDown") ?? UIImage() }
        
        static var LogoKidsOnline: UIImage { return UIImage(named: "LogoKidsOnline") ?? UIImage() }
        
        static var Add: UIImage { return UIImage(named: "Add") ?? UIImage() }
        
        static var Minus: UIImage { return UIImage(named: "Minus") ?? UIImage() }
        
        static var Time: UIImage { return UIImage(named: "Time") ?? UIImage() }
        
        static var Delete: UIImage { return UIImage(named: "Delete") ?? UIImage() }
        
        static var Upload: UIImage { return UIImage(named: "Upload") ?? UIImage() }
        
        static var Calendar: UIImage { return UIImage(named: "Calendar") ?? UIImage() }
        
        static var DoubleArrowRight: UIImage { return UIImage(named: "NextYear") ?? UIImage() }
        
        static var Rating: UIImage { return UIImage(named: "Rating") ?? UIImage() }
        
        static var AddMedicine: UIImage { return UIImage(named: "AddMedicine") ?? UIImage() }
        
        static var Views: UIImage { return UIImage(named: "Views") ?? UIImage() }
        
        static var IconCommentPhoto: UIImage { return UIImage(named: "IconCommentPhoto") ?? UIImage() }
        
        static var RadioOff: UIImage { return UIImage(named: "RadioOff") ?? UIImage() }
        
        static var RadioOn: UIImage { return UIImage(named: "RadioOn") ?? UIImage() }
        
        static var Send: UIImage { return UIImage(named: "Send_ICON") ?? UIImage() }
        
        static var AddImage: UIImage { return UIImage(named: "AddImageIC") ?? UIImage() }
        
        static var CallVideo: UIImage { return UIImage(named: "CallVideo") ?? UIImage() }
        
        
        static var switchCamera: UIImage { return UIImage(named: "ic_switch_camera") ?? UIImage() }
        
        static var videoOff: UIImage { return UIImage(named: "ic_video_off") ?? UIImage() }
        
        static var videoOn: UIImage { return UIImage(named: "ic_video_on") ?? UIImage() }
        
        
        static var speakerOn: UIImage { return UIImage(named: "ic_speaker_on") ?? UIImage() }
        
        static var speakerOff: UIImage { return UIImage(named: "ic_speaker_off") ?? UIImage() }
        
        static var micOn: UIImage { return UIImage(named: "ic_mic_on") ?? UIImage() }
        
        static var micOff: UIImage { return UIImage(named: "ic_mic_off") ?? UIImage() }
        
        
    }
    
    // Icon cho Navigation Bar
    struct DatePicker {
        static var BackMonth: UIImage { return UIImage(named: "BackMonth") ?? UIImage() }
        
        static var NextMonth: UIImage { return UIImage(named: "NextMonth") ?? UIImage() }
        
        static var BackYear: UIImage { return UIImage(named: "BackYear") ?? UIImage() }
        
        static var NextYear: UIImage { return UIImage(named: "NextYear") ?? UIImage() }
    }
    
    // Icon cho Navigation Bar
    struct Navigation {
        static var Done: UIImage { return UIImage(named: "NavDone") ?? UIImage() }
        
        static var Back: UIImage { return UIImage(named: "NavBack") ?? UIImage() }
        
        static var Menu: UIImage { return UIImage(named: "NavMenu") ?? UIImage() }
        
        static var Edit: UIImage { return UIImage(named: "NavEdit") ?? UIImage() }
        
        static var Delete: UIImage { return UIImage(named: "NavDelete") ?? UIImage() }
        
        static var Help: UIImage { return UIImage(named: "NavHelp") ?? UIImage() }
        
        static var Trash: UIImage { return UIImage(named: "NavTrash") ?? UIImage() }
        
        static var AddPerson: UIImage { return UIImage(named: "NavAddPerson") ?? UIImage() }
        
        static var Info: UIImage { return UIImage(named: "NavInfo") ?? UIImage() }
        
        static var Setting: UIImage { return UIImage(named: "NavSetting") ?? UIImage() }
        
        static var Survey: UIImage { return UIImage(named: "NavSurvey") ?? UIImage() }
        
        static var Promotion: UIImage { return UIImage(named: "NavPromotion") ?? UIImage() }
        
        
    }
    
    
    // Icon cho Login
    struct Login {
        static var SelectedCheckbox: UIImage { return UIImage(named: "CheckBox") ?? UIImage() }

        static var DeselectedCheckbox: UIImage { return UIImage(named: "UncheckBox") ?? UIImage() }
        
//        static var SelectedCheckbox: UIImage { return UIImage(named: "CheckBoxLogin") ?? UIImage() }
//
//        static var DeselectedCheckbox: UIImage { return UIImage(named: "UncheckBoxLogin") ?? UIImage() }
        
        static var Personal: UIImage { return UIImage(named: "Person") ?? UIImage() }
        
        static var Phone: UIImage { return UIImage(named: "Phone") ?? UIImage() }
        
        static var SchoolCode: UIImage { return UIImage(named: "SchoolCode") ?? UIImage() }
        
        static var Key: UIImage { return UIImage(named: "Key") ?? UIImage() }
        
        static var Search: UIImage { return UIImage(named: "Search") ?? UIImage() }
        
        static var FlagVN: UIImage { return UIImage(named: "FlagVN") ?? UIImage()}
        
        static var FlagEN: UIImage { return UIImage(named: "FlagEN") ?? UIImage()}

    }
    
    
    
    /// Icon các Items cho Home
    struct Home {
        
        static var HeaderBg: UIImage { return UIImage(named: "HeaderBg") ?? UIImage() }
        
        static var Next: UIImage { return UIImage(named: "Next") ?? UIImage() }
        
        static var Kcoin: UIImage { return UIImage(named: "Kcoin") ?? UIImage() }
        
        /// Icon cho các chức năng chính
        
        static var AbsentRequest: UIImage { return UIImage(named: "xinnghi_icon") ?? UIImage() }
        
        static var Expire: UIImage { return UIImage(named: "Expire") ?? UIImage() }
        
        
        
    }
    
    /// Icon các Items cho Home
    struct Contact {
        static var Message: UIImage { return UIImage(named: "ContactMessage") ?? UIImage() }
        
        
        /// Icon trong details
        
        static var DetailPhone: UIImage { return UIImage(named: "DetailPhone") ?? UIImage() }
        
        static var DetailAddress: UIImage { return UIImage(named: "DetailAddress") ?? UIImage() }
        
        static var DetailEmail: UIImage { return UIImage(named: "DetailEmail") ?? UIImage() }
        
        static var DetailJob: UIImage { return UIImage(named: "DetailJob") ?? UIImage() }
    }
    
    
    
    /// Icon các Items cho TabBar Chính
    struct TabBar {
        
        static var Home: UIImage { return UIImage(named: "TabBarHome") ?? UIImage() }
        
        static var Message: UIImage { return UIImage(named: "TabBarMessage") ?? UIImage() }
        
        static var Contact: UIImage { return UIImage(named: "TabBarContact") ?? UIImage() }
        
        static var Notification: UIImage { return UIImage(named: "TabBarNotification") ?? UIImage() }
        
        static var Account: UIImage { return UIImage(named: "TabBarAccount") ?? UIImage() }
        
        static var Medical: UIImage { return UIImage(named: "TabBarMedical") ?? UIImage() }
        
        
    
    }
    
    
    /// Icon các Items cho TabBar Chính
    struct Account {
        
        static var AvatarDefault: UIImage { return UIImage(named: "AvatarDefault") ?? UIImage() }
        
        static var ChangedPassword: UIImage { return UIImage(named: "ChangedPassword") ?? UIImage() }
        
        static var Feedback: UIImage { return UIImage(named: "Feedback") ?? UIImage() }
        
        static var Logout: UIImage { return UIImage(named: "Logout") ?? UIImage() }
        
        static var Note: UIImage { return UIImage(named: "Note") ?? UIImage() }
        
        static var Gender: UIImage { return UIImage(named: "Gender") ?? UIImage() }
        
    }
    
    /// Icon các Items cho Note
    struct Note {
        static var ArrowDown: UIImage { return UIImage(named: "ArrowDown") ?? UIImage() }
    }
    
    
    /// Icon các Items cho Promotion
    struct Promotion {
        static var Copy: UIImage { return UIImage(named: "Promo_Copy") ?? UIImage() }
        
        static var QR: UIImage { return UIImage(named: "Promo_QR") ?? UIImage() }
        
        static var Share: UIImage { return UIImage(named: "Promo_Share") ?? UIImage() }
        
        static var Warning: UIImage { return UIImage(named: "Promo_Warning") ?? UIImage() }
        
    }
    
    struct Subject {
        
        static var Description: UIImage { return UIImage(named: "Description") ?? UIImage() }
        
        static var Lesson: UIImage { return UIImage(named: "Lesson") ?? UIImage() }
        
        static var Assignment: UIImage { return UIImage(named: "Assignment") ?? UIImage() }
        
        static var File: UIImage { return UIImage(named: "File") ?? UIImage() }
        
        static var Comment: UIImage { return UIImage(named: "sb_Comment") ?? UIImage() }
        
        static var Attendance: UIImage { return UIImage(named: "Attendance") ?? UIImage() }
        
        
        static var Bus: UIImage { return UIImage(named: "ic_bus_attendance") ?? UIImage() }
        
        
    }
    
    
    
    /// Icon các Items cho Note
    struct KidsOnlineCorner {
        static var Commented: UIImage { return UIImage(named: "Commented")?.tint(UIColor.Navigation.mainColor()) ?? UIImage() }
        static var Liked: UIImage { return UIImage(named: "Liked")?.tint(UIColor.Navigation.mainColor()) ?? UIImage() }
        
        
        static var Share: UIImage { return UIImage(named: "Share") ?? UIImage() }
        static var Like: UIImage { return UIImage(named: "Like") ?? UIImage() }
        static var Comment: UIImage { return UIImage(named: "Comment") ?? UIImage() }
        
    }
    
    /// Icon các Items cho Note
    struct Post {
        static var AddPicture: UIImage { return UIImage(named: "AddPicture") ?? UIImage() }
    }
    
    struct About {
        static var Email: UIImage { return UIImage(named: "AboutEmail") ?? UIImage() }
        
        static var Skype: UIImage { return UIImage(named: "AboutSkype") ?? UIImage() }
        
        static var Phone: UIImage { return UIImage(named: "AboutPhone") ?? UIImage() }
    }
    
    
    /// Icon Empty Table
    struct EmptyTable {
        static var Notification: UIImage { return UIImage(named: "Notification_Empty") ?? UIImage() }
    }
    
    // Icon Rating
    struct Rating {
        static var Rate: UIImage { return UIImage(named: "Rating")! }
        static var UnRate: UIImage { return UIImage(named: "UnRating")! }
    }
    
    
    // Icon Rating
    struct MedicalBook {
        static var birthDay: UIImage { return UIImage(named: "ic_medical_birth_day")! }
        static var gender: UIImage { return UIImage(named: "ic_medical_gender")! }
    }
    
    
    /// Finance
    
    struct Finance {
        static var receipt: UIImage { return UIImage(named: "ic_receipt")! }
        
        static var debt: UIImage { return UIImage(named: "ic_debt")! }
        
        static var refund: UIImage { return UIImage(named: "ic_refund")! }
    }
    

}
