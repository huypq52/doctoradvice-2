//
//  AppData.swift
//  NguoiBA
//
//  Created by Pham Huy on 6/19/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import UIKit
//import CleanroomLogger
import Firebase
import JSQMessagesViewController
import SwiftyUserDefaults

class AppData: NSObject {
    static let instance = AppData()
    
    var gestureRecognizerShouldReceiveTouch = false
    

    var deviceToken: String = ""

    var myInfo: User?
    
    var helpLink: String = ""
    
    var noteChangePassword: String = ""


    var isRequesting: Bool = false
    
  
 
    
    var postUpdateLastTime: TimeInterval = 0
    
    var timelineUpdateLastTime: TimeInterval = 0
    
    var currentOffsetAlbum: Int = 1
    
    var willShowLoadAddAlbum: Bool = true
    
    var willReloadHome: Bool = false
    
    var willReloadTimeline: Bool = false
    
    var listImageTemp: [Photo] = []
    
 
    // Danh sach danh ba
    var guestInfoList: [GuestInfo] = []
    
    var messageData: [(roomName: String, messages: [JSQMessage])] = []
    
    // Danh sách các kênh chat
    var channels: [Channel] = []

    
    var isCanChat: Bool = true
    
    var showAlertWhenNotification: Bool = false
    
    
    
    /// Đăng nhập tự động (Không qua màn hình login)
    var isAutoLogin: Bool = false
    
    var isReloadContact: Bool = true
    
    
    /// Nhận diện thương hiệu
    var useBrandIdentity: Bool = false

    
    var tabbarItems: [TabbarItem] = []
//    
//    var aboutInfo: AboutInfo? =  nil
    

    /// Reset data
    func resetData() {
        
        guestInfoList = []
        messageData = []
        
        /*
        
        /// Xóa hết Album hiện tại
        
        if let user = AppData.instance.myInfo {
            Defaults[.UserDeleteAlbum].append(user.id)
            DatabaseSupport.instance.deleteAllAlbum()
            DatabaseSupport.instance.deleteAllRealmHighlightComment()
            
            Defaults[.AlbumLastTime] = 0
            
            if let accountTemp = DatabaseSupport.instance.getAccount() {
                accountTemp.students.forEach { $0.updateAlbums(Defaults[.AlbumLastTime]) }
            }
            
        }
        
        
        currentOffsetAlbum      = 1
        postUpdateLastTime      = 0
        timelineUpdateLastTime  = 0
        helpLink                = ""
        noteChangePassword      = ""
        
        isCanChat               = false
        willShowLoadAddAlbum    = true
        willReloadTimeline      = false
        willReloadHome          = false

        aboutInfo           = nil
        myInfo              = nil
        categorySelected    = nil
        termAssessmentInfo  = nil
        
        tabbarItems         = []
        surveyList          = []
        studentList         = []
        modules             = []
        parentTransporter   = []
        guestInfoList       = []
        albums              = []
        listImageTemp       = []
        
        

        
        URLCache.shared.removeAllCachedResponses()
        
 */
    }
}
