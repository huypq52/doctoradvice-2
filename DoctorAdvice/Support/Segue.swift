//
//  Segue.swift
//  G5TaxiUser
//
//  Created by Hoan Pham on 9/24/15.
//  Copyright © 2015 Hoan Pham. All rights reserved.
//

import UIKit
//import CleanroomLogger

/**
 Segue
 */
struct Segue {

    struct HomeController {
        static let ToListChild      = "ShowListChildController"
        
        static let ToStudentDetail  = "ShowStudentDetailController"
        
        static let ToAbsentRequest  = "ShowAbsentRequestController"
        
        static let ToDrugNote       = "ShowDrugNoteController"
        
        static let ToDrugNoteDetail = "ShowDrugNoteDetailController"
        
        static let ToAlbumPhoto     = "ShowAlbumPhotoController"
        
        static let ToPhotoDetail    = "ShowPhotoDetailController"
        
        /// Post
        static let ToPost           = "showPostController"
        
        static let ToPostDetail     = "ShowPostDetailController"
        
        /// Activity
        static let ToActivityLearning = "ShowActivityLearningController"
        
        static let ToActivitySleep = "ShowActivitySleepController"
        
        static let ToActivityDining = "ShowActivityDiningController"
        
        static let ToActivityHygienic = "ShowActivityHygienicController"
        
        /// Attendance
        
        static let ToAttendanceCome = "ShowAttendanceComeController"
        
        static let ToAttendanceComeDetail = "ShowToAttendanceComeDetailController"
        
        static let ToAttendanceLeave = "ShowAttendanceLeaveController"
        
        static let ToGeneralHealth = "ShowGeneralHealthController"
        
    }
    
    struct PersonalController {
        static let ToChangedPasswordController  = "ShowChangedPassword"
        
        static let ToFeedbackController         = "ShowFeedback"
    }
    
    struct ContactController {
        static let ToContactDetailController    = "ShowContactDetail"
    }
}


/**
 Enum các ViewController trong Story board
 */
enum StoryboardController: String {
    
    case Login                  = "LoginViewController"
    
    ///Home
    case Home                   = "HomeViewController"
    
    case ListChild              = "ListChildViewController"
    
    case DrugNote               = "MedicineNoteViewController"
    
    case AttendanceCome         = "AttendanceComeViewController"
    
    case AttendanceComeDetail   = "AttendanceComeDetailViewController"
    
    case ActivityLearning       = "ActivityLearningViewController"
    
    case ActivitySleep          = "ActivitySleepViewController"
    
    case PostList               = "PostViewController"
    
    /// Message
    case Message                = "MessageViewController"
    
    
    /// Contact
    case Contact                = "ContactViewController"
    
    case ContactDetails         = "ContactDetailsViewController"
    
    case Notification           = "NotificationViewController"
    
    case Account                = "AccountViewController"

}

/*
 Enum Story board
 */
enum Storyboard: String {
    case Main          = "Main"
    case Login         = "Login"
    case Home          = "Home"
    case Message       = "Message"
    case Contact       = "Contact"
    case Notification  = "Notification"
    case Account       = "Account"

}
