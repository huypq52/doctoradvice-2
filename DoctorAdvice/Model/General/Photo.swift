//
//  Photo.swift
//  KidsOnline
//
//  Created by Pham Huy on 8/21/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit

class Photo {
    let id: Int
    let description: String
    var path: String
    let time: Double
    
    var imageAvatar: UIImage?
    
    init(id: Int, description: String, path: String, time: Double) {
        self.id = id
        self.description = description
        self.path =  path
        self.time = time
    }
}
