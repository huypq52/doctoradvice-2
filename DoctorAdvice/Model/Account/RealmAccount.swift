//
//  RealmUser.swift
//  GPSMobile
//
//  Created by Hoan Pham on 3/31/16.
//  Copyright © 2016 Hoan Pham. All rights reserved.
//

import Foundation
//import RealmSwift


class RealmAccount: Object {
    @objc dynamic var username: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var userID: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var rememberMe: Bool = false
    @objc dynamic var lastUpdate: Double = 0
    var listIDRead     = List<IntObject>()
    var listIDNotificationDelete   = List<Int>()
    var listIDNotificationRead     = List<Int>()
    
    convenience init(username: String,
                     password: String,
                     userID: String,
                     name: String,
                     rememberMe: Bool,
                     lastUpdate: Double,
                     listIDRead: [Int],
                     listIDNotificationDelete: [Int],
                     listIDNotificationRead: [Int]) {
        self.init()
        self.username = username
        self.password = password
        self.userID = userID
        self.name = name
        self.rememberMe = rememberMe
        self.lastUpdate = lastUpdate
        self.listIDRead.append(objectsIn: listIDRead.map { IntObject($0) } )
        
        self.listIDNotificationDelete.append(objectsIn: listIDNotificationDelete)
        self.listIDNotificationRead.append(objectsIn: listIDNotificationRead)
    }
    
    override static func primaryKey() -> String? {
        return "username"
    }
    
    
    func updateLastTime(_ time: Double) {
        do {
            try Realm().write {
                self.lastUpdate = time
            }
        }
        catch {
            print("Realm - Cannot mark Notification as read: \(error)")
        }
    }
    
    func resetNotificationID() {
        do {
            try Realm().write {
                self.listIDNotificationDelete.removeAll()
                self.listIDNotificationRead.removeAll()
            }
        }
        catch {
            print("Realm - Cannot mark Notification as read: \(error)")
        }
    }
    
    func appendListIDRead(_ notificationIDs: [Int]) {
        do {
            try Realm().write {
                
                notificationIDs.forEach { if !self.listIDNotificationRead.contains($0) { self.listIDNotificationRead.append($0) } }
            }
        }
        catch {
            print("Realm - Cannot mark appendListIDRead: \(error)")
        }
    }
    
    func appendListIDDelete(_ notificationIDs: [Int]) {
        do {
            try Realm().write {
                
                notificationIDs.forEach { if !self.listIDNotificationDelete.contains($0) { self.listIDNotificationDelete.append($0) } }
            }
        }
        catch {
            print("Realm - Cannot mark appendListIDRead: \(error)")
        }
    }
    
}

class IntObject: Object {
    @objc dynamic var value = 0
    
    convenience init(_ value: Int) {
        self.init()
        self.value = value
    }
}

