//
//  GuestInfo.swift
//  KO4Teacher
//
//  Created by Pham Huy on 3/5/17.
//  Copyright © 2017 KidOnline. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

//1:quản lý trường; 2:gvcn; 3:gvbm; 4: phụ huynh; 5: học sinh; 6: khác; 7: Trưởng khối

enum GuestType: Int {
    case manager = 1, teacher, subjectTeacher, parent, student, other, dean
}

public class GuestInfo: NSObject {
    
    var id: String = ""
    var name: String = ""
    var avatar: String = ""
    var type: AccountType = .patient
    var phone: String = ""
    var email: String = ""
    var address: String = ""
    var gender: GenderType = .male

    var lastUpdate: TimeInterval = 0
    var isNewMessage: Bool = false
    
    var isLoading: Bool = false
    
    var isOnline: Bool {
        return lastUpdate > Date().timeIntervalSince1970 - 20.seconds
    }

    var imageAvatar: UIImage? = nil

    lazy var searchName: String = {
        return Utility.normalizeString(self.name + " "  + self.email + " " + self.phone)
    }()
    
    init(data: [String : Any]) {
        
        self.id = data["id"] as? String ?? ""
        self.avatar = (data["avatar"] as? String) ?? ""
        self.name   = (data["full_name"] as? String) ?? ""
        self.email   = (data["email"] as? String) ?? ""
        self.phone   = (data["phone"] as? String) ?? ""
        self.type    =  AccountType(rawValue: data["account_type"] as? Int ?? -1) ?? .patient
        self.gender = GenderType(rawValue: data["gender"] as? Int ?? -1) ?? .male
        self.lastUpdate = data["last_update"] as? Double ?? 0
         
        
    }
    
    init(id: String) {
        self.id = id
    }

}
