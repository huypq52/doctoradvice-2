//
//  UserInfo.swift
//  KidsOnline
//
//  Created by Pham Huy on 10/1/16.
//  Copyright © 2020 Pham Huy. All rights reserved.
//

import Foundation
import UIKit


enum AccountType: Int {
    case doctor = 0, patient
    
    var title: String {
        switch self {
        case .doctor:
            return "Bác sĩ"
            
        case .patient:
            return "Bệnh nhân"
        }
    }
}

enum GenderType: Int {
    case male = 1, female = 0
    
    var title: String {
        switch self {
        case .male:
            return "Nam"
            
        case .female:
            return "Nữ"
        }
    }
}

enum StateUser: Int {
    case normal = 0, ring, videoCall
}

struct User {
    
    var uid: String
    var avatar: String
    var name: String
    var email: String
    var phone: String
    var address: String
    var dob: TimeInterval
    var gender: GenderType?
    var accountType: AccountType

    var cover: String
    
    // Danh sách gửi lời mời
    var guestSendRequest: [String]
    
    // Danh sách chờ xác nhận
    var guestConfirmRequest: [String]
    
    // Danh sách bạn bè
    var guestFriend: [String]
    
    var state: StateUser = .normal
    
    var imageAvatar: UIImage? = nil
    
    init(data: [String : Any]) {
        
        self.uid        = data["id"] as? String ?? ""
        self.name       = data["full_name"] as? String ?? ""
        self.avatar     = data["avatar"] as? String ?? ""
        self.phone      = data["phone"] as? String ?? ""
        self.address    = data["address"] as? String ?? ""
        self.email      = data["email"] as? String ?? ""
        self.dob        = data["dob"] as? Double ?? 0
        self.gender     = GenderType(rawValue: data["gender"] as? Int ?? -1)
        self.cover      = data["cover"] as? String ?? ""
        
        if let callInfo = data["call_info"] as? [String : Any] {
            self.state      = StateUser(rawValue: callInfo["state"] as? Int ?? 0) ?? .normal
        }
        
        
        
        self.guestSendRequest = data["guest_send_friend"] as? [String] ?? []
        self.guestConfirmRequest = data["guest_confirm_friend"] as? [String] ?? []
        
        self.guestFriend = data["guest_friend"] as? [String] ?? []
        
        self.accountType     = AccountType(rawValue: (data["account_type"] as? Int) ?? 1) ?? .patient
    
    }
}
